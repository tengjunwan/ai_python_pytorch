# -*- coding: utf-8 -*-
"""
Created on Wed Apr 28 19:16:55 2021

@author: EDZ
"""

from app import app
import json


with open('config/config.json') as f:
    config = json.load(f)
print(config)
app.config.update(config)

from app.controller import controller
from app.controller import rule_controller

app.run(app.run(host='0.0.0.0', port=app.config['PORT']))