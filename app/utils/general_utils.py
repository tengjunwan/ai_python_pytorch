# -*- coding: utf-8 -*-
"""
Created on Wed Apr 28 20:28:16 2021

@author: EDZ
"""

import logging
import urllib
import string
import wget
from logging.handlers import TimedRotatingFileHandler
# from urllib import parse, request
# from functools import reduce
import socket


def get_logger(name, log_level=logging.INFO, file_name="log.txt"):
    file_name = 'log/'+file_name
    # 日志配置
    logger = logging.getLogger(name)
    logger.setLevel(level=log_level)
    # handler = logging.FileHandler(file_name)
    handler = TimedRotatingFileHandler(file_name, when='D', interval=1)
    handler.setLevel(log_level)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    handler.setFormatter(formatter)

    console = logging.StreamHandler()
    console.setLevel(log_level)

    logger.addHandler(handler)
    logger.addHandler(console)
    # when=’D’,interval=1

    # 打印测试
    logger.info("==========INFO===========")
    logger.debug("==========DEBUG===========")
    logger.warning("==========WARNING===========")
    return logger


def _download(url, save_path):
    # 文翔的老方法
    try:
        _url = urllib.request.quote(url, safe=string.printable)
        urllib.request.urlretrieve(_url, save_path)
    except BaseException as e:
        try:
            wget.download(url, save_path)
        except:
            print("download_error:%s"%(str(e)))
            print('下载文件失败，地址为:%s'%(url))
            raise e
                
                
def download(url, save_path, timeout=15):
    socket.setdefaulttimeout(timeout)
    url_encoded = urllib.parse.quote(url, safe=string.printable).replace(" ", "%20")
    try:
        urllib.request.urlretrieve(url_encoded, save_path)
    except BaseException as e:
        print("download_error:%s"%(str(e)))
        print('下载文件失败，地址为:%s'%(url))
        raise e
            
            
# def download_zhoucheng(url, save_path):
#     try:
#         parsedURL=parse.urlparse(url)
#         parsedURLQueryDict = parse.parse_qsl(parsedURL.query)
#         def encodeQueryV(total,queryKV):
#             return total+queryKV[0]+"="+parse.quote(queryKV[1])
    
#         parsedURLQuery = reduce(encodeQueryV,parsedURLQueryDict,"")
#         encodedURL = parsedURL.scheme+"://"+parsedURL.netloc+parsedURL.path+parsedURL.params+'?'+parsedURLQuery+parsedURL.fragment
#         request.urlretrieve(encodedURL, save_path)
#     except BaseException as e:
#         print("download_error:%s"%(str(e)))
#         print('下载文件失败，地址为:%s'%(url))
#         raise e
        