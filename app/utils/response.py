# -*- coding: utf-8 -*-
"""
Created on Wed Apr 28 20:14:38 2021

@author: EDZ
"""


def get_return(data, taskId, version=None, code=1, msg=''):
    rt = {'status':code, 'msg':msg, 'taskId': taskId, 'data':data}
    if version!=None:
        rt['version'] = version
    return rt