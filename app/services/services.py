# -*- coding: utf-8 -*-
"""
Created on Thu Apr 29 10:47:00 2021

@author: EDZ
"""


import multiprocessing
from app.utils.general_utils import get_logger
import time
from model.yolov5_jinglong.layout_analysis_v2 import Layout_Processor
from model.yolov5_jinglong.utils.deployment import page2image, check_imgs, get_char_bboxes, get_line_bboxes
from model.no_line_table_based_on_deeplabv3plus.unlined_table_analysis import Unlined_Table_Processor
from model.no_line_table_based_on_deeplabv3plus.utils.pdf_utils import extract_img_and_text_in_rect
import hashlib
from pathlib import Path
from app.utils.general_utils import download
import fitz
import requests
from app.utils.response import get_return
import numpy as np
from model_trt.yolov5.layout_analysis import Layout_Analyzer_For_A_Share


process_map = {}  # 线程集合


def create_new_process(process_id, fn, params):
    """
    执行任务，需求资源检查不通过的进入缓存队列
    :param task_id:
    :param fn:
    :param params:
    :param is_check:
    :return:
    """
    mp = multiprocessing.Process(target=fn, args=params)
    mp.start()
    process_map[process_id] = mp
    
    
# def distribute_service(input_queue, output_queue, config):
#     """
#     分发服务
#     """
#     logger = get_logger(name='distribute_service', file_name='distribute_service.txt')
#     while True:
#         try:
#             task_id, data, callback_url, process_flow = input_queue.get() 
#         # next_q_name = process_flow.pop(0)
#         # output_queue = queues[next_q_name]
#             output_queue.put([task_id, data, callback_url, process_flow])
#         except BaseException as e:
#             logger.error('%s : taskId: %s, error_msg: %s, time: %s'%("detach_services",
#                                                                       task_id, str(e),
#                                                                       time.strftime("%Y-%m-%d %H:%M:%S",
#                                                                                     time.localtime())))
            
            
def yolov5_service(input_queue, output_queue, config):
    """
    yolov5处理（a股）
    """
    config = config['Model_Config']['yolov5_config']
    logger = get_logger(name='yolov5_service', file_name='yolov5_service.txt')
    # input_queue = queues['recieve_q']
    
    # 模型初始化
    names = ['single para', 'ordinary para', 'lined table', 'unlined table',  
             'header', 'footer', 'picture', 'chart', 'footnote', 'formula',
             'catalog']  # 结构块名称
    lp = Layout_Processor(weight=config['weight'],
                          device=str(config['device']),
                          img_size=config['img_size'], 
                          batch_size=config['batch_size'],
                          verbose=True,
                          names=names,
                          deploy_thres=None,
                          load_from_state_dict=True,
                          cfg=config['cfg'],
                          target='a_share'
                          )
    
    # 推理config
    conf_thres = config['conf_thres']  # nms的conf阈值
    iou_thres = config['iou_thres']  # nms的iou阈值
    
    while True:
        task_id, data, callback_url = input_queue.get()
        logger.info("yolov5任务开始, taskId:%s" % task_id)
        try:
            # 下载pdf
            pdf_url = data
            pdf_save_name = hashlib.md5(pdf_url.encode('utf8')).hexdigest() + '.pdf'
            pdfs_exsited = set([i.name for i in Path('data/pdf').glob('*.pdf')])
            pdf_save_path = 'data/pdf/'+pdf_save_name
            if pdf_save_name not in pdfs_exsited:
                logger.info("下载中, url:%s, 保存为:%s" % (pdf_url, pdf_save_name))
                try:
                    download(pdf_url, pdf_save_path)
                except BaseException as e:
                    logger.info('下载失败：%s' % pdf_url)
                    raise e
                    
            else:
                logger.info("pdf文件已经下载：%s" % pdf_save_name)
            
            # pdf to images
            doc = fitz.open(pdf_save_path)
            imgs = [page2image(page) for page in doc]
            imgs = check_imgs(imgs)  # 确认图片符合标准
            lines = [get_line_bboxes(page) for page in doc]  # 提取line bbox作为补全单句段落的依据
            
            # yolov5 process
            _, p_list = lp(list_of_images=imgs, 
                           list_of_regions=None,  # None则用cv中的投影的方式作后处理
                           list_of_lines=lines,
                           conf_thre=conf_thres,
                           iou_thres=iou_thres)
            
            # 转换为协议数据格式
            data_out = {}
            data_out["status"] = 1  # 状态 1-成功，2-可重试失败，3-跳过失败
            data_out["pageIndex"] = 0  # 页码  可选
            data_out["size"] = len(doc)  # 数量   可选
            data_out["pdfUrl"] = ""  # 可选  
            data_out["pdfPath"] = ""  # 可选
            data_out["pages"] = []
            for i, predict in enumerate(p_list):  # int, numpy array=(#preds, xyxy+conf+cls)
                if len(predict) == 0:  # 该页面什么也没有
                    data_page = {}
                    data_page["pageIndex"] = i 
                    data_page["rclasses"] = []  # 空list
                    data_page["rescuers"] = []  # 空list
                    data_page["rbboxes"] = []  # 空list
                    data_out["pages"].append(data_page)
                else:
                    data_page = {}
                    data_page["pageIndex"] = i 
                    data_page["rclasses"] = (predict[:, 5] + 1).astype(np.int64).tolist()  # 分类类别将从0改为从1开始
                    data_page["rescuers"] = predict[:, 4].tolist()
                    data_page["rbboxes"] = predict[:, [1, 0, 3, 2]].tolist()  # yxyx
                    data_out["pages"].append(data_page)
                    
            
            
            # p_list = [i.tolist() for i in p_list]
            # print(type(p_list))
            # print(type(p_list[0]))
            # print(p_list[0])
            # print(data_out[0])
        
        # try:
        #     next_q_name = process_flow.pop(0)
        #     output_queue = queues[next_q_name]
        # except IndexError:
        #     output_queue = queues['call_back_q']
        except BaseException as e:
            logger.error('%s : taskId: %s, error_msg: %s, time: %s'%("yolov5_service",
                                                                      task_id, str(e),
                                                                      time.strftime("%Y-%m-%d %H:%M:%S",
                                                                                    time.localtime())))
            # 失败了也要返回，告诉任务失败
            data_out = {}
            data_out["status"] = 2  # 状态 1-成功，2-可重试失败，3-跳过失败
            data_out["pageIndex"] = 0  # 页码  可选
            data_out["size"] = len(doc)  # 数量   可选
            data_out["pdfUrl"] = ""  # 可选  
            data_out["pdfPath"] = ""  # 可选
            data_out["pages"] = []
            
        output_queue.put([task_id, data_out, callback_url])
        

def deeplabv3plus_service(input_queue, output_queue, config):
    """
    无线表格(基于deeplabv3+)
    """
    config = config['Model_Config']['deeplabv3plus_config']
    logger = get_logger(name='deeplabv3plus_service', file_name='deeplabv3plus_service.txt')
    # input_queue = queues['recieve_q']
    
    # 模型初始化
    utp = Unlined_Table_Processor(weight=config['weight'],
                                  device=str(config['device']), 
                                  ngram_load_path=config['ngram_load_path'],
                                  verbose=True,
                                  )
                          
    while True:
        task_id, data, callback_url = input_queue.get()
        logger.info("deeplabv3plus任务开始, taskId:%s" % task_id)
        try:
            # 下载pdf
            pdf_url = data['pdf_url']
            pdf_save_name = hashlib.md5(pdf_url.encode('utf8')).hexdigest() + '.pdf'
            pdfs_exsited = set([i.name for i in Path('data/pdf').glob('*.pdf')])
            pdf_save_path = 'data/pdf/'+pdf_save_name
            if pdf_save_name not in pdfs_exsited:
                logger.info("下载中, url:%s, 保存为:%s" % (pdf_url, pdf_save_name))
                download(pdf_url, pdf_save_path)
            else:
                logger.info("pdf文件已经下载：%s" % pdf_save_name)
            
            # extract image, text, text bboxes from the given pdf
            doc = fitz.open(pdf_save_path)  # load pdf file
            data_out = []  # api返回的数据
            for unlined_table in data['unlined table']:
                imgs = []  # 一个页面的所有无线表格图片
                text_list = []  
                text_bboxes_list = []  
                page_index = unlined_table['page_index']
                region = np.array(unlined_table['region']).reshape(-1, 4)
                for r in region:
                    img, text_bboxes, text = extract_img_and_text_in_rect(doc[page_index], r)
                    imgs.append(img)
                    text_bboxes_list.append(text_bboxes)
                    text_list.append(text)
                _, post_preds = utp(imgs, text_list, text_bboxes_list)
                post_preds = [[i.tolist(), j.tolist()] for i, j in post_preds]  # ndarray to list
                data_out.append({'page_index': page_index,
                                 'preds': post_preds 
                                 }
                    )
            # print('data_out', data_out)
            output_queue.put([task_id, data_out, callback_url])
        except BaseException as e:
            logger.error('%s : taskId: %s, error_msg: %s, time: %s'%("deeplabv3plus_service",
                                                                      task_id, str(e),
                                                                      time.strftime("%Y-%m-%d %H:%M:%S",
                                                                                    time.localtime())))


def yolov5_hk_service(input_queue, output_queue, config):
    """
    yolov5处理（港股）
    """
    config = config['Model_Config']['yolov5_hk_config']
    logger = get_logger(name='yolov5_hk_service', file_name='yolov5_hk_service.txt')
    # input_queue = queues['recieve_q']
    
    # 模型初始化
    names = ['single para', 'ordinary para', 'lined table', 'unlined table',
             'header', 'footer', 'picture', 'chart', 'footnote', 'catalog', 
             'side header', 'data block', 'signature']  # 港股结构块名称
    
    lp = Layout_Processor(weight=config['weight'],
                          device=str(config['device']),
                          img_size=config['img_size'], 
                          batch_size=config['batch_size'],
                          verbose=True,
                          names=names,
                          deploy_thres=None,
                          load_from_state_dict=True,
                          cfg=config['cfg'],
                          target='hk_share'
                          )
    
    # 推理config
    conf_thres = config['conf_thres']  # nms的conf阈值
    iou_thres = config['iou_thres']  # nms的iou阈值
    
    while True:
        task_id, data, callback_url = input_queue.get()
        logger.info("yolov5_hk任务开始, taskId:%s" % task_id)
        try:
            # 下载pdf
            pdf_url = data
            pdf_save_name = hashlib.md5(pdf_url.encode('utf8')).hexdigest() + '.pdf'
            pdfs_exsited = set([i.name for i in Path('data/pdf').glob('*.pdf')])
            pdf_save_path = 'data/pdf/'+pdf_save_name
            if pdf_save_name not in pdfs_exsited:
                logger.info("下载中, url:%s, 保存为:%s" % (pdf_url, pdf_save_name))
                download(pdf_url, pdf_save_path)
            else:
                logger.info("pdf文件已经下载：%s" % pdf_save_name)
            
            # pdf to images
            doc = fitz.open(pdf_save_path)
            imgs = [page2image(page) for page in doc]
            imgs = check_imgs(imgs)  # 确认图片符合标准
            regions = [get_char_bboxes(page)[0] for page in doc]  # 提取文字bbox作为后处理依据
            lines = [get_line_bboxes(page) for page in doc]  # 提取line bbox作为补全单句段落的依据
            
            # yolov5 process
            _, p_list = lp(list_of_images=imgs, 
                           list_of_regions=regions,
                           list_of_lines=lines,
                           conf_thre=conf_thres,
                           iou_thres=iou_thres)
            
            # 转换为协议数据格式
            data_out = {}
            data_out["status"] = 1  # 状态 1-成功，2-可重试失败，3-跳过失败
            data_out["pageIndex"] = 0  # 页码  可选
            data_out["size"] = len(doc)  # 数量   可选
            data_out["pdfUrl"] = ""  # 可选  
            data_out["pdfPath"] = ""  # 可选
            data_out["pages"] = []
            for i, predict in enumerate(p_list):  # int, numpy array=(#preds, xyxy+conf+cls)
                if len(predict) == 0:  # 该页面什么也没有
                    data_page = {}
                    data_page["pageIndex"] = i 
                    data_page["rclasses"] = []  # 空list
                    data_page["rescuers"] = []  # 空list
                    data_page["rbboxes"] = []  # 空list
                    data_out["pages"].append(data_page)
                else:
                    data_page = {}
                    data_page["pageIndex"] = i 
                    data_page["rclasses"] = (predict[:, 5] + 1).astype(np.int64).tolist()  # 分类类别将从0改为从1开始
                    data_page["rescuers"] = predict[:, 4].tolist()
                    data_page["rbboxes"] = predict[:, [1, 0, 3, 2]].tolist()  # yxyx
                    data_out["pages"].append(data_page)
                    
            
            
            # p_list = [i.tolist() for i in p_list]
            # print(type(p_list))
            # print(type(p_list[0]))
            # print(p_list[0])
            # print(data_out[0])
        
        # try:
        #     next_q_name = process_flow.pop(0)
        #     output_queue = queues[next_q_name]
        # except IndexError:
        #     output_queue = queues['call_back_q']
            output_queue.put([task_id, data_out, callback_url])
        except BaseException as e:
            logger.error('%s : taskId: %s, error_msg: %s, time: %s'%("yolov5_hk_service",
                                                                      task_id, str(e),
                                                                      time.strftime("%Y-%m-%d %H:%M:%S",
                                                                                    time.localtime())))
            

def yolov5_trt_service(input_queue, output_queue, config):
    """
    yolov5处理（港股）
    """
    config = config['Model_Config']['yolov5_trt_config']
    logger = get_logger(name='yolov5_trt_service', file_name='yolov5_trt_service.txt')
    
    # 模型初始化
    names = ['single para', 'ordinary para', 'lined table', 'unlined table',
             'header', 'footer', 'picture', 'chart', 'footnote', 'formula',
             'catalog']  # a股结构块名称
    
    time.sleep(10)
    lp = Layout_Analyzer_For_A_Share(engine_file_path=config['engine_file_path'], 
                                     plugin_file_path=config['plugin_file_path'],
                                     device=int(config['device']),
                                     verbose=True,
                                     names=names,
                                     deploy_thres=None)
    
    # 推理config
    conf_thres = config['conf_thres']  # nms的conf阈值
    iou_thres = config['iou_thres']  # nms的iou阈值
    
    while True:
        task_id, data, callback_url = input_queue.get()
        logger.info("yolov5_trt任务开始, taskId:%s" % task_id)
        try:
            # 下载pdf
            pdf_url = data
            pdf_save_name = hashlib.md5(pdf_url.encode('utf8')).hexdigest() + '.pdf'
            pdfs_exsited = set([i.name for i in Path('data/pdf').glob('*.pdf')])
            pdf_save_path = 'data/pdf/'+pdf_save_name
            if pdf_save_name not in pdfs_exsited:
                logger.info("下载中, url:%s, 保存为:%s" % (pdf_url, pdf_save_name))
                download(pdf_url, pdf_save_path)
            else:
                logger.info("pdf文件已经下载：%s" % pdf_save_name)
            
            # pdf to images
            doc = fitz.open(pdf_save_path)
            imgs = [page2image(page) for page in doc]
            imgs = check_imgs(imgs)  # 确认图片符合标准
            
            # infer process
            _, p_list = lp.infer(imgs, conf_thres, iou_thres)
            
            # 转换为协议数据格式
            data_out = {}
            data_out["status"] = 1  # 状态 1-成功，2-可重试失败，3-跳过失败
            data_out["pageIndex"] = 0  # 页码  可选
            data_out["size"] = len(doc)  # 数量   可选
            data_out["pdfUrl"] = ""  # 可选  
            data_out["pdfPath"] = ""  # 可选
            data_out["pages"] = []
            for i, predict in enumerate(p_list):  # int, numpy array=(#preds, xyxy+conf+cls)
                if len(predict) == 0:  # 该页面什么也没有
                    data_page = {}
                    data_page["pageIndex"] = i 
                    data_page["rclasses"] = []  # 空list
                    data_page["rescuers"] = []  # 空list
                    data_page["rbboxes"] = []  # 空list
                    data_out["pages"].append(data_page)
                else:
                    data_page = {}
                    data_page["pageIndex"] = i 
                    data_page["rclasses"] = (predict[:, 5] + 1).astype(np.int64).tolist()  # 分类类别将从0改为从1开始
                    data_page["rescuers"] = predict[:, 4].tolist()
                    data_page["rbboxes"] = predict[:, [1, 0, 3, 2]].tolist()  # yxyx
                    data_out["pages"].append(data_page)
                    
            
            
            # p_list = [i.tolist() for i in p_list]
            # print(type(p_list))
            # print(type(p_list[0]))
            # print(p_list[0])
            # print(data_out[0])
        
        # try:
        #     next_q_name = process_flow.pop(0)
        #     output_queue = queues[next_q_name]
        # except IndexError:
        #     output_queue = queues['call_back_q']
            output_queue.put([task_id, data_out, callback_url])
        except BaseException as e:
            logger.error('%s : taskId: %s, error_msg: %s, time: %s'%("yolov5_trt_service",
                                                                      task_id, str(e),
                                                                      time.strftime("%Y-%m-%d %H:%M:%S",
                                                                                    time.localtime())))
            
            
def call_back_service(input_queue, config):
    """
    回调服务
    """
    logger = get_logger(name='call_back_service', file_name='call_back_service.txt')
    # input_queue = queues['recieve_q']
    while True:
        try:
            task_id, data, callback_url = input_queue.get() 
            rt = requests.post(callback_url,
                               json=get_return(data, task_id),
                               headers={'Content-Type': 'application/json'},
                               timeout=(10, 10)
                               )
            logger.info('taskIds: %s\n, callback_status:%s' % (
                         task_id, str(rt.status_code)))
        except BaseException as e:
            logger.error('%s : taskId: %s, error_msg: %s（%s）, time: %s' % ("call_back_service",
                                                                            task_id, str(e),
                                                                            str(e.__traceback__.tb_lineno),
                                                                            time.strftime(
                                                                                "%Y-%m-%d %H:%M:%S",
                                                                                time.localtime())))
                    
    
            

    
    
