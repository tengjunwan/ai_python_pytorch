# -*- coding: utf-8 -*-
"""
Created on Wed Apr 28 19:47:57 2021

@author: EDZ
"""

import multiprocessing
from flask import jsonify
from flask import request
from app import app
import json
import uuid
from app.utils.response import get_return
from app.utils.general_utils import get_logger
from app.services.services import create_new_process
from app.services.services import deeplabv3plus_service
from app.services.services import yolov5_service
from app.services.services import call_back_service
from app.services.services import yolov5_hk_service
from app.services.services import yolov5_trt_service


logger = get_logger(name="controller", file_name = "controller.log")

# 建立数据流队列
yolov5_process_q = multiprocessing.Queue()
deeplabv3plus_process_q = multiprocessing.Queue()
call_back_q = multiprocessing.Queue()
yolov5_hk_process_q = multiprocessing.Queue()
yolov5_trt_process_q = multiprocessing.Queue()

# 建立与数据流队列对应的进程
create_new_process('deeplabv3plus_service', deeplabv3plus_service, (deeplabv3plus_process_q, call_back_q, app.config["CONFIG"]))
create_new_process('yolov5_service', yolov5_service, (yolov5_process_q, call_back_q, app.config["CONFIG"]))
create_new_process('yolov5_hk_service', yolov5_hk_service, (yolov5_hk_process_q, call_back_q, app.config["CONFIG"]))
create_new_process('yolov5_trt_service', yolov5_trt_service, (yolov5_trt_process_q, call_back_q, app.config["CONFIG"]))
create_new_process('call_back_service', call_back_service, (call_back_q, app.config["CONFIG"]))


@app.route('/structure/yolov5_jinglong_predict', methods=['POST'])
def yolov5_service():
    """
    结构块识别（a股）
    :param type: 请求的服务
    :return:
    """
    data = request.get_data()
    json_data = json.loads(data.decode("utf-8"))

    callback_url = json_data['callback_url']
    # task_id = ''.join(str(uuid.uuid1()).split('-'))
    try:
        task_id = json_data['task_id']
    except KeyError:
        task_id = ''.join(str(uuid.uuid1()).split('-'))
        print("generate task id: %s" % str(task_id))
        
    data = json_data['data']['pdf_url']
    yolov5_process_q.put([task_id, data, callback_url])

    return jsonify(get_return('', task_id))


@app.route('/structure/yolov5_jinglong_predict_hk', methods=['POST'])
def yolov5_hk_service():
    """
    结构块识别（港股）
    :param type: 请求的服务
    :return:
    """
    data = request.get_data()
    json_data = json.loads(data.decode("utf-8"))

    callback_url = json_data['callback_url']
    # task_id = ''.join(str(uuid.uuid1()).split('-'))
    try:
        task_id = json_data['task_id']
    except KeyError:
        task_id = ''.join(str(uuid.uuid1()).split('-'))
        print("generate task id: %s" % str(task_id))
        
    data = json_data['data']['pdf_url']
    yolov5_hk_process_q.put([task_id, data, callback_url])

    return jsonify(get_return('', task_id))


@app.route('/unlined_table_recognition/deeplabv3plus_predict', methods=['POST'])
def unlined_table_service():
    """
    无线表格识别
    """
    data = request.get_data()
    json_data = json.loads(data.decode("utf-8"))
    callback_url = json_data['callback_url']
    # task_id = ''.join(str(uuid.uuid1()).split('-'))
    try:
        task_id = json_data['task_id']
    except KeyError:
        task_id = ''.join(str(uuid.uuid1()).split('-'))
        print("generate task id: %s" % str(task_id))
    data = json_data['data']
    deeplabv3plus_process_q.put([task_id, data, callback_url])

    return jsonify(get_return('', task_id))


@app.route('/structure/yolov5_jinglong_predict_trt', methods=['POST'])
def yolov5_trt_service():
    """
    结构块识别(A股),tensorRT
    :param type: 请求的服务
    :return:
    """
    data = request.get_data()
    json_data = json.loads(data.decode("utf-8"))

    callback_url = json_data['callback_url']
    # task_id = ''.join(str(uuid.uuid1()).split('-'))
    try:
        task_id = json_data['task_id']
    except KeyError:
        task_id = ''.join(str(uuid.uuid1()).split('-'))
        print("generate task id: %s" % str(task_id))
        
    data = json_data['data']['pdf_url']
    yolov5_trt_process_q.put([task_id, data, callback_url])

    return jsonify(get_return('', task_id))


