# -*- coding: utf-8 -*-
"""
Created on Tue Jun 15 16:32:19 2021

@author: EDZ
"""

import time
from flask import jsonify
from flask import request
from app import app
import json
import hashlib
from pathlib import Path
from app.utils.general_utils import download
from app.utils.response import get_return
from model.rule.xy_cut_preprocess.pre_process import preprocess_pdf
from model.rule.xy_cut_preprocess.pre_process import preprocess_pdf_single_page
from model.rule.lined_table.grid_extractor import GridExtractor
from app.utils.general_utils import get_logger



logger = get_logger(name='rule', file_name='rule.txt')
ge = GridExtractor()

"""
###################################################
###################################################
》》》》》》》     预处理：结构识别 + xycut  《《《《《《
###################################################
###################################################
"""
@app.route('/label/preprocess_xycut', methods=['POST'])
def preprocess():
    """
    预处理
    :return:
    """
    data = request.get_data()
    json_data = json.loads(data.decode("utf-8"))
    pdf_url = json_data['pdf_url']
    pdf_save_name = hashlib.md5(pdf_url.encode('utf8')).hexdigest() + '.pdf'
    pdfs_exsited = set([i.name for i in Path('data/pdf').glob('*.pdf')])
    if pdf_save_name not in pdfs_exsited:
        download(pdf_url, 'data/pdf/'+pdf_save_name)
    
    pdf_path = 'data/pdf/' + pdf_save_name
    predict = json_data['predict']
    preprocess_bboxes = preprocess_pdf(pdf_path, predict)
    
    
    return jsonify(get_return(preprocess_bboxes, ""))


"""
###################################################
###################################################
》》》》》》》     预处理(单页)：结构识别 + xycut  《《《《《《
###################################################
###################################################
"""
@app.route('/label/preprocess_xycut_single_page', methods=['POST'])
def preprocess_single_page():
    """
    预处理
    :return:
    """
    
    data = request.get_data()
    json_data = json.loads(data.decode("utf-8"))
    pdf_url = json_data['pdf_url']
    predict = json_data['predict']
    pageIndex = json_data['pageIndex']
    logger.info("xy_cut url: %s, pageIndex: %d" % (pdf_url, pageIndex))
    pdf_save_name = hashlib.md5(pdf_url.encode('utf8')).hexdigest() + '.pdf'
    pdfs_exsited = set([i.name for i in Path('data/pdf').glob('*.pdf')])
    if pdf_save_name not in pdfs_exsited:
        logger.info("开始下载url: %s" % pdf_url)
        download(pdf_url, 'data/pdf/'+pdf_save_name)
    else:
        logger.info("已下载url: %s" % pdf_url)
    
    pdf_path = 'data/pdf/' + pdf_save_name
    
    preprocess_bboxes = preprocess_pdf_single_page(pdf_path, pageIndex, predict)
    
    
    return jsonify(get_return(preprocess_bboxes, ""))



"""
###################################################
###################################################
》》》》》》》          有线表格         《《《《《《
###################################################
###################################################
"""
@app.route('/image/table_ocr_v4', methods=['POST'])
def process_lined_table():
    """
    :return:
    """
    data = request.get_data()
    json_data = json.loads(data.decode("utf-8"))
    pdf_url = json_data['pdf_url']
    rects = json_data['rects']
    pageIndex = json_data['page_index']
    logger.info("lined_table url: %s, pageIndex: %d" % (pdf_url, pageIndex))
    pdf_save_name = hashlib.md5(pdf_url.encode('utf8')).hexdigest() + '.pdf'
    pdfs_exsited = set([i.name for i in Path('data/pdf').glob('*.pdf')])
    if pdf_save_name not in pdfs_exsited:
        logger.info("开始下载url: %s" % pdf_url)
        try:
            download(pdf_url, 'data/pdf/'+pdf_save_name)
        except:
            logger.error('download error: %s' % pdf_url)
            
            return jsonify(get_return([], ""))
    else:
        logger.info("已下载url: %s" % pdf_url)
    
    pdf_path = 'data/pdf/' + pdf_save_name
    print('有线表格处理开始')
    try:
        time_s = time.time()
        table_list = ge.process_lined_table(pdf_path, pageIndex, rects)
        time_e = time.time()
        print('有线表格处理结束，耗时：%.3fs' % (time_e - time_s))
    
        return jsonify(get_return(table_list, ""))
    except:
        return jsonify(get_return([], ""))


    
