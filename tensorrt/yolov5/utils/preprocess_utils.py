# -*- coding: utf-8 -*-
"""
Created on Thu Aug 19 17:09:26 2021

@author: EDZ
"""

import numpy as np
import cv2


def erode_elong_save(img, y_small=50, y_large=100, pad=50, times=3, inplace=False, erode=True):
    """
    对传入的图像列表做预处理，包括侵蚀、拉长和padding by 菁龙
    args:
        img(array): bgr H*W*3
        y_small(int): 图片y方向0:50保留
        y_large(int):  图片y方向50到100拉长三倍
        pad(int): 再在top padding的像素（replicate）
        times(int): 上面拉长三倍是这里设置的
        inplace(bool): 是否对输入图片进行修改
        erode(bool): 是否对图片进行erode处理
    return:
        img: bgr H*W*3, 菁龙预处理过后的图片
    """
    if not inplace:
        img = img.copy()  
    h0, w0 = img.shape[:2]
    # erode
    if erode:
        kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (5, 5))
        img = cv2.erode(img,kernel)
    # elongate
    part1 = img[:y_small]
    part2 = img[y_small: y_large]
    part3 = img[y_large:]
    part2_new = cv2.resize(part2, None, fx=1,fy = times)
    img = np.concatenate((part1, part2_new, part3), axis = 0)
    # pad
    img = cv2.copyMakeBorder(img, pad, 0, 0, 0, cv2.BORDER_REPLICATE)
    return img


