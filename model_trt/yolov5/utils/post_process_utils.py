# -*- coding: utf-8 -*-
"""
Created on Thu Aug 19 17:00:47 2021

@author: EDZ
"""

from bisect import bisect
import cv2
import numpy as np
from model_trt.yolov5.utils.general_utils import point2range
from model_trt.yolov5.utils.general_utils import box_IntersectionOverOneBox


def generate_text_regions(img, verbose=False):
    """参考叶龙的方法写的规则得到pdf页面图片的text region坐标
    step1: 对图片进行二值化
    step2: 进行y轴投影，得到连续的区域的起始和中止坐标[[y_min, y_max],...]
    step3: 根据y轴投影对原图进行裁剪(宽度保持原图宽)
    step4: 对裁剪区域进行x轴投影，找到有text的区域的坐标[x_min, x_max]
    Args:
        img(array): 维度H*W*3 (bgr模式)
        verbose(bool): 是否打印结果
    return:
        regions(2d array): 维度(#regions, 4), 4表示yyxx
    """
    # config
    thresh = 200  # 灰度图二值化的阈值
    y_region_thres = 0  #  忽略调y方向投影连续区域过小的阈值
    x_region_thres = 10  #  忽略调x方向投影连续区域过小的阈值
    delta = 0.1  # 拓宽y方向投影连续区域的值，为了ymin=ymax的“线”拥有一点面积
    
    # binary
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    _, img = cv2.threshold(img, thresh, 255, cv2.THRESH_BINARY)
    img = np.logical_not(img).astype(np.uint8)  # 0背景， 1前景
    verbose = False
    if verbose:
        cv2.imshow('binary map', img * 255)
        cv2.waitKey()
    
    # y axis projection
    projection_in_y = np.any(img==1, axis=1).astype(np.uint8)  
    y_cuts = point2range(projection_in_y)  # [[y_s1, y_e1], [y_s2, y_e2], ...]
    y_cuts = y_cuts[(y_cuts[:, 1] - y_cuts[:, 0]) >= y_region_thres]  
    
    # y轴裁剪，对每个y轴裁剪的结果进行x轴投影，投影区域只考虑最小起始点和最大中止点
    x_cuts = np.zeros(y_cuts.shape)
    for i, y_cut in enumerate(y_cuts):
        projection_in_x = np.any(img[y_cut[0]: y_cut[1]+1, :]==1, axis=0)  # 维度(W,)
        x_max = np.max(projection_in_x.nonzero())
        x_min = np.min(projection_in_x.nonzero())
        if x_max - x_min >= x_region_thres:
            x_cuts[i] = [x_min, x_max]
        
    # regions = np.concatenate((y_cuts, x_cuts), axis=1)  # yyxx
    regions = np.concatenate((x_cuts[:, [0]], y_cuts[:, [0]], x_cuts[:, [1]], y_cuts[:, [1]]), axis=1)  #xyxy
    regions[:, 3] += delta  # 使得直线也可以成为region,拥有非0的面积  
    
    return regions


def adjust_preds(list_of_images, p_list, regions=None, inplace=False):
    """后处理
    思路：通过在模型预测框内包含的“投影region”进行预测框的修正
    Args:
        list_of_images(list of array): H*W*3, BGR模式
        p_list(list of array): 模型预测框, (#box, 6), 6 = xyxy, conf, cls_index
        regions(None or array): 作为后处理的参考区域，例如在港股中，是对应pdf的char
            bounding box。如果是None，则用投影的方法的得到regions。
        inplace(bool): 是否对p_list进行inplace操作
    return:
        p_list(list of array): 模型预测框, (#box, 6), 6 = xyxy, conf, cls_index
    """
    if regions is None:
        regions = [generate_text_regions(i) for i in list_of_images]  # 叶龙的投影方法, [(#region, xyxy),...]
        
    if len(regions) ==0 or regions is None:
        return p_list
    
    if not inplace:
        p_list = [x.copy() for x in p_list]  
    
    io_region_thres = 0.45  # intersection over region阈值
    for pred, region in zip(p_list, regions):
        if len(region) == 0:
            continue
        io_region = box_IntersectionOverOneBox(pred[:, :4], region)  # (#pred, #region)
        io_region_bool = io_region > io_region_thres
        try:
            for i, p in enumerate(pred):  # p (6, )
                # 预测框调整为所有io_region>0.5的region的外接矩形
                region_arg = io_region_bool[i]  # [True, False, ...]
                if np.sum(region_arg) > 0:
                    p[: 2] = region[region_arg][:, :2].min(axis=0)  # lt取最小
                    p[2: 4] = region[region_arg][:, 2:].max(axis=0)  # br取最大
        except:
            print(region.shape)
            print(io_region.shape)
            print(region_arg.shape)
            raise ValueError

    return p_list


def modify_pred(pred, y_small=50, y_large=100, pad=50, times=3):
    # 根据菁龙的修改规则对label进行修改,还原到原图上
    # pred: (#box, 6), 6=xyxy+conf+cls_index
    def modify_y(y):
        if y < y_small + pad:  # 在头部，part1
            return max(y - pad, 0)
        elif y_small + pad <= y < pad + y_small + times * (y_large - y_small):  # 在中部，part2
            y_in_part2 = (y - y_small -pad) / times
            return y_in_part2 + y_small
        else:  # 在尾部，part3
            part2_gain = (y_large - y_small) * (times - 1)
            return y - part2_gain - pad
    
    new_pred = np.zeros_like(pred)
    for i, l in enumerate(pred):  # xyxy+conf+cls_index
        new_pred[i][0] = l[0]
        new_pred[i][1] = modify_y(l[1])
        new_pred[i][2] = l[2]
        new_pred[i][3] = modify_y(l[3])
        new_pred[i][4] = l[4]
        new_pred[i][5] = l[5]
        
    return new_pred


def modify_preds(preds):
    return [modify_pred(i) for i in preds]


def correct_single_row_lined_table(pred, img, conf_thres, cls_index, verbose=False):
    """修正单行有线表格的置信度为阈值置信度，为了解决单行有线表格的置信度过低的问题
    判断思路：
    1.单行：从单行的宽高进行约束，并且需要满足一定的h,和w的要求，例如太过小或者太过
    大的区域。
    2.有线表格：从预选框内容中是否有线进行判断
    Args:
        pred(numpy array): (#box, 6), xyxy+conf+cls_index
        img(numpy array): 页面原图，bgr模式
        conf_thres(float): 阈值的置信度，工程中低于此值的有线表格会被略过,最后会修改
            一些单行有线表格的置信度为这个值
        cls_index(int): 有线表格的分类序号
    return:
        new_pred(numpy array):
    """
    def is_lined_table(table_img):
        # 暂时不支持深色表格，仅仅做简单二值化
        gray_img = cv2.cvtColor(table_img, cv2.COLOR_BGR2GRAY)
        binary_thres = 200
        _, binary_img = cv2.threshold(gray_img, binary_thres, 1, cv2.THRESH_BINARY)
        binary_img = np.logical_not(binary_img).astype(np.uint8)
        # open
        h = table_img.shape[0]
        v_kernel = np.ones((h, 1))
        img_eroded = cv2.erode(binary_img, v_kernel, iterations=1)
        img_open = cv2.dilate(img_eroded, v_kernel, iterations=1)
        # project to x axis
        vertical_proj = (img_open == 1).any(axis=1)
        if np.sum(vertical_proj) >= 0.9 * h:
            return True
        else:
            return False
    
    # config
    hmin, hmax = 15, 35  # 有线表格高度范围
    wmin = 500  # 有线表格宽度范围
    new_pred = pred.copy()
    
    for i, (x0, y0, x1, y1, conf, cls_i) in enumerate(pred):
        h = y1 - y0 + 1
        w = x1 - x0 + 1
        cls_i = int(cls_i)
        
        # print('w:', w,'h', h, 'cls', cls_index, 'conf', conf)
        if cls_i != cls_index:  # 非有线表格
            continue
        elif cls_i == cls_index and conf >= conf_thres:  # 有线表格但是置信度
            continue
        elif not (hmin <= h <= hmax) or w < wmin:  # 有线表格但不满足形状
            continue
        elif not is_lined_table(img[int(y0): int(y1)+1, int(x0): int(x1)+1, :]):  # 不满足“有线”判断
            continue
        else:
            new_pred[i, 4] = conf_thres  # 修正置信度
            if verbose:
                print('单行有线表格修正')
    # print('debug1', pred)
    # print('debug2', new_pred)
            
    return new_pred


def adjust_lined_table_preds(preds, imgs, conf_thres, cls_index, verbose=False):
    return [correct_single_row_lined_table(pred, img, conf_thres, cls_index, verbose) 
            for pred, img in zip(preds, imgs)]  


def filter_pred(pred, thres):
    """过滤置信度不满足的预选框
    Args:
        pred(numpy array): (#box, 6), xyxy+conf+cls_index
        thres(list of float): 对应每个类别的置信度阈值
    return:
        pred_filtered(numpy array): (#box, 6), xyxy+conf+cls_index
    """
    # print('debug3', pred)
    thres = np.array(thres)
    pred_thres = thres[pred[:, 5].astype(np.int32)]  # (#box, )
    pred_filtered = pred[pred[:, 4] >= pred_thres]  
    # print('debug4', pred_filtered)

    return pred_filtered


def filter_preds(preds, thres):
    return [filter_pred(i, thres) for i in preds]



def complete_header(preds, pics, header_cls, verbose=False):
    """对一个pdf文档的页眉进行补齐后处理，包括，补漏+除重
        补漏：部分页面中的页眉可能会有漏判，实现逻辑是对页眉缺省页面，向前、后找最近
              两页带有页眉的页面，然后对比页眉区域的像素，如果相等，那么认为此处应该
              补上页眉（为什么设置两页是为了应对）。
        除重：一页中页眉可能同时被识别为单句段落，判断逻辑是除去满足和header的bounding 
              box一定重叠率的其他结构块。
    Args:
        preds(list): [pred_0, pred_1, ...],其中pred_i是pageIndex=i页面的预测结果，
                     pred为numpy array，维度为(#box, 6), xyxy+conf+cls_index
        pics(list): [pic_0, pic_1, ...], 每一面的图片，bgr形式，ndarray，长度等于preds
        header_cls(int): header的分类序号
        verbose(bool): 是否打印处理结果
    return:
        preds_processed(list):经过页眉后处理的预测
    """
    def keep_only_one_header_in_predict(pred):
        # 对一面里面出现多个页眉情况，只保留置信度最高的页眉
        header_args = pred[:, 5]==header_cls
        multi_header_pred = pred[header_args]  # (#header, 6)
        if len(multi_header_pred) == 0:
            return pred
        arg_max_conf = np.argmax(multi_header_pred[:, 4])
        header_pred = multi_header_pred[arg_max_conf].reshape(1, 6)  # 只保留最大置信度header
        not_header_pred = pred[np.logical_not(header_args)]
        new_pred = np.concatenate((header_pred, not_header_pred), axis=0)  # header被放在第一个位置
        return new_pred
    
    def get_rid_of_single_paragraph_overlapped(pred):
        # 去除页面内在header内的其他结构块
        if len(pred) <= 1:  # 页面内只有一个或者以下的结构块
            return pred
        if pred[0][5] != header_cls:
            return pred
        overlapped = box_IntersectionOverOneBox(pred[[0], :4], pred[1:, :4]).reshape(-1)  # (#pred-1,)
        return np.concatenate((pred[[0]], pred[1:][overlapped < overlap_thres]), axis=0)
    
    def generate_search_page_args(page_arg):
        # 搜寻规则：先找前面最近的有页眉的页面，再找后面最近的有页眉的页面，再找前面
        # 第二近的有页眉的页面，再找后面第二近的有页眉的页面
        before_page_args = [page_arg - i - 1 for i in range(search_range)]
        after_page_args = [page_arg + i for i in range(search_range)]
        candidate_args = sum(zip(before_page_args, after_page_args), ())  # interleave
        return candidate_args
    
    # config
    search_range = 2  # 前后搜索的有页眉的页面数量
    overlap_thres = 0.5  # 其他结构块与页眉重合度超过此值，去除
    
    preds = [keep_only_one_header_in_predict(pred) for pred in preds]  
    header_pages = []  # 记录有页眉的页面编号(sorted), 例如[1,2,4,5]
    no_header_pages = []  # 记录无页眉的页面编号(sorted)，例如[0,3]
    for i, pred in enumerate(preds):
        if len(pred) == 0:
            no_header_pages.append(i)  # 如果该页什么都没有
        elif pred[0][5] == header_cls:  # 如果有一个header,header被放在第一个位置
            header_pages.append(i)
        else:
            no_header_pages.append(i)
    
    # 补漏
    n_add = 0
    page_reference_result = []  # 记录页眉补齐页面编号，以及其参考的页面编号
    for pageIndex in no_header_pages:
        page_arg = bisect(header_pages, pageIndex)
        candidate_args = generate_search_page_args(page_arg)
        for candidate_arg in candidate_args:
            if candidate_arg < 0 or candidate_arg > len(header_pages) -1:
                continue
            else:
                pageIndex_candidate = header_pages[candidate_arg]
                # 首先对比图片的尺寸，不满足直接pass
                if pics[pageIndex].shape != pics[pageIndex_candidate].shape:
                    continue
                
                # 对比header区域的图片是否相等
                x0, y0, x1, y1 = preds[pageIndex_candidate][0][:4].astype(np.int32)
                header_area = pics[pageIndex][y0:y1+1, x0:x1+1]
                header_area_candidate = pics[pageIndex_candidate][y0:y1+1, x0:x1+1]
                if not np.all(header_area==header_area_candidate):
                    continue
                else:
                    page_reference_result.append((pageIndex, pageIndex_candidate))
                    preds[pageIndex] = np.concatenate((preds[pageIndex_candidate][[0]],
                                                       preds[pageIndex]),
                                                      axis=0)
                    header_pages.insert(page_arg, pageIndex)  # 更新该页为有页眉的页面
                    n_add += 1
                    break
    if verbose:
        print('页眉规则补齐数目：%d' % n_add)
        for pageIndex, pageIndex_candidate in page_reference_result:
            print('%d --> %d' %(pageIndex, pageIndex_candidate))
    
    # 去重
    preds = [get_rid_of_single_paragraph_overlapped(pred) for pred in preds]
        
    return preds
    

def change_pic_structure_to_header(preds, pics, header_cls, pic_cls, verbose=False):
    """对一个pdf文档没有识别页眉结构块的某个页面，出现了第一个结构块(y方向)为图片结
    构块的情况进行判断，查看这个图片结构块是否为页眉结构块
        思路：将这个图片结构块和前后相邻的两页进行同区域的比较，这个图片结构块重复的
    出现，那么就认为这个图片结构块是页眉结构块。
    Args:
        preds(list): [pred_0, pred_1, ...],其中pred_i是pageIndex=i页面的预测结果，
                     pred为numpy array，维度为(#box, 6), xyxy+conf+cls_index
        pics(list): [pic_0, pic_1, ...], 每一面的图片，bgr形式，ndarray，长度等于preds
        header_cls(int): header的分类序号
        pic_cls(int): pic的分类序号
        verbose(bool): 是否打印处理结果
    return:
        preds_processed(list):经过页眉后处理的预测
    """
    def select_pic_candidate(pred):
        # 如果一个页面内第一个结构块是图片，并且没有其他结构块在其y0-y1的范围内，则
        # 认为这是一个candidate.
        if len(pred) == 0:  # 页面什么结构也没有
            return None 
        if len(pred[pred[:, 5]==header_cls]) != 0:  # 已经有页眉结构块
            return None
        first_structure_arg = np.argmin(pred[:, 1])  # y0最小即第一个结构块
        if pred[first_structure_arg][5] != pic_cls:  # 不是图片结构块
            return None
        first_structure_y1 = pred[first_structure_arg][3]
        other_structure_args = [True for _ in range(len(pred))]
        other_structure_args[first_structure_arg] = False
        other_structure_y0s = pred[other_structure_args][:, 1]  
        if np.all(other_structure_y0s >= first_structure_y1):  # other_structure为空这里也为True
            return first_structure_arg  # 返回第一个结构块的arg
        else:
            return None
        
    def generate_search_index(index):
        # 搜寻规则：先找前面最近的有页眉的页面，再找后面最近的有页眉的页面，再找前面
        # 第二近的有页眉的页面，再找后面第二近的有页眉的页面
        before_page_index = [index - i - 1 for i in range(search_range)]
        after_page_index = [index + i + 1 for i in range(search_range)]
        search_index = sum(zip(before_page_index, after_page_index), ())  # interleave
        return search_index

    # config
    search_range = 2  # 前后搜索的页面范围
    
    page_groups = {}  # 按照页面尺寸h,w对页面进行分组
    for i ,pic in enumerate(pics):
        size = pic.shape[: 2]  # (h, w)
        page_groups.setdefault(size, []).append(i)  # (h, w): [1,2,4,5,..]
    
    page_change_record = []  # 记录图片转为页眉的页面编号以及其参考的页面编号 
    for _, page_group in page_groups.items():
        for i, page_index in enumerate(page_group):
            pred = preds[page_index]
            cand_structure_arg = select_pic_candidate(pred)  # None or structure
            if not cand_structure_arg:
                continue
            x0, y0, x1, y1 = pred[cand_structure_arg][: 4].round().astype(np.int32)
            search_index = generate_search_index(i)  # (i-1, i+1, i-2, i+2)
            for j in search_index:
                if j < 0 or j > len(page_group) - 1:
                    continue
                search_page_index = page_group[j]
                page_area = pics[page_index][y0:y1+1, x0:x1+1]  # 本页的图片结构块区域
                search_area = pics[search_page_index][y0:y1+1, x0:x1+1]  # 搜寻页面的对应区域
                if not np.all(page_area==search_area):
                    continue
                else:  
                    pred[cand_structure_arg, 5] = header_cls
                    page_change_record.append((page_index, search_page_index))
                    break
                
    if verbose:
        print('图片结构块转为页眉数量：%d' % len(page_change_record))
        for i, j in page_change_record:
            print('%d --> %d' %(i, j))
            
    return preds
            

def process_header(preds, pics, names, verbose=False):
    """Wrapper
    Args:
        preds(list): [pred_0, pred_1, ...],其中pred_i是pageIndex=i页面的预测结果，
                     pred为numpy array，维度为(#box, 6), xyxy+conf+cls_index
        pics(list): [pic_0, pic_1, ...], 每一面的图片，bgr形式，ndarray，长度等于preds
        names(list): 模型预测的结构块名字
        verbose(bool): 是否打印处理结果
    return:
        preds(list)
    """
    header_cls = names.index('header')
    pic_cls = names.index('picture')
    preds = change_pic_structure_to_header(preds, pics, header_cls, pic_cls, verbose)
    preds = complete_header(preds, pics, header_cls, verbose)
            
    return preds