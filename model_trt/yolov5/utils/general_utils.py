# -*- coding: utf-8 -*-
"""
Created on Thu Aug 19 17:00:24 2021

@author: EDZ
"""

import numpy as np
import cv2


def point2range(vector):
    """从连续标记的1的list找连续1区域的开始和结束坐标
    Arg:
        vector(1d array):例如[1, 1, 0, 1, 1, 0]
    return:
        ran(2d array): 例如[[0,1], [3,4]], 
    """
    vector = np.insert(vector, 0, 0)
    vector = np.append(vector, 0)
    diff = np.diff(vector)
    start = np.argwhere(diff==1).flatten()           
    end = np.argwhere(diff==-1).flatten() - 1  
    ran = np.stack((start, end), axis=1)
         
    return ran


def box_IntersectionOverOneBox(box1, box2):
    # 与box_iou不同的是，这里分母是box2的面积，而不是union面积
    # Both sets of boxes are expected to be in (x1, y1, x2, y2) format.
    """
    Arguments:
        box1 (numpy[N, 4])
        box2 (numpy[M, 4])
    Returns:
        io_box2 (numpy[N, M])
    """

    def box_area(box):
        # box = 4xn
        return (box[2] - box[0]) * (box[3] - box[1])

    # area1 = box_area(box1.transpose())
    area2 = box_area(box2.transpose())  # (M,)

    # inter(N,M) = (rb(N,M,2) - lt(N,M,2)).clamp(0).prod(2)
    inter = (np.minimum(box1[:, None, 2:], box2[:, 2:]) - np.maximum(box1[:, None, :2], box2[:, :2])).clip(0).prod(2)
    return inter / (area2 + 1E-12)  # iou = inter / (area2)


def filter_bboxes_in_rect(bboxes:np.ndarray, rect:np.ndarray) -> np.ndarray:
    """filter bboxes(xyxy) if they are in a given rect area(xyxy)"""
    centers = (bboxes[:, [0, 1]] + bboxes[:, [2, 3]]) / 2.0  #  (#bboxes, xy)
    centers = np.concatenate((centers*(-1), centers), axis=1)  # (#bboxes, -x-yxy)
    rect = rect * np.array([-1, -1, 1, 1])  # (-x-yxy)
    bool_array = (rect >= centers).all(axis=1)  # (#bboxes, )
    bboxes_filtered = bboxes[bool_array]
    return bboxes_filtered, bool_array