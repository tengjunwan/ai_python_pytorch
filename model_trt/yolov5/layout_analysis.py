# -*- coding: utf-8 -*-
"""
Created on Thu Aug 19 15:29:56 2021

@author: EDZ
"""

from abc import ABC, abstractmethod
from model_trt.yolov5.tensorrt_wrapper import Yolov5_TRT
from model_trt.yolov5.pre_post_process import read_factory
from model_trt.yolov5.utils.post_process_utils import modify_preds


class Layout_Analyzer(ABC):
    """basic representation of layout analysis"""
    
    @abstractmethod
    def prepare_data():
        """prepare data"""
    
    @abstractmethod
    def infer():
        """do inference, includes prepare_data and post_process"""
        
    @abstractmethod
    def post_process():
        """do post process"""
        
        
class Layout_Analyzer_For_A_Share(Layout_Analyzer):
    """layout analysis for a share"""
    
    def __init__(self, engine_file_path, plugin_file_path, device=0,
                 verbose=False, names=None, deploy_thres=None):
        """
        Args:
            engine_file_path(str): path of serialized engine.
            plugin_file_path(str): path of corresponding plugin.
            device(int): index of gpu device for inference.
            verbose(bool): print messages if true.
            names(list or None): names for structures.
            deploy_thres(list or None): threshold for filtering structures.
        """
        # store
        self.engine_file_path = engine_file_path
        self.plugin_file_path = plugin_file_path
        self.device = device
        self.verbose = verbose

        # load tensorrt wrapper
        self.yolov5_wrapper = Yolov5_TRT(engine_file_path, plugin_file_path, device)
        self.batch_size = self.yolov5_wrapper.batch_size
        
        # name list for each index classification
        if names is None:
            self.names = ['single para', 'ordinary para', 'lined table', 'unlined table',
                          'header', 'footer', 'picture', 'chart', 'footnote', 'formula',
                          'catalog']
        else:
            self.names = names
            
        # threshold used in deployment
        if deploy_thres is None:
            self.deploy_thres = [0.4 for _ in range(len(self.names))]  
        else:
            self.deploy_thres = deploy_thres
            
        # get preprocessor and post-processor
        fac = read_factory('a share')
        self.preprocessor = fac.get_preprocessor()
        self.post_processor = fac.get_post_processor()
        
        # warmup
        self.yolov5_wrapper.warmup(verbose=self.verbose)
            
    def prepare_data(self, imgs):
        """prepare data for inference
        Args:
            imgs(list): a list of images(BGR)
        return:
            imgs_processed(list): a list of processed images(BGR)
        """
        imgs_processed = self.preprocessor(imgs, self.verbose)
        
        return imgs_processed
                
    def infer(self, imgs, conf_thres=0.4, iou_thres=0.6):
        """do inference with a deserialized engine
        Args:
            imgs(list): list of images, BGR.
            conf_thres(float): parameter used in nms.
            iou_thres(float): parameter used in nms.
        return:
            
        """
        # preprocess
        imgs_preprocessed = self.prepare_data(imgs)
        
        # group images in batch
        img_batches = self.provide_data_in_batch(imgs_preprocessed, self.batch_size)
        
        # do inference
        raw_preds = []
        post_preds = []
        try:
            total_use_time = 0
            for img_batch in img_batches:
                pred_batch, use_time = self.yolov5_wrapper.infer(img_batch, conf_thres, iou_thres)  
                raw_preds.extend(pred_batch)
                total_use_time += use_time
        finally:
            # self.yolov5_wrapper.destroy()
            pass
            
        if self.verbose:
            print('tensorrt inference, time->{:.3f}s'.format(total_use_time))
            
        raw_preds = modify_preds(raw_preds)  # due to jinglong pre process
        
        # post process
        post_preds = self.post_process(imgs, raw_preds, verbose=self.verbose)
        
        return raw_preds, post_preds
        
    def post_process(self, imgs, raw_preds, verbose=False):
        """post process of the raw predictions
        Args:
            imgs(list): a list of images(BGR).
            raw_preds(list): a list of preds made by model, numpy, each is 
                (#boxes, 6), 6 means (xyxy+conf+cls_id).
            verbose(bool): print messages if True.
        return:
            post_preds(list): a list of preds made by model, numpy, each is 
                (#boxes, 6), 6 means (xyxy+conf+cls_id).
        """
        post_preds = self.post_processor(imgs, raw_preds, self.names, 
                                         self.deploy_thres, self.verbose)
        return post_preds
        
        
    def provide_data_in_batch(self, data, batch_size):
        batches = []  # list for batches
        batch = []  # list for a batch
        for d in data:
            if len(batch) == batch_size:
                batches.append(batch)
                batch = []
            batch.append(d)
        if len(batch) > 0:
            batches.append(batch)
        return batches
        
        
    
    
    