# -*- coding: utf-8 -*-
"""
Created on Thu Aug 19 18:36:02 2021

@author: EDZ
"""

import time
from abc import ABC, abstractmethod
from model_trt.yolov5.utils.preprocess_utils import erode_elong_save
from model_trt.yolov5.utils.post_process_utils import adjust_preds
from model_trt.yolov5.utils.post_process_utils import adjust_lined_table_preds
from model_trt.yolov5.utils.post_process_utils import filter_preds
from model_trt.yolov5.utils.post_process_utils import process_header


# =========== preprocessor ================
class PreProcessor(ABC):
    """interface for preprocess"""
    
    @abstractmethod
    def __call__(self, imgs, verbose):
        """process raw images"""
        
        
class PreProcessor_For_A_Share(PreProcessor):
    # TODO
    def __init__(self, erode=True):
        self.erode = erode
        
    def __call__(self, imgs, verbose=False):
        """preprocess for a share, which is defined by jinglong yang, including
        a serial operations of deformation, erosion and so on.
        Args:
            imgs(list): a list of images, BGR.
        return:
            p_imgs(list): a list of processed images, BGR.
        """
        t0 = time.time()
        p_imgs = [erode_elong_save(img, erode=self.erode) for img in imgs]
        t1 = time.time()
        if verbose:
            print("preprocess consuming time->%.3fs" % (t1 - t0))
        return p_imgs
    
    
# =========== post processor ===============
class Post_Processor(ABC):
    """interface for post process"""
    
    @abstractmethod
    def __call__(self, imgs, raw_preds, verbose):
        """post process raw predictions"""
        
        
class Post_Processor_For_A_Share(Post_Processor):
    # TODO
    def __init__(self):        
        # procedures(functions) 
        self.adjust_preds = adjust_preds
        self.adjust_lined_table_preds = adjust_lined_table_preds
        self.filter_preds = filter_preds
        self.process_header = process_header
        
        
    def __call__(self, imgs, raw_preds, names, deploy_thres, verbose=False):
        """
        # TODO
        """
        t0 = time.time()
        # adjust raw preds to regions 
        preds_adjusted = self.adjust_preds(imgs, raw_preds)
        # adjust the confidence score of lined table pred
        preds_adjusted = self.adjust_lined_table_preds(preds_adjusted,
                                                       imgs,
                                                       deploy_thres[names.index('lined table')],
                                                       verbose)
        # filter by deploy_thres
        preds_filtred = self.filter_preds(preds_adjusted, deploy_thres)
        # add missing header
        preds_header_added = self.process_header(preds_filtred, imgs, 
                                                 names, verbose)
        t1 = time.time()
        if verbose:
            print("post process consuming time->%.3fs" % (t1 - t0))
        return preds_header_added
    
    
# ============ factory ================
class Pre_Post_Process_Factory(ABC):
    """
    Factory that return a combination of preprocess and postprocess.
    The factory dosen't maintain any of the instances it create.
    """
    
    @abstractmethod
    def get_preprocessor(self) -> PreProcessor:
        """returns a preprocessor"""
        
    @abstractmethod
    def get_post_processor(self) -> Post_Processor:
        """returns a post_processor"""
        
        
class Pre_Post_Process_Factory_For_A_Share(Pre_Post_Process_Factory):
    """Factory aimed at providing preprocess and post process for a share"""
    
    def get_preprocessor(self) -> PreProcessor:
        return PreProcessor_For_A_Share()
    
    def get_post_processor(self) -> Post_Processor:
        return Post_Processor_For_A_Share()


# =========== function ============
def read_factory(task:str) -> Pre_Post_Process_Factory:
    """construct a factory based on the given task"""
    factories = {
        'a share': Pre_Post_Process_Factory_For_A_Share,
        }
    try:
        fac_type = factories[task]
    except KeyError:
        print('%s not found' % task)
        print('not included in:', *factories.keys())
        raise KeyError
        
    return fac_type()
    
    


