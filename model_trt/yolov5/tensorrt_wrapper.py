# -*- coding: utf-8 -*-
"""
Created on Thu Aug 19 14:17:15 2021

@author: EDZ
"""

import threading
import time
import ctypes
# import pycuda.autoinit
import numpy as np
import cv2
import pycuda.driver as cuda
import tensorrt as trt
import torch
import torchvision
        
        
class Yolov5_TRT():
    """A YOLOv5 class that warps TensorRT ops, preprocess and postprocess ops.
    """
    
    def __init__(self, engine_file_path, plugin_file_path, device=0):
        """
        Args:
            engine_file_path(str): path of serialized engine.
            plugin_file_path(str): path of corresponding plugin.
            device(int): index of gpu device for inference.
        """
        # load custom plugins
        ctypes.CDLL(plugin_file_path)
        
        # create a Context on this device
        cuda.init()
        self.ctx = cuda.Device(device).make_context()
        stream = cuda.Stream()
        TRT_LOGGER = trt.Logger(trt.Logger.INFO)
        runtime = trt.Runtime(TRT_LOGGER)

        # deserialize the engine from file
        with open(engine_file_path, "rb") as f:
            engine = runtime.deserialize_cuda_engine(f.read())
        context = engine.create_execution_context()

        host_inputs = []
        cuda_inputs = []
        host_outputs = []
        cuda_outputs = []
        bindings = []

        for binding in engine:
            print('bingding:', binding, engine.get_binding_shape(binding))
            size = trt.volume(engine.get_binding_shape(binding)) * engine.max_batch_size
            dtype = trt.nptype(engine.get_binding_dtype(binding))
            # allocate host and device buffers
            host_mem = cuda.pagelocked_empty(size, dtype)
            cuda_mem = cuda.mem_alloc(host_mem.nbytes)
            # append the device buffer to device bindings.
            bindings.append(int(cuda_mem))
            # append to the appropriate list.
            if engine.binding_is_input(binding):
                self.input_w = engine.get_binding_shape(binding)[-1]
                self.input_h = engine.get_binding_shape(binding)[-2]
                host_inputs.append(host_mem)
                cuda_inputs.append(cuda_mem)
            else:
                host_outputs.append(host_mem)
                cuda_outputs.append(cuda_mem)

        # store
        self.stream = stream
        self.context = context
        self.engine = engine
        self.host_inputs = host_inputs
        self.cuda_inputs = cuda_inputs
        self.host_outputs = host_outputs
        self.cuda_outputs = cuda_outputs
        self.bindings = bindings
        self.batch_size = engine.max_batch_size
        
    def infer(self, img_list, conf_thres=0.5, iou_thres=0.4):
        """main function
        Args:
            img_list(list): the size of it should be less than a batch size.
            conf_thres(float): confidence threshold for nms.
            iou_thres(float): iou threshold for nms.
        """
        threading.Thread.__init__(self)
        # make self the active context, pushing it on top of the context stack.
        self.ctx.push()
        # restore
        stream = self.stream
        context = self.context
        engine = self.engine
        host_inputs = self.host_inputs
        cuda_inputs = self.cuda_inputs
        host_outputs = self.host_outputs
        cuda_outputs = self.cuda_outputs
        bindings = self.bindings
        # do image preprocess
        batch_image_raw = []
        batch_origin_h = []
        batch_origin_w = []
        batch_input_image = np.empty(shape=[self.batch_size, 3, self.input_h, self.input_w])
        for i, image_raw in enumerate(img_list):
            input_image, image_raw, origin_h, origin_w = self.preprocess_image(image_raw)
            batch_image_raw.append(image_raw)
            batch_origin_h.append(origin_h)
            batch_origin_w.append(origin_w)
            np.copyto(batch_input_image[i], input_image)
        batch_input_image = np.ascontiguousarray(batch_input_image)

        # copy input image to host buffer
        np.copyto(host_inputs[0], batch_input_image.ravel())
        start = time.time()
        # transfer input data  to the GPU.
        cuda.memcpy_htod_async(cuda_inputs[0], host_inputs[0], stream)
        # run inference.
        context.execute_async(batch_size=self.batch_size, bindings=bindings, stream_handle=stream.handle)
        # transfer predictions back from the GPU.
        cuda.memcpy_dtoh_async(host_outputs[0], cuda_outputs[0], stream)
        # synchronize the stream
        stream.synchronize()
        end = time.time()
        # remove any context from the top of the context stack, deactivating it.
        self.ctx.pop()
        # here we use the first row of output in that batch_size = 1
        output = host_outputs[0]
        # do postprocess
        preds = []
        for i in range(len(img_list)):
            result = self.post_process(
                output=output[i * 6001: (i + 1) * 6001], 
                origin_h=batch_origin_h[i],
                origin_w=batch_origin_w[i],
                conf_thres=conf_thres, 
                iou_thres=iou_thres
            )
            preds.append(result)
            
        return preds, end - start
    
    def preprocess_image(self, raw_bgr_image):
        """Convert BGR image to RGB,
        resize and pad it to target size, normalize to [0,1],
        transform to NCHW format.
        Args:
            raw_bgr_image(np.ndarray): BGR
        return:
            image(np.ndarray): the processed image
            image_raw(np.ndarray): the original image
            h(int): original height
            w(int): original width
        """
        image_raw = raw_bgr_image
        h, w, c = image_raw.shape
        image = cv2.cvtColor(image_raw, cv2.COLOR_BGR2RGB)
        # calculate widht and height and paddings
        r_w = self.input_w / w
        r_h = self.input_h / h
        if r_h > r_w:
            tw = self.input_w
            th = int(r_w * h)
            tx1 = tx2 = 0
            ty1 = int((self.input_h - th) / 2)
            ty2 = self.input_h - th - ty1
        else:
            tw = int(r_h * w)
            th = self.input_h
            tx1 = int((self.input_w - tw) / 2)
            tx2 = self.input_w - tw - tx1
            ty1 = ty2 = 0
        # resize the image with long side while maintaining ratio
        image = cv2.resize(image, (tw, th))
        # pad the short side with (128,128,128)
        image = cv2.copyMakeBorder(
            image, ty1, ty2, tx1, tx2, cv2.BORDER_CONSTANT, (128, 128, 128)
        )
        image = image.astype(np.float32)
        # normalize to [0,1]
        image /= 255.0
        # HWC to CHW format:
        image = np.transpose(image, [2, 0, 1])
        # CHW to NCHW format
        image = np.expand_dims(image, axis=0)
        # convert the image to row-major order, also known as "C order":
        image = np.ascontiguousarray(image)
        return image, image_raw, h, w
    
    def post_process(self, output, origin_h, origin_w, conf_thres, iou_thres):
        """
        postprocess the prediction: 
            1.Non-maximum-suppression
            2.
        Args:
            output(tensor): A tensor likes [num_boxes,cx,cy,w,h,conf,cls_id, cx,cy,w,h,conf,cls_id, ...] 
            origin_h(int): height of original image
            origin_w:   width of original image
        return:
            result(numpy): a numpy array likes (#boxes, 6), 6 means xyxy+conf+cls_id.
        """
        # get the num of boxes detected
        num = int(output[0])
        # reshape to a two dimentional ndarray
        pred = np.reshape(output[1:], (-1, 6))[:num, :]  # (num_boxes, 6)
        # to a torch Tensor
        pred = torch.Tensor(pred).cuda()
        # get the boxes
        boxes = pred[:, :4]  # (num_boxes, 4)
        # get the scores
        scores = pred[:, 4]  # (num_boxes,)
        # get the classid
        classid = pred[:, 5]  # (num_boxes,)
        # choose those boxes that score > CONF_THRESH
        si = scores > conf_thres
        boxes = boxes[si, :]
        scores = scores[si]
        classid = classid[si]
        # trandform bbox from [center_x, center_y, w, h] to [x1, y1, x2, y2]
        boxes = self.xywh2xyxy(origin_h, origin_w, boxes)
        # do nms
        indices = torchvision.ops.nms(boxes, scores, iou_threshold=iou_thres).cpu()
        result_boxes = boxes[indices, :].cpu().numpy()
        result_scores = scores[indices].cpu().numpy().reshape(-1, 1)
        result_classid = classid[indices].cpu().numpy().reshape(-1, 1)
        # to xyxy+conf+cls_id form
        result = np.concatenate((result_boxes, result_scores, result_classid), axis=1)
        return result
    
    def xywh2xyxy(self, origin_h, origin_w, x):
        """
        Convert nx4 boxes from [x, y, w, h] to [x1, y1, x2, y2] where xy1=top-left,
        xy2=bottom-right:
            1. eliminate padding
            2. convert from xywh to xyxy
        Args:
            origin_h(int): height of original image
            origin_w(int): width of original image
            x(tensor): A boxes tensor, each row is a box [center_x, center_y, w, h]
        return:
            y(tensor): A boxes tensor, each row is a box [x1, y1, x2, y2]
        """
        y = torch.zeros_like(x) if isinstance(x, torch.Tensor) else np.zeros_like(x)
        r_w = self.input_w / origin_w
        r_h = self.input_h / origin_h
        if r_h > r_w:
            y[:, 0] = x[:, 0] - x[:, 2] / 2
            y[:, 2] = x[:, 0] + x[:, 2] / 2
            y[:, 1] = x[:, 1] - x[:, 3] / 2 - (self.input_h - r_w * origin_h) / 2
            y[:, 3] = x[:, 1] + x[:, 3] / 2 - (self.input_h - r_w * origin_h) / 2
            y /= r_w
        else:
            y[:, 0] = x[:, 0] - x[:, 2] / 2 - (self.input_w - r_h * origin_w) / 2
            y[:, 2] = x[:, 0] + x[:, 2] / 2 - (self.input_w - r_h * origin_w) / 2
            y[:, 1] = x[:, 1] - x[:, 3] / 2
            y[:, 3] = x[:, 1] + x[:, 3] / 2
            y /= r_h

        return y
        
    def destroy(self):
        # remove any context from the top of the context stack, deactivating it.
        self.ctx.pop()
        
    def warmup(self, times=10, verbose=False):
        def fake_data_generator():   
            # a generator which generates a batch size of data
            for _ in range(self.batch_size):
                yield np.zeros([self.input_h, self.input_w, 3], dtype=np.uint8)
        # runs several times with fake data
        for _ in range(times):
            _, use_time = self.infer(list(fake_data_generator()))
            if verbose:
                print('tensorrt warm up, time->{:.3f}s'.format(use_time))
        
        
        
    
        