# -*- coding: utf-8 -*-
"""
Created on Wed Dec  2 19:19:35 2020

@author: EDZ
"""


import time
import os
from utils.deployment import generate_text_regions, box_IntersectionOverOneBox, \
    erode_elong_save, modify_pred
from utils.deployment import LoadListOfImages, check_img_size, non_max_suppression, \
    scale_coords
from utils.deployment import select_device, time_synchronized
from utils.deployment import correct_single_row_lined_table, filter_pred
import numpy as np
import torch
from numpy import random


# 我把需要的函数都放在deployment.py文件内
# from utils.datasets import LoadListOfImages
# from utils.utils import check_img_size, non_max_suppression, scale_coords
# from utils.torch_utils import select_device, time_synchronized


def postprocess(list_of_images, p_list, inplace=False):
    """后处理
    思路：通过在模型预测框内包含的“投影region”进行预测框的修正
    Args:
        list_of_images(list of array): H*W*3, BGR模式
        p_list(list of array): 模型预测框, (#box, 6), 6 = xyxy, conf, cls_index
        inplace(bool): 是否对p_list进行inplace操作
    return:
        p_list(list of array): 模型预测框, (#box, 6), 6 = xyxy, conf, cls_index
    """
    regions = [generate_text_regions(i) for i in list_of_images]  # 叶龙的投影方法, (#region, xyxy)
        
    if len(regions) ==0 or regions is None:
        return p_list, regions
    
    if not inplace:
        p_list = [x.copy() for x in p_list]  
    
    io_region_thres = 0.45  # intersection over region阈值
    for pred, region in zip(p_list, regions):
        if len(region) == 0:
            continue
        io_region = box_IntersectionOverOneBox(pred[:, :4], region)  # (#pred, #region)
        io_region_bool = io_region > io_region_thres
        try:
            for i, p in enumerate(pred):  # p (6, )
                # 预测框调整为所有io_region>0.5的region的外接矩形
                region_arg = io_region_bool[i]  # [True, False, ...]
                if np.sum(region_arg) > 0:
                    p[: 2] = region[region_arg][:, :2].min(axis=0)  # lt取最小
                    p[2: 4] = region[region_arg][:, 2:].max(axis=0)  # br取最大
        except:
            print(region.shape)
            print(io_region.shape)
            print(region_arg.shape)
            raise ValueError

    return p_list, regions
    

class Layout_Processor():
    """版面识别，识别类型：
       0.有线表格
       1.无线表格
       2.页眉
       3.图片
       4.图表
       5.公式
       6.目录
       步骤：
       1.yolov5
       2.规则后处理
    """
    def __init__(self,
                 weight,
                 device='0',
                 img_size=960, 
                 batch_size=16,
                 verbose=False,
                 deploy_thres=None):
        """
        Args:
            weight(str): 模型的参数pt文件地址
            device(str): "0","1",选择的显卡编号
            img_size(int): 训练时候的图片固定图片长边的参数（和训练时设置一样）
            batch_size(int): infer的batch大小
            verbose(bool): 是否打印infer的信息
            deploy_thres(list of float): 工程应用中使用的阈值（可调节）
        """
        # config
        self.weight = weight  # 训练的checkpoint文件地址
        self.img_size = img_size  # 固定图片长边为img_size并且padding到正方形作为模型输入
        self.batch_size = batch_size  
        self.device = select_device(device)
        self.half = self.device.type != 'cpu'  # half precision only supported on CUDA
        self.verbose = verbose
        
        # Load model
        # self.model = attempt_load(weight, map_location=self.device)  # load FP32 model
        self.model = torch.load(weight, map_location=self.device)['model'].float().eval()  # load FP32 model
        if verbose:
            m = self.model.model[-1] 
            print(m.anchors)
            print(m.stride)
            # print(self.model.info(img_size=self.img_size))
        self.imgsz = check_img_size(img_size, s=self.model.stride.max())  # check img_size
        if self.half:
            self.model.half()  # to FP16
            
        # Get names and colors
        self.names = ['lined table', 'unlined table', 'header', 'pic', 'chart', 'formular', 'catalog']
        self.colors = [[random.randint(0, 255) for _ in range(3)] for _ in self.names]
        
        # class index of lined table
        self.index_of_lined_table = self.names.index('lined table')
        
        # Get thres for deployment request
        if deploy_thres is None:
            self.deploy_thres = [0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4]  
        else:
            self.deploy_thres = deploy_thres
        
        # run once
        img = torch.zeros((1, 3, int(self.imgsz), int(self.imgsz)), device=self.device)  # init img
        _ = self.model(img.half() if self.half else img) if self.device.type != 'cpu' else None  # run once
        
    def model_predict(self, list_of_images, conf_thres=0.4, iou_thres=0.6):
        """模型预测部分
        Args:
            list_of_images(list of 2d array): 包含图片数组的列表，维度H*W*3，BGR
            conf_thres(float): NMS系数，过滤掉置信度低于此的预测框
            iou_thres(float): NMS系数，融合高于此值的不同预测框
        return:
            p_list(list of 2d array): 对应每个图片的模型预测，(#box, 6), xyxy+conf+cls_index
        """
        # Get dataset
        dataset = LoadListOfImages(images=list_of_images,
                                   img_size=self.img_size,
                                   batch_size=self.batch_size, 
                                   pad=0.0, # deprecate
                                   rect=True)  # rect infer
        # dataloader
        batch_size = min(self.batch_size, len(dataset))
        nw = min([os.cpu_count(), batch_size if batch_size > 1 else 0, 8])  
        dataloader = torch.utils.data.DataLoader(dataset,
                                                 batch_size=batch_size,
                                                 num_workers=nw,
                                                 shuffle=False,  
                                                 pin_memory=True,
                                                 collate_fn=dataset.collate_fn
                                                 )
        
        p_list = [np.zeros((0, 6)) for _ in range(len(list_of_images))]  # 预测list, 顺序对应输入list_of_images
        t = 0
        # inference
        for batch_i, (imgs, order, shapes) in enumerate(dataloader):
            imgs = imgs.to(self.device) 
            imgs = imgs.half() if self.half else imgs.float()  
            imgs /= 255.0
            with torch.no_grad():
                t1 = time_synchronized()
                preds = self.model(imgs, augment=False)[0]
        
                # Apply NMS
                preds = non_max_suppression(preds, 
                                            conf_thres, 
                                            iou_thres,
                                            agnostic=False)
                t2 = time_synchronized()
                t += t2 -t1
                for i, pred in enumerate(preds):  
                    img0_shape = shapes[i][0]  # (h0,w0),原始图片尺寸
                    if pred is not None and len(pred):
                        # 把预测坐标从batch size坐标转化为原图坐标
                        pred[:, :4] = scale_coords(imgs.shape[2:], pred[:, :4], img0_shape).round()
                        p_list[int(order[i])] = pred.cpu().numpy()  # 转为numpy array
        
        if self.verbose:
            # Print time (inference + NMS)
            print('inference+NMS处理时间: %.3fs' % (t))
        
        
        return p_list
            
    def _call(self, list_of_images, conf_thres=0.1, iou_thres=0.1):
        """主要调用方法: 预处理 + 模型预测 + 后处理
        Args:
            list_of_images(list of 2d array): 包含图片数组的列表，维度H*W*3，BGR
            conf_thres(float): NMS系数，过滤掉置信度低于此的预测框
            iou_thres(float): NMS系数，融合高于此值的不同预测框
        return:
            p_list_raw(list of 2d array): 对应每个图片的模型预测，(#box, 6), xyxy+conf+cls_index
            p_list(list of 2d array): 对应每个图片的模型预测+后处理，(#box, 6), xyxy+conf+cls_index  
        """
        t0= time.time()
        # 预处理，菁龙进行了erode + 图片部分拉伸以及顶部padding
        list_of_images_changed = [erode_elong_save(img) for img in list_of_images]  
        t1 = time.time()
        
        # 模型预测
        p_list_raw = self.model_predict(list_of_images_changed, conf_thres, iou_thres)
        t2 = time.time()
        
        # 后处理
        p_list_raw = [modify_pred(pred) for pred in p_list_raw]  # 把预测坐标转到原图上
        p_list, _ = postprocess(list_of_images, p_list_raw)
        t3 = time.time()
        
        if self.verbose:
            print('预处理处理时间: %.3fs' % (t1 - t0))
            print('模型处理总时间: %.3fs' % (t2 - t1))
            print('后处理总时间: %.3fs' % (t3 - t2))
        
        return p_list_raw, p_list
    
    def __call__(self, list_of_images, conf_thre=0.1, iou_thres=0.1):
        """预处理 + 模型预测 + 后处理 + 工程阈值筛选
        Args:
            list_of_images(list of 2d array): 包含图片数组的列表，维度H*W*3，BGR
            conf_thres(float): NMS系数，过滤掉置信度低于此的预测框
            iou_thres(float): NMS系数，融合高于此值的不同预测框
        return:
            p_list_raw(list of 2d array): 对应每个图片的模型预测，(#box, 6), xyxy+conf+cls_index
            p_list_filtered(list of 2d array): 模型预测+后处理+修正置信度+置信度过滤，(#box, 6), xyxy+conf+cls_index  
        """
        # 预处理 + 模型预测 + 后处理
        p_list_raw, p_list = self._call(list_of_images, conf_thre, iou_thres)
        # print(p_list[0])
        
        t0 = time.time()
        # 修正有线表格
        p_list_corrected = [correct_single_row_lined_table(pred, img, self.deploy_thres[0], self.index_of_lined_table) 
                            for pred, img in zip(p_list, list_of_images)]  
        t1 = time.time()
        # print(p_list_corrected[0])
        
        # 置信度过滤
        p_list_filtered = [filter_pred(pred, self.deploy_thres) for pred in p_list_corrected]
        t2 = time.time()
        # print(p_list_filtered[0])
        
        if self.verbose:
            print('修正单行有线表格时间: %.3fs' % (t1 - t0))
            print('过滤阈值时间: %.3fs' % (t2 - t1))
        
        return p_list_raw, p_list_filtered
            
    
if __name__ == "__main__":
    pass
    
                     
    
        
                        
            
                        
                
        
        
        
        
    
        
        