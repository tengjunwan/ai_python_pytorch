# -*- coding: utf-8 -*-
"""
Created on Thu Dec 10 12:56:48 2020

@author: EDZ
"""

# 测试layout_analysis

from layout_analysis import Layout_Processor
import cv2
import argparse
import pickle


def detect(opt):
    """
    模型预测+后处理
    args:
        imgs_path_ls(list of str): 一组图片路径 
        device(str): 显卡编号
    return:
        p_list(list of numpy array): 和图片路径一一对应的一组预测值，numpy array
            维度为（#预测框，6）,其中6表示为xyxy+conf+cls,即四个坐标，置信度和分类序号
    """
    # 设置
    device = opt.device
    weight = opt.weights  
    img_size = opt.img_size
    batch_size = opt.batch_size  
    conf_thres = opt.conf_thres
    iou_thres = opt.iou_thres
    
    # 初始化
    lp = Layout_Processor(weight=weight,
                          device=device,
                          img_size=img_size, 
                          batch_size=batch_size,
                          verbose=False,
                          )
    
    # load image
    with open(opt.source, 'rb') as f:
        imgs_path_ls = pickle.load(f)  # list of str, image_path
    img_list = [cv2.imread(str(i)) for i in imgs_path_ls]
    for i, img in enumerate(img_list):
        assert img is not None, 'image path not loaded: %s' % imgs_path_ls[i]
        
    # 预测, 第一个返回参数是没有后处理的预测结果，第二个是有后处理的预测结果,如果想
    # 查看模型的原始输出，可以取第一个返回参数；其中p_list是list of numpy array，
    # 和图片路径一一对应的一组预测值，numpy array 维度为（#预测框，6）,其中6表示
    # 为xyxy+conf+cls,即四个坐标，置信度和分类序号
    _, p_list = lp(img_list, conf_thres, iou_thres)
    
    # 处理为固定格式的输出
    cls_mapping = {0: 3, 1: 4, 2: 5, 3: 7, 4: 8, 5: 10, 6: 11}  # 改变菁龙分类序号至叶龙分类序号
    detect_list = []  
    for i, predict in enumerate(p_list):
        cls_list, yxyx_list, conf_list = [], [], []
        for x1, y1, x2, y2, conf, cls_index in predict:
            cls_list.append(cls_mapping[cls_index])
            yxyx_list.append([float(y1), float(x1), float(y2), float(x2)])
            conf_list.append(float(conf))
        detect_list.append([cls_list, yxyx_list, conf_list])
    
    with open(opt.save_path, 'wb') as f:
        imgs_path_ls = pickle.dump(detect_list, f)  # list of str, image_path
        
    
    
    
    
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--weights', type=str, default='weights/yolov5s.pt', help='model.pt path')
    parser.add_argument('--source', type=str, default='', help='pickle file path of image list') 
    parser.add_argument('--save-path', type=str, default='result.pkl', help='result save path(as pickle)') 
    parser.add_argument('--batch-size', type=int, default='16', help='infer batch size') 
    parser.add_argument('--img-size', type=int, default=960, help='inference size (pixels)')
    parser.add_argument('--conf-thres', type=float, default=0.001, help='object confidence threshold')
    parser.add_argument('--iou-thres', type=float, default=0.1, help='IOU threshold for NMS')
    parser.add_argument('--device', default='', help='cuda device, i.e. 0 or 1 or cpu')
    opt = parser.parse_args()
    
    detect(opt)
    
    
    
    
    