import os
import subprocess
import math

import shutil
import pdb
import cv2
from tqdm import tqdm
import numpy as np
from app.util.tools import dowbload_images
import os
from app.util.tools import get_time_dif
import time
from urllib import parse
# from yolo_v5.post_process import post_process
import json

from XXX.layout_analysis import Layout_Processor




def get_y_ori(y_after, y_small=50, y_large=100, pad=50, times=3):
    """
    由拉长后的图片的y值计算出原始图片上对应的y值。
    args:
        y_after
        y_small: 原图上拉长区域的下限
        y_large: 原图上拉长区域的上限
        pad: 图片顶端做replica padding的长度
        times: 拉长倍数
    return:
        y_ori
    """
    if y_after > pad + y_small + (y_large - y_small) * times:
        y_ori = y_after - (y_large - y_small) * (times - 1) - pad
    elif y_after > pad + y_small:
        y_ori = (y_after - pad - y_small) / times + y_small
    else:
        y_ori = y_after - pad
    return y_ori


def erode_elong_save(imgs_path_ls, tmp_img_dir, y_small=50, y_large=100, pad=50, times=3):
    """
    对传入的图像列表做预处理，包括侵蚀、拉长和padding，再存入当前文件夹的一个临时文件夹。这个文件夹后面由别的函数来删除。
    args:
        imgs_path_ls: 列表中是每张图片的地址
        tmp_img_dir: 需要显式地输入,由后面别的函数来删除
    return:
        无
    """
    if not os.path.exists(tmp_img_dir):
        os.mkdir(tmp_img_dir)
    for i, img_path in enumerate(imgs_path_ls):
        img = cv2.imread(img_path)
        # erode
        kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (5, 5))
        img = cv2.erode(img,kernel)
        # elongate
        part1 = img[:y_small]
        part2 = img[y_small: y_large]
        part3 = img[y_large:]
        part2_new = cv2.resize(part2, None, fx=1,fy = times)
        img = np.concatenate((part1, part2_new, part3), axis = 0)
        # pad
        img = cv2.copyMakeBorder(img, pad, 0, 0, 0, cv2.BORDER_REPLICATE)
        # save
        cv2.imwrite(os.path.join(tmp_img_dir, str(i)+'.jpg'), img)


def detect(imgs_path_ls, device='0'):
    """
    模型预测+后处理
    args:
        imgs_path_ls(list of str): 一组图片路径 
        device(str): 显卡编号
    return:
        p_list(list of numpy array): 和图片路径一一对应的一组预测值，numpy array
            维度为（#预测框，6）,其中6表示为xyxy+conf+cls,即四个坐标，置信度和分类序号
    """
    # 设置
    weight = '/HDD/pj/yolov5_jinglong/weights/best.pt'  # 模型保存路径
    img_size = 960
    batch_size = 16  # infer batch
    conf_thres=0.001  # 可改，输出的最低置信度
    iou_thres=0.1  # 可改
    
    # 初始化
    lp = Layout_Processor(weight=weight,
                          device=device,
                          img_size=img_size, 
                          batch_size=batch_size,
                          verbose=False,
                          )
    
    # load image
    img_list = [cv2.imread(str(i)) for i in imgs_path_ls]
    for i, img in enumerate(img_list):
        assert img is not None, 'image path not loaded: %s' % imgs_path_ls[i]
        
    # 预测, 第一个返回参数是没有后处理的预测结果，第二个是有后处理的预测结果,如果想
    # 查看模型的原始输出，可以取第一个返回参数；其中p_list是list of numpy array，
    # 和图片路径一一对应的一组预测值，numpy array 维度为（#预测框，6）,其中6表示
    # 为xyxy+conf+cls,即四个坐标，置信度和分类序号
    _, p_list = lp(img_list, conf_thres, iou_thres)
    
    # 处理为固定格式的输出
    cls_mapping = {0: 3, 1: 4, 2: 5, 3: 7, 4: 8, 5: 10, 6: 11}  # 改变菁龙分类序号至叶龙分类序号
    detect_list = []  
    for i, predict in enumerate(p_list):
        cls_list, yxyx_list, conf_list = [], [], []
        for x1, y1, x2, y2, conf, cls_index in predict:
            cls_list.append(cls_mapping[cls_index])
            yxyx_list.append([y1, x1, y2, x2])
            conf_list.append(conf)
        detect_list.append([cls_list, yxyx_list, conf_list])
        
    return detect_list

def read_output(output, num):
    """
    从当前路径中的output文件夹中读入txt文件，返回的是最终的结果，格式为： 1 list(n 图片)->list(3 数据类型)->list(m bboxes)
    对每个box的三中数据类型，class用int，得分用float，边框坐标用int。
    边框坐标是[y1, x1, y2, x2]
    需要读入图片来得到height和width，才能计算出坐标值
    args:
        output: 包含txt和画检测框的jpg的文件夹
        num: 由于这里的output如果没检测到bbox时不生成txt，要传入图片数量
    """
    detect_list = []
    new2ori_class_dict = {0: 3, 1: 4, 2: 5, 3: 7, 4: 8, 5: 10, 6: 11}

    
    #pdb.set_trace()
    for i in range(num):
        classes, scores, positions = [], [], []
        img = cv2.imread(os.path.join(output, str(i)+'.jpg'))
        height, width, _ = img.shape
        # 如果没有检测到
        if not os.path.exists(os.path.join(output, str(i)+'.txt')):
            detect_list.append([[], [], []])
            continue

        with open(os.path.join(output, str(i)+'.txt')) as f:
            bboxes = f.readlines()
            for bbox in bboxes:
                items = bbox.split(' ')
                classes.append(new2ori_class_dict[int(items[0])])
                scores.append(1.0)
                # 下面要从normalize之后的x_center, y_center, width, height去还原位置
                x_c, y_c, w, h = map(float, items[1: -1])
                x_c, w = map(lambda x: x * width, (x_c, w))
                y_c, h = map(lambda x: x * height, (y_c, h))
                y1 = math.floor(y_c - h / 2)
                y2 = math.ceil(y_c + h / 2)
                x1 = math.floor(x_c - w / 2)
                x2 = math.ceil(x_c + w / 2)
                # 再将y转换成ori
                y1 = math.floor(get_y_ori(y1))
                y2 = math.ceil(get_y_ori(y2))
                positions.append([y1, x1, y2, x2])
        detect_list.append([classes, scores, positions])
    return detect_list


def predict_pages(doc_id, page_index, size, root_path, pdf, pdf_path, batch_size, logger=None):
    """
    传入pdf文件预测指定页
    :param doc_id: 文档id
    :param page_index: 待预测页码
    :param pdf: pdf文件url
    :param root_path: pdf和img的存储根目录
    :param pdf_path: pdf文件路径
    :param task_id:此次任务的id
    :return: 类别，分数，定位框
    """
    tag_map = {1: 3, 2: 4, 3: 8, 4: 7, 5: 5, 6: 11}
    # 待预测页面图片路径
    pdf_c = pdf[0]
    for index in range(len(pdf)):
        if pdf[index]!=None and len(pdf[index])>0:
            pdf[index] = pdf[index].split('/')
            pdf[index][-1] = parse.quote(pdf[index][-1])
            pdf[index] = '/'.join(pdf[index])


    # 下载pdf并转成图片
    start_time = time.time()

    img_paths = dowbload_images(root_path, pdf_path, doc_id, page_index, size, pdf[-1])
    if img_paths['code'] in [1, 2]:
        if logger!=None:
            logger.error("原始地址无法下载：%s"%(pdf[-1]))
        img_paths = dowbload_images(root_path, pdf_path, doc_id, page_index, size, pdf[0])

    if img_paths['code'] in [1,2]:
        if logger!=None:
            logger.error("阿里云地址无法下载：%s" % (pdf[0]))
            logger.error("下载或者转换文件失败，参数如下: %s"%(str([root_path, pdf_path, doc_id, page_index, size, pdf])))

        if img_paths['code']==1:
            # recall_p_r.put([callback_url, {'doc_id': doc_id,  'pdfUrl': pdf_c, 'status':3, 'rt': None}, task_id])
            return {'doc_id': doc_id,  'pdfUrl': pdf_c, 'status':3, 'rt': None}
        else:
            # recall_p_r.put([callback_url, {'doc_id': doc_id, 'pdfUrl': pdf_c, 'status': 2, 'rt': None}, task_id])
            {'doc_id': doc_id, 'pdfUrl': pdf_c, 'status': 2, 'rt': None}
        return {}

    print('下载和处理文件共 %d 页耗时：%s' % (size, get_time_dif(start_time)))
    start_time = time.time()

    img_paths = img_paths['data']

    page_index = page_index
    o_page_index = page_index
    rt = []

    # print(img_paths)
    detect_ls = detect(img_paths, device='0')

    # with open('/HDD/run/test/yolo/rt.json', 'w', encoding='utf-8') as f:
    #     f.write(json.dumps(detect_ls, ensure_ascii=False))

    # with open('/HDD/run/test/yolo/images.json', 'w', encoding='utf-8') as f:
    #     f.write(json.dumps(img_paths, ensure_ascii=False))


    # rclasses = []
    # rscores = []
    # rbboxes = []

    # for rclasse, rscore, rbboxe in detect_ls:
    #     rscores.append(rscore)
    #     rclasses.append(rclasse)
    #     rbboxes.append(rbboxe)

    detect_ls_ = []
    rt_images = []
    for index, x in enumerate(detect_ls):
        rclasse, rscore, rbboxe = x
        if len(rscore)==0:
            rt.append({
                'pageIndex':page_index + index
            })
            continue
        rt_images.append(img_paths[index])
        detect_ls_.append([])
        for i in range(len(rscore)):
            detect_ls_[-1].append([rbboxe[i][1],rbboxe[i][0],rbboxe[i][3],rbboxe[i][2]]+[rscore[i], rclasse[i]])
        detect_ls_[-1] = np.array(detect_ls_[-1], dtype=np.int32)

    detect_ls, _ = post_process(rt_images, detect_ls_)


    rclasses = []
    rscores = []
    rbboxes = []

    for x in detect_ls:
        x = x.tolist()
        rclasses.append([])
        rscores.append([])
        rbboxes.append([])

        for _ in x:
            rscores[-1].append(_[4])
            rclasses[-1].append(_[-1])
            rbboxes[-1].append([_[:4][1],_[:4][0],_[:4][3],_[:4][2]])

    index_c = []
    for index in range(len(rclasses)):
        index_ = img_paths.index(rt_images[index])
        index_c.append(page_index + index_)

        rt.append({'pageIndex': page_index + index_, 'rclasses': rclasses[index],
                   'rscores': rscores[index], 'rbboxes': rbboxes[index], 'status': 1})

    print('%d %d'%(len(img_paths), len(rt)))
    # # 补上空页
    # for index in range(len(img_paths)):
    #     if index+page_index not in index_c:
    #         rt.append({
    #             'pageIndex':page_index + index
    #         })

    # print('%d'%(len(rt)))
    
    page_index += len(rclasses)

    print('预测 %d 页耗时：%s' % (len(img_paths), get_time_dif(start_time)))

    rt = {'pageIndex': o_page_index, 'size': size, 'pdfUrl': pdf_c, 'pdfPath': pdf_path,
          'docId': doc_id,
          'pages': rt}

    return rt

if __name__ == "__main__":
    detect_ls = detect(['/HDD/run/aidata-python/app/../upload/10735245_240.jpg', '/HDD/run/aidata-python/app/../upload/10735245_241.jpg'], device='1')
    print(detect_ls)

