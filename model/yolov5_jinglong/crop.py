# -*- coding: utf-8 -*-
"""
Created on Wed Feb  3 16:36:18 2021

@author: EDZ
"""

# 从pdf中裁剪各个判定区域形成图片保存起来

from layout_analysis import Layout_Processor
import cv2
import argparse
from tqdm import tqdm
from pathlib import Path


img_format = ['.png', '.jpeg', '.jpg', '.PNG', '.JPEG', '.JPG']


def crop(opt):
    """
    模型预测+后处理
    args:
        imgs_path_ls(list of str): 一组图片路径 
        device(str): 显卡编号
    return:
        p_list(list of numpy array): 和图片路径一一对应的一组预测值，numpy array
            维度为（#预测框，6）,其中6表示为xyxy+conf+cls,即四个坐标，置信度和分类序号
    """
    # 设置
    device = opt.device
    weight = opt.weights  
    img_size = opt.img_size
    batch_size = opt.batch_size  
    conf_thres = opt.conf_thres
    iou_thres = opt.iou_thres
    
    # 初始化
    lp = Layout_Processor(weight=weight,
                          device=device,
                          img_size=img_size, 
                          batch_size=batch_size,
                          verbose=True,
                          )
    
    # 载入测试图片
    sample_dir = opt.source
    sample_paths = [path for path in Path(sample_dir).glob('*') if path.suffix in img_format]
    sample_img_list = [cv2.imread(str(i)) for i in sample_paths]
    assert sample_img_list[0] is not None, 'image not loaded'
        
    # 预测
    _, p_list = lp(sample_img_list, conf_thres, iou_thres)
    
    # DIR
    categories = opt.category  # [0,1]
    print(categories)
    names = lp.names  # ['lined table', 'unlined table', 'header', 'pic', 'chart', 'formular', 'catalog']
    for i in categories:
        save_dir = Path(opt.output) / names[i]
        if not save_dir.is_dir():
            save_dir.mkdir(parents=True, exist_ok=True)
    
    # 裁剪并保存
    expand = 0  # pixels to expand predict bbox
    for i, preds in tqdm(enumerate(p_list)):
        for j, (x0, y0, x1, y1, conf, cls_index) in enumerate(preds):
            if int(cls_index) in categories:
                img = sample_img_list[i]  # load orignal image
                crop_img = img[int(y0)-expand: int(y1)+1+expand, 
                               int(x0)-expand: int(x1)+1+expand,
                               :]
                img_stem = sample_paths[i].stem  # image name without suffix
                
                save_dir = Path(opt.output) / names[int(cls_index)]
                save_name = img_stem + '_' + str(j) + '.png'  # page_objectIndex.png
                cv2.imwrite(str(save_dir/save_name), crop_img)
                


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--weights', type=str, default='weights/best.pt', help='model.pt path')
    parser.add_argument('--source', type=str, default='', help='source img directory') 
    parser.add_argument('--output', type=str, default='', help='crop image save directory') 
    parser.add_argument('--batch-size', type=int, default='16', help='infer batch size') 
    parser.add_argument('--img-size', type=int, default=960, help='inference size (pixels)')
    parser.add_argument('--conf-thres', type=float, default=0.001, help='object confidence threshold')
    parser.add_argument('--iou-thres', type=float, default=0.01, help='IOU threshold for NMS')
    parser.add_argument('--device', default='', help='cuda device, i.e. 0 or 1 or cpu')
    parser.add_argument('--category', type=int, nargs="+", default=[0,1,2,3,4,5,6], help='categories to be cropped')
    
    opt = parser.parse_args()
    crop(opt)
    pass