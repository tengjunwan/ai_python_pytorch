# -*- coding: utf-8 -*-
"""
Created on Thu Dec 10 11:16:09 2020

@author: EDZ
"""

import cv2
import numpy as np
from torch.utils.data import Dataset
import torch
import math
import time
import torchvision
import os
import random
from bisect import bisect
import fitz


def generate_text_regions(img):
    """参考叶龙的方法写的规则得到pdf页面图片的text region坐标
    step1: 对图片进行二值化
    step2: 进行y轴投影，得到连续的区域的起始和中止坐标[[y_min, y_max],...]
    step3: 根据y轴投影对原图进行裁剪(宽度保持原图宽)
    step4: 对裁剪区域进行x轴投影，找到有text的区域的坐标[x_min, x_max]
    Args:
        img(array): 维度H*W*3 (bgr模式)
    return:
        regions(2d array): 维度(#regions, 4), 4表示yyxx
    """
    # config
    thresh = 200  # 灰度图二值化的阈值
    y_region_thres = 0  #  忽略调y方向投影连续区域过小的阈值
    x_region_thres = 10  #  忽略调x方向投影连续区域过小的阈值
    delta = 0.1  # 拓宽y方向投影连续区域的值，为了ymin=ymax的“线”拥有一点面积
    
    # 二值化
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    _, img = cv2.threshold(img, thresh, 255, cv2.THRESH_BINARY)
    img = np.logical_not(img).astype(np.uint8)  # 0背景， 1前景
    verbose = False
    if verbose:
        cv2.imshow('binary map', img * 255)
        cv2.waitKey()
    
    # y轴投影
    projection_in_y = np.any(img==1, axis=1).astype(np.uint8)  
    y_cuts = point2range(projection_in_y)  # 投影中连续区域的起始和中止坐标[[y_s1, y_e1], [y_s2, y_e2], ...]
    y_cuts = y_cuts[(y_cuts[:, 1] - y_cuts[:, 0]) >= y_region_thres]  # 过滤
    
    # y轴裁剪，对每个y轴裁剪的结果进行x轴投影，投影区域只考虑最小起始点和最大中止点
    x_cuts = np.zeros(y_cuts.shape)
    for i, y_cut in enumerate(y_cuts):
        projection_in_x = np.any(img[y_cut[0]: y_cut[1]+1, :]==1, axis=0)  # 维度(W,)
        x_max = np.max(projection_in_x.nonzero())
        x_min = np.min(projection_in_x.nonzero())
        if x_max - x_min >= x_region_thres:
            x_cuts[i] = [x_min, x_max]
        
    # regions = np.concatenate((y_cuts, x_cuts), axis=1)  # yyxx
    regions = np.concatenate((x_cuts[:, [0]], y_cuts[:, [0]], x_cuts[:, [1]], y_cuts[:, [1]]), axis=1)  #xyxy
    regions[:, 3] += delta  # 使得直线也可以成为region,拥有非0的面积  
    
    return regions


def point2range(vector):
    """从连续标记的1的list找连续1区域的开始和结束坐标
    Arg:
        vector(1d array):例如[1, 1, 0, 1, 1, 0]
    return:
        ran(2d array): 例如[[0,1], [3,4]], 
    """
    vector = np.insert(vector, 0, 0)
    vector = np.append(vector, 0)
    diff = np.diff(vector)
    start = np.argwhere(diff==1).flatten()           
    end = np.argwhere(diff==-1).flatten() - 1  
    ran = np.stack((start, end), axis=1)
         
    return ran


def box_IntersectionOverOneBox(box1, box2):
    # 与box_iou不同的是，这里分母是box2的面积，而不是union面积
    # Both sets of boxes are expected to be in (x1, y1, x2, y2) format.
    """
    Arguments:
        box1 (numpy[N, 4])
        box2 (numpy[M, 4])
    Returns:
        io_box2 (numpy[N, M])
    """

    def box_area(box):
        # box = 4xn
        return (box[2] - box[0]) * (box[3] - box[1])

    # area1 = box_area(box1.transpose())
    area2 = box_area(box2.transpose())  # (M,)

    # inter(N,M) = (rb(N,M,2) - lt(N,M,2)).clamp(0).prod(2)
    inter = (np.minimum(box1[:, None, 2:], box2[:, 2:]) - np.maximum(box1[:, None, :2], box2[:, :2])).clip(0).prod(2)
    return inter / (area2 + 1E-12)  # iou = inter / (area2)


def erode_elong_save(img, y_small=50, y_large=100, pad=50, times=3, inplace=False, erode=True):
    """
    对传入的图像列表做预处理，包括侵蚀、拉长和padding by 菁龙
    args:
        img(array): bgr H*W*3
        y_small(int): 图片y方向0:50保留
        y_large(int):  图片y方向50到100拉长三倍
        pad(int): 再在top padding的像素（replicate）
        times(int): 上面拉长三倍是这里设置的
        inplace(bool): 是否对输入图片进行修改
        erode(bool): 是否对图片进行erode处理
    return:
        img: bgr H*W*3, 菁龙预处理过后的图片
    """
    if not inplace:
        img = img.copy()  
    h0, w0 = img.shape[:2]
    # erode
    if erode:
        kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (5, 5))
        img = cv2.erode(img,kernel)
    # elongate
    part1 = img[:y_small]
    part2 = img[y_small: y_large]
    part3 = img[y_large:]
    part2_new = cv2.resize(part2, None, fx=1,fy = times)
    img = np.concatenate((part1, part2_new, part3), axis = 0)
    # pad
    img = cv2.copyMakeBorder(img, pad, 0, 0, 0, cv2.BORDER_REPLICATE)
    return img

    

def modify_pred(pred, y_small=50, y_large=100, pad=50, times=3):
    # 根据菁龙的修改规则对label进行修改,还原到原图上
    # pred: (#box, 6), 6=xyxy+conf+cls_index
    def modify_y(y):
        if y < y_small + pad:  # 在头部，part1
            return max(y - pad, 0)
        elif y_small + pad <= y < pad + y_small + times * (y_large - y_small):  # 在中部，part2
            y_in_part2 = (y - y_small -pad) / times
            return y_in_part2 + y_small
        else:  # 在尾部，part3
            part2_gain = (y_large - y_small) * (times - 1)
            return y - part2_gain - pad
    
    new_pred = np.zeros_like(pred)
    for i, l in enumerate(pred):  # xyxy+conf+cls_index
        new_pred[i][0] = l[0]
        new_pred[i][1] = modify_y(l[1])
        new_pred[i][2] = l[2]
        new_pred[i][3] = modify_y(l[3])
        new_pred[i][4] = l[4]
        new_pred[i][5] = l[5]
        
    return new_pred


class LoadListOfImages(Dataset):  # 我写的, for deployment
    def __init__(self, images, img_size, batch_size, pad=0.0, rect=True):
        """
        Args:
            images(list of 2d array): bgr模式的图片列表, H*W*3
            img_size(int): 长边固定的长度
            batch_size(int): infer时候的batch size
            rect(bool):是否rect infer，减少padding，增加效率
        """
        self.images = images
        self.images_order = [x for x in range(len(images))]  # 记录输入图片的顺序
        self.img_size = img_size
        self.pad = pad
        self.rect = rect
        self.shapes = np.array([image.shape[:2] for image in images])  # (#images, 2), hw
    
        bi = np.floor(np.arange(len(images)) / batch_size).astype(np.int)  # batch index array([0, 0, 0, 1, 1, 1, 2, 2, 2, 3])
        nb = bi[-1] + 1  # number of batches
        self.batch = bi
        
        if self.rect:
            # 把图片按照长宽比进行排序,"矮胖" → "高瘦"
            # Sort by aspect ratio
            s = self.shapes 
            ar = s[:, 0] / s[:, 1]  # aspect ratio h/w
            # h/w从小到大排列的原图片序列
            irect = ar.argsort() 
            self.images = [self.images[i] for i in irect]  # 重新排序
            self.images_order = [self.images_order[i] for i in irect]  # 重新排序
            self.shapes = s[irect]  # hw
            ar = ar[irect]

            # Set batch image shapes
            shapes = [[1, 1]] * nb
            for i in range(nb):
                ari = ar[bi == i]  # 在一个batch内的ratio
                mini, maxi = ari.min(), ari.max()
                if maxi < 1:  # 如果h/w最大小于1,都属于“矮胖型”
                    shapes[i] = [maxi, 1]  # 选取“最矮胖”的作为batch的统一size
                elif mini > 1:   # 同理,选取“最高瘦”的作为batch的统一size
                    shapes[i] = [1, 1 / mini]
                # 如果说batch中既有“高瘦型”也有“矮胖型”,按照1：1的比例来处理
                
            # 这一步的操作是使得长边为letterbox的预设值,例如50,长边会调整到32的整数去上界(64)
            self.batch_shapes = np.ceil(np.array(shapes) * img_size / 32.).astype(np.int) * 32  # H,W
            
    def __len__(self):
        return len(self.images)
    
    def __getitem__(self, index):
        """
        return:
            img(tensor): 维度(C=3or5, H, W)
            order(int): 图片对应输入图片的list的编号
            shapes:
        """

        # 固定长边为self.img_size
        img = self.images[index]
        h0, w0 = img.shape[: 2]  # 原始长宽
        r = self.img_size / max(h0, w0)
        if r != 1:
            interp = cv2.INTER_AREA 
            img = cv2.resize(img, (int(w0 * r), int(h0 * r)), interpolation=interp)
        h, w = img.shape[: 2]  # 固定长边的长宽

        # Letterbox
        # 如果不是rect training, letterbox将图片padding为(img_size, img_size)
        # 如果是rect training,则调整为batch_shape
        shape = self.batch_shapes[self.batch[index]] if self.rect else self.img_size  
        # letterbox的实质作用是padding,但也会涉及到resize
        img, ratio, pad = letterbox(img, shape, auto=False, scaleup=False)
        shapes = (h0, w0), ((h / h0, w / w0), pad) 

        # Convert img(H,W,c)变为（H,W,C）变为（C,H,W)
        img = img[:, :, ::-1].transpose(2, 0, 1)  # BGR to RGB, to 3x416x416     
        img = np.ascontiguousarray(img)
        order = self.images_order[index]
        
        return torch.from_numpy(img), order, shapes
    
    @staticmethod
    def collate_fn(batch):
        """ batch是一个list,list每个元素是getitem中的返回值
        Arg:
            batch(list of getitem):
        return(也是dataloader的return):
            img(tensor):维度(batch_size, 3, H, W)
            order(tensor):维度(#batch,)
            shapes(tuple):包含每个图片的形状信息(原始+读取+padding)
        """
        img, order, shapes = zip(*batch)  # transposed
        return torch.stack(img, 0), order, shapes

    
def letterbox(img, new_shape=(640, 640), color=(114, 114, 114), auto=True, scaleFill=False, scaleup=True):
    # Resize image to a 32-pixel-multiple rectangle https://github.com/ultralytics/yolov3/issues/232
    shape = img.shape[:2]  # current shape [height, width]
    if isinstance(new_shape, int):
        new_shape = (new_shape, new_shape)

    # Scale ratio (new / old)
    r = min(new_shape[0] / shape[0], new_shape[1] / shape[1])
    if not scaleup:  # only scale down, do not scale up (for better test mAP)
        r = min(r, 1.0)

    # Compute padding
    ratio = r, r  # width, height ratios
    new_unpad = int(round(shape[1] * r)), int(round(shape[0] * r))
    dw, dh = new_shape[1] - new_unpad[0], new_shape[0] - new_unpad[1]  # wh padding
    if auto:  # minimum rectangle
        dw, dh = np.mod(dw, 64), np.mod(dh, 64)  # wh padding
    elif scaleFill:  # stretch
        dw, dh = 0.0, 0.0
        new_unpad = new_shape
        ratio = new_shape[0] / shape[1], new_shape[1] / shape[0]  # width, height ratios

    dw /= 2  # divide padding into 2 sides
    dh /= 2

    if shape[::-1] != new_unpad:  # resize
        img = cv2.resize(img, new_unpad, interpolation=cv2.INTER_LINEAR)
    top, bottom = int(round(dh - 0.1)), int(round(dh + 0.1))
    left, right = int(round(dw - 0.1)), int(round(dw + 0.1))
    img = cv2.copyMakeBorder(img, top, bottom, left, right, cv2.BORDER_CONSTANT, value=color)  # add border
    return img, ratio, (dw, dh)


def check_img_size(img_size, s=32):
    # Verify img_size is a multiple of stride s
    new_size = make_divisible(img_size, s)  # ceil gs-multiple
    if new_size != img_size:
        print('WARNING: --img-size %g must be multiple of max stride %g, updating to %g' % (img_size, s, new_size))
    return new_size


def make_divisible(x, divisor):
    # Returns x evenly divisble by divisor
    return math.ceil(x / divisor) * divisor


def non_max_suppression(prediction, conf_thres=0.1, iou_thres=0.6, merge=False, classes=None, agnostic=False):
    """Performs Non-Maximum Suppression (NMS) on inference results

    Returns:
         detections with shape: nx6 (x1, y1, x2, y2, conf, cls)
    """
    if prediction.dtype is torch.float16:
        prediction = prediction.float()  # to FP32

    nc = prediction[0].shape[1] - 5  # number of classes
    xc = prediction[..., 4] > conf_thres  # candidates

    # Settings
    min_wh, max_wh = 2, 4096  # (pixels) minimum and maximum box width and height
    max_det = 300  # maximum number of detections per image
    time_limit = 10.0  # seconds to quit after
    redundant = True  # require redundant detections
    multi_label = False  # multiple labels per box (adds 0.5ms/img)  原本代码这里 = nc > 1,改为false，即一个box只有一个分类
    merge = False

    t = time.time()
    output = [None] * prediction.shape[0]
    for xi, x in enumerate(prediction):  # image index, image inference
        # Apply constraints
        # x[((x[..., 2:4] < min_wh) | (x[..., 2:4] > max_wh)).any(1), 4] = 0  # width-height
        x = x[xc[xi]]  # confidence

        # If none remain process next image
        if not x.shape[0]:
            continue

        # Compute conf
        x[:, 5:] *= x[:, 4:5]  # conf = obj_conf * cls_conf

        # Box (center x, center y, width, height) to (x1, y1, x2, y2)
        box = xywh2xyxy(x[:, :4])

        # Detections matrix nx6 (xyxy, conf, cls)
        if multi_label:
            i, j = (x[:, 5:] > conf_thres).nonzero().t()
            x = torch.cat((box[i], x[i, j + 5, None], j[:, None].float()), 1)
        else:  # best class only
            conf, j = x[:, 5:].max(1, keepdim=True)
            x = torch.cat((box, conf, j.float()), 1)[conf.view(-1) > conf_thres]

        # Filter by class
        if classes:
            x = x[(x[:, 5:6] == torch.tensor(classes, device=x.device)).any(1)]

        # Apply finite constraint
        # if not torch.isfinite(x).all():
        #     x = x[torch.isfinite(x).all(1)]

        # If none remain process next image
        n = x.shape[0]  # number of boxes
        if not n:
            continue

        # Sort by confidence
        # x = x[x[:, 4].argsort(descending=True)]

        # Batched NMS
        c = x[:, 5:6] * (0 if agnostic else max_wh)  # classes
        boxes, scores = x[:, :4] + c, x[:, 4]  # boxes (offset by class), scores
        i = torchvision.ops.boxes.nms(boxes, scores, iou_thres)
        if i.shape[0] > max_det:  # limit detections
            i = i[:max_det]
        if merge and (1 < n < 3E3):  # Merge NMS (boxes merged using weighted mean)
            try:  # update boxes as boxes(i,4) = weights(i,n) * boxes(n,4)
                iou = box_iou(boxes[i], boxes) > iou_thres  # iou matrix
                weights = iou * scores[None]  # box weights
                x[i, :4] = torch.mm(weights, x[:, :4]).float() / weights.sum(1, keepdim=True)  # merged boxes
                if redundant:
                    i = i[iou.sum(1) > 1]  # require redundancy
            except:  # possible CUDA error https://github.com/ultralytics/yolov3/issues/1139
                print(x, i, x.shape, i.shape)
                pass

        output[xi] = x[i]
        if (time.time() - t) > time_limit:
            break  # time limit exceeded

    return output


def xywh2xyxy(x):
    # Convert nx4 boxes from [x, y, w, h] to [x1, y1, x2, y2] where xy1=top-left, xy2=bottom-right
    y = torch.zeros_like(x) if isinstance(x, torch.Tensor) else np.zeros_like(x)
    y[:, 0] = x[:, 0] - x[:, 2] / 2  # top left x
    y[:, 1] = x[:, 1] - x[:, 3] / 2  # top left y
    y[:, 2] = x[:, 0] + x[:, 2] / 2  # bottom right x
    y[:, 3] = x[:, 1] + x[:, 3] / 2  # bottom right y
    return y


def box_iou(box1, box2):
    # https://github.com/pytorch/vision/blob/master/torchvision/ops/boxes.py
    """
    Return intersection-over-union (Jaccard index) of boxes.
    Both sets of boxes are expected to be in (x1, y1, x2, y2) format.
    Arguments:
        box1 (Tensor[N, 4])
        box2 (Tensor[M, 4])
    Returns:
        iou (Tensor[N, M]): the NxM matrix containing the pairwise
            IoU values for every element in boxes1 and boxes2
    """

    def box_area(box):
        # box = 4xn
        return (box[2] - box[0]) * (box[3] - box[1])

    area1 = box_area(box1.t())
    area2 = box_area(box2.t())

    # inter(N,M) = (rb(N,M,2) - lt(N,M,2)).clamp(0).prod(2)
    inter = (torch.min(box1[:, None, 2:], box2[:, 2:]) - torch.max(box1[:, None, :2], box2[:, :2])).clamp(0).prod(2)
    return inter / (area1[:, None] + area2 - inter)  # iou = inter / (area1 + area2 - inter)


def scale_coords(img1_shape, coords, img0_shape, ratio_pad=None):
    # Rescale coords (xyxy) from img1_shape to img0_shape
    if ratio_pad is None:  # calculate from img0_shape
        gain = max(img1_shape) / max(img0_shape)  # gain  = old / new
        pad = (img1_shape[1] - img0_shape[1] * gain) / 2, (img1_shape[0] - img0_shape[0] * gain) / 2  # wh padding
    else:
        gain = ratio_pad[0][0]
        pad = ratio_pad[1]

    coords[:, [0, 2]] -= pad[0]  # x padding
    coords[:, [1, 3]] -= pad[1]  # y padding
    coords[:, :4] /= gain
    clip_coords(coords, img0_shape)
    return coords


def clip_coords(boxes, img_shape):
    # Clip bounding xyxy bounding boxes to image shape (height, width)
    boxes[:, 0].clamp_(0, img_shape[1])  # x1
    boxes[:, 1].clamp_(0, img_shape[0])  # y1
    boxes[:, 2].clamp_(0, img_shape[1])  # x2
    boxes[:, 3].clamp_(0, img_shape[0])  # y2


def select_device(device='', apex=False, batch_size=None):
    # device = 'cpu' or '0' or '0,1,2,3'
    cpu_request = device.lower() == 'cpu'
    if device and not cpu_request:  # if device requested other than 'cpu'
        os.environ['CUDA_VISIBLE_DEVICES'] = device  # set environment variable
        assert torch.cuda.is_available(), 'CUDA unavailable, invalid device %s requested' % device  # check availablity

    cuda = False if cpu_request else torch.cuda.is_available()
    if cuda:
        c = 1024 ** 2  # bytes to MB
        ng = torch.cuda.device_count()
        if ng > 1 and batch_size:  # check that batch_size is compatible with device_count
            assert batch_size % ng == 0, 'batch-size %g not multiple of GPU count %g' % (batch_size, ng)
        x = [torch.cuda.get_device_properties(i) for i in range(ng)]
        s = 'Using CUDA ' + ('Apex ' if apex else '')  # apex for mixed precision https://github.com/NVIDIA/apex
        for i in range(0, ng):
            if i == 1:
                s = ' ' * len(s)
            print("%sdevice%g _CudaDeviceProperties(name='%s', total_memory=%dMB)" %
                  (s, i, x[i].name, x[i].total_memory / c))
    else:
        print('Using CPU')

    print('')  # skip a line
    return torch.device('cuda:0' if cuda else 'cpu')


def time_synchronized():
    torch.cuda.synchronize() if torch.cuda.is_available() else None
    return time.time()


def plot_one_box(x, img, color=None, label=None, line_thickness=None):
    # Plots one bounding box on image img
    tl = line_thickness or round(0.002 * (img.shape[0] + img.shape[1]) / 2) + 1  # line/font thickness
    color = color or [random.randint(0, 255) for _ in range(3)]
    c1, c2 = (int(x[0]), int(x[1])), (int(x[2]), int(x[3]))
    cv2.rectangle(img, c1, c2, color, thickness=tl, lineType=cv2.LINE_AA)
    if label:
        tf = max(tl - 1, 1)  # font thickness
        t_size = cv2.getTextSize(label, 0, fontScale=tl / 3, thickness=tf)[0]
        c2 = c1[0] + t_size[0], c1[1] - t_size[1] - 3
        cv2.rectangle(img, c1, c2, color, -1, cv2.LINE_AA)  # filled
        cv2.putText(img, label, (c1[0], c1[1] - 2), 0, tl / 3, [225, 255, 255], thickness=tf, lineType=cv2.LINE_AA)
        
        
def correct_single_row_lined_table(pred, img, conf_thres, cls_index, verbose=False):
    """修正单行有线表格的置信度为阈值置信度，为了解决单行有线表格的置信度过低的问题
    判断思路：
    1.单行：从单行的宽高进行约束，并且需要满足一定的h,和w的要求，例如太过小或者太过
    大的区域。
    2.有线表格：从预选框内容中是否有线进行判断
    Args:
        pred(numpy array): (#box, 6), xyxy+conf+cls_index
        img(numpy array): 页面原图，bgr模式
        conf_thres(float): 阈值的置信度，工程中低于此值的有线表格会被略过
        cls_index(int): 有线表格的分类序号
    return:
        new_pred(numpy array):
    """
    def is_lined_table(table_img):
        # 暂时不支持深色表格，仅仅做简单二值化
        gray_img = cv2.cvtColor(table_img, cv2.COLOR_BGR2GRAY)
        binary_thres = 200
        _, binary_img = cv2.threshold(gray_img, binary_thres, 1, cv2.THRESH_BINARY)
        binary_img = np.logical_not(binary_img).astype(np.uint8)
        # open
        h = table_img.shape[0]
        v_kernel = np.ones((h, 1))
        img_eroded = cv2.erode(binary_img, v_kernel, iterations=1)
        img_open = cv2.dilate(img_eroded, v_kernel, iterations=1)
        # project to x axis
        vertical_proj = (img_open == 1).any(axis=1)
        if np.sum(vertical_proj) >= 0.9 * h:
            return True
        else:
            return False
    
    # config
    hmin, hmax = 15, 35  # 有线表格高度范围
    wmin = 500  # 有线表格宽度范围
    
    new_pred = pred.copy()
    
    
    for i, (x0, y0, x1, y1, conf, cls_i) in enumerate(pred):
        h = y1 - y0 + 1
        w = x1 - x0 + 1
        cls_i = int(cls_i)
        
        # print('w:', w,'h', h, 'cls', cls_index, 'conf', conf)
        if cls_i != cls_index:  # 非有线表格
            continue
        elif cls_i == cls_index and conf >= conf_thres:  # 有线表格但是置信度
            continue
        elif not (hmin <= h <= hmax) or w < wmin:  # 有线表格但不满足形状
            continue
        elif not is_lined_table(img[int(y0): int(y1)+1, int(x0): int(x1)+1, :]):  # 不满足“有线”判断
            continue
        else:
            new_pred[i, 4] = conf_thres  # 修正置信度
            if verbose:
                print('单行有线表格修正')
    # print('debug1', pred)
    # print('debug2', new_pred)
            
    return new_pred
                

def filter_pred(pred, thres):
    """过滤置信度不满足的预选框
    Args:
        pred(numpy array): (#box, 6), xyxy+conf+cls_index
        thres(list of float): 对应每个类别的置信度阈值
    return:
        pred_filtered(numpy array): (#box, 6), xyxy+conf+cls_index
    """
    # print('debug3', pred)
    thres = np.array(thres)
    pred_thres = thres[pred[:, 5].astype(np.int32)]  # (#box, )
    pred_filtered = pred[pred[:, 4] >= pred_thres]  
    # print('debug4', pred_filtered)

    return pred_filtered


def process_header(preds, pics, header_cls, verbose=False):
    """对一个pdf文档的页眉进行后处理，包括，补漏+除重
        补漏：部分页面中的页眉可能会有漏判，实现逻辑是对页眉缺省页面，向前、后找最近
              两页带有页眉的页面，然后对比页眉区域的像素，如果相等，那么认为此处应该
              补上页眉（为什么设置两页是为了应对）。
        除重：一页中页眉可能同时被识别为单句段落，判断逻辑是除去满足和header的bounding 
              box一定重叠率的其他结构块。
    Args:
        preds(list): [pred_0, pred_1, ...],其中pred_i是pageIndex=i页面的预测结果，
                     pred为numpy array，维度为(#box, 6), xyxy+conf+cls_index
        pics(list): [pic_0, pic_1, ...], 每一面的图片，bgr形式
        header_cls(int): header的分类序号
    return:
        preds_processed(list):经过页眉后处理的预测
    """
    def keep_only_one_header_in_predict(pred):
        # 对一面里面出现多个页眉情况，只保留置信度最高的页眉
        header_args = pred[:, 5]==header_cls
        multi_header_pred = pred[header_args]  # (#header, 6)
        if len(multi_header_pred) == 0:
            return pred
        arg_max_conf = np.argmax(multi_header_pred[:, 4])
        header_pred = multi_header_pred[arg_max_conf].reshape(1, 6)  # 只保留最大置信度header
        not_header_pred = pred[np.logical_not(header_args)]
        new_pred = np.concatenate((header_pred, not_header_pred), axis=0)  # header被放在第一个位置
        return new_pred
    
    def get_rid_of_single_paragraph_overlapped(pred):
        # 去除页面内在header内的其他结构块
        if len(pred) <= 1:  # 页面内只有一个或者以下的结构块
            return pred
        if pred[0][5] != header_cls:
            return pred
        overlapped = box_IntersectionOverOneBox(pred[[0], :4], pred[1:, :4]).reshape(-1)  # (#pred-1,)
        return np.concatenate((pred[[0]], pred[1:][overlapped < overlap_thres]), axis=0)
    
    def generate_search_page_args(page_arg):
        # 搜寻规则：先找前面最近的有页眉的页面，再找后面最近的有页眉的页面，再找前面
        # 第二近的有页眉的页面，再找后面第二近的有页眉的页面
        before_page_args = [page_arg - i - 1 for i in range(search_range)]
        after_page_args = [page_arg + i for i in range(search_range)]
        candidate_args = sum(zip(before_page_args, after_page_args), ())  # interleave
        return candidate_args
    
    # config
    search_range = 2  # 前后搜索的有页眉的页面数量
    overlap_thres = 0.5  # 其他结构块与页眉重合度超过此值，去除
    
    preds = [keep_only_one_header_in_predict(pred) for pred in preds]  
    header_pages = []  # 记录有页眉的页面编号(sorted), 例如[1,2,4,5]
    no_header_pages = []  # 记录无页眉的页面编号(sorted)，例如[0,3]
    for i, pred in enumerate(preds):
        if len(pred) == 0:
            no_header_pages.append(i)  # 如果该页什么都没有
        elif pred[0][5] == header_cls:  # 如果有一个header,header被放在第一个位置
            header_pages.append(i)
        else:
            no_header_pages.append(i)
    
    # 补漏
    n_add = 0
    page_reference_result = []  # 记录页眉补齐页面编号，以及其参考的页面编号
    for pageIndex in no_header_pages:
        page_arg = bisect(header_pages, pageIndex)
        candidate_args = generate_search_page_args(page_arg)
        for candidate_arg in candidate_args:
            if candidate_arg < 0 or candidate_arg > len(header_pages) -1:
                continue
            else:
                pageIndex_candidate = header_pages[candidate_arg]
                # 首先对比图片的尺寸，不满足直接pass
                if pics[pageIndex].shape != pics[pageIndex_candidate].shape:
                    continue
                
                # 对比header区域的图片是否相等
                x0, y0, x1, y1 = preds[pageIndex_candidate][0][:4].astype(np.int32)
                header_area = pics[pageIndex][y0:y1+1, x0:x1+1]
                header_area_candidate = pics[pageIndex_candidate][y0:y1+1, x0:x1+1]
                if not np.all(header_area==header_area_candidate):
                    continue
                else:
                    page_reference_result.append((pageIndex, pageIndex_candidate))
                    preds[pageIndex] = np.concatenate((preds[pageIndex_candidate][[0]],
                                                       preds[pageIndex]),
                                                      axis=0)
                    header_pages.insert(page_arg, pageIndex)  # 更新该页为有页眉的页面
                    n_add += 1
                    break
    if verbose:
        print('页眉规则补齐数目：%d' % n_add)
        for pageIndex, pageIndex_candidate in page_reference_result:
            print('%d --> %d' %(pageIndex, pageIndex_candidate))
    
    # 去重
    preds = [get_rid_of_single_paragraph_overlapped(pred) for pred in preds]
        
    return preds
    

def page2image(page:fitz.fitz.Page, scale_ratio:float=1.33333333, 
               rotate:'int'=0) -> np.ndarray:
    trans = fitz.Matrix(scale_ratio, scale_ratio).preRotate(rotate)  # rotate means clockwise 
    pm = page.getPixmap(matrix=trans, alpha=False)
    data = pm.getImageData()  # bytes
    # img = read_image_in_bytes(data)
    img_np = np.frombuffer(data, dtype=np.uint8)
    img = cv2.imdecode(img_np, flags=1)
    return img


def check_imgs(imgs: list) -> list:
    # 为了防止有些pdf会产生极小的页面，故对pdf转化的imgs进行检查，并对极小页面进行修正
    new_imgs = []
    for img in imgs:
        h, w = img.shape[:2]
        if h < 100:  # 对不满足做菁龙的预处理变换进行pad
            padding_part = np.full((100-h, w, 3), 255, dtype=np.uint8)  # 白色背景
            img = np.concatenate((img, padding_part), axis=0)
        new_imgs.append(img)
    return new_imgs
        

def get_char_bboxes(page:fitz.fitz.Page, scale_ratio:float=1.33333333, 
                    shrink:bool=True) -> tuple:
    # use Page.getText('rawdict') to get chars bboxes
    text = []
    bboxes = []
    content = page.getText('rawdict')  # content(dict): 'width', 'height', 'blocks'
    for block in content['blocks']:  # block(dict): 'number', 'type', 'bbox', 'lines'
        if block['type'] != 0:  # not a text block
            continue
        for line in block['lines']:  # line(dict): 'spans', 'wmode', 'dir', 'bbox'
            for span in line['spans']:  # span(dict): 'size', 'flags', 'font', 'color', 
                                        # 'ascender', 'descender', 'chars', 'origin', 'bbox'
                ascender = span['ascender']
                descender = span['descender']
                size = span['size']
                for char in span['chars']:  # char(dict): 'origin', 'bbox', 'c'
                    text.append(char['c'])
                    if not shrink:
                        bboxes.append(char['bbox'])        
                    else:
                        x0, y0, x1, y1 = char['bbox']
                        y_origin = char['origin'][1]
                        y0, y1 = shrink_bbox(ascender, descender, size, y0, y1, y_origin)
                        bboxes.append((x0, y0, x1, y1))
                    
    bboxes = np.array(bboxes).reshape(-1, 4) *  scale_ratio # in case bboxes is empty
    
    # adjust coordinate according to Page.rotation
    bboxes = change_bbox_coordinate_by_rotation(bboxes, page)
    return bboxes, text


def change_bbox_coordinate_by_rotation(bboxes:np.ndarray, page:fitz.fitz.Page, 
                                       scale_ratio:float=1.33333333) -> np.ndarray:
    # cause getText return the coordinate before rotation, we need to ajust to
    # the coordinate after rotation
    if page.rotation == 0:
        return bboxes
    else:
        img = page2image(page, scale_ratio)
        h, w = img.shape[: 2]
        if page.rotation == 90:
            # y = x', x = w - y'
            x0 = w - bboxes[:, 3]
            y0 = bboxes[:, 0]
            x1 = w - bboxes[:, 1]
            y1 = bboxes[:, 2]
            bboxes = np.stack((x0, y0, x1, y1), axis=1)
            return bboxes
        elif page.rotation == 180:
            # y = h - y', x = w - x'
            x0 = w - bboxes[:, 3]
            y0 = h - bboxes[:, 2]
            x1 = w - bboxes[:, 1]
            y1 = h - bboxes[:, 0]
            bboxes = np.stack((x0, y0, x1, y1), axis=1)
            return bboxes
        elif page.rotation == 270:
            # y = h - x', x = y'
            x0 = bboxes[:, 1]
            y0 = h - bboxes[:, 2]
            x1 = bboxes[:, 3]
            y1 = h - bboxes[:, 0]
            bboxes = np.stack((x0, y0, x1, y1), axis=1)
            return bboxes
        else:
            raise ValueError('page.rotation should be 0, 90, 180, 270, not %g'
                             % page.rotation)
            

def shrink_bbox(ascender:float, descender:float, size:float, y0:float, y1:float,
               y_origin:float) -> tuple:
    # shink bbox to the reduced glyph heights
    # details on https://pymupdf.readthedocs.io/en/latest/textpage.html#dictionary-structure-of-extractdict-and-extractrawdict
    if size >= y1 - y0:  # don't need to shrink
        return y0, y1
    else:
        new_y1 = y_origin - size * descender / (ascender - descender)
        new_y0 = new_y1 - size
        return new_y0, new_y1          
            

def get_line_bboxes(page:fitz.fitz.Page, scale_ratio:float=1.33333333, ) -> tuple:
    # use Page.getText('rawdict') to get line bboxes
    bboxes = []
    content = page.getText('dict')  # content(dict): 'width', 'height', 'blocks'
    for block in content['blocks']:  # block(dict): 'number', 'type', 'bbox', 'lines'
        if block['type'] != 0:  # not a text block
            continue
        for line in block['lines']:  # line(dict): 'spans', 'wmode', 'dir', 'bbox', 'text'
            line_text = []
            for span in line['spans']:
                line_text.append(span['text'])
            if len(''.join(line_text).split()):  # line has actual content
                bboxes.append(line['bbox'])
                    
    bboxes = np.array(bboxes).reshape(-1, 4) *  scale_ratio # in case bboxes is empty
    
    # adjust coordinate according to Page.rotation
    bboxes = change_bbox_coordinate_by_rotation(bboxes, page)
    return bboxes


def add_missing_single_para(pred, line, cls_index=0, conf=0.4, verbose=True):
    """解决单句段落缺失的问题，逻辑是如果mupdf给出的lines的bbox中没有和预测的bbox重
    叠，则认为是模型漏判的单句段落，并补上
    Args:
        pred(array):某页面的结构块预测结果，格式为xyxy+conf+cls_index
        line(array):用pdf信息提取的工具提取的“行”的坐标，格式为xyxy
        cls_index(int):单句段落的分类序号
        conf(float):补充的单句段落的置信度默认值
        verbose(bool):是否打印处理结果
    return:
        pred_added(array):补齐单句段落处理后的结构块
    """
    if len(line) == 0:
        return pred
    elif len(pred) == 0:
        extra_added = np.tile([conf, cls_index], (len(line), 1))  # (len(line), 2)
        line_added = np.concatenate((line, extra_added), axis=1)  # (len(line), 6)
        if verbose:
            print(print("补充单句段落：", len(line_added)))
        return line_added     
    else:
        # config
        iou_thre = 0.3
        
        pred_box = pred[:, :4]  # xyxy
        iou = box_IntersectionOverOneBox(pred_box, line)  # (len(pred), len(line))
        iou_b = np.all(iou < iou_thre, axis=0)  # (len(line),)
        line_added = line[iou_b]  # (len(line_added), 4)
        extra_added = np.tile([conf, cls_index], (len(line_added), 1))  # (len(line_added), 2)
        line_added = np.concatenate((line_added, extra_added), axis=1)  # (len(line_added), 6)
        if verbose:
            print("补充单句段落：", len(line_added))
        pred_added = np.concatenate((pred, line_added), axis=0)  
        
        return pred_added
            


if __name__ == "__main__":
    pass