# -*- coding: utf-8 -*-
"""
Created on Wed Mar 17 10:06:34 2021

@author: EDZ
"""

# 读取pdf文件，并生成pdf文件的

from layout_analysis_v2 import Layout_Processor
from tqdm import tqdm 
from pathlib import Path
import cv2
from utils.deployment import plot_one_box
import fitz
import numpy as np
import json


# 设置
device = '1'
weight = 'weights/last.pt'
img_size = 960
batch_size = 16
conf_thres=0.001
iou_thres=0.01


# 初始化
lp = Layout_Processor(weight=weight,
                      device=device,
                      img_size=img_size, 
                      batch_size=batch_size,
                      verbose=True,
                      )


def page2image(page:fitz.fitz.Page, scale_ratio:float=1.33333333, 
               rotate:'int'=0) -> np.ndarray:
    trans = fitz.Matrix(scale_ratio, scale_ratio).preRotate(rotate)  # rotate means clockwise 
    pm = page.getPixmap(matrix=trans, alpha=False)
    data = pm.getImageData()  # bytes
    # img = read_image_in_bytes(data)
    img_np = np.frombuffer(data, dtype=np.uint8)
    img = cv2.imdecode(img_np, flags=1)
    return img


def cv_imwrite(path:str, img:np.ndarray) -> bool:
    # support chinese path
    return cv2.imencode('.png', img)[1].tofile(path)


def detect_pdf(pdf_path:str, save_vis=True) -> None:
    # load page to image
    doc = fitz.open(pdf_path)
    print("converting pdf to images: %s" % pdf_path)
    sample_img_list = [page2image(page) for page in tqdm(doc)]
    
    # predict
    p_list_raw, p_list = lp(sample_img_list, conf_thres, iou_thres)
    
    # directory
    save_dir = Path(pdf_path).parent / Path(pdf_path).stem
    if not save_dir.is_dir():
        save_dir.mkdir(parents=True, exist_ok=True)
    
    # Get names and colors
    names = lp.names 
    colors = lp.colors

    # save visualization
    if save_vis:
        save_dir_raw = save_dir / 'raw'
        save_dir_post = save_dir / 'post'
        if not save_dir_raw.is_dir():
            save_dir_raw.mkdir(parents=True, exist_ok=True)
        if not save_dir_post.is_dir():
            save_dir_post.mkdir(parents=True, exist_ok=True)
            
        for i in tqdm(range(len(sample_img_list))):
            img_raw = sample_img_list[i]
            img_post = img_raw.copy()
            
            pred_raw = p_list_raw[i]
            pred_post = p_list[i]
            
            # raw predict
            for *xyxy, conf, cls in pred_raw:
                label = '%s %.4f' % (names[int(cls)], conf)
                
                plot_one_box(xyxy, img_raw, label=label, color=colors[int(cls)])
                
            cv_imwrite(str(save_dir_raw / ('%g.png' % i)), img_raw)
            
            # post predict
            for *xyxy, conf, cls in pred_post:
                label = '%s %.4f' % (names[int(cls)], conf)
                plot_one_box(xyxy, img_post, label=label, color=colors[int(cls)])
        
            cv_imwrite(str(save_dir_post / ('%g.png' % i)), img_post)
        
    # save result as json
    with open(str(save_dir / (Path(pdf_path).stem + '.json')), 'w', encoding='utf-8') as f:
        json.dump([p.tolist() for p in p_list], f)
    
    print('done')
    
    
if __name__ == "__main__":
    pdf_dir = 'pdf_sample'
    pdf_paths = list(Path(pdf_dir).glob("*.pdf")) + list(Path(pdf_dir).glob("*.PDF"))
    for pdf_path in pdf_paths:
        detect_pdf(str(pdf_path))
    
    