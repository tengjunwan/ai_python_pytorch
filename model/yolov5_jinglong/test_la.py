# -*- coding: utf-8 -*-
"""
Created on Thu Dec 10 12:56:48 2020

@author: EDZ
"""

# 测试layout_analysis

from layout_analysis_v2 import Layout_Processor
from tqdm import tqdm 
from pathlib import Path
import cv2
from utils.deployment import plot_one_box


# 设置
device = '0'
weight = 'weights/best.pt'
img_size = 960
batch_size = 16
conf_thres=0.001
iou_thres=0.01

# 初始化
lp = Layout_Processor(weight=weight,
                      device=device,
                      img_size=img_size, 
                      batch_size=batch_size,
                      verbose=True,
                      )
                      

# 载入测试图片
# sample_dir = 'img_for_deployment_test'
sample_dir = 'small_input'
sample_paths = list(Path(sample_dir).glob('*.png')) + list(Path(sample_dir).glob('*.jpg')) 
sample_img_list = [cv2.imread(str(i)) for i in sample_paths]
assert sample_img_list[0] is not None, 'shit'

# 预测
p_list_raw, p_list = lp(sample_img_list, conf_thres, iou_thres)

# 设置可视化输出目录
save_dir = 'output_for_deployment_test'
save_dir = 'small_output'
save_dir_raw = save_dir + '/raw'
save_dir_post = save_dir + '/post'

save_paths_raw = [Path(save_dir_raw) / (i.name) for i in sample_paths]
save_paths_post = [Path(save_dir_post) / (i.name) for i in sample_paths]
# print(save_paths_post[0])

# Get names and colors
names = lp.names 
colors = lp.colors

# 可视化
for i in tqdm(range(len(sample_img_list))):
    img_raw = sample_img_list[i]
    img_post = img_raw.copy()
    
    pred_raw = p_list_raw[i]
    pred_post = p_list[i]
    
    # 模型处理可视化
    for *xyxy, conf, cls in pred_raw:
        label = '%s %.4f' % (names[int(cls)], conf)
        
        plot_one_box(xyxy, img_raw, label=label, color=colors[int(cls)])
        
    cv2.imwrite(str(save_paths_raw[i]), img_raw)
    
    # 模型处理+后处理可视化
    for *xyxy, conf, cls in pred_post:
        label = '%s %.4f' % (names[int(cls)], conf)
        plot_one_box(xyxy, img_post, label=label, color=colors[int(cls)])

    cv2.imwrite(str(save_paths_post[i]), img_post)

print('done')