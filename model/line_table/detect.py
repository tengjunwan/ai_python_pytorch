# -*- coding: utf-8 -*-
"""
Created on Thu Sep 16 09:48:46 2021

@author: EDZ
"""

import shutil
from pathlib import Path

import torch
from tqdm import tqdm
import numpy as np

from utils.datasets import LoadImages
from utils.torch_utils import select_device
from utils.vis_utils import cv_imwrite


pic_dir = 'samples'
device = '1'
device = select_device(device)
out = 'samples/result'
weight = 'weights/best_stateDict.pt'


def detect():
    # Initialize
    if Path(out).is_dir():
        shutil.rmtree(out)  # delete output folder
    Path(out).mkdir(parents=True, exist_ok=True)  # make new output folder
    half = device.type != 'cpu'  # half precision only supported on CUDA

    # Load model
    try:  
        model = torch.load(weight, map_location=device)['model'].float()  # load to FP32
    except KeyError:
        from unet import UNet
        model = UNet(n_channels=3, n_classes=1, bilinear=True)
        model.load_state_dict(torch.load(weight), strict=False)
        model.float()
    model.to(device).eval()
    if half:
        model.half()  # to FP16

    # set dataset
    dataset = LoadImages(pic_dir, scale=1)

    # Run inference
    for img_path, img, img_tensor, shape in tqdm(dataset):
        img_tensor = img_tensor.to(device)
        img_tensor = img_tensor.half() if half else img_tensor.float()  # uint8 to fp16/32
        if img_tensor.ndimension() == 3:
            img_tensor = img_tensor.unsqueeze(0)

        # Inference
        try:
            pred = model(img_tensor)  # (1, 1, h, w)
        except RuntimeError:
            print('wrong:', img_path)
            continue
        
        # to image
        pred = torch.sigmoid(pred)  # digit to probability
        pred = pred.squeeze()  # (h, w)
        pred = pred.cpu().numpy()
        pred = pred > 0.5
        pred = (pred * 255).astype(np.uint8)
        
        # save
        pred = np.expand_dims(pred, axis=2)  # (h, w, 1)
        pred = np.repeat(pred, 3, axis=2)
        save_img = np.concatenate([img, pred], axis=1)
        save_path = Path(out) / Path(img_path).name
        cv_imwrite(str(save_path), save_img)
    
    print('Done.')


if __name__ == '__main__':
    with torch.no_grad():
        detect()
    