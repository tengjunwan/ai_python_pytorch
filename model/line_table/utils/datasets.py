# -*- coding: utf-8 -*-
"""
Created on Fri Dec 18 19:07:26 2020

@author: tengjunwan
"""

from torch.utils.data import Dataset
from tqdm import tqdm
import torch 
from torchvision import transforms
import cv2
import numpy as np
from utils.utils import augment_hsv
import torch.nn.functional as F
from pathlib import Path
from utils.utils import cv_imread


class LoadImageAndLabel(Dataset):
    """dataset for training
    """
    def __init__(self, txt_path, pic_dir, mask_dir, hyp, cache_mask=False,
                 hsv_augment=False, scale=1.0, crop=False, 
                 normalize=False):
        """
        Args:
            txt_path(str):训练图片路径
            pic_dir(str):
            mask_dir(str):
            hyp(dict):超参
            cache_ocr(bool): 是否缓存图片ocr结果
            hsv_augment(bool): 是否进行色彩增强
            multi_scale(bool):是否缩放
            crop(bool): 是否裁剪  # todo
            normalize(bool):是否用统计值u=[0.485, 0.456, 0.406],σ=[0.229, 0.224, 0.225]
                进行normalize
        """
        # store
        self.pic_dir = Path(pic_dir)
        self.mask_dir = Path(mask_dir)
        self.hsv_augment = hsv_augment
        self.hyp = hyp
        self.scale = scale
        self.normalize = normalize
        self.cache_mask = cache_mask
        #
        with open(txt_path, encoding='utf-8') as f:
            self.img_names = f.read().splitlines()
            
        if cache_mask:
            pbar = tqdm(self.img_names)
            pbar.desc = 'caching masks'
            self.masks = []
            for img_name in pbar:
                mask0 = cv_imread(str(self.mask_dir / img_name))
                mask0 = cv2.cvtColor(mask0, cv2.COLOR_BGR2GRAY)  # bgr to gray
                _, mask0 = cv2.threshold(mask0, 127, 1, cv2.THRESH_BINARY)  # to 0, 1
                self.masks.append(mask0)
        
                
    def __len__(self):
        return len(self.img_names)
    
    def __getitem__(self, idx):
        """
        return:
            img_name(str): 图片名称
            img_tensor(tensor): 图片tensor
            rows(array): 行方向的label, [[start, end], [start, end],...]
            columns(array): 列方向的label, [[start, end], [start, end],...]
            shapes(tuple): ((h0, w0), (h, w)), h0,w0是原始尺寸，h,w是改变后的尺寸。 
        """
        img_name = self.img_names[idx]
        # 载入图片
        img0 = cv_imread(str(self.pic_dir / img_name))  # bgr
        h0, w0 = img0.shape[: 2]
        
        # 载入mask
        if not self.cache_mask:
            mask0 = cv_imread(str(self.mask_dir / img_name))
            mask0 = cv2.cvtColor(mask0, cv2.COLOR_BGR2GRAY)  # bgr to gray
            _, mask0 = cv2.threshold(mask0, 127, 1, cv2.THRESH_BINARY)  # to 0, 1
        else:
            mask0 = self.masks[idx]
        assert img0.shape[:2] == mask0.shape[:2], 'something wrong'
        
        # 色彩增强
        if self.hsv_augment:
            augment_hsv(img0, 
                        hgain=self.hyp['hsv_h'],
                        sgain=self.hyp['hsv_s'], 
                        vgain=self.hyp['hsv_v'])

        # 放缩
        
        
        
        
        if self.scale != 1:  # 
            # 放缩图片
            w, h =  int(w0 * self.scale), int(h0 * self.scale)
            interp = cv2.INTER_AREA if self.scale < 1 else cv2.INTER_LINEAR  # https://stackoverflow.com/questions/23853632/which-kind-of-interpolation-best-for-resizing-image
            img = cv2.resize(img0, (w, h), interpolation=interp)
            # 放缩label
            mask = cv2.resize(mask0, (w, h), interpolation=interp)
        else:
            img = img0
            mask = mask0
            h, w = h0, w0
        
        # to tensor
        img_tensor = transforms.ToTensor()(img)  # (3, h, w)
        mask_tensor = torch.from_numpy(mask)  # (h, w)
        mask_tensor = mask_tensor.unsqueeze(0)  # (1, h, w)
        
        # normalize
        if self.normalize:
            # 注意是bgr模式下,和网上提供的rgb参数要稍微修改下
            img_tensor = transforms.Normalize([0.456, 0.485, 0.406],
                                              [0.224, 0.229, 0.225])(img_tensor)
        
        # 原图和实际输入图片的尺寸信息
        shape = ((h0, w0), (h, w))  
        
        return img_name, img_tensor, mask_tensor, shape
        
    @staticmethod
    def collate_fn(batch):
        """
        return:
            imgs(torch.Tensor): (batch, 3, h, w).
            masks(torch.Tensor): (batch, h, w).
            img_names(tuple): 长度为batch，保存对应的图片名称。
            shapes(tuple): (((h0, w0), (h, w)),...)，保存图片原始图片和变形后的尺寸。
            paddings(tuple): ((top, bottom, left, right),...)，保存图片padding信息。
        """
        pad_value = 0.3
        
        # 动态padding到该batch的最大size(max length and max width)
        img_names, img_tensors, mask_tensors, shapes = zip(*batch)

        # 找到最大尺寸
        sizes = np.array([s[1] for s in shapes])
        h_max, w_max = np.amax(sizes, axis=0)
        
        # padding
        img_tensors_padded = []
        mask_tensors_padded = []
        paddings = []
        for j, (img_tensor, mask_tensor) in enumerate(zip(img_tensors, mask_tensors)):
            # 图像padding
            dh = h_max - img_tensor.shape[1]
            dw = w_max - img_tensor.shape[2]
            dh /= 2  # divide padding into 2 sides
            dw /= 2  
            top, bottom = int(round(dh - 0.1)), int(round(dh + 0.1))
            left, right = int(round(dw - 0.1)), int(round(dw + 0.1))
            paddings.append((top, bottom, left, right))
            img_tensors_padded.append(F.pad(img_tensor, 
                                           (left, right, top, bottom), 
                                           "constant", 
                                           pad_value))
            mask_tensors_padded.append(F.pad(mask_tensor, 
                                            (left, right, top, bottom), 
                                            "constant",
                                            0))
            
        imgs = torch.stack(img_tensors_padded, 0)  # (b, 3, H, W)
        masks = torch.stack(mask_tensors_padded, 0)  # (b, 1, H, W)
             
        return imgs, masks, img_names, shapes, tuple(paddings)
    
    
class LoadImages(Dataset):  
    """dataset for detect
    """
    def __init__(self, image_dir, hsv_augment=False, scale=1.0, normalize=False):
        """
        Args:
            image_dir(str):图片所在文件夹
            hsv_augment(bool): 是否进行色彩增强
            multi_scale(bool):是否缩放
            normalize(bool):是否用统计值u=[0.485, 0.456, 0.406],σ=[0.229, 0.224, 0.225]
                进行normalize
        """
        image_format = ['.png', '.jpg', '.jepg', '.PNG', '.JPG', '.JEPG']
        self.image_dir = image_dir
        self.image_paths = [x for x in Path(image_dir).glob('*') if x.suffix in image_format]
        self.total_nums_of_pics = len(self.image_paths)
        self.hsv_augment = hsv_augment
        self.scale = scale
        self.normalize = normalize
                
    def __len__(self):
        return self.total_nums_of_pics
    
    def __getitem__(self, idx):
        """
        return:
            img_path(str): 图片名称
            img_tensor(tensor): 图片tensor
            img(numpy asrray): 图片(bgr)
            shapes(list of tuple): [(h0, w0), (h, w)]  
        """
        # Load image
        img_path = self.image_paths[idx]  # csv文件的一行数据
        img0 = cv_imread(str(img_path))  # bgr
        h0, w0 = img0.shape[: 2]
        
        # 色彩增强
        if self.hsv_augment:
            # config
            hsv_h = 0.0138  # image HSV-Hue augmentation (fraction)
            hsv_s = 0.678  # image HSV-Saturation augmentation (fraction)
            hsv_v = 0.36  # image HSV-Value augmentation (fraction)
            augment_hsv(img0, 
                        hgain=hsv_h,
                        sgain=hsv_s, 
                        vgain=hsv_v)

        # 多尺度
        if self.scale != 1:  # 3/4-4/3倍缩放(0.5倍概率)
            # 放缩图片
            w, h =  int(w0 * self.scale), int(h0 * self.scale)
            interp = cv2.INTER_AREA if self.scale < 1 else cv2.INTER_LINEAR  # https://stackoverflow.com/questions/23853632/which-kind-of-interpolation-best-for-resizing-image
            img = cv2.resize(img0, (w, h), interpolation=interp)
        else:
            img = img0
            h, w = h0, w0
            
        # to tensor
        img_tensor = transforms.ToTensor()(img)  # (3, h, w)
        
        # normalize
        if self.normalize:
            # 注意是bgr模式下,和网上提供的rgb参数要稍微修改下
            img_tensor = transforms.Normalize([0.456, 0.485, 0.406],
                                              [0.224, 0.229, 0.225])(img_tensor)
            
        img_tensor = img_tensor.unsqueeze(0)  # (1, 3, h, w)
        
        # 原图和实际输入图片的尺寸信息
        shape = [(h0, w0), (h, w)]  
        
        return str(img_path), img, img_tensor, shape
    
    
class LoadImage_V2(Dataset):  
    """dataset for detect
    """
    def __init__(self, imgs, hsv_augment=False, multi_scale=False, normalize=False):
        """
        Args:
            imgs(list):包含bgr格式的图片list
            hsv_augment(bool): 是否进行色彩增强
            multi_scale(bool):是否缩放
            normalize(bool):是否用统计值u=[0.485, 0.456, 0.406],σ=[0.229, 0.224, 0.225]
                进行normalize
        """
        self.imgs = imgs
        self.total_nums_of_pics = len(imgs)
        self.hsv_augment = hsv_augment
        self.multi_scale = multi_scale
        self.normalize = normalize
                
    def __len__(self):
        return self.total_nums_of_pics
    
    def __getitem__(self, idx):
        """
        return:
            img_path(str): 图片名称
            img_tensor(tensor): 图片tensor
            img(numpy asrray): 图片(bgr)
            shapes(list of tuple): [(h0, w0), (h, w)]  
        """
        # Load image
        img0 = self.imgs[idx]  # bgr
        h0, w0 = img0.shape[: 2]
        
        # 色彩增强
        if self.hsv_augment:
            # config
            hsv_h = 0.0138  # image HSV-Hue augmentation (fraction)
            hsv_s = 0.678  # image HSV-Saturation augmentation (fraction)
            hsv_v = 0.36  # image HSV-Value augmentation (fraction)
            augment_hsv(img0, 
                        hgain=hsv_h,
                        sgain=hsv_s, 
                        vgain=hsv_v)

        # 多尺度
        if self.multi_scale:  # 3/4-4/3倍缩放(0.5倍概率)
            min_ratio, max_ratio = 3/4, 4/3
            ratio = np.random.uniform(min_ratio, max_ratio)
            # 放缩图片
            w, h =  int(w0 * ratio), int(h0 * ratio)
            interp = cv2.INTER_AREA if ratio < 1 else cv2.INTER_LINEAR  # https://stackoverflow.com/questions/23853632/which-kind-of-interpolation-best-for-resizing-image
            img = cv2.resize(img0, (w, h), interpolation=interp)
        else:
            img = img0
            h, w = h0, w0
            
        # to tensor
        img_tensor = transforms.ToTensor()(img)  # (3, h, w)
        
        # normalize
        if self.normalize:
            # 注意是bgr模式下,和网上提供的rgb参数要稍微修改下
            img_tensor = transforms.Normalize([0.456, 0.485, 0.406],
                                              [0.224, 0.229, 0.225])(img_tensor)
        
        # 原图和实际输入图片的尺寸信息
        shape = [(h0, w0), (h, w)]  
        
        return img_tensor, shape
    
    
if __name__ == "__main__":
    from torch.utils.data import DataLoader
    from utils.utils import Denormalize
    
    
    # 超参设置
    hyp = {
       'lr0': 0.0075,  # initial learning rate (SGD=5E-3, Adam=5E-4)
       'lrf': 0.0001,  # final learning rate (with cos scheduler)
       'momentum': 0.937,  # SGD momentum
       'weight_decay': 0.0005, 
       'hsv_h': 0.0138,  # image HSV-Hue augmentation (fraction)
       'hsv_s': 0.678,  # image HSV-Saturation augmentation (fraction)
       'hsv_v': 0.36,  # image HSV-Value augmentation (fraction)
       'scale': (0.6, 1),  # scale ratio
       'warmup_epochs': 3,
       'warmup_bias_lr': 0.1,
       'warmup_momentum': 0.9,  
       'loss_weight': [1.0, 1.0]  # row and column
       }
    
    # 载入dataloader
    dataset = LoadImageAndLabel(txt_path='../data/train.txt',
                                pic_dir='../data/pic',
                                mask_dir='../data/label',
                                hyp=hyp,
                                hsv_augment=True,
                                scale=0.5, 
                                crop=False, 
                                normalize=True)
    
    dataloader = DataLoader(dataset, 
                            batch_size=3, 
                            shuffle=True, 
                            num_workers=0,
                            pin_memory=True,
                            collate_fn=dataset.collate_fn)
    
    # 载入denormalizer
    denorm = Denormalize(mean=[0.456, 0.485, 0.406], 
                         std=[0.224, 0.229, 0.225])
    
    for i, (imgs, masks, img_names, shapes, paddings) in enumerate(dataloader):
        for img, mask, name in zip(imgs, masks, img_names): 
            img = denorm(img).numpy().transpose(1, 2, 0)  # denormalize, (h, w, 3)
            img = (img * 255).astype(np.uint8)  # 0-1 to 0-255
            cv2.imshow(name, img)
            cv2.waitKey()
            mask = mask.numpy().astype(np.uint8).transpose(1, 2, 0)
            print(mask.shape)
            cv2.imshow(name, mask * 255)
            cv2.waitKey()
        
        
        break