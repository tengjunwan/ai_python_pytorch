# -*- coding: utf-8 -*-
"""
Created on Mon Dec 21 11:37:16 2020

@author: tengjunwan
"""

import cv2
import numpy as np
from torchvision.transforms.functional import normalize


def augment_hsv(img, hgain=0.5, sgain=0.5, vgain=0.5):
    r = np.random.uniform(-1, 1, 3) * [hgain, sgain, vgain] + 1  # random gains
    hue, sat, val = cv2.split(cv2.cvtColor(img, cv2.COLOR_BGR2HSV))
    dtype = img.dtype  # uint8

    x = np.arange(0, 256, dtype=np.int16)
    lut_hue = ((x * r[0]) % 180).astype(dtype)
    lut_sat = np.clip(x * r[1], 0, 255).astype(dtype)
    lut_val = np.clip(x * r[2], 0, 255).astype(dtype)

    img_hsv = cv2.merge((cv2.LUT(hue, lut_hue), cv2.LUT(sat, lut_sat), cv2.LUT(val, lut_val))).astype(dtype)
    cv2.cvtColor(img_hsv, cv2.COLOR_HSV2BGR, dst=img)  # no return needed
    
    
def range2point(size, ran):
    """转化label从range型式到point型式
    Arg:
        size(int):图片的长或者宽,例如6
        ran(2d array): 例如[[0,1], [3,4]]
    return:
        vector(1d array):例如[1, 1, 0, 1, 1, 0]
    """
    if len(ran) == 0:  
        return np.zeros(size)
    else:
        assert (np.amax(ran)) < size, 'something w'
        vector = np.zeros(size)
        if len(ran) != 0:  # 
            for r in ran.round():
                vector[int(r[0]): int(r[1])+1] = 1
    return vector
   
    
def point2range(vector):
    """转化label从point型式到ran型式
    Arg:
        vector(1d array):例如[1, 1, 0, 1, 1, 0]
    return:
        ran(2d array): 例如[[0,1], [3,4]], 
    """
    vector = np.insert(vector, 0, 0)
    vector = np.append(vector, 0)
    
    diff = np.diff(vector)
    start = np.argwhere(diff==1).flatten()           
    end = np.argwhere(diff==-1).flatten() - 1  
    
    ran = np.stack((start, end), axis=1)
         
    return ran


def cv_imread(path:str, channel_fixed_3=True) -> np.ndarray:
    # support chinese path
    img = cv2.imdecode(np.fromfile(path, dtype=np.uint8), -1)
    if not channel_fixed_3:
        return img
    else:
        if img.shape[2] == 3:
            return img
        elif img.shape[2] == 4:  # bgra
            img = cv2.cvtColor(img, cv2.COLOR_BGRA2BGR)
            return img
        elif img.shape[2] == 1:  # gray
            img = img = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
            return img
        else:
            raise ValueError("image has weird channel number: %d" % img.shape[2])


def denormalize(tensor, mean, std):
    mean = np.array(mean)
    std = np.array(std)

    _mean = -mean/std
    _std = 1/std
    return normalize(tensor, _mean, _std)


class Denormalize(object):
    def __init__(self, mean, std):
        mean = np.array(mean)
        std = np.array(std)
        self._mean = -mean/std
        self._std = 1/std

    def __call__(self, tensor):
        # tensor can be numpy array or tensor
        if isinstance(tensor, np.ndarray):
            return (tensor - self._mean.reshape(-1,1,1)) / self._std.reshape(-1,1,1)
        return normalize(tensor, self._mean, self._std)


def f1_score(gt_content, pred_content):
    """一种近似的评估方法，有机会可以提升
    两个list中，如果有两个str是一模一样，那么accurate count +1, 然后两个list分别剔
    除，然后重复上一步。
    Args:
        gt_content(list of str): 由gt separator经过后处理得到的格子内的文本内容
        pred_content(list of str): 由模型预测的separator经过后处理得到的格子的文本内容
    return:
        f1(float): F1值
    """
    gt_content = [x.strip() for x in gt_content]
    pred_content = [x.strip() for x in pred_content]
    
    gt_total = len(gt_content)
    pred_total = len(pred_content)
    if pred_total == 0:
        return 0
    count = 0
    for i in pred_content:
        try:
            idx = gt_content.index(i)
            del gt_content[idx]
            count += 1
        except ValueError:
            pass
    precision = count / gt_total + 1e-17
    recall = count /pred_total + 1e-17
    f1_score = 2 * (precision * recall) / (precision + recall + 1e-17)

    return f1_score


def f1_score_sep(r_gt_sep, c_gt_sep, r_pred_sep, c_pred_sep):
    """将pred_sep转化为pred_line,如果pred_line在gt_sep中，则match，并消耗掉一个
    gt_sep。
    Args:
        r_gt_sep(array): [[start, end],...]
        c_gt_sep(array): [[start, end],...]
        r_pred_sep(array): [[start, end],...]
        c_pred_sep(array): [[start, end],...]
    return:
        f1_score
    """
    def _f1_score_sep(gt_sep, pred_sep):
        # one side(row or column)
        pred_sep = np.array(pred_sep)
        gt_sep_copy = np.array(gt_sep)  # copy 
        pred_lines = (pred_sep[:, 0] + pred_sep[:, 1]) / 2.0
        tp = 0  # true positive
        for pred_line in pred_lines:
            b_s = gt_sep_copy[:, 0] <= pred_line  # (#sep,)
            b_e = gt_sep_copy[:, 1] >= pred_line  # (#sep,)
            b_in = b_s * b_e  # b_s ∩ b_e, (#sep,)
            if np.count_nonzero(b_in) == 0:  # line has no match
                pass
            elif np.count_nonzero(b_in) == 1:  # line has a match
                tp += 1
                gt_sep_copy = gt_sep_copy[np.logical_not(b_in)]  # eliminate matched gt sep
            else:  # line has more than 1 matches
                print("gt_sep", gt_sep)
                print("pred_sep", pred_sep)
                raise ValueError("something wrong with gt_sep:%s" % repr(gt_sep))
        return tp
    
    r_tp = _f1_score_sep(r_gt_sep, r_pred_sep)
    c_tp = _f1_score_sep(c_gt_sep, c_pred_sep)
            
    tp = r_tp + c_tp
    pred_count = len(r_pred_sep) + len(c_pred_sep)
    gt_count = len(r_gt_sep) + len(c_gt_sep)
    precision = tp / (pred_count + 1e-17)
    recall = tp / (gt_count + 1e-17)
    f1 = 2 * precision * recall / (precision + recall + 1e-17)
    
    return f1
            
            

def filter_bboxes_in_rect(bboxes:np.ndarray, rect:np.ndarray) -> np.ndarray:
    """filter bboxes(xyxy) if they are in a given rect area(xyxy)"""
    centers = (bboxes[:, [0, 1]] + bboxes[:, [2, 3]]) / 2.0  #  (#bboxes, xy)
    centers = np.concatenate((centers*(-1), centers), axis=1)  # (#bboxes, -x-yxy)
    rect = rect * np.array([-1, -1, 1, 1])  # (-x-yxy)
    bool_array = (rect >= centers).all(axis=1)  # (#bboxes, )
    bboxes_filtered = bboxes[bool_array]
    return bboxes_filtered, bool_array


def get_uinque_values_with_tolerace_and_order(values:np.ndarray, tolerance:float) -> tuple:
    """从一组values（一维向量）中得到该向量中的unique values with tolerance和这组向量
    对应的unique values with tolerance的排序关系
    例如[1,15,10,2,9,1,2], tolerance=1
    返回[1,9,15](unique values with tolerance) + [0,2,1,0,1,0,0](order)
    """
    # get unique values with tolerance
    args_sort = np.argsort(values)  # [0,5,3,6,4,2,1]
    diff = np.diff(values[args_sort])  # [0,1,0,7,1,5]
    arg = diff > tolerance  # [F,F,F,T,F,T]
    arg = np.append(True, arg)  # [T,F,F,F,T,F,T]
    unique_values = values[args_sort][arg]  # [1,9,15]
    # get order
    arg_acc = np.add.accumulate(arg) - 1  # [0,0,0,0,1,1,2]
    order = np.zeros_like(values, dtype=np.int64)
    order[args_sort] = arg_acc[np.arange(len(values))]  # [0,2,1,0,1,0,0]
    
    return unique_values, order


def filter_text_make_no_sense(bbox, text):
    # 过滤文字中无意义的部分，例如反斜杠，空格等
    text_filtered = []
    bbox_filtered = []
    for t, b in zip(text, bbox):
        if repr(t).startswith("'\\") and repr(t) != "'\\\\'":  # 过滤特殊字符反斜杠开头(但是反斜杠本身保留)
            continue
        elif t == ' ':  # 空格过滤
            continue
        else:
            text_filtered.append(t)
            bbox_filtered.append(b)
            
    return np.array(bbox_filtered), text_filtered





