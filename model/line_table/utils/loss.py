# -*- coding: utf-8 -*-
"""
Created on Mon Dec 21 18:02:32 2020

@author: tengjunwan
"""

import torch
import torch.nn as nn
import torch.nn.functional as F


def compute_loss(p, targets, hyp, mode='original'):
    """可采用三种方式计算loss:
        1.单纯的bce
        2.文章采用的clip的处理方式，即如果预测概率pred>0.9(gt=1)或者pred<0.1(gt=0)
         ,则loss为0
        3.采用label smoothing
    Args:
        p(list):imgs, (b, num_class, h, w)
        targets(list): (b, num_class, h, w)
        hyp(dict): 超参
        mode(str):控制计算loss的方式
    return:
        loss(tensor): 维度为0,模型的总loss,requires_grad=True
        loss_items(tensor): tensor([row_loss, col_loss, total_loss])
    """
    modes = ['original', 'clip', 'focal', 'dice', 'dice_bce', 'iou', 'tversky',
             'focal_tversky']
    assert mode in modes, "mode type is not considered."
    red = 'mean'  # 所有计算bce元素上取平均
    # 选择计算loss的方式
    if mode == 'clip':
        # todo
        pass
    elif mode == 'focal':
        criterion = FocalLoss(nn.BCEWithLogitsLoss(reduction=red))
    elif mode == 'original':
        criterion = nn.BCEWithLogitsLoss(reduction=red)
    elif mode == 'dice':
        criterion = DiceLoss()
    elif mode == 'dice_bce':
        criterion = DiceBCELoss()
    elif mode == 'iou':
        criterion = IoULoss()
    elif mode == 'tversky':
        alpha = 0.7
        beta = 0.3
        criterion = TverskyLoss(alpha, beta)
    elif mode == 'focal_tversky':
        alpha = 0.7
        beta = 0.3
        gamma = 4/3
        criterion = FocalTverskyLoss(alpha, beta, gamma)
    else:
        raise IndexError
    
    loss = criterion(p, targets) 
    loss_item = loss.detach()
    
    return loss, loss_item


class FocalLoss(nn.Module):
    # Wraps focal loss around existing loss_fcn(), i.e. criteria = FocalLoss(nn.BCEWithLogitsLoss(), gamma=1.5)
    def __init__(self, loss_fcn, gamma=1.5, alpha=0.25):
        super(FocalLoss, self).__init__()
        self.loss_fcn = loss_fcn  # must be nn.BCEWithLogitsLoss()
        self.gamma = gamma
        self.alpha = alpha
        self.reduction = loss_fcn.reduction
        self.loss_fcn.reduction = 'none'  # required to apply FL to each element

    def forward(self, pred, true):
        loss = self.loss_fcn(pred, true)
        # p_t = torch.exp(-loss)
        # loss *= self.alpha * (1.000001 - p_t) ** self.gamma  # non-zero power for gradient stability

        # TF implementation https://github.com/tensorflow/addons/blob/v0.7.1/tensorflow_addons/losses/focal_loss.py
        pred_prob = torch.sigmoid(pred)  # prob from logits
        p_t = true * pred_prob + (1 - true) * (1 - pred_prob)
        alpha_factor = true * self.alpha + (1 - true) * (1 - self.alpha)
        modulating_factor = (1.0 - p_t) ** self.gamma
        loss *= alpha_factor * modulating_factor

        if self.reduction == 'mean':
            return loss.mean()
        elif self.reduction == 'sum':
            return loss.sum()
        else:  # 'none'
            return loss
        
        
class DiceLoss(nn.Module):
    # https://www.kaggle.com/bigironsphere/loss-function-library-keras-pytorch
    def __init__(self, weight=None, size_average=True):
        super(DiceLoss, self).__init__()
        
    def forward(self, inputs, targets, smooth=1e-6):
        # comment out if your model contains a sigmoid or equivalent activation
        inputs = torch.sigmoid(inputs)
        
        # flatten label and prediction tensors
        inputs = inputs.view(-1)
        targets = targets.view(-1)
        
        intersection = (inputs * targets).sum()
        dice = (2. * intersection + smooth) / (inputs.sum() + targets.sum() + smooth)
        
        return 1 - dice
    
    
class DiceBCELoss(nn.Module):
    # https://www.kaggle.com/bigironsphere/loss-function-library-keras-pytorch
    def __init__(self, red='mean', weight=None, size_average=True):
        super(DiceBCELoss, self).__init__()
        self.red = 'mean'
        
    def forward(self, inputs, targets, smooth=1e-6):
        inputs_before_sigmoid = inputs.view(-1)
        # commemt out if your model contains a sigmoid or equivalent activation 
        inputs = torch.sigmoid(inputs)
        
        # flatten label and prediction tensors
        inputs = inputs.view(-1)
        targets = targets.view(-1)
        
        intersection = (inputs * targets).sum()
        dice_loss = 1 - (2. * intersection + smooth) / (inputs.sum() + targets.sum() + smooth)
        #RuntimeError: torch.nn.functional.binary_cross_entropy and torch.nn.BCELoss are unsafe to autocast.
        # Many models use a sigmoid layer right before the binary cross entropy layer.
        # In this case, combine the two layers using torch.nn.functional.binary_cross_entropy_with_logits
        # or torch.nn.BCEWithLogitsLoss.  binary_cross_entropy_with_logits and BCEWithLogits are
        # safe to autocast.
        bce_loss = F.binary_cross_entropy_with_logits(inputs_before_sigmoid, targets, reduction=self.red)
        
        return bce_loss + dice_loss
    
    
class IoULoss(nn.Module):
    # https://www.kaggle.com/bigironsphere/loss-function-library-keras-pytorch
    def __init__(self, weight=None, size_average=True):
        super(IoULoss, self).__init__()
        
    def forward(self, inputs, targets, smooth=1e-6):
        # comment out if your model contains a sigmoid or equivalent activation
        inputs = F.sigmoid(inputs)
        
        # flatten label and prediction tensors
        inputs = inputs.view(-1)
        targets = targets.view(-1)
        
        # intersection is equivalent to True Positive count
        # union is the mutually inclusive area of all labels & predictions
        intersection = (inputs * targets).sum()
        total = (inputs + targets).sum()
        union = total - intersection
        
        IoU = (intersection + smooth) / (union + smooth)
        
        return 1 - IoU
    
    
class TverskyLoss(nn.Module):
    # https://www.kaggle.com/bigironsphere/loss-function-library-keras-pytorch
    def __init__(self, alpha, beta, weight=None, size_average=True):
        super(TverskyLoss, self).__init__()
        self.alpha = alpha
        self.beta = beta
        
    def forward(self, inputs, targets, smooth=1e-6):
        # comment out if your model contains a sigmoid or equivalent activation
        inputs = torch.sigmoid(inputs)
        
        # flatten label and prediction tensors
        inputs = inputs.view(-1)
        targets = targets.view(-1)
        
        # True Positives, False Positives & False Negatives
        tp = (inputs * targets).sum()
        fp = ((1 - targets) * inputs).sum()
        fn = (targets * (1 - inputs)).sum()
        
        tversky = (tp + smooth) / (tp + self.alpha * fp + self.beta * fn + smooth)
        
        return 1 - tversky
    
    
class FocalTverskyLoss(nn.Module):
    # https://www.kaggle.com/bigironsphere/loss-function-library-keras-pytorch
    def __init__(self, alpha, beta, gamma, weight=None, size_average=True):
        super(FocalTverskyLoss, self).__init__()
        self.alpha = alpha
        self.beta = beta
        self.gamma = gamma
        
    def forward(self, inputs, targets, smooth=1e-6):
        # comment out if your model contains a sigmoid or equivalent activation
        inputs = torch.sigmoid(inputs)
        
        # flatten label and prediction tensors
        inputs = inputs.view(-1)
        targets = targets.view(-1)
        
        # True Positives, False Positives & False Negatives
        tp = (inputs * targets).sum()
        fp = ((1 - targets) * inputs).sum()
        fn = (targets * (1 - inputs)).sum()
        
        tversky = (tp + smooth) / (tp + self.alpha * fp + self.beta * fn + smooth)
        focal_tversky = (1 - tversky) ** self.gamma
        
        return focal_tversky
        
        
#def bce_clip(pred, gt, red):
#    """这里的计算是利用cross entropy，但是文章说，防止过拟合，采取了cliping，即预
#    测的概率在0.1或者0.9(分别对应0，1)，那么就认为loss为0
#    Arg:
#        pred(tensor): 维度(#batch, H), 模型预测 例如[[0.1, 0.1, 0.1, 0.8]]
#        gt(tensor): 维度(#batch, H,), label数据 例如[[0, 0, 0, 1]]
#        red(str): reduction
#    return:
#        loss(tensor):scalar
#    """
#    # 针对0的mask
#    pred_clone = pred.clone()
#    mask_0 = torch.mul(gt==0, pred_clone < 0.1)
#    # 针对1的mask
#    mask_1 = torch.mul(gt==1, pred_clone > 0.9) 
#    pred_clone[mask_0] = 0
#    pred_clone[mask_1] = 1
#    loss = F.binary_cross_entropy(pred_clone, gt, reduction=red)
#
#    return loss
#
#def bce_org(pred, gt, red):
#    """标准的bce"""
#    return F.binary_cross_entropy(pred, gt, reduction=red)
#
#
#def bce_smooth(pred, gt, red, eps=0.1):
#    """smooth labeling的bce
#    Arg:
#        pred(tensor): 维度(#batch, H), 模型预测 例如[[0.1, 0.1, 0.1, 0.8]]
#        gt(tensor): 维度(#batch, H), label数据 例如[[0, 0, 0, 1]]
#    return:
#        loss(tensor):scalar
#    """
#    cp, cn = 1.0 - 0.5 * eps, 0.5 * eps  # return positive, negative label smoothing BCE targets
#    gt_clone = torch.full_like(gt, cn)
#    gt_clone[gt==1] = cp
#    return F.binary_cross_entropy(pred, gt_clone, reduction=red)