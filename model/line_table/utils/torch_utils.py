# -*- coding: utf-8 -*-
"""
Created on Thu Dec 17 19:26:11 2020

@author: tengjunwan
"""

import torch.nn as nn
import torch
import torch.nn.functional as F
import math
from copy import deepcopy
import os


class ASPPConv(nn.Sequential):
    # padding = dilate
    def __init__(self, in_channels, out_channels, dilation):
        modules = [
            nn.Conv2d(in_channels, out_channels, 3, padding=dilation,
                      dilation=dilation, bias=False),
            nn.BatchNorm2d(out_channels),
            nn.ReLU(inplace=True)
        ]
        super(ASPPConv, self).__init__(*modules)  # =nn.Sequential(*modules)


class ASPPPooling(nn.Sequential):
    # ASSPPooling=AvgPooling + conv1*1 + bn + relu
    def __init__(self, in_channels, out_channels):
        super(ASPPPooling, self).__init__(
            nn.AdaptiveAvgPool2d(1),
            nn.Conv2d(in_channels, out_channels, 1, bias=False),
#            nn.BatchNorm2d(out_channels),
            nn.ReLU(inplace=True))

    def forward(self, x):
        size = x.shape[-2:]
        x = super(ASPPPooling, self).forward(x)
        return F.interpolate(x, size=size, mode='bilinear', align_corners=False)


class ASPP(nn.Module):
    """ASPP = [conv1*1, conv3*3, conv3*3, conv3*3, avgPool], 其中三个conv3*3是
    dilated conv, dilated rate = [6, 12, 18],并且之后有一个conv1*1+bn+relu+dropout
    
    """
    def __init__(self, in_channels, atrous_rates, project_pool=True):
        """
        Args:
            in_channels(int): 输入channel数, resnet50以上一般为2048
            atrous_rates(list of int): 三个卷积的dilate rate, [6, 12, 18]
            project_pool(bool): 是否加入project pool(改装原本的ASPP)
        """
        super(ASPP, self).__init__()
        out_channels = 256  # 每一个模块输出都为256，最后输出是256*5
        modules = []
        
        # 1*1 conv
        modules.append(nn.Sequential(
            nn.Conv2d(in_channels, out_channels, 1, bias=False),
            nn.BatchNorm2d(out_channels),
            nn.ReLU(inplace=True)))
        
        # dilated conv
        rate1, rate2, rate3 = tuple(atrous_rates)
        modules.append(ASPPConv(in_channels, out_channels, rate1))
        modules.append(ASPPConv(in_channels, out_channels, rate2))
        modules.append(ASPPConv(in_channels, out_channels, rate3))
        
        # avgpool
        modules.append(ASPPPooling(in_channels, out_channels))
        
        # projpool
        if project_pool:
            for direction in ['R', 'C']:
                modules.append(ProjPooling(in_channels, out_channels, direction))

        self.convs = nn.ModuleList(modules)
        
        # project, integrate all features concatenated
        n_path = 5 if not project_pool else 7
        self.project = nn.Sequential(
            nn.Conv2d(n_path * out_channels, out_channels, 1, bias=False),
            nn.BatchNorm2d(out_channels),
            nn.ReLU(inplace=True),
            nn.Dropout(0.1),)

    def forward(self, x):
        res = []
        for conv in self.convs:
            res.append(conv(x))
        res = torch.cat(res, dim=1)
        return self.project(res)
    

class AtrousSeparableConvolution(nn.Module):
    """ Atrous Separable Convolution
    """
    def __init__(self, in_channels, out_channels, kernel_size,
                 stride=1, padding=0, dilation=1, bias=True):
        super(AtrousSeparableConvolution, self).__init__()
        self.body = nn.Sequential(
            # Separable Conv
            nn.Conv2d(in_channels, in_channels, kernel_size=kernel_size,
                      stride=stride, padding=padding, dilation=dilation, 
                      bias=bias, groups=in_channels ),
            # PointWise Conv
            nn.Conv2d(in_channels, out_channels, kernel_size=1, stride=1, 
                      padding=0, bias=bias),
        )
        
        self._init_weight()

    def forward(self, x):
        return self.body(x)

    def _init_weight(self):
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.kaiming_normal_(m.weight)
            elif isinstance(m, (nn.BatchNorm2d, nn.GroupNorm)):
                nn.init.constant_(m.weight, 1)
                nn.init.constant_(m.bias, 0)
                

def convert_to_separable_conv(module):
    new_module = module
    if isinstance(module, nn.Conv2d) and module.kernel_size[0]>1:
        new_module = AtrousSeparableConvolution(module.in_channels,
                                      module.out_channels, 
                                      module.kernel_size,
                                      module.stride,
                                      module.padding,
                                      module.dilation,
                                      module.bias)
    for name, child in module.named_children():
        new_module.add_module(name, convert_to_separable_conv(child))
    return new_module


def set_bn_momentum(model, momentum=0.1):
    for m in model.modules():
        if isinstance(m, nn.BatchNorm2d):
            m.momentum = momentum
            # print("momentum change: %.3f --> %.3f" % (m.momentum, momentum))

                
class ProjPool_(nn.Module):
    """Project pool层
    如果是Row方向，把输入的feature map从(batch, n, H, W)在W上进行平均；
    如果是Column方向，把输入的feature map从(batch, n, H, W)在H上进行平均
    """
    def __init__(self, direction, expand=True):
        super(ProjPool_, self).__init__()
        assert direction in ['R', 'C'], 'only R or C is available'
        if direction == "C":
            self.dim = 2  # projection pool的方向
        if direction == "R":
            self.dim = 3  # projection pool的方向
            
        self.expand = expand  
            
    def forward(self, x):
        size = x.shape[self.dim]
        x = torch.mean(x, dim=self.dim, keepdim=True)  
        if not self.expand:
            return x  # "R": (batch, n, H, 1), "C"：(batch, n, 1, W)
        
        if self.dim == 3:
            x = x.expand(-1, -1, -1, size)  # (batch, n, H, W)
        if self.dim == 2:
            x = x.expand(-1, -1, size, -1)  # (batch, n, H, W)
        return x
    

class ProjPooling(nn.Sequential):
    # ProjPooling=ProjPool_ + conv1*1 + bn + relu
    def __init__(self, in_channels, out_channels, direction):
        modules = [
            ProjPool_(direction),
            nn.Conv2d(in_channels, out_channels, 1, bias=False),
#            nn.BatchNorm2d(out_channels),
            nn.ReLU(inplace=True)
        ]
        super(ProjPooling, self).__init__(*modules)  # =nn.Sequential(*modules)




class ModelEMA:
    """ 
    来源：https://github.com/ultralytics/yolov3/blob/master/utils/torch_utils.py
    Model Exponential Moving Average from https://github.com/rwightman/pytorch-image-models
    Keep a moving average of everything in the model state_dict (parameters and buffers).
    This is intended to allow functionality like
    https://www.tensorflow.org/api_docs/python/tf/train/ExponentialMovingAverage
    A smoothed version of the weights is necessary for some training schemes to perform well.
    E.g. Google's hyper-params for training MNASNet, MobileNet-V3, EfficientNet, etc that use
    RMSprop with a short 2.4-3 epoch decay period and slow LR decay rate of .96-.99 requires EMA
    smoothing of weights to match results. Pay attention to the decay constant you are using
    relative to your update count per epoch.
    To keep EMA from using GPU resources, set device='cpu'. This will save a bit of memory but
    disable validation of the EMA weights. Validation will have to be done manually in a separate
    process, or after the training stops converging.
    This class is sensitive where it is initialized in the sequence of model init,
    GPU assignment and distributed training wrappers.
    I've tested with the sequence in my own train.py for torch.DataParallel, apex.DDP, and single-GPU.
    """

    def __init__(self, model, decay=0.9999, device=''):
        # make a copy of the model for accumulating moving average of weights
        self.ema = deepcopy(model)  # 深度copy，不干扰原来的模型，占用一定的gpu空间
        self.ema.eval()  # 调节为eval模式
        self.updates = 0  # number of EMA updates,越大，则越考虑历史平均
        self.decay = lambda x: decay * (1 - math.exp(-x / 2000))  # decay exponential ramp (to help early epochs)
        self.device = device  # perform ema on different device from model if set
        if device:
            self.ema.to(device=device)
        for p in self.ema.parameters():
            p.requires_grad_(False)  # ema复刻模型中所有参数设置为无需求导

    def update(self, model):
        self.updates += 1
        d = self.decay(self.updates)
        with torch.no_grad():
            if type(model) in (nn.parallel.DataParallel, nn.parallel.DistributedDataParallel):  # 分布训练略过
                msd, esd = model.module.state_dict(), self.ema.module.state_dict()
            else:
                msd, esd = model.state_dict(), self.ema.state_dict()

            for k, v in esd.items():
                if v.dtype.is_floating_point:
                    v *= d
                    v += (1. - d) * msd[k].detach()

    def update_attr(self, model):
        # Assign attributes (which may change during training)
        for k in model.__dict__.keys():
            if not k.startswith('_'):  # 如果不是私有属性,给ema模型也加上
                setattr(self.ema, k, getattr(model, k))
                
                
def strip_optimizer(f='weights/best.pt', s=''):  # from utils.general import *; strip_optimizer()
    # Strip optimizer from 'f' to finalize training, optionally save as 's'
    x = torch.load(f, map_location=torch.device('cpu'))
    x['optimizer'] = None
    x['training_results'] = None
    x['epoch'] = -1
    x['model'].half()  # to FP16
    for p in x['model'].parameters():
        p.requires_grad = False
    torch.save(x, s or f)
    mb = os.path.getsize(s or f) / 1E6  # filesize
    print('Optimizer stripped from %s,%s %.1fMB' % (f, (' saved as %s,' % s) if s else '', mb))
    
    
def select_device(device='', apex=False, batch_size=None):
    # device = 'cpu' or '0' or '0,1,2,3'
    cpu_request = device.lower() == 'cpu'
    if device and not cpu_request:  # if device requested other than 'cpu'
        os.environ['CUDA_VISIBLE_DEVICES'] = device  # set environment variable
        assert torch.cuda.is_available(), 'CUDA unavailable, invalid device %s requested' % device  # check availablity

    cuda = False if cpu_request else torch.cuda.is_available()
    if cuda:
        c = 1024 ** 2  # bytes to MB
        ng = torch.cuda.device_count()
        if ng > 1 and batch_size:  # check that batch_size is compatible with device_count
            assert batch_size % ng == 0, 'batch-size %g not multiple of GPU count %g' % (batch_size, ng)
        x = [torch.cuda.get_device_properties(i) for i in range(ng)]
        s = 'Using CUDA ' + ('Apex ' if apex else '')  # apex for mixed precision https://github.com/NVIDIA/apex
        for i in range(0, ng):
            if i == 1:
                s = ' ' * len(s)
            print("%sdevice%g _CudaDeviceProperties(name='%s', total_memory=%dMB)" %
                  (s, i, x[i].name, x[i].total_memory / c))
    else:
        print('Using CPU')

    print('')  # skip a line
    return torch.device('cuda:0' if cuda else 'cpu')
                
if __name__ == "__main__":
    projpool = ProjPooling(3, 1, 'R')
    x = torch.rand(1, 3, 10, 10)
    y = projpool(x)
