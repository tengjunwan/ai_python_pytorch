# -*- coding: utf-8 -*-
"""
Created on Mon Dec 21 11:54:55 2020

@author: tengjunwan
"""

#import cv2
import numpy as np
from utils.utils import point2range
import cv2
import matplotlib.pyplot as plt


def draw_pic_with_sep(img, r_sep, c_sep, color=(0, 0, 255)):
    # r_pred = [[start, end],...] or [00000111100001111000]
    img = np.copy(img)  # copy
    
    r_sep = np.array(r_sep)
    c_sep = np.array(c_sep)
    
    if r_sep.ndim == 1:  # [00000111100001111000] -> [[start, end],...]
        r_sep = point2range(r_sep)
    if c_sep.ndim == 1:
        c_sep = point2range(c_sep)
        
    for r0, r1 in r_sep.round().astype(np.int32):
        img[r0: r1 + 1, :] = color
    for c0, c1 in c_sep.round().astype(np.int32):
        img[:, c0: c1 + 1] = color
    return img

def draw_table_cells(img, cells, text=None, color=(0, 0, 255), mode='xywh'):
    # cells [#cells ,4], 4表示xywh
    img = np.copy(img)  # copy
    cells = np.array(cells).reshape(-1, 4)
    for cell in cells:
        if mode == 'xywh':
            x0, y0, w, h = cell
            x1 = x0 + w
            y1 = y0 + h
        else:  # xyxy
            x0, y0, x1, y1 = cell
            
        start_point = (int(round(x0)), int(round(y0)))
        end_point = (int(round(x1)), int(round(y1)))
        img = cv2.rectangle(img, start_point, end_point, color, thickness=3)
    if text is not None:
        text_x = 5
        text_y = img.shape[0] - 5
        # 画上额外信息
        cv2.putText(img, text, (text_x, text_y), cv2.FONT_HERSHEY_SIMPLEX, 
                    0.7, [255, 0, 0], thickness=2, lineType=cv2.LINE_AA)
    return img


def draw_histogram(x, bins, msg='', title=''):
    """直方图"""
    # 画图
    fig, ax = plt.subplots()
    arr = ax.hist(x, bins=bins)  # [count, bin_edges]
    plt.title(title)
    # 标记count
    for i in range(10):
        plt.text(arr[1][i], arr[0][i], str(arr[0][i]))
    # 放置message box
    props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)
    ax.text(0.05, 0.95, msg, transform=ax.transAxes, fontsize=14,
            verticalalignment='top', bbox=props)
    
    return fig


def draw_table_with_lines(img, r_line, c_line):
    # r_line: [1, 133, 155]
    color = (0, 0, 255)  # red bgr
    img = np.copy(img)  # copy
    thickness = 2
    
    r_line = np.array(r_line)
    c_line = np.array(c_line)
    
    h, w = img.shape[: 2]
        
    for r in r_line.round().astype(np.int32):
        start = (0, r)
        end = (w, r)
        img = cv2.line(img, start, end, color, thickness)
    for c in c_line.round().astype(np.int32):
        start = (c, 0)
        end = (c, h)
        img = cv2.line(img, start, end, color, thickness)
    return img


def cv_imwrite(path:str, img:np.ndarray) -> bool:
    # support chinese path
    return cv2.imencode('.png', img)[1].tofile(path)

    