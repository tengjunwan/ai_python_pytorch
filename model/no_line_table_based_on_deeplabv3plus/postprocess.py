# -*- coding: utf-8 -*-
"""
Created on Tue Jul  6 16:04:45 2021

@author: EDZ
"""
# model.no_line_table_based_on_deeplabv3plus.
import numpy as np
import fitz
import json
import cv2
from model.no_line_table_based_on_deeplabv3plus.utils.utils import get_uinque_values_with_tolerace_and_order
from model.no_line_table_based_on_deeplabv3plus.utils.pdf_utils import extract_img_and_text_in_rect
from model.no_line_table_based_on_deeplabv3plus.utils.utils import filter_bboxes_in_rect
from math import log
from model.no_line_table_based_on_deeplabv3plus.ngram.ngram import KneserNeyGram
from model.no_line_table_based_on_deeplabv3plus.ngram.tokenizer import Tokenizer, tokenize_with_coord
import re


class PostProcessor():
    def __init__(self, load_path):
        self.ngram = KneserNeyGram(load_path=load_path)
        self.tokenizer = Tokenizer()
    
    def __call__(self, img, row_sep, col_sep, text, bboxes):
        table = Table(text, bboxes, row_sep, col_sep, img)
        table.eliminate_extra_col_line(self.ngram, self.tokenizer)
        table.eliminate_extra_row_line(self.ngram, self.tokenizer)
        
        return table

    
class Cell():
    """cell cutted by the rows and columns"""
    def __init__(self, text, bboxes, xyxy, index):
        """
        Args:
            text(list):
            bboxes(ndarray):
            index(tuple):
            xyxy(ndarray):
        """
        self.xyxy = np.array(xyxy)  # coordinate of cell
        text_sorted, bboxes_sorted, lines, line_ys, lines_coor, lines_content = self.form_content(text, bboxes)
        self.text = text_sorted
        self.bboxes = bboxes_sorted
        self.lines = lines
        self.line_ys = line_ys
        self.content = ''.join(lines_content)  # content in cell
        self.lines_coor = lines_coor
        self.lines_content = lines_content
        self.index = index
        
        # tokenize
        self.token_lines = []
        self.token_lines_coor = []
        for line, line_coor in zip(self.lines, self.lines_coor):
            token, token_coor = tokenize_with_coord(line, line_coor)
            if len(token):
                self.token_lines.append(token)
                self.token_lines_coor.append(token_coor)
        
    def form_content(self, text, bboxes):
        """form content based on the coorinate(bboxes) of each character
        Args:
            text(list):
            bboxes(ndarray): xyxy form.
        return:
            text_sorted(list): text rearanged by reading order.
            bboxes_sorted(ndarray): bboxers rearanged by reading order.
            lines(list): content of each line. 
            line_ys(list): y coordinate of each line.
        """ 
        text = np.array(text)
        if not len(text):  # emtpy cell
            return text, bboxes, [], [], [], []
        
        # config
        y_tolerance = 4  
        # get left top y order with a tolerance
        y_unique_values, y_orders = get_uinque_values_with_tolerace_and_order(
                                                                bboxes[:, 1],
                                                                tolerance=y_tolerance)
        
        # sort
        index = y_orders * 10000 + bboxes[:, 0]  # y first then x(left top)
        order = index.argsort()  
        text_sorted = text[order] 
        bboxes_sorted = bboxes[order]
        y_orders_sorted =y_orders[order]  
        
        # split into lines
        lines = []  
        line_ys = []
        lines_content = []
        lines_coor = []
        split_index = np.argwhere(np.diff(y_orders_sorted) > 0).flatten() + 1  
        split_index = np.insert(split_index, 0, 0)  
        split_index = np.append(split_index, len(text_sorted))  
        for j in range(len(split_index) - 1):  # loop in pairs
            start , end = split_index[j], split_index[j+1]  
            lines.append(text_sorted[start: end])
            lines_content.append(''.join(text_sorted[start: end].tolist()))
            line_ys.append(y_unique_values[j])
            lines_coor.append(bboxes_sorted[start: end])
        
        return text_sorted.tolist(), bboxes_sorted, lines, line_ys, lines_coor, lines_content
    
    def merge(self, cell):
        # coordinate
        new_x0 = min(self.xyxy[0], cell.xyxy[0])
        new_y0 = min(self.xyxy[1], cell.xyxy[1])
        new_x1 = max(self.xyxy[2], cell.xyxy[2])
        new_y1 = max(self.xyxy[3], cell.xyxy[3])
        new_xyxy = np.array([new_x0, new_y0, new_x1, new_y1])
        # text and coresponding bboxes
        new_text = np.concatenate((self.text, cell.text), axis=0)
        new_bboxes = np.concatenate((self.bboxes, cell.bboxes))
        
        new_index = 'merge'
        
        return Cell(new_text, new_bboxes, new_xyxy, new_index)
        
    def is_gap_even(self):
        """This algorithm takes care of both english sentences and chinese
        sentences. The definition of gap is different in both situations because
        of the boundaries of 'words'.
        """
        
                
        # config
        d_thresh = 2
        
        line_even = []
        # parse words and coresponding coordinates
        for token_line_coor in self.token_lines_coor:
            if len(token_line_coor) <= 2:
                line_even.append(True)
            else:
                x0 = token_line_coor[:, 0].flatten()[1: ]
                x1 = token_line_coor[:, 2].flatten()[:-1]
                gap = x1 - x0
                deviation = np.std(gap)
                # print(deviation)
                line_even.append(deviation < d_thresh)
        # print(np.all(line_even))
        
        return np.all(line_even)
    
    def __repr__(self):
        return self.content
                
    
class Table():
    """consist of a set of cells in a table"""
    def __init__(self, text, bboxes, row_sep, col_sep, img):
        """
        Args:
            text(list): 
            bboxes(ndarray):
            row_sep(list): [[start, end], [start, end], ...]
            col_sep(list): [[start, end], [start, end], ...]
            img(ndarray):
        """
        # word and its location within
        self.text = np.array(text)
        self.bboxes = np.array(bboxes)
        
        # separators and lines
        self.row_sep = np.array(row_sep).reshape(-1, 2)
        self.row_line = np.mean(self.row_sep, axis=1)
        self.col_sep = np.array(col_sep).reshape(-1, 2)
        self.col_line = np.mean(self.col_sep, axis=1)
        
        self.img = img
        
        # get table
        self.table = self._form_table(self.row_line, self.col_line, self.img.shape[:2],
                                      self.text, self.bboxes)
        
    def _form_table(self, row_line, col_line, size, text, bboxes):
        """
        Args:
            row_line(ndarray):
            col_line(ndarray):
            size(tuple): (h, w)
            text(ndarray):
            bboxes(ndarray):
        return:
            table(ndarray): dtype=cell
        """
        h, w = size
        
        # initialize empty table
        table = np.empty((len(row_line)+1, len(col_line)+1), dtype=Cell)
        
        # fill the empty table with cells
        row_line = [0] + row_line.tolist() + [h-1]
        col_line = [0] + col_line.tolist() + [w-1]
        for i in range(len(row_line) - 1):
            for j in range(len(col_line) - 1):
                y0 = row_line[i]
                y1 = row_line[i + 1]
                x0 = col_line[j]
                x1 = col_line[j + 1]
                cell_coor = np.array([x0, y0, x1, y1])
                bboxes_filtered, bool_array = filter_bboxes_in_rect(bboxes, cell_coor)
                text_filtered = text[bool_array]
                table[i, j] = Cell(text=text_filtered,
                                   bboxes=bboxes_filtered, 
                                   xyxy=cell_coor,
                                   index=(i,j))
                
        return table
    
    def eliminate_extra_col_line(self, ngram, tokenizer):
        """iterate over each pair of cells in neigbouring columns.
        Args:
            ngram(NGram):
            tokenizer(Tokenizer):
        """
        n_row, n_col = self.table.shape[: 2]
        if n_col <= 1:
            return None
        
        empty_cells = np.zeros((n_col - 1, n_row, 2))  # 0: emtpy, 1: not empty, keep track of empty cell patterns
        merge_scores = np.zeros((n_col - 1, n_row))    # 1: content after merge makes sense;
                                                       # -1: content after merge makes sense;
                                                       # 0: at least one cell is empty
        
        for i in range(n_col - 1):
            col_a = self.table[:, i]
            col_b = self.table[:, i+1]
            col_merge = np.empty(n_row, dtype=Cell)
            
            for j, (cell_a, cell_b) in enumerate(zip(col_a, col_b)):
                cell_merge = cell_a.merge(cell_b)
                if len(cell_a.content.strip()) and len(cell_b.content.strip()):  # both cells are not empty
                    # ngram check
                    merge_score = merge_criterion(cell_a.content,
                                                  cell_b.content,
                                                  cell_merge.content,
                                                  ngram,
                                                  tokenizer)
                    
                    # gap-even check, denied if gap isn't even after merge.
                    if merge_score > 0:  
                        if not cell_merge.is_gap_even():
                            merge_score = -1
                    merge_scores[i, j] = merge_score
                    col_merge[j] = cell_merge
                else:  # at least one of them is empty
                    merge_scores[i, j] = 0  
                    # keep track the pattern of emtpy cells, one means empty
                    empty_cells[i, j] = (len(cell_a.content.strip()) > 0, 
                                         len(cell_b.content.strip()) > 0)
        
        # make decision whether keep or eliminate the column separator
        col_eli_index = []  # to be eliminated
        for i in range(n_col - 1):
            if np.any(merge_scores[i] == -1):  # one rejection rejects all, keep it
                pass
            elif np.any(merge_scores[i] == 1):  # then one approval approves all, eliminate it
                col_eli_index.append(i)
            else:  # all 0, means each pair of neighbouring cells contains at least one empty cell
                # all cells in one column are all empty, eliminate it
                if np.all(empty_cells[i, :, 0] == 0) and np.all(empty_cells[i, :, 1] == 0):  # 2 empty columns
                    col_eli_index.append(i)
                elif np.any(empty_cells[i, :, 0] == 1) and np.all(empty_cells[i, :, 1] == 0):  # left not empty, right empty
                    col_eli_index.append(i)
                elif np.all(empty_cells[i, :, 0] == 0) and np.any(empty_cells[i, :, 1] == 1):  # left empty, right not empty
                    # check previous empty pattern
                    if np.all(empty_cells[:i, :, :] == [0, 0]):
                        col_eli_index.append(i)
                    
                elif i == 0 and n_row >= 3:
                    if np.all(empty_cells[i, 1:-1]==[1, 0]) and np.all(empty_cells[i, [0,-1]]==[0, 1]):
                        col_eli_index.append(i)        

        # reform table 
        self.col_sep = np.delete(self.col_sep, col_eli_index, axis=0)
        self.col_line = np.delete(self.col_line, col_eli_index)
        self.table = self._form_table(self.row_line, self.col_line, self.img.shape[:2],
                                      self.text, self.bboxes)
        
    
    def eliminate_extra_row_line(self, ngram, tokenizer):
        """
        Args:
            ngram(Ngram):
            tokenizer(Tokenizer):
        """
        n_row, n_col = self.table.shape[: 2]
        if n_row <= 1:
            return None
        
        empty_cells = np.zeros((n_row - 1, n_col, 2))  # 0: emtpy, 1: not empty, keep track of empty cell patterns
        merge_scores = np.zeros((n_row - 1, n_col))    # 1: content after merge makes sense;
                                                       # -1: content after merge makes sense;
                                                       # 0: at least one cell is empty.
        
        for i in range(n_row - 1):
            row_a = self.table[i, :]
            row_b = self.table[i+1, :]
            row_merge = np.empty(n_col, dtype=Cell)
            
            for j, (cell_a, cell_b) in enumerate(zip(row_a, row_b)):
                cell_merge = cell_a.merge(cell_b)
                if len(cell_a.content.strip()) and len(cell_b.content.strip()):  # both cells are not empty
                    # ngram check
                    merge_score = merge_criterion(cell_a.content,
                                                  cell_b.content,
                                                  cell_merge.content,
                                                  ngram,
                                                  tokenizer)
                    
                    # line span check
                    if merge_score > 0:  
                        if not vertical_merge_extra_check(cell_a, cell_b):
                            merge_score = -1
                    merge_scores[i, j] = merge_score
                    row_merge[j] = cell_merge
                else:  # at least one of them is empty
                    merge_scores[i, j] = 0  
                    # keep track the pattern of emtpy cells, one means empty
                    empty_cells[i, j] = (len(cell_a.content.strip()) > 0, 
                                         len(cell_b.content.strip()) > 0)
        
        # make decision whether keep or eliminate the row separator
        row_eli_index = []  # to be eliminated
        for i in range(n_row - 1):
            if np.any(merge_scores[i] == -1):  # one rejection rejects all, keep it
                pass
            elif np.any(merge_scores[i] == 1):  # then one approval approves all, eliminate it
                row_eli_index.append(i)
            else:  # all 0, means each pair of neighbouring cells contains at least one empty cell
                # all cells in one column are all empty, eliminate it
                if np.all(empty_cells[i, :, 0] == 0) and np.all(empty_cells[i, :, 1] == 0):  # 2 empty columns
                    row_eli_index.append(i)
                elif np.any(empty_cells[i, :, 0] == 1) and np.all(empty_cells[i, :, 1] == 0):  # left not empty, right empty
                    row_eli_index.append(i)
                elif np.all(empty_cells[i, :, 0] == 0) and np.any(empty_cells[i, :, 1] == 1):  # left empty, right not empty
                    # check previous empty pattern
                    if np.all(empty_cells[:i, :, :] == [0, 0]):
                        row_eli_index.append(i)
                    
                elif i == 0 and n_row >= 3:
                    if np.all(empty_cells[i, 1:-1]==[1, 0]) and np.all(empty_cells[i, [0,-1]]==[0, 1]):
                        row_eli_index.append(i)        

        # reform table 
        self.row_sep = np.delete(self.row_sep, row_eli_index, axis=0)
        self.row_line = np.delete(self.row_line, row_eli_index)
        self.table = self._form_table(self.row_line, self.col_line, self.img.shape[:2],
                                      self.text, self.bboxes)
        
        
        pass
                
                
def merge_criterion(sent_a, sent_b, sent_c, ngram, tokenizer):
    # return -1, 0, 1
    # config
    margin = 0.2
    
    sent_a = tokenizer(sent_a)
    sent_b = tokenizer(sent_b)
    sent_c = tokenizer(sent_c)

    m1 = len(sent_a)
    m2 = len(sent_b)
    m3 = len(sent_c)
    assert m1 > 0 and m2 > 0, "sentences shouldn't be empty." 
    p_a = ngram.perplexity([sent_a])
    p_b = ngram.perplexity([sent_b])
    p_c = ngram.perplexity([sent_c])
    left = log(p_c)
    right = log(p_a) * m1 / (m3) + log(p_b) * m2 / (m3)
    
    # print('A:', sent_a)
    # print('B:', sent_b)
    # print('C:', sent_c)
    # print('left: %.3f' % left)
    # print('right: %.3f' % right)
    if left < right - margin:      
        return 1
    else:
        return -1
                
        
                
def vertical_merge_extra_check(cell_a, cell_b):
    """the extra check to ensure the merge of two given cells.
    Args:
        cell_a(Cell): the upper cell.
        cell_b(Cell): the lower cell.
    return:
        merge(bool): True or False.
    """
    # part 1 display pattern
    # config
    # tolerance = 2
    # # coordinate pattern check
    # # upper cell last line x span
    # u_x0 = cell_a.token_lines_coor[-1][0, 0]  # left x
    # u_x1 = cell_a.token_lines_coor[-1][-1, 2]  # right x
    # # lower cell first line x span
    # l_x0 = cell_b.token_lines_coor[0][0, 0]  # left x
    # l_x1 = cell_b.token_lines_coor[0][-1, 2]  # right x
    
    # # criterion
    # if abs(l_x0 - u_x0) <= tolerance:  # left sides align
    #     if l_x1 - u_x1 > tolerance:
    #         return False
    #     else:
    #         return True
        
    # elif l_x0 - u_x0 > tolerance:  # upper_left on the left side of the lower_left 
    #     return True
    
    # elif u_x0 - l_x0 > tolerance:  # upper_left on the right side of the lower_left 
    #     if abs(l_x1 - u_x1) <= tolerance:
    #         return True
    #     else:
    #         return False
    
    # part 2 numerical content pattern
    cell_a_tokens = []
    for i in cell_a.token_lines:
        cell_a_tokens = cell_a_tokens + i
    cell_a_token_content = ''.join(cell_a_tokens)
    
    cell_b_tokens = []
    for i in cell_b.token_lines:
        cell_b_tokens = cell_b_tokens + i
    cell_b_token_content = ''.join(cell_b_tokens)
    
    # print(cell_a_token_content)
    if is_legal_numeric_cell(cell_a_token_content) or is_legal_numeric_cell(cell_b_token_content):
        # print('1')
        return False
    elif is_legal_numeric_cell(cell_a_token_content + cell_b_token_content):
        # print('2')
        return True
    else:
        # print('3')
        return True
    
        
def is_legal_numeric_cell(txt):
    return bool(re.search(r'-?(\()?\d{1,3},(?:\d{3},)*\d{3}(?(1)\)|)$|^-?(\()?\d+(?(2)\)|)$|^-{1,2}$', txt))

    

if __name__ == "__main__":
    from utils.vis_utils import draw_table_cells, draw_pic_with_label
    # load unlined table
    pdf_id = 6812
    page_index = 51
    pdf_path = 'data/pdf/%d.pdf' % pdf_id
    json_path = 'data/json/%d.json' % pdf_id
    doc = fitz.open(pdf_path)
    page = doc[page_index]
    with open(json_path, 'r') as f:
        preds = json.load(f)
    pred = np.array(preds[page_index])
    unlined_box_cls_idx = 3
    if False:
        pred_unlined_box = pred[pred[:, 5]==unlined_box_cls_idx][0][:4]
    else:
        a = np.array([141, 131, 580, 179])
        b = a[:2]
        c = a[:2] + a[2:]
        pred_unlined_box = np.concatenate((b, c))
    img, bboxes, text = extract_img_and_text_in_rect(page, pred_unlined_box)
    cv2.imshow("", img)
    cv2.waitKey()
    img_with_box = draw_table_cells(img, bboxes, mode='xyxy')
    cv2.imshow("img_with_box", img_with_box)
    cv2.waitKey()
    # load its prediction
    json_name = '6812_52_1_[141, 131, 580, 179].json'
    pred_path = 'samples_output/json/%s' % json_name
    with open(pred_path, 'r') as f:
        pred = json.load(f)
    row_sep = pred['row']    
    col_sep = pred['col']
    img_pred = draw_pic_with_label(img, row_sep, col_sep)
    cv2.imshow('predict', img_pred)
    cv2.waitKey()
    
    # test table
    table = Table(text, bboxes, row_sep, col_sep, img)
    
    # test pp
    pp = PostProcessor('ngram/kn_dict_n4_d96.pkl')
    table = pp(img, row_sep, col_sep, text, bboxes)
    img_post = draw_pic_with_label(img, table.row_sep, table.col_sep)
    cv2.imshow('postprocess', img_post)
    cv2.waitKey()
            