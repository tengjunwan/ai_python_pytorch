# -*- coding: utf-8 -*-
"""
Created on Sat Jul  3 15:37:00 2021

@author: EDZ
"""

import fitz
import numpy as np
import cv2
from model.no_line_table_based_on_deeplabv3plus.utils.utils import filter_bboxes_in_rect


def page2image(page:fitz.fitz.Page, scale_ratio:float=1.33333333, 
               rotate:'int'=0) -> np.ndarray:
    trans = fitz.Matrix(scale_ratio, scale_ratio).preRotate(rotate)  # rotate means clockwise 
    pm = page.getPixmap(matrix=trans, alpha=False)
    data = pm.getImageData()  # bytes
    # img = read_image_in_bytes(data)
    img_np = np.frombuffer(data, dtype=np.uint8)
    img = cv2.imdecode(img_np, flags=1)
    return img


def get_word_bboxes(page:fitz.fitz.Page, scale_ratio:float=1.33333333) -> tuple:
    # use Page.getText('words')
    words = page.getText('words')  # (x0, y0, x1, y1, "string", blocknumber, linenumber, wordnumber)
    bboxes = []
    text = []
    for word in words:
        bboxes.append([word[0], word[1], word[2], word[3]])
        text.append(word[4])
    bboxes = np.array(bboxes).reshape(-1, 4) *  scale_ratio # in case bboxes is empty
    
    # adjust coordinate according to Page.rotation
    bboxes = change_bbox_coordinate_by_rotation(bboxes, page)
    
    return bboxes, text


def get_block_bboxes(page:fitz.fitz.Page, scale_ratio:float=1.33333333) -> tuple:
    # use Page.getText('blocks')
    words = page.getText('blocks')  # (x0, y0, x1, y1, "lines in block", block_type, block_no)
    bboxes = []
    text = []
    for word in words:
        bboxes.append([word[0], word[1], word[2], word[3]])
        text.append(word[4])
    bboxes = np.array(bboxes).reshape(-1, 4) *  scale_ratio # in case bboxes is empty
    
    # adjust coordinate according to Page.rotation
    bboxes = change_bbox_coordinate_by_rotation(bboxes, page)
    return bboxes, text


def change_bbox_coordinate_by_rotation(bboxes:np.ndarray, page:fitz.fitz.Page, 
                                       scale_ratio:float=1.33333333) -> np.ndarray:
    # cause getText return the coordinate before rotation, we need to ajust to
    # the coordinate after rotation
    if page.rotation == 0:
        return bboxes
    else:
        img = page2image(page, scale_ratio)
        h, w = img.shape[: 2]
        if page.rotation == 90:
            # y = x', x = w - y'
            x0 = w - bboxes[:, 3]
            y0 = bboxes[:, 0]
            x1 = w - bboxes[:, 1]
            y1 = bboxes[:, 2]
            bboxes = np.stack((x0, y0, x1, y1), axis=1)
            return bboxes
        elif page.rotation == 180:
            # y = h - y', x = w - x'
            x0 = w - bboxes[:, 3]
            y0 = h - bboxes[:, 2]
            x1 = w - bboxes[:, 1]
            y1 = h - bboxes[:, 0]
            bboxes = np.stack((x0, y0, x1, y1), axis=1)
            return bboxes
        elif page.rotation == 270:
            # y = h - x', x = y'
            x0 = bboxes[:, 1]
            y0 = h - bboxes[:, 2]
            x1 = bboxes[:, 3]
            y1 = h - bboxes[:, 0]
            bboxes = np.stack((x0, y0, x1, y1), axis=1)
            return bboxes
        else:
            raise ValueError('page.rotation should be 0, 90, 180, 270, not %g'
                             % page.rotation)
    

def get_char_bboxes(page:fitz.fitz.Page, scale_ratio:float=1.33333333, 
                    shrink:bool=True, return_font:bool=False) -> tuple:
    # use Page.getText('rawdict') to get chars bboxes
    text = []
    bboxes = []
    fonts = []
    content = page.getText('rawdict')  # content(dict): 'width', 'height', 'blocks'
    for block in content['blocks']:  # block(dict): 'number', 'type', 'bbox', 'lines'
        if block['type'] != 0:  # not a text block
            continue
        for line in block['lines']:  # line(dict): 'spans', 'wmode', 'dir', 'bbox'
            for span in line['spans']:  # span(dict): 'size', 'flags', 'font', 'color', 
                                        # 'ascender', 'descender', 'chars', 'origin', 'bbox'
                ascender = span['ascender']
                descender = span['descender']
                size = span['size']
                font = span['font']
                for char in span['chars']:  # char(dict): 'origin', 'bbox', 'c'
                    text.append(char['c'])
                    fonts.append(font)
                    if not shrink:
                        bboxes.append(char['bbox'])        
                    else:
                        x0, y0, x1, y1 = char['bbox']
                        y_origin = char['origin'][1]
                        y0, y1 = shink_bbox(ascender, descender, size, y0, y1, y_origin)
                        bboxes.append((x0, y0, x1, y1))
                    
    bboxes = np.array(bboxes).reshape(-1, 4) *  scale_ratio # in case bboxes is empty
    
    # adjust coordinate according to Page.rotation
    bboxes = change_bbox_coordinate_by_rotation(bboxes, page)
    if return_font:
        return bboxes, text, fonts
    else:
        return bboxes, text


def get_word_bboxes_by_dict(page:fitz.fitz.Page, scale_ratio:float=1.33333333, 
                            shrink:bool=True) -> tuple:
    # use Page.getText('dict') to get word bboxes
    text = []
    bboxes = []
    content = page.getText('dict')  # content(dict): 'width', 'height', 'blocks'
    for block in content['blocks']:  # block(dict): 'number', 'type', 'bbox', 'lines'
        if block['type'] != 0:  # not a text block
            continue
        for line in block['lines']:  # line(dict): 'spans', 'wmode', 'dir', 'bbox'
            for span in line['spans']:  # span(dict): 'size', 'flags', 'font', 'color', 
                                        # 'ascender', 'descender', 'chars', 'origin', 'bbox'
                ascender = span['ascender']
                descender = span['descender']
                size = span['size']
                y_origin = span['origin'][1]
                text.append(span['text'])
                if not shrink:
                    bboxes.append(span['bbox'])        
                else:
                    x0, y0, x1, y1 = span['bbox']
                    y0, y1 = shink_bbox(ascender, descender, size, y0, y1, y_origin)
                    bboxes.append((x0, y0, x1, y1))
                    
    bboxes = np.array(bboxes).reshape(-1, 4) *  scale_ratio # in case bboxes is empty
    
    # adjust coordinate according to Page.rotation
    bboxes = change_bbox_coordinate_by_rotation(bboxes, page)
    return bboxes, text


def shink_bbox(ascender:float, descender:float, size:float, y0:float, y1:float,
               y_origin:float) -> tuple:
    # shink bbox to the reduced glyph heights
    # details on https://pymupdf.readthedocs.io/en/latest/textpage.html#dictionary-structure-of-extractdict-and-extractrawdict
    if size >= y1 - y0:  # don't need to shrink
        return y0, y1
    else:
        new_y1 = y_origin - size * descender / (ascender - descender)
        new_y0 = new_y1 - size
        return new_y0, new_y1
    
    
def extract_img_and_text_in_rect(page:fitz.fitz.Page, rect:np.ndarray, scale_ratio:float=1.33333333) -> tuple:
    # given a rect area, extract img and all text within it, then ajust the 
    # coordinate to the left top of the rect area
    img = page2image(page, scale_ratio)
    bboxes, text = get_char_bboxes(page, scale_ratio, return_font=False)
    rect = rect.round().astype(np.int32)
    img = img[rect[1]: rect[3]+1, rect[0]: rect[2]+1, :]  # slice
    bboxes, bool_array = filter_bboxes_in_rect(bboxes, rect)  # filter bboxes
    text = np.array(text)[bool_array].tolist()  # filter text in bboxes
    
    # ajust coordinate to the left top of the rect area
    bboxes = bboxes - rect[[0, 1, 0, 1]]
    return img, bboxes, text