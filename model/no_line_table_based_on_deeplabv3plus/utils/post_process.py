# -*- coding: utf-8 -*-
"""
Created on Wed Dec 23 14:08:32 2020

@author: tengjunwan
"""

import numpy as np
import cv2
from itertools import combinations
from ast import literal_eval


class PostProcessor():
    def __init__(self):
        pass

    def __call__(self, img, pred_y, pred_x, ocr=None):
        """
        Args:
            img(array): 维度(3,H,W), BGR
            pred_y(2d array): [[start, end], ...]
            pred_x(2d array): [[start, end], ...]
            ocr:
        return:
            cells(numpy array):维度(#cells, 8),每一行为[x,y,w,h,c_s,r_s,c_e,r_e],
                x,y表示cell坐上角的坐标,w,h表示cell的宽高,c_s,c_e表示cell的起始和
                中止的column序号(0开始),r_s,r_e同理
            g
        """
        if ocr is None:
            cells = self.process_with_img(img, pred_y, pred_x)
        else:
            cells = self.process_with_ocr(img, pred_y, pred_x, ocr)

        return cells
    
    def process_with_img(self, img, pred_y, pred_x):
        """纯图片，没有ocr辅助的的后处理
        Args:
            img(array): 维度(3,H,W), BGR
            pred_y(2d array): [[start, end], ...]
            pred_x(2d array): [[start, end], ...]
        return:
            cells(numpy array):维度(#cells, 8),每一行为[x,y,w,h,c_s,r_s,c_e,r_e],
                x,y表示cell坐上角的坐标,w,h表示cell的宽高,c_s,c_e表示cell的起始和
                中止的column序号(0开始),r_s,r_e同理
        """
        # 获取线框坐标，得到表格基本cells矩阵
        row_lines = sep_to_line(pred_y, img.shape[0])
        column_lines = sep_to_line(pred_x, img.shape[1])
        cell_matrix = form_cell_matrix(row_lines, column_lines)  # [rows, cols, 4], 4 means xywh
        
        # 判断separator是否穿过文本
        text_mask = get_text_mask(img)
        r_sep_matrix, c_sep_matrix = form_sep_matrix(pred_y, pred_x, img.shape[:2])  # (,,4), 4 means xywh

        r_mark_matrix = text_overlap(text_mask, r_sep_matrix, direction="R")  # 0,1矩阵 1表示需要去除的sep
        c_mark_matrix = text_overlap(text_mask, c_sep_matrix, direction="C")  # 0,1矩阵 1表示需要去除的sep
                                                      
        # 进行cells merge
        cells = merge_cells(cell_matrix, r_mark_matrix, c_mark_matrix)
        
        return cells
    
    def process_with_ocr(self, img, pred_y, pred_x, ocr):
        # todo
        pass


def get_cells_text(grid, content_coor, content):
    """通过文字的坐标和表格grid的坐标，返回各个grid对应的文字string,以及各个grid中
    文字的区域
    Arg:
        grid(np array):维度(#grid, 4)，坐标型式xywh
        content_coor(np array): 维度(#content, 4)，坐标型式xywh
        content(list of string): 长度= #content, 一般是单个字符，例如'他'，'1'
    return:
        grid_region(np array):维度(#grid, 4),坐标形式yxyx，没有内容的grid的region
            为-1,-1,-1,-1
        grid_string(list of string): 长度= #grid,没有内容的grid对应的stirng为''
    """
    assert len(content_coor) == len(content), "something wrong about ocr file"
    grid = xywh2yxyx(grid)  # xywh 变为yxyx
    content_coor = xywh2yxyx(content_coor)  # xywh 变为yxyx
    
    content =np.array(content)  # list of str转化为array型式
    center = (content_coor[:, [0, 1]] + content_coor[:, [2, 3]]) / 2.0  # (#content, yx)
    center_ = np.concatenate((center*(-1), center), axis=1)  # (#content, -y-xyx)
    center_ = np.expand_dims(center_, axis=0)  # (1, #grid, -y-xyx)
    grid_ = grid * np.array([[-1, -1, 1, 1]])  # (#grid, -y-xyx)
    grid_ = np.expand_dims(grid_, axis=1)  # (#grid, 1, -y-xyx)
    
    belong_matrix = (grid_ >= center_).all(axis=2)  #  (#grid, #content), boolean matrix
    # 初始化
    grid_region = np.ones((len(grid), 4)) * (-1)  # 每个grid的内容总区域坐标(初始化标记为-1)
    grid_string = ['' for _ in range(len(grid))]  # 每个grid的内容string
    # 遍历每一个grid
    for i, g in enumerate(belong_matrix):
        g_content_coor = content_coor[g]  # (#content in g, yxyx)
        if len(g_content_coor) > 0:  # 如果有内容
            # 未排序的content array
            grid_content_raw = content[g]
            
            # part.1:求得文字区域
            ymax = np.amax(g_content_coor[:, 2])
            ymin = np.amin(g_content_coor[:, 0])
            xmax = np.amax(g_content_coor[:, 3])
            xmin = np.amin(g_content_coor[:, 1])
            grid_region[i] = [ymin, xmin, ymax, xmax]  # yxyx
            
            # part.2:求得排序过后的内容string
            # 取得y x上的unique values(容许浮动)
            y_values = get_values(g_content_coor[:, 2], tolerance=4)
            x_values = get_values(g_content_coor[:, 3], tolerance=1)
            # 取得y,x对应的index
            y_matrix = np.expand_dims(g_content_coor[:, 2], axis=0) - np.expand_dims(y_values, axis=1) # (#y_values, #y)
            y_index = np.abs(y_matrix).argmin(axis=0)  # (#y), 例如[0,0,1]
            x_matrix = np.expand_dims(g_content_coor[:, 3], axis=0) - np.expand_dims(x_values, axis=1) # (#x_values, #x)
            x_index = np.abs(x_matrix).argmin(axis=0)  # (#y), 例如[0,1,0]
            # 排序,获得排序过的string list
            index = y_index * 100000 + x_index  # 优先考虑y_index，再考虑x_index
            grid_content = grid_content_raw[index.argsort()].tolist()
            # 插入‘换行符’
            nums_of_newline = np.amax(y_index)  # 例如1
            newline_index = []
            for j in range(nums_of_newline):
                newline_index.append(np.sum(y_index <= j))
            for j in reversed(newline_index):  # 反向遍历插入
                grid_content.insert(j, '\n')
            # 合成list of str 为一个str
            grid_string[i] = ''.join(grid_content)
        else:
            pass
            
    return grid_region, grid_string


#=========helper funcions=======
def name_analyze(img_name):
    """ 图片命名规则是docId_pageIndex_index_procedFrame.png
    例如：36263_123_3_[80, 462, 646, 414].png
    """
    blocks = img_name.split('.')[0].split('_')
    docid = int(blocks[0])
    page = int(blocks[1])
    frame = literal_eval(blocks[3])
    
    return docid, page, frame

    
def sep_to_line(pred, size):
    """得到表格的线框坐标,例如一个边长为10：
    [[3,5]] → [0, 4, 9]
    Args:
        pred(2d array): [[start, end], ...]
        size
    return:
        midpoint(1d array): 取每一个[start, end]的平均值作为表格线框坐标，并补齐
            边框。
    """
    if len(pred) == 0:  # []
        midpoint = np.zeros(0)
    else:
        midpoint = (pred[:, 0] + pred[:, 1])/2.0       
    
    # 补加外框，宽度为1
    midpoint = np.insert(midpoint, 0, 0)
    midpoint = np.append(midpoint, size-1)
    
    return midpoint


def form_cell_matrix(row_lines, column_lines):
    """从row和column形成cell矩阵
    Args:
        row_lines(1d array):记录包括边框在内的线的行坐标(y)
        column_lines(1d array):记录包括边框在内的线的列坐标(x)
    return:
        cell_matrix(3d array):维度为(#rows, #cols, 4),第三维放每个cell的坐标(x,y,w,h)
    """
    xv, yv = np.meshgrid(column_lines[: -1], row_lines[: -1])  # 维度(rows, cols)
    wv, hv = np.meshgrid(np.diff(column_lines), np.diff(row_lines))  # 维度(rows, cols)
    cell_matrix = np.stack((xv, yv, wv, hv), axis=2)
    return cell_matrix


def form_sep_matrix(pred_y, pred_x, size):
    """形成separator矩阵
    Args:
        pred_y(1d array):
        pred_x(1d array):
        size(tuple of ints): (h, w)
    return:
        r_sep_matrix(3d array): 维度(#row -1, #cols, 4),4表示为separator的xywh
        c_sep_matrix(3d array): 维度(#row, #cols -1, 4),4表示为separator的xywh
    """
    h, w = size
    # r_sep_matrix
    r_sep_xv, r_sep_yv = np.meshgrid(np.append(0, pred_x[:, 1]), pred_y[:, 0])
    r_sep_wv, r_sep_hv = np.meshgrid(np.append(pred_x[:, 0], w-1) - np.append(0, pred_x[:, 1]),
                                     pred_y[:, 1] - pred_y[:, 0])
    r_sep_matrix = np.stack((r_sep_xv, r_sep_yv, r_sep_wv, r_sep_hv), axis=2)
    # c_sep_matrix
    c_sep_xv, c_sep_yv = np.meshgrid(pred_x[:, 0], np.append(0, pred_y[:, 1]))
    c_sep_wv, c_sep_hv = np.meshgrid(pred_x[:, 1] - pred_x[:, 0],
                                     np.append(pred_y[:, 0], h-1) - np.append(0, pred_y[:, 1]))
    c_sep_matrix = np.stack((c_sep_xv, c_sep_yv, c_sep_wv, c_sep_hv), axis=2)
    
    # debug
#    debug = True
#    if debug:
#        img = np.zeros((h, w, 3)).astype(np.uint8)
#        for i in range(c_sep_matrix.shape[0]):
#            for j in range(c_sep_matrix.shape[1]):
#                x, y, w, h = c_sep_matrix[i, j]
#                img[int(y):int(y+h)+1, int(x):int(x+w)+1] = (0, 0 , 255)
#        cv2.imshow('c', img) 
#        cv2.waitKey(0)
    
    
    return r_sep_matrix, c_sep_matrix
    
    
def cv_imread(filePath):
    """读中文名图"""
    cv_img = cv2.imdecode(np.fromfile(filePath, dtype=np.uint8), -1)
    return cv_img
    

def get_text_mask(img, thresh=200):
    """简单二值化，并去除无线表格中的长线，得到01mask，其中0表示背景，1表示前景"""
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY) 
    _, img = cv2.threshold(img, thresh, 255, cv2.THRESH_BINARY)
    img = np.logical_not(img).astype(np.uint8)
    
    # 去残留线
    h_kernel_size = 25 
    v_kernel_size=21
    h_kernel = np.ones((1, h_kernel_size))
    h_mask = cv2.erode(img, h_kernel,iterations = 1)
    h_mask = cv2.dilate(h_mask, h_kernel,iterations = 1)
    v_kernel = np.ones((v_kernel_size, 1))
    v_mask = cv2.erode(img, v_kernel, iterations = 1)
    v_mask = cv2.dilate(v_mask, v_kernel, iterations = 1)
    line_mask = np.logical_or(h_mask, v_mask).astype(np.uint8)
    img_text_mask = img - line_mask 
        
    return img_text_mask  # np.uint8


def text_overlap(text_mask, sep_matrix, direction):
    """"""
    
    def if_overlap(area, direction):
        """判断条件：
        1.相对overlap，相对重叠区域中像素点占“宽”或者“长”的比例
        2.绝对overlap，区域中总像素点，主要为了排除标点符号的影响
        3.细长sep区域，采取相对严格的标准
        Args:
            area(2d array): 0,1矩阵，0表示背景，1表示前景
            direction(str): 表示area所属的separator的方向
        return
            result(3 floats): （是否重叠, 相对overlap,绝对overlap）
        """
        if direction == "R":
            axis = 1
        elif direction == "C":
            axis = 0
        else:
            pass

        rel_overlap = area.any(axis).sum() / area.shape[1-axis]
        abs_overlap = area.sum()
        
        if area.shape[1-axis] <= 5 and rel_overlap < 1:
            return 0
        elif area.shape[1-axis] <= 5 and rel_overlap == 1:
            return 1
        elif area.shape[1-axis] > 5 and abs_overlap < 15:
            return 0
        elif area.shape[1-axis] > 5 and rel_overlap <= 0.5:
            return 0
        else:
            return 1

    
    # 初始化标记为0   
    mask_matrix = np.zeros(sep_matrix.shape[:2])
    # 遍历separator区域，判断是否与文字overlap
    for i in range(mask_matrix.shape[0]):
        for j in range(mask_matrix.shape[1]):            
            s = sep_matrix[i, j].round().astype(np.int32)  # (4, ), xywh                                                  
            mask_matrix[i, j] = if_overlap(text_mask[s[1]: s[1] + s[3] + 1,
                                                     s[0]: s[0] + s[2] + 1],
                                           direction=direction)

    return mask_matrix
    

def merge_cells(cell_matrix, r_mark_matrix, c_mark_matrix):
    """根据separator区域合并，这种合并是需要进行L形状考虑，即不允许出现L型区域，而
    会进一步合并为口型区域，其中涉及到span，span是需要融合的cells所在的行和列的范围
    (min_row, min_col, max_row, max_col)
    Args:
        cell_matrix(3d array): (rows, cols, 4), 4表示xywh
        r_line_mark
    return:
        cells:[{"coor":[start_column, start_row, end_column, end_row]
                "range":[x, y, w, h]}]
    """
    def overlapped_area(span_a, span_b):
    #area_a, area_b是两个numpy 1d array, [ymin,xmin,ymax,xmax]
        span_a += np.array([0, 0, 1, 1])
        span_b += np.array([0, 0, 1, 1])
        tl = np.maximum(span_a[:2], span_b[:2])  
        br = np.minimum(span_a[2:], span_b[2:])
        area = np.prod(br - tl) * (tl < br).all()
        return area
    
    
    def merge_2_span(span_a, span_b):
        ymin = min(span_a[0], span_b[0])
        xmin = min(span_a[1], span_b[1])
        ymax = max(span_a[2], span_b[2])
        xmax = max(span_a[3], span_b[3])
        return [ymin, xmin, ymax, xmax]
    
    
    col_index = np.argwhere(c_mark_matrix==1)
    row_index = np.argwhere(r_mark_matrix==1)
    spans = []  
    for idx in col_index:  
        min_row = idx[0]       
        min_col = idx[1]
        max_row = idx[0]
        max_col = idx[1] + 1
        span = [min_row, min_col, max_row, max_col]
        spans.append(span)
    for idx in row_index:
        min_row = idx[0]       
        min_col = idx[1]
        max_row = idx[0] + 1
        max_col = idx[1] 
        span = [min_row, min_col, max_row, max_col]
        spans.append(span)
    
    # 合并到没有span是相互重叠为止
    while len(spans) > 1:
        size = len(spans)
        pairs = combinations(range(size), 2) 
        flag = 0
        for pair in pairs:
            span_a = spans[pair[0]]
            span_b = spans[pair[1]]
            area = overlapped_area(span_a, span_b)
            if area > 0:
                # 去除span_a, span_b,补充合并区域
                del spans[pair[1]]
                del spans[pair[0]]
                spans.append(merge_2_span(span_a, span_b))
                flag = 1
                break
        if flag == 0:
            break

    # 合并的cells
    merged_cells = []
    mask_matrix = np.ones((cell_matrix.shape[0], cell_matrix.shape[1]))  # 导出的cell标记为0
    for span in spans:
        x = cell_matrix[span[0], span[1]][0]
        y = cell_matrix[span[0], span[1]][1]
        w = np.sum(cell_matrix[span[0], span[1]:span[3]+1, 2])
        h = np.sum(cell_matrix[span[0]:span[2]+1, span[1], 3])
        c_s = span[1]
        c_e = span[3] + 1
        r_s = span[0] 
        r_e = span[2] + 1
        mask_matrix[span[0]: span[2]+1, span[1]: span[3]+1] = 0
        merged_cells.append([x, y, w, h, c_s, r_s, c_e, r_e])
    merged_cells = np.array(merged_cells)
    # 导出未合并的cells
    shape = cell_matrix[mask_matrix == 1]  # xywh
    coor = np.argwhere(mask_matrix==1)[:, [1,0]] # c_s, r_s
    left_cells = np.concatenate((shape, coor, coor+1), axis=1)
    
    if len(merged_cells) >= 1:
        cells = np.concatenate((merged_cells, left_cells), axis=0)
    else:
        cells = left_cells
    
    return cells   
            

def get_values(values, tolerance):
    """从一组values（一维向量）中得到该向量中的unique values with tolerance.
    例如[1,1,2,2,9,10,15], tolerance=1
    返回[1,9,15]
    """
    values = np.unique(values)
    
    diff = np.diff(values)  # 相邻像之差
    arg = diff >= tolerance  # 筛选差大于阈值为True
    arg = np.append(True, arg)  # 永远保留首项
    values = values[arg]
    
    return values            
                

def xywh2yxyx(boxes):
    # 坐标型式从xywh变为yxyx
    b = np.zeros_like(boxes)
    b[:, 0] = boxes[:, 1]
    b[:, 1] = boxes[:, 0]
    b[:, 2] = boxes[:, 1] + boxes[:, 3]
    b[:, 3] = boxes[:, 0] + boxes[:, 2]
    return b


if __name__ == "__main__":
    pass