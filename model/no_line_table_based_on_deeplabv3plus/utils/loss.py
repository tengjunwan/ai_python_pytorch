# -*- coding: utf-8 -*-
"""
Created on Mon Dec 21 18:02:32 2020

@author: tengjunwan
"""

import torch
import torch.nn as nn


def compute_loss(p, targets, hyp, mode='original'):
    """可采用三种方式计算loss:
        1.单纯的bce
        2.文章采用的clip的处理方式，即如果预测概率pred>0.9(gt=1)或者pred<0.1(gt=0)
         ,则loss为0
        3.采用label smoothing
    Args:
        p(2 lists of tensor):[r_preds, c_preds], 维度为(#batch, H)和(#batch, W)
        targets(2 tensor): (r_gt, c_gt), 维度为(#batch, H)和(#batch, W)
        hyp(dict): 超参
        mode(str):控制计算loss的方式
    return:
        loss(tensor): 维度为0,模型的总loss,requires_grad=True
        loss_items(tensor): tensor([row_loss, col_loss, total_loss])
    """
    assert mode in ['original', 'clip', 'smooth'], "mode type is not considered."
    losses = []  # 
    red = 'mean'  # 所有计算bce元素上取平均
    # 选择计算loss的方式
    if mode == 'clip':
        # todo
        pass
    elif mode == 'smooth':
        # todo
        pass
    elif mode ==  'original':
        criterion = nn.BCEWithLogitsLoss(reduction=red)
    else:
        pass
    for i in range(len(p)): 
        loss = criterion(p[i], targets[i]) * hyp['loss_weight'][i]
        losses.append(loss)
        
    
    loss = torch.sum(torch.stack(losses))
    losses.append(loss)  # total loss at last
    
    return loss, torch.stack(losses).detach()


#def bce_clip(pred, gt, red):
#    """这里的计算是利用cross entropy，但是文章说，防止过拟合，采取了cliping，即预
#    测的概率在0.1或者0.9(分别对应0，1)，那么就认为loss为0
#    Arg:
#        pred(tensor): 维度(#batch, H), 模型预测 例如[[0.1, 0.1, 0.1, 0.8]]
#        gt(tensor): 维度(#batch, H,), label数据 例如[[0, 0, 0, 1]]
#        red(str): reduction
#    return:
#        loss(tensor):scalar
#    """
#    # 针对0的mask
#    pred_clone = pred.clone()
#    mask_0 = torch.mul(gt==0, pred_clone < 0.1)
#    # 针对1的mask
#    mask_1 = torch.mul(gt==1, pred_clone > 0.9) 
#    pred_clone[mask_0] = 0
#    pred_clone[mask_1] = 1
#    loss = F.binary_cross_entropy(pred_clone, gt, reduction=red)
#
#    return loss
#
#def bce_org(pred, gt, red):
#    """标准的bce"""
#    return F.binary_cross_entropy(pred, gt, reduction=red)
#
#
#def bce_smooth(pred, gt, red, eps=0.1):
#    """smooth labeling的bce
#    Arg:
#        pred(tensor): 维度(#batch, H), 模型预测 例如[[0.1, 0.1, 0.1, 0.8]]
#        gt(tensor): 维度(#batch, H), label数据 例如[[0, 0, 0, 1]]
#    return:
#        loss(tensor):scalar
#    """
#    cp, cn = 1.0 - 0.5 * eps, 0.5 * eps  # return positive, negative label smoothing BCE targets
#    gt_clone = torch.full_like(gt, cn)
#    gt_clone[gt==1] = cp
#    return F.binary_cross_entropy(pred, gt_clone, reduction=red)