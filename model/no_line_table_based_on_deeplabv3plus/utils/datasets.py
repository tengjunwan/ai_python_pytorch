# -*- coding: utf-8 -*-
"""
Created on Fri Dec 18 19:07:26 2020

@author: tengjunwan
"""

from ast import literal_eval
import pandas as pd
from torch.utils.data import Dataset
import torch 
from torchvision import transforms
import cv2
import numpy as np
from model.no_line_table_based_on_deeplabv3plus.utils.utils import augment_hsv
import torch.nn.functional as F
from model.no_line_table_based_on_deeplabv3plus.utils.utils import range2point
from tqdm import tqdm
from pathlib import Path
import json
from model.no_line_table_based_on_deeplabv3plus.utils.utils import cv_imread
import fitz


class LoadImageAndLabel(Dataset):
    """dataset for training
    """
    def __init__(self, csv_file, image_dir, hyp, cache_ocr=False, 
                 hsv_augment=False, multi_scale=False, crop=False, 
                 normalize=False,):
        """
        Args:
            csv_file(str):包含图片名的文件
            image_dir(str):图片所在文件夹
            hyp(dict):超参
            cache_ocr(bool): 是否缓存图片ocr结果
            hsv_augment(bool): 是否进行色彩增强
            multi_scale(bool):是否缩放
            crop(bool): 是否裁剪  # todo
            normalize(bool):是否用统计值u=[0.485, 0.456, 0.406],σ=[0.229, 0.224, 0.225]
                进行normalize
        """
        self.csv_file = pd.read_csv(csv_file, encoding="gb2312")  
        self.image_dir = image_dir
        self.total_nums_of_pics = len(self.csv_file)
        self.hsv_augment = hsv_augment
        self.hyp = hyp
        self.multi_scale = multi_scale
        self.normalize = normalize
        
        # 缓存ocr结果,用于评估时候判断预测的正确cell
        self.ocr = None
        if cache_ocr:
            self.ocr = {}
            ocr_dir = Path('data') / 'ocr'
            print('loading ocr content from: %s' % str(ocr_dir))
            s, m = 0, 0
            pbar = tqdm(range(len(self.csv_file)))
            for i in pbar:
                ocr_file = Path(self.csv_file.iloc[i][0]).stem + '.json'
                ocr_path = ocr_dir / ocr_file
                
                try:
                    with open(str(ocr_path), 'r', encoding='utf-8') as f:
                        ocr = json.loads(f.read(), encoding='utf-8')
                    self.ocr.update(ocr)
                    s += 1
                except:
                    m += 1
                pbar.desc = ('ocr files: %g cached successfully, %g missing' % (s, m))
                
    def __len__(self):
        return self.total_nums_of_pics
    
    def __getitem__(self, idx):
        """
        return:
            img_name(str): 图片名称
            img_tensor(tensor): 图片tensor
            rows(array): 行方向的label, [[start, end], [start, end],...]
            columns(array): 列方向的label, [[start, end], [start, end],...]
            shapes(list of tuple): 
        """
        data = self.csv_file.iloc[idx]  # csv文件的一行数据
        img_name = data[0]  # 文件名
        path = Path(self.image_dir) / img_name
        
        try:
            img0 = cv_imread(str(path))  # bgr
            h0, w0 = img0.shape[: 2]
        except:
            raise IndexError("missing file path: %s" % str(path))
        
        # 色彩增强
        if self.hsv_augment:
            augment_hsv(img0, 
                        hgain=self.hyp['hsv_h'],
                        sgain=self.hyp['hsv_s'], 
                        vgain=self.hyp['hsv_v'])
        
        # label处理
        rows = np.array(literal_eval(data[1]), dtype=np.float32)  # [[start, end], [start, end],...]
        columns = np.array(literal_eval(data[2]), dtype=np.float32)  # [[start, end], [start, end],...]
        
        
        if self.multi_scale:  # 3/4-4/3倍缩放(0.5倍概率)
            min_ratio, max_ratio = self.hyp['scale']
            ratio = np.random.uniform(min_ratio, max_ratio)
            # 放缩图片
            w, h =  int(w0 * ratio), int(h0 * ratio)
            interp = cv2.INTER_AREA if ratio < 1 else cv2.INTER_LINEAR  # https://stackoverflow.com/questions/23853632/which-kind-of-interpolation-best-for-resizing-image
            img = cv2.resize(img0, (w, h), interpolation=interp)
            # 放缩label
            rows = np.around(rows * (h / h0))
            columns = np.around(columns * (w / w0))
        else:
            img = img0
            h, w = h0, w0
            
        # to tensor
        img_tensor = transforms.ToTensor()(img)  # (3, h, w)
        
        # normalize
        if self.normalize:
            # 注意是bgr模式下,和网上提供的rgb参数要稍微修改下
            img_tensor = transforms.Normalize([0.456, 0.485, 0.406],
                                              [0.224, 0.229, 0.225])(img_tensor)
        
        # 原图和实际输入图片的尺寸信息
        shape = [(h0, w0), (h, w)]  
        
        # segmap
        segmap = torch.zeros(h, w)
        for r_s, r_e in rows.astype(np.int32):
            segmap[r_s:r_e+1, :] = 1
        for c_s, c_e in columns.astype(np.int32):
            segmap[:, c_s: c_e+1] = 1
        
        return  img_name, img_tensor, rows, columns, shape, segmap
        
    @staticmethod
    def collate_fn(batch):
        # 动态padding到该batch的最大size(max length and max width)
        img_names, img_tensors, row_lists, column_lists, shapes, segmaps = zip(*batch)

        # 找到最大尺寸
        sizes = np.array([s[1] for s in shapes])
        h_max, w_max = np.amax(sizes, axis=0)
        
        img_tensors_padded = []
        rows = []
        columns = []
        paddings = []
        segmaps_padded = []
        for j, img_tensor in enumerate(img_tensors):
            # 图像padding
            dh = h_max - img_tensor.shape[1]
            dw = w_max - img_tensor.shape[2]
            dh /= 2  # divide padding into 2 sides
            dw /= 2  
            top, bottom = int(round(dh - 0.1)), int(round(dh + 0.1))
            left, right = int(round(dw - 0.1)), int(round(dw + 0.1))
            paddings.append((top, bottom, left, right))
            img_tensors_padded.append(F.pad(img_tensor, 
                                           (left, right, top, bottom), 
                                           "constant", 0.5))
            # label padding
            rows.append(torch.from_numpy(range2point(h_max, row_lists[j] + top)))
            columns.append(torch.from_numpy(range2point(w_max, column_lists[j] + left)))
            
            # seg map padding
            segmaps_padded.append(F.pad(segmaps[j], 
                                           (left, right, top, bottom), 
                                           "constant", 0.0))
            
            
        imgs = torch.stack(img_tensors_padded, 0)  # (b, 3, H, W)
        rows = torch.stack(rows, 0).unsqueeze(axis=1)  # (b, 1, H)
        cols = torch.stack(columns, 0).unsqueeze(axis=1)  # (b, 1, W)
        segmaps = torch.stack(segmaps_padded, 0).unsqueeze(axis=1)  # (b, 1, W, H)
             
        return imgs, rows, cols, img_names, shapes, paddings, segmaps
    
    
class LoadImage(Dataset):  
    """dataset for detect
    """
    def __init__(self, image_dir, hsv_augment=False, multi_scale=False, normalize=False):
        """
        Args:
            image_dir(str):图片所在文件夹
            hsv_augment(bool): 是否进行色彩增强
            multi_scale(bool):是否缩放
            normalize(bool):是否用统计值u=[0.485, 0.456, 0.406],σ=[0.229, 0.224, 0.225]
                进行normalize
        """
        image_format = ['.png', '.jpg', '.jepg', '.PNG', '.JPG', '.JEPG']
        self.image_dir = image_dir
        self.image_paths = [x for x in Path(image_dir).glob('*') if x.suffix in image_format]
        self.total_nums_of_pics = len(self.image_paths)
        self.hsv_augment = hsv_augment
        self.multi_scale = multi_scale
        self.normalize = normalize
                
    def __len__(self):
        return self.total_nums_of_pics
    
    def __getitem__(self, idx):
        """
        return:
            img_path(str): 图片名称
            img_tensor(tensor): 图片tensor
            img(numpy asrray): 图片(bgr)
            shapes(list of tuple): [(h0, w0), (h, w)]  
        """
        # Load image
        img_path = self.image_paths[idx]  # csv文件的一行数据
        img0 = cv_imread(str(img_path))  # bgr
        h0, w0 = img0.shape[: 2]
        
        # 色彩增强
        if self.hsv_augment:
            # config
            hsv_h = 0.0138  # image HSV-Hue augmentation (fraction)
            hsv_s = 0.678  # image HSV-Saturation augmentation (fraction)
            hsv_v = 0.36  # image HSV-Value augmentation (fraction)
            augment_hsv(img0, 
                        hgain=hsv_h,
                        sgain=hsv_s, 
                        vgain=hsv_v)

        # 多尺度
        if self.multi_scale:  # 3/4-4/3倍缩放(0.5倍概率)
            min_ratio, max_ratio = 3/4, 4/3
            ratio = np.random.uniform(min_ratio, max_ratio)
            # 放缩图片
            w, h =  int(w0 * ratio), int(h0 * ratio)
            interp = cv2.INTER_AREA if ratio < 1 else cv2.INTER_LINEAR  # https://stackoverflow.com/questions/23853632/which-kind-of-interpolation-best-for-resizing-image
            img = cv2.resize(img0, (w, h), interpolation=interp)
        else:
            img = img0
            h, w = h0, w0
            
        # to tensor
        img_tensor = transforms.ToTensor()(img)  # (3, h, w)
        
        # normalize
        if self.normalize:
            # 注意是bgr模式下,和网上提供的rgb参数要稍微修改下
            img_tensor = transforms.Normalize([0.456, 0.485, 0.406],
                                              [0.224, 0.229, 0.225])(img_tensor)
        
        # 原图和实际输入图片的尺寸信息
        shape = [(h0, w0), (h, w)]  
        
        return str(img_path), img, img_tensor, shape
    
    
class LoadImage_V2(Dataset):  
    """dataset for detect
    """
    def __init__(self, imgs, hsv_augment=False, multi_scale=False, normalize=False):
        """
        Args:
            imgs(list):包含bgr格式的图片list
            hsv_augment(bool): 是否进行色彩增强
            multi_scale(bool):是否缩放
            normalize(bool):是否用统计值u=[0.485, 0.456, 0.406],σ=[0.229, 0.224, 0.225]
                进行normalize
        """
        self.imgs = imgs
        self.total_nums_of_pics = len(imgs)
        self.hsv_augment = hsv_augment
        self.multi_scale = multi_scale
        self.normalize = normalize
                
    def __len__(self):
        return self.total_nums_of_pics
    
    def __getitem__(self, idx):
        """
        return:
            img_path(str): 图片名称
            img_tensor(tensor): 图片tensor
            img(numpy asrray): 图片(bgr)
            shapes(list of tuple): [(h0, w0), (h, w)]  
        """
        # Load image
        img0 = self.imgs[idx]  # bgr
        h0, w0 = img0.shape[: 2]
        
        # 色彩增强
        if self.hsv_augment:
            # config
            hsv_h = 0.0138  # image HSV-Hue augmentation (fraction)
            hsv_s = 0.678  # image HSV-Saturation augmentation (fraction)
            hsv_v = 0.36  # image HSV-Value augmentation (fraction)
            augment_hsv(img0, 
                        hgain=hsv_h,
                        sgain=hsv_s, 
                        vgain=hsv_v)

        # 多尺度
        if self.multi_scale:  # 3/4-4/3倍缩放(0.5倍概率)
            min_ratio, max_ratio = 3/4, 4/3
            ratio = np.random.uniform(min_ratio, max_ratio)
            # 放缩图片
            w, h =  int(w0 * ratio), int(h0 * ratio)
            interp = cv2.INTER_AREA if ratio < 1 else cv2.INTER_LINEAR  # https://stackoverflow.com/questions/23853632/which-kind-of-interpolation-best-for-resizing-image
            img = cv2.resize(img0, (w, h), interpolation=interp)
        else:
            img = img0
            h, w = h0, w0
            
        # to tensor
        img_tensor = transforms.ToTensor()(img)  # (3, h, w)
        
        # normalize
        if self.normalize:
            # 注意是bgr模式下,和网上提供的rgb参数要稍微修改下
            img_tensor = transforms.Normalize([0.456, 0.485, 0.406],
                                              [0.224, 0.229, 0.225])(img_tensor)
        
        # 原图和实际输入图片的尺寸信息
        shape = [(h0, w0), (h, w)]  
        
        return img_tensor, shape