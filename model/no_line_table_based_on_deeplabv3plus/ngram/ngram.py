# -*- coding: utf-8 -*-
"""
Created on Fri Jun  4 16:26:03 2021

@author: EDZ
"""

from collections import defaultdict
from math import log
from tqdm import tqdm
from random import random
import pickle


class NGram():
    def __init__(self, n:int, sents:list, vocab:set=None):
        """
        Args:
            n(int): order of the model.
            sents(list): list of sentences, each one being a list of tokens.
            vocab(set): a given vocabulary, if None, generating one by sents.
        """
        assert n > 0
        self.n = n
        
        if vocab is None:
            self.vocab = self._get_vocab(sents)
        else:
            self.vocab = set(vocab)
            
        sents = self._add_symbol_and_unk(sents)
        self.counts = defaultdict(int)
        # count 
        for sent in sents:
            for i in range(len(sent) - n + 1):
                ngram = tuple(sent[i: i + n])  # list is unhashable
                self.counts[ngram] += 1  
                # sum of each row of the count matrix, and we don't need count 
                # last </s> * (n-1) of each sentence, cause there is no count 
                # for </s>*(n-1)+token.
                self.counts[ngram[:-1]] += 1  
                
    def _get_vocab(self, sents):
        """Return a vocabulary based on the sents.The algorithm will treat words
        as <unk> of which feqencies are below a given threshold.
        Args:
            sents(list): list of sents
        return:
            vocab(set): vocabulary includes "</s>" and "<unk>"
        """
        # config
        thresh_n = 1 
        vocab = ['<unk>', '</s>']
        
        # count
        print('counting words to generate vocabulary')
        count_dict = defaultdict(int)
        for sent in tqdm(sents):
            for word in sent:
                count_dict[word] += 1
        
        n_unk = 0
        for word, count in count_dict.items():
            if count > thresh_n:
                vocab.append(word)
            else:
                n_unk += 1
        
        print('%d out of %d words are treated as <unk>' % (n_unk, len(count_dict)))
            
        return set(vocab)
    
    def _add_symbol_and_unk(self, sents:list) -> list:
        """Add n-1 * <s> and 1 * </s>, and substitude words out of vocabulary 
        with <unk>.
        The reason why we just need only 1 </s> instead of n-1 </s> is that we
        don't need to count probability like p(</s>|wi-2</s>), once we reach
        </s> the sentence is over, for example Ngram(n=3):
            
            ∵
                p(wi|wi-2</s>) = 0 if wi != </s>,
                p(wi|wi-2</s>) = 1 if wi == </s>
            ∴
                p(<s><s>w0w1...wn</s></s>) = p(<s><s>w0w1...wn</s>)
                
        One more thing about when n=1. We still add </s> to the end of each 
        sentence, so we can stop if we want to infer from a given few words
        (avoid infering a sentence of infinite length). 
        
        Args:
            sents(list): list of sentences.
        return:
            sents_processed(list): list of sentences 
        """
        start_s = ['<s>'] * (self.n - 1)
        end_s = ['</s>'] * (1)
        sents_processed = []
        for sent in sents:
            sent_processed = []
            for token in sent:
                if token in self.vocab:
                    sent_processed.append(token)
                else:
                    sent_processed.append("<unk>")
            sents_processed.append(start_s + sent_processed + end_s)
            
        return sents_processed
    
    def count(self, tokens:tuple) -> int:
        """Return the count of given tokens.
        tokens(tuple)
        """
        return self.counts[tokens]
    
    def cond_prob(self, token:str, prev_tokens:tuple=None) -> float:
        """Conditional probability of a token.
        Args:
            token(str): the token
            prev_tokens(tuple): the preivious n-1 token(None if n-gram is unigram)
        """
        if not prev_tokens:
            assert self.n == 1
            prev_tokens = tuple()
        tokens_count = self.count(prev_tokens + (token,))
        if tokens_count == 0:  # early return to avoid prev_count is 0
            return 0
        
        prev_count = self.count(tuple(prev_tokens))
        
        return tokens_count / float(prev_count)
    
    def sent_prob(self, sent:list) -> float:
        """Probability of a given sentence.
        Args:
            sent(list): a list of tokens
        """
        prob = 1.0
        # add symbol
        sent = self._add_symbol_and_unk([sent])[0]
        
        for i in range(self.n-1, len(sent)):
            prob *= self.cond_prob(sent[i], tuple(sent[i-self.n+1:i]))
            if not prob:  # already reach 0, early break
                break
            
        return prob
    
    def sent_log_prob(self, sent:list) -> float:
        """Log-probability of a given sentence.
        Args:
            sent(list): a list of tokens
        """
        prob = 0
        # add symbol
        sent = self._add_symbol_and_unk([sent])[0]
        
        for i in range(self.n-1, len(sent)):  
            c_p = self.cond_prob(sent[i], tuple(sent[i-self.n+1:i]))
            # to catch a math error
            if not c_p:  # reach 0, the log probability reaches negative infinite
                return float('-inf')
            prob += log(c_p, 2)  # log base is 2
            
        return prob
    
    def perplexity(self, sents:list) -> float:
        """Perplexity of a model.
        sents(list): the test corpus as a list of sents
        """
        # total words unseen
        m = 0  
        for sent in sents:
            m += len(sent)
        # cross-entropy
        l = 0
        for sent in sents:
            l += self.sent_log_prob(sent) / m
            
        return pow(2, -l)
    
    def get_special_param(self):
        return None, None
    
    def get_vocab_size(self):
        """Size of the vocabulary.
        """
        return len(self.vocab)
    
    
class AddOneNGram(NGram):
    """Laplace smoothing"""
    def __init__(self, n:int, sents:list, vocab:set=None):
        super(self, AddOneNGram).__init__(n, sents, vocab)  # get count
        
    def cond_prob(self, token:str, prev_tokens:tuple=None) -> float:
        """Conditional probability of a token.
        Args:
            token(str): the token
            prev_tokens(list): the preivious n-1 token(None if n-gram is unigram)
        """
        if not prev_tokens:
            assert self.n == 1
            prev_tokens = tuple()
            
        tokens_count = self.count(tuple(prev_tokens) + tuple(token,)) + 1
        prev_count = self.count(tuple(prev_tokens)) + self.get_vocab_size()
        
        return tokens_count / float(prev_count)
    
   
class KneserNeyGram(NGram):
    # reference: https://web.stanford.edu/~jurafsky/slp3/3.pdf
    def __init__(self, n:int=3, d:float=0.75, sents:list=[], vocab:set=None, 
                 load_path:str=None):
        """
        Args:
            n(int): highest order of the moder
            d(float): dicount value
            sents(list): list of sents
            vocab(set): a given vocabulary
            load_path(str): a pickle file already save the count of ngram.
        """
        if load_path is not None:
            self.load_count(load_path)
        else:  # counts 
            self.n = n  
            self.d = d  # discount value (0-1)
            if vocab is None:
                self.vocab = self._get_vocab(sents)
            else:
                self.vocab = set(vocab)
        
            # count highest order of n-gram which is absolute count
            self.counts = defaultdict(int)  
            # countination count 
            self.counts_dot_token = defaultdict(int)  
            self.counts_dot_token_dot = defaultdict(int)
            self.counts_token_dot = defaultdict(int)
            
            # count lower order of n-gram which is continuation count:
            # 'token' means one or several consecutive given words(token even can 
            # be 0 word in dot_token_dot and token_dot), 'dot' means one word that 
            # we apply continuation count to.
            dot_token = defaultdict(set)  # continuation count of *token, 
            dot_token_dot = defaultdict(set)  # continuation count of *token*
            token_dot = defaultdict(set)  # continuation count of token* for lambda caculation
            print('counting ngram with %d sentences' % len(sents))
            sents = self._add_symbol_and_unk(sents)  
            for sent in tqdm(sents):
                if n > 1:
                    # windows size = n
                    for i in range(len(sent) - n + 1):
                        ngram = tuple(sent[i: i + n]) 
                        # =======n order=======
                        # absolute count for highest order
                        self.counts[ngram] += 1  # count of n consecutive words 
                        self.counts[ngram[:-1]] += 1  # count of n-1 consecutive words
                        # for lambda term of highest order, i.e., how many 'd's is going
                        # to be distributed, or |w: C(w1...wn-1w) > 0|
                        token_dot[ngram[:-1]].add(ngram[-1:])  
                        
                        # =======n-1 order======
                        # continuation count for second highest order
                        dot_token[ngram[1:]].add(ngram[:1])  # *token
                        dot_token_dot[ngram[1:-1]].add((ngram[:1], ngram[-1:]))  # (left, right)
                        
                    # windows size = 2 to n-1
                    for j in range(2, n):
                        for i in range(n - j, len(sent) - j + 1):
                            ngram = tuple(sent[i: i + j])
                            
                            # =======j order=======
                            # for lambda term of j order
                            token_dot[ngram[:-1]].add(ngram[-1:])  # token*
                            
                            # =======j-1 order=======
                            # continuation count for j-1 order
                            dot_token[ngram[1:]].add(ngram[:1])  # *token
                            dot_token_dot[ngram[1:-1]].add((ngram[:1], ngram[-1:]))  # (left, right)
                    
                    # windows size = 1
                    for i in range(n - 1, len(sent)):
                        ngram = tuple(sent[i: i + 1])  
                        # =====1 order=====
                        # for lambda term of 1 order, quote "ε is the emtpy string" 
                        token_dot[ngram[:-1]].add(ngram[-1:])  # token*, token=empty string
            
                else:  # n=1, which is not the common case, but for generalization purpose
                    # windows size = 1
                    for i in range(len(sent) -n + 1):
                        ngram = tuple(sent[i: i + j])
                        # absolute count for 1 order
                        self.counts[ngram] += 1
                        self.counts[ngram[:-1]] += 1  # ngram[:-1] = ()
                        # for lambda term of 1 order
                        token_dot[ngram[:-1]].add(ngram[-1:]) 
                        
            # convert set to count            
            for key, value in dot_token.items():
                self.counts_dot_token[key] = len(value)
                
            for key, value in dot_token_dot.items():
                self.counts_dot_token_dot[key] = len(value)
                
            for key, value in token_dot.items():
                self.counts_token_dot[key] = len(value)
                
            delete_set = True
            if delete_set:
                del dot_token
                del dot_token_dot
                del token_dot
                
                
                        
            
                        
                
        
    def count_dot_token(self, tokens:tuple) -> int:
        """Return how many types of single word contexts it follows, e.g len({*tokens})
        Args:
            tokens(tuple): a tuple of strings
        """
        if not isinstance(tokens, tuple):
            raise TypeError("'tokens' has to be a tuple of strings")
            
        return self.counts_dot_token[tokens]
    
    def count_token_dot(self, tokens:tuple) -> set:
        """Return how many types of single word contexts it precedes, e.g len({tokens*})
        Args:
            tokens(tuple): a tuple of strings
        """
        if not isinstance(tokens, tuple):
            raise TypeError("'tokens' has to be a tuple of strings")
            
        return self.counts_token_dot[tokens]
    
    def count_dot_token_dot(self, tokens:tuple) -> set:
        """Return how many types of single word contexts it interleaves, e.g len({*tokens*})
        Args:
            tokens(tuple): a tuple of strings
        """
        if not isinstance(tokens, tuple):
            raise TypeError("'tokens' has to be a tuple of strings")
            
        return self.counts_dot_token_dot[tokens]
    
    
    def cond_prob(self, token:str, prev_tokens:tuple, verbose=False) -> float:
        """Conditional probability of a token. It's a recursive function.
        Args:
            token(str): the token
            prev_tokens(tuple): the preivious n-1 token
        """
        # two cases:
        # 1) n == 1
        # 2) n > 1:
            # 2.1) k == 1
            # 2.2) 1 < k < n
            # 2.3) k == n
        
        # case 1
        if not prev_tokens and self.n == 1:  # prev_tokens = ()
            nominator = max(self.count((token,)) - self.d, 0)
            denominator = self.count(prev_tokens)
            term_1 = nominator / denominator 
            lambda_nominator = self.count_token_dot(prev_tokens) 
            term_lambda = self.d * lambda_nominator / denominator
            term_2 = 1 / self.get_vocab_size()
            if verbose:
                if not prev_tokens:
                    p_p = 'p(%s)' % token
                else:
                    p_p = 'p(%s|%s)' % (token, prev_tokens)
                p_term_1 = '%d/%d' % ((nominator, denominator))
                p_term_lambda = '(%.3f*%d/%d)' % (self.d, lambda_nominator, denominator)
                p_term_2 = '1/%d' % self.get_vocab_size()
                print('%s = %s + %s * %s' % (p_p, p_term_1, p_term_lambda, p_term_2))
            
            return term_1 + term_lambda * term_2
        
        # case 2.1
        # lowest ngram, the termination of the recursion
        if not prev_tokens and self.n > 1:  # prev_tokens = ()
            nominator = max(self.count_dot_token((token,)) - self.d, 0)  # *token
            denominator = self.count_dot_token_dot(prev_tokens)  # **
            term_1 = nominator / denominator 
            lambda_nominator = self.count_token_dot(prev_tokens) 
            term_lambda = self.d * lambda_nominator / denominator
            term_2 = 1 / self.get_vocab_size()
            
            if verbose:
                p_p = 'p(%s)' % token
                p_term_1 = '%d/%d' % ((nominator, denominator))
                p_term_lambda = '(%.3f*%d/%d)' % (self.d, lambda_nominator, denominator)
                p_term_2 = '1/%d' % self.get_vocab_size()
                print('%s = %s + %s * %s' % (p_p, p_term_1, p_term_lambda, p_term_2))
            
            return term_1 + term_lambda * term_2
        
        # case 2.2
        # middle ngram
        if 0 < len(prev_tokens) < self.n - 1 and self.n > 1:
            nominator = max(self.count_dot_token(prev_tokens + (token,)), 0)
            denominator = max(self.count_dot_token_dot(prev_tokens), self.d)
            term_1 = nominator / denominator
            lambda_nominator = max(self.count_token_dot(prev_tokens), self.d)
            term_lambda = self.d * lambda_nominator / denominator
            term_2 = self.cond_prob(token, prev_tokens[1:], verbose)  # recursive
            
            if verbose:
                p_p = 'p(%s|%s)' % (token, prev_tokens)
                p_term_1 = '%d/%d' % ((nominator, denominator))
                p_term_lambda = '(%.3f*%.3f/%d)' % (self.d, lambda_nominator, denominator)
                p_term_2 = '%.3f' % term_2
                print('%s = %s + %s * %s' % (p_p, p_term_1, p_term_lambda, p_term_2))
            
            return term_1 + term_lambda * term_2
            
        # case 2.3
        # highest ngram
        if len(prev_tokens) == self.n - 1 and self.n > 1:
            nominator = max(self.count(prev_tokens + (token,)), 0)
            denominator = max(self.count(prev_tokens), self.d)
            term_1 = nominator / denominator
            lambda_nominator = max(self.count_dot_token_dot(prev_tokens), self.d)
            term_lambda = self.d * lambda_nominator / denominator
            term_2 = self.cond_prob(token, prev_tokens[1:], verbose)  # recursive
            
            if verbose:
                p_p = 'p(%s|%s)' % (token, prev_tokens)
                p_term_1 = '%d/%d' % ((nominator, denominator))
                p_term_lambda = '(%.3f*%.3f/%d)' % (self.d, lambda_nominator, denominator)
                p_term_2 = '%.3f' % term_2
                print('%s = %s + %s * %s' % (p_p, p_term_1, p_term_lambda, p_term_2))
            
            return term_1 + term_lambda * term_2
        
    def save(self, save_path):
        """save all count dictionaries as a pickle file
        Args:
            save_path(str): save path
        """
        save_dict = {'n': self.n,
                     'd': self.d,
                     'vocab': self.vocab,
                     'counts': self.counts,
                     'counts_dot_token': self.counts_dot_token,
                     'counts_dot_token_dot': self.counts_dot_token_dot,
                     'counts_token_dot': self.counts_token_dot}
        
        with open(save_path, 'wb') as f:
            pickle.dump(save_dict, f)
            
    def load_count(self, load_path):
        """load all count dictionaries from a pickle file
        Args:
            load_path(str): load path
        """
        print('directly load from file: %s, ...' % load_path)
        with open(load_path, 'rb') as f:
            load_dict = pickle.load(f)
            
        self.n = load_dict['n']
        self.d = load_dict['d']
        self.vocab = load_dict['vocab']
        self.counts = load_dict['counts']
        self.counts_dot_token = load_dict['counts_dot_token']
        self.counts_dot_token_dot = load_dict['counts_dot_token_dot']
        self.counts_token_dot = load_dict['counts_token_dot']
        
        print('model parameters: n=%d, d=%.3f' % (self.n, self.d))
        print('vacab size: %d' % len(self.vocab))        
                
            
        
class NgramGenerator():
    """with a NGram model, we can generate sentences."""
    def __init__(self, model):
        """
        Args:
            model(NGram): a ngram model.
        """
        self.n = model.n
        self.probs = defaultdict(dict)
        self.sorted_probs = defaultdict(list)
            
        # get a dict with which, given previous tokens, you can find all following
        # words that follows the given previous tokens in training corpus and 
        # their conditional probabilities.
        for elem in model.counts.keys():
            if len(elem) != self.n:  # elem be n or n-1 gram 
                continue
            prev_tokens = elem[:-1]
            token = elem[-1]
            prob = model.cond_prob(token, prev_tokens)
            self.probs[prev_tokens].update({token: prob})
        
        # rearrange dict above so that, given previous tokens, you can get all 
        # following words sorted with their conditional probabilities.
        for prev_tokens, token_prob in self.probs.items():
            token_prob_sorted = sorted(token_prob.items(),  # [(token, prob), ...]
                                       key=lambda x: (-x[1], x[0])  # if prob same, 'c' is before 'd'.
                                       )  # suf_prob_sorted is a list
            self.sorted_probs[prev_tokens] = token_prob_sorted  
            
    def generate_token(self, prev_tokens=None):
        """Randomly generate a token, given prev_tokens.
        Args:
            prev_tokens(tuple): the previsou n-1 tokens, None if n=1.
        return:
            token(str): a random result given prev_tokens.
        """
        if prev_tokens is None:
            assert self.n == 1
            prev_tokens = tuple()
            
        token = '</s>'  # default value
        choices = self.sorted_probs[prev_tokens]  # [(token, prob), ...], sorted
        # applying the inverse transform method
        p = random()  # range(0, 1)
        c_p = 0  # cumulative probability
        for i in range(0, len(choices)):
            c_p += choices[i][1]
            if p <= c_p:
                token = choices[i][0]
                break
            
        if c_p < p and len(choices):  # in case probability sum < 1 due to smoothing.
            token = choices[-1][0]
            
        return token
    
    def generate_sent(self):
        """Randomly generate a sentence
        return:
            sent(tuple): a tuple of tokens, already excludes <s> and </s>
        """
        sent = ('<s>', ) * (self.n - 1)
        if self.n == 1:  # make sent not empty
            token = self.generate_token()
            sent += (token,)
        
        # generate util </s> comes up
        while sent[-1] != '</s>':
            prev_tokens = sent[-self.n + 1:]
            token = self.generate_token(prev_tokens)
            sent += (token,)
        
        return sent[self.n - 1: -1]
    

if __name__ == "__main__":
    from ngram.tokenizer import Tokenizer
    # config
    margin = 0.2

    ngram = KneserNeyGram(load_path='kn_dict_n4_d96.pkl')
    tokenizer = Tokenizer()
    sent_a = '境内法人持有股份'
    sent_b = '其他'
    sent_a = tokenizer(sent_a)
    sent_b = tokenizer(sent_b)
    m1 = len(sent_a)
    m2 = len(sent_b)
    
    
    p_a = ngram.perplexity([sent_a])
    p_b = ngram.perplexity([sent_b])
    p_ab = ngram.perplexity([sent_a + sent_b])
    print('p_a: %.3f' % p_a)
    print('p_b: %.3f' % p_b)
    print('p_ab: %.3f' % p_ab)
    left = log(p_ab)
    right = log(p_a) * m1 / (m1 + m2) + log(p_b) * m2 / (m1 + m2)
    if left < right - margin:
        print("%.3f < %.3f" % (left, right))
        print('merge')
    else:
        print("%.3f > %.3f" % (left, right))
        print('divide')
    
            