# -*- coding: utf-8 -*-
"""
Created on Sat Jul  3 15:33:25 2021

@author: EDZ
"""

from opencc import OpenCC
import time
import numpy as np

class Tokenizer():
    """basicaly same as function 'tokenize' as a class, but keep track of
       consuming time. By keeping 'OpenCC(t2s)' as a attribute, we are not 
       forced to create a brand new instance of 'OpenCC(t2s)' each time, saving 
       a lot of time.
    """
    def __init__(self):
        self.cc =  OpenCC('t2s')
        self.t2s_time = 0
        self.tokenize_time = 0
    
    def __call__(self, sent):
        # traditional chinese to simplified chinese
        t0 = time.perf_counter()
        sent = self.cc.convert(sent)
        t1 = time.perf_counter()
        self.t2s_time += t1 - t0
        
        # tokenize
        sent_tokenized = []  
        temp_list = []  # store consecutive alphabetic characters 
        for i in sent:
            if i.isalpha() and i.isascii():  # tips: chinese is considered alphabetic in unicode but not ascii
                temp_list.append(i.lower())  
            elif i == ' ':  # white space
                merge_listA_and_flush_to_listB(temp_list, sent_tokenized)
            else:  # chinese characters, numbers or punctuations.
                merge_listA_and_flush_to_listB(temp_list, sent_tokenized)
                sent_tokenized.append(i)
        
        merge_listA_and_flush_to_listB(temp_list, sent_tokenized)
        t2 = time.perf_counter()
        self.tokenize_time += t2 - t1
        return sent_tokenized

    def _merge_listA_and_flush_to_listB(listA, listB):
        if len(listA):
            merge_word = ''.join(listA)  # merge
            listB.append(merge_word)  # flush to listB
            listA *= 0  # empty listA
        
    
def convert_to_simplified_chinese(sent:str) -> str:
    cc = OpenCC('t2s')  # simplified to traditional
    return cc.convert(sent)


def tokenize(sent:str, simplified=False, sent_coor=None) -> list:
    """process a given string, generate a list of tokens, e.g.,
    1.chinese sentence with random white spaces.
    '3境 外 上 市 的 外 资 股份' -> ['3', '境', '外', '上', '市', '的', '外', 
                                   '资', '股', '份']
    2.english sentence.
    'Tianxiang Construction Group Inc. Ltd' -> ['tianxiang', 'construction,'
                                                'group', 'inc', '.', 'ltd']
    3.hybrid sentence.
    '公司A级纳税人' -> ['公', '司', 'A', '级', '纳', '税', '人']
    
    Args:
        sent(str): 
        simplified(bool):
    return:
        sent_tokenized(list): a list of tokens
    """
    if simplified:
        sent = convert_to_simplified_chinese(sent)
    sent_tokenized = []  
    temp_list = []  # store consecutive alphabetic characters 
    
    for i in sent:
        if i.isalpha() and i.isascii():  # tips: chinese is considered alphabetic in unicode but not ascii
            temp_list.append(i.lower())  
        elif i == ' ':  # white space
            merge_listA_and_flush_to_listB(temp_list, sent_tokenized)
        else:  # chinese characters, numbers or punctuations.
            merge_listA_and_flush_to_listB(temp_list, sent_tokenized)
            sent_tokenized.append(i)
    
    merge_listA_and_flush_to_listB(temp_list, sent_tokenized)
    return sent_tokenized


def tokenize_with_coord(sent:str, sent_coor:np.ndarray, simplified=False) -> list:
    """tokenize function with each word coordinate, and return the token 
    coordinates.
    Args:
        sent(str): 
        coor(np.ndarray):
        simplified(bool):
    return:
        sent_tokenized(list): a list of tokens
        token_coor(np.ndarray)
    """
    if simplified:
        sent = convert_to_simplified_chinese(sent)
    sent_tokenized = []  # store tokens
    token_coor = []  # store token's coordinates
    
    temp_word_list = []  # store consecutive alphabetic characters 
    temp_coor_list = []  # store coresponding coordinates
    for i, j in zip(sent, sent_coor):
        if i.isalpha() and i.isascii():  # tips: chinese is considered alphabetic in unicode but not ascii
            temp_word_list.append(i.lower())  
            temp_coor_list.append(j)
        elif i == ' ':  # white space
            merge_listA_and_flush_to_listB(temp_word_list, sent_tokenized)
            merge_listA_and_flush_to_listB(temp_coor_list, token_coor)
        else:  # chinese characters, numbers or punctuations.
            merge_listA_and_flush_to_listB(temp_word_list, sent_tokenized)
            merge_listA_and_flush_to_listB(temp_coor_list, token_coor)
            sent_tokenized.append(i)
            token_coor.append(j)
    
    merge_listA_and_flush_to_listB(temp_word_list, sent_tokenized)
    merge_listA_and_flush_to_listB(temp_coor_list, token_coor)
    
    return sent_tokenized, np.array(token_coor)


def merge_listA_and_flush_to_listB(listA, listB):
    if not len(listA):
        return None
    
    if isinstance(listA[0], str):  # listA is a list of str
        merge_word = ''.join(listA)  # merge
        listB.append(merge_word)  # flush to listB
        listA *= 0  # empty listA
    elif isinstance(listA[0], np.ndarray):  # listA is a list of np.ndarray
        coors = np.array(listA)
        x0 = np.min(coors[:, 0])
        y0 = np.min(coors[:, 1])
        x1 = np.max(coors[:, 2])
        y1 = np.max(coors[:, 3])
        merge_coor = np.array([x0, y0, x1, y1])
        listB.append(merge_coor)  # flush to listB
        listA *= 0  # empty listA
    else:
        pass
        
        
if __name__ == "__main__":
    a = '債券投資及其它'
    b = tokenize(a)
    print(b)