# -*- coding: utf-8 -*-
"""
Created on Tue Jul 13 15:43:10 2021

@author: EDZ
"""
# model.no_line_table_based_on_deeplabv3plus.
from model.no_line_table_based_on_deeplabv3plus.model import deeplabv3plus_resnet101
from model.no_line_table_based_on_deeplabv3plus.utils.torch_utils import convert_to_separable_conv
from model.no_line_table_based_on_deeplabv3plus.utils.torch_utils import select_device
import torch
from model.no_line_table_based_on_deeplabv3plus.postprocess import PostProcessor
from model.no_line_table_based_on_deeplabv3plus.utils.datasets import LoadImage_V2
from tqdm import tqdm
import numpy as np
from model.no_line_table_based_on_deeplabv3plus.utils.utils import point2range
import time


class Unlined_Table_Processor():
    """the class object to process unlined table"""
    def __init__(self,   
                 weight,
                 device='0', 
                 ngram_load_path='ngram/kn_dict_n4_d96.pkl',
                 load_from_state_dict=True,
                 verbose=False,
                 ):
        """
        Args:
            weight(str): 模型参数地址。
            device(str): "0","1",选择的显卡编号。
            load_from_state_dict(bool): pt文件存储的是state_dict还是模型。
            verbose(bool): 是否打印推理的信息(debug用)。
        """
        # config
        self.weight = weight
        self.device = select_device(device)
        self.half = self.device.type != 'cpu'  # half precision
        self.verbose = verbose
        
        # initialize model
        self.model = deeplabv3plus_resnet101(num_classes=1, 
                                            output_stride=8,
                                            pretrained_backbone=False)
        convert_to_separable_conv(self.model.classifier)
        
        # load checkpoint
        if not load_from_state_dict:
            ckpt = torch.load(weight, map_location=self.device)
            state_dict = ckpt['model'].float().state_dict()  # FP32
            self.model.load_state_dict(state_dict, strict=False)  
        else:
            self.model.load_state_dict(torch.load(weight), strict=False)
        self.model.eval().to(self.device)
        if self.half:
            self.model.half()  # to FP16
            
        # load post processor
        self.post_processor = PostProcessor(ngram_load_path)
    
    def model_predict(self, imgs):
        """模型推理
        Args:
            imgs(list): 一组需要处理的无线表格图片(BGR模式)。
        return:
            raw_preds(list): 无线表格图片的模型预测结果, [(行预测， 列预测)]，其
                中：
                    行separator预测 = [[行开始, 行结束],...]
                    列separator预测 = [[列开始, 列结束],...]
        """
        raw_preds = []
        
        # get dataset
        dataset = LoadImage_V2(imgs,
                               hsv_augment=False, 
                               multi_scale=False,
                               normalize=True)
        
        t0 = time.perf_counter()
        for img_tensor, shape in tqdm(dataset):
            img_tensor = img_tensor.to(self.device)
            img_tensor = img_tensor.half() if self.half else img_tensor.float()
            if img_tensor.ndimension() == 3:
                img_tensor = img_tensor.unsqueeze(0)  # add batch dimension
            preds = self.model(img_tensor)  # [(1, 1, h,), (1, 1, w)]
            
            # to 1，0
            r_pred_point = torch.sigmoid(preds[0]) >= 0.5
            c_pred_point = torch.sigmoid(preds[1]) >= 0.5
            
            # to numpy
            r_pred_point = r_pred_point.view(-1).cpu().numpy().astype(np.int32)  # (h,)
            c_pred_point = c_pred_point.view(-1).cpu().numpy().astype(np.int32)  # (w,)
        
            # point to separator
            r_sep = point2range(r_pred_point).astype(np.int32)  # [[start, end],...]
            c_sep = point2range(c_pred_point).astype(np.int32)  # [[start, end],...]
            
            raw_preds.append((r_sep, c_sep))
        
        # 打印处理信息
        t1 = time.perf_counter()
        if self.verbose:
            print('模型推理--%d张无线表格图片消耗时间：%.3fs' % (len(imgs), t1 - t0))
            
        return raw_preds
        
    
    def post_process(self, imgs, raw_preds, text_list, text_bboxes_list):
        """后处理
        Args:
            imgs(list): 一组无线表格图片(bgr)。
            raw_preds(list): 无线表格图片的模型预测结果。
            text_list(list): 无线表格中的文本，与文本框bboxes一一对应。
            text_bboxes_list(list): 无线表格中的文本框坐标，xyxy形式。
        return:
            post_preds(list): 模型预测结果+后处理的最终预测结果。
        """
        t0 = time.perf_counter()
        post_preds = []
        for img, (row_sep, col_sep), text, text_bboxes in zip(imgs, 
                                                              raw_preds, 
                                                              text_list,
                                                              text_bboxes_list):
            table = self.post_processor(img, row_sep, col_sep, text, text_bboxes)
            row_line = table.row_line
            col_line = table.col_line
            
            post_preds.append((row_line, col_line))
            
        t1 = time.perf_counter()
        # 打印处理信息
        if self.verbose:
            print('后处理--%d张无线表格图片消耗时间：%.3fs' % (len(imgs), t1-t0))
        
        return post_preds
    
    def __call__(self, imgs, text_list, text_bboxes_list):
        """无线表格处理： 模型+后处理
        Args:
            imgs(list): 一组需要处理的无线表格图片(BGR模式)。
            text_list(list): 无线表格中的文本，与文本框bboxes一一对应。
            text_bboxes_list(list): 无线表格中的文本框坐标，xyxy形式。
        return:
            raw_preds(list): 无线表格图片的模型预测结果。
            post_preds(list): 模型预测结果+后处理的最终预测结果。
        """
        raw_preds = self.model_predict(imgs)
        post_preds = self.post_process(imgs, raw_preds, text_list, text_bboxes_list)
        
        return raw_preds, post_preds
        
        