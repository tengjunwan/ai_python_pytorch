# -*- coding: utf-8 -*-
"""
Created on Thu Dec 17 16:54:34 2020

@author: tengjunwan
"""
# model.no_line_table_based_on_deeplabv3plus.
import torch.nn as nn
import torch.nn.functional as F
from model.no_line_table_based_on_deeplabv3plus.backbone import resnet
from model.no_line_table_based_on_deeplabv3plus.utils.torch_utils import ASPP, ProjPool_
import torch
from collections import OrderedDict


class DeepLabV3(nn.Module):
    """ 
    这个DeepLabV3类可以是deeplabv3或者deeplabv3+，取决于backbone返回的features以及
    classifier
    Arguments:
        backbone (nn.Module): the network used to compute the features for the model.
            The backbone should return an OrderedDict[Tensor], with the key being
            "out" for the last feature map used, and "aux" if an auxiliary classifier
            is used.
        classifier (nn.Module): module that takes the "out" element returned from
            the backbone and returns a dense prediction.
        aux_classifier (nn.Module, optional): auxiliary classifier used during training
    """
    def __init__(self, backbone, classifier):
        super(DeepLabV3, self).__init__()
        self.backbone = backbone
        self.classifier = classifier
        
    def forward(self, x):
        features = self.backbone(x)
        output = self.classifier(features, x)
#        x = F.interpolate(x, size=input_shape, mode='bilinear', align_corners=False)
        return output
    

class DeepLabHeadV3Plus(nn.Module):
    """DeepLabHeadV3Plus,除去backbone之后的部分，包括：
    ASPP feature map + low feature map → classifier
    """
    def __init__(self, in_channels, low_level_channels, num_classes,
                 aspp_dilate=[12, 24, 36], project_pool=True):
        """
        Args:
            in_channels(int): backbone末端输入的channel数
            low_level_channels(int): backbone前端输出的low level feature的channel数
            num_classes(int): 分类数
            aspp_dilate(list of int):  ASPP中的dilate rate（保持原样）
            project_pool(bool):  ASPP中加入project 模块，我改的
        """
        super(DeepLabHeadV3Plus, self).__init__()
        # project for low_feature
        self.project = nn.Sequential( 
            nn.Conv2d(low_level_channels, 48, 1, bias=False),
            nn.BatchNorm2d(48),
            nn.ReLU(inplace=True),
            )

        self.aspp = ASPP(in_channels, aspp_dilate, project_pool)
        
        # col分支 
        self.col_conv3 = nn.Sequential(
            nn.Conv2d(304, 256, 3, padding=1, bias=False),
            nn.BatchNorm2d(256),
            nn.ReLU(inplace=True)
            )
        
        self.col_classifier = nn.Sequential(
            nn.Conv2d(256, num_classes, 1),
            ProjPool_(direction='C', expand=False)
            )
        
        # row分支
        self.row_conv3 = nn.Sequential(
            nn.Conv2d(304, 256, 3, padding=1, bias=False),
            nn.BatchNorm2d(256),
            nn.ReLU(inplace=True)
            )
        
        self.row_classifier = nn.Sequential(
            nn.Conv2d(256, num_classes, 1),
            ProjPool_(direction='R', expand=False)
            )
        
        # 初始化参数
        self._init_weight()

    def forward(self, feature, x):
        """
        Args:
            feature(dict): {
                            'low_level': resnet layer1输出的feature map(tensor),
                            'out': resnet layer4输出的output(tensor)
                            }
            x(tensor): 模型输入,(batch, 3, H, W),并没有使用，仅仅是为了读取它的H和
                       W。
        return:
            r_output(tensor): (batch, c, H), 其中c为分类数
            c_output(tensor): (batch, c, W), 其中c为分类数
        """
        # concatenate low-level features and high-level feature
        low_level_feature = self.project(feature['low_level'] )
        output_feature = self.aspp(feature['out'])
        output_feature = F.interpolate(output_feature, size=low_level_feature.shape[2:], 
                                       mode='bilinear', align_corners=False)
        feature_mixed = torch.cat([low_level_feature, output_feature], dim=1)
        feature_mixed = F.interpolate(feature_mixed, size=x.shape[2:], 
                                      mode='bilinear', align_corners=False)
        
        # row分支
        r_output = self.row_conv3(feature_mixed)  # (batch, c, h, w)
        r_output = self.row_classifier(r_output)  # (batch, c, h, 1)
        r_output.squeeze_(axis=3)  # (batch, c, H)
        
        # col分支
        c_output = self.col_conv3(feature_mixed)  # (batch, c, h, w)
        c_output = self.col_classifier(c_output)  # (batch, c, 1, w)
        c_output.squeeze_(axis=2)  # (batch, c, W)
        
        return r_output, c_output
    
    def _init_weight(self):
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.kaiming_normal_(m.weight)
            elif isinstance(m, (nn.BatchNorm2d, nn.GroupNorm)):
                nn.init.constant_(m.weight, 1)
                nn.init.constant_(m.bias, 0)
                

class DeepLabHead(nn.Module):
    def __init__(self, in_channels, num_classes, aspp_dilate=[12, 24, 36]):
        super(DeepLabHead, self).__init__()

        self.classifier = nn.Sequential(
            ASPP(in_channels, aspp_dilate),
            nn.Conv2d(256, 256, 3, padding=1, bias=False),
            nn.BatchNorm2d(256),
            nn.ReLU(inplace=True),
            nn.Conv2d(256, num_classes, 1)
        )
        self._init_weight()

    def forward(self, feature):
        return self.classifier( feature['out'] )

    def _init_weight(self):
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.kaiming_normal_(m.weight)
            elif isinstance(m, (nn.BatchNorm2d, nn.GroupNorm)):
                nn.init.constant_(m.weight, 1)
                nn.init.constant_(m.bias, 0)
                

class IntermediateLayerGetter(nn.ModuleDict):
    """
    Module wrapper that returns intermediate layers from a model
    返回模型内部feature map的输出,例如resnet中各个layer的输出,等同于把原本拆解并
    形成一个新的模型和forward，以及更改了输出。

    It has a strong assumption that the modules have been registered
    into the model in the same order as they are used.
    This means that one should **not** reuse the same nn.Module
    twice in the forward if you want this to work.
    这里的假设是，输入的模型的module顺序是按照forward顺序来的

    Additionally, it is only able to query submodules that are directly
    assigned to the model. So if `model` is passed, `model.feature1` can
    be returned, but not `model.feature1.layer2`.
    仅仅考虑一级子module，不再深入到子module内部，对于resnet就是layer1,layer2,
    layer3,layer4

    Arguments:
        model (nn.Module): model on which we will extract the features
                           用来抽取feature的模型
        return_layers (Dict[name, new_name]): a dict containing the names
            of the modules for which the activations will be returned as
            the key of the dict, and the value of the dict is the name
            of the returned activation (which the user can specify).
            给模型中相应module的输出的字典的key进行命名
            例如在resnet中{'layer1': 'low level', 'layer4': 'out'}

    Examples::

        >>> m = torchvision.models.resnet18(pretrained=True)
        >>> # extract layer1 and layer3, giving as names `feat1` and feat2`
        >>> new_m = torchvision.models._utils.IntermediateLayerGetter(m,
        >>>     {'layer1': 'feat1', 'layer3': 'feat2'})
        >>> out = new_m(torch.rand(1, 3, 224, 224))
        >>> print([(k, v.shape) for k, v in out.items()])
        >>>     [('feat1', torch.Size([1, 64, 56, 56])),
        >>>      ('feat2', torch.Size([1, 256, 14, 14]))]
    """
    def __init__(self, model, return_layers):
        if not set(return_layers).issubset([name for name, _ in model.named_children()]):
            raise ValueError("return_layers are not present in model")

        orig_return_layers = return_layers
        return_layers = {k: v for k, v in return_layers.items()}  # deepcopy
        layers = OrderedDict()
        # 遍历模型，如果run out of return layers, 停止
        for name, module in model.named_children():  # tuple(name, module)
            layers[name] = module
            if name in return_layers:
                del return_layers[name]
            if not return_layers:
                break
        # 例如在resnet101中
#         layers = ['conv1': model.conv1, 
#                   'bn1': model.bn1,
#                   'relu': model.relu,
#                   'maxpool': model.maxpool,
#                   'layer1': model.layer1,
#                   'layer2': model.layer2,
#                   'layer3': model.layer3,
#                   'layer4': model.layer4,] break

        super(IntermediateLayerGetter, self).__init__(layers)
        self.return_layers = orig_return_layers

    def forward(self, x):
        """
        Args:
            x(tensor): 维度(batch, 3, H, W)
        return:
            out(OrderedDict): 例如在resnet中是{'low level': layer1的输出，
                                               'out': layer4的输出}
        """
        out = OrderedDict()
        for name, module in self.named_children():
            x = module(x)
            if name in self.return_layers:
                out_name = self.return_layers[name]
                out[out_name] = x
        return out
    
    
def _segm_resnet(name, backbone_name, num_classes, output_stride, pretrained_backbone):
    """核心函数，返回deeplabv3+或者deeplabv3
    这里有两个变体，可以设置
    Args:
        name(str): "deeplabv3"或者"deeplabv3plus", 二选一
        backbone_name(str): 例如"resnet101"
        num_classes(int): 分类数量
        output_stride(int): 最终输出feature map的收缩率(8或者16)
        pretrained_backbone(bool): 是否采用预处理backbone
    """

    if output_stride==8:  # bigger feature map
        replace_stride_with_dilation=[False, True, True]
        aspp_dilate = [12, 24, 36]  # bigger assp dilate rate
    else:
        replace_stride_with_dilation=[False, False, True]
        aspp_dilate = [6, 12, 18]

    backbone = resnet.__dict__[backbone_name](  # backbone还没有进行拆解,仅仅是resnet
        pretrained=pretrained_backbone,
        replace_stride_with_dilation=replace_stride_with_dilation)
    
    if backbone_name.endswith('50') or backbone_name.endswith('101'):
        inplanes = 2048  # backbone输出channel数
        low_level_planes = 256  # backbone中低feature map输出channel数
    elif backbone_name.endswith('18') or backbone_name.endswith('34'):
        inplanes = 512  # backbone输出channel数
        low_level_planes = 34  # backbone中低feature map输出channel数
    else:
        raise NameError("no model named %s" % backbone_name)

    if name=='deeplabv3plus':
        return_layers = {'layer4': 'out', 'layer1': 'low_level'}
        classifier = DeepLabHeadV3Plus(inplanes, low_level_planes, num_classes, aspp_dilate)
    elif name=='deeplabv3':
        return_layers = {'layer4': 'out'}
        classifier = DeepLabHead(inplanes , num_classes, aspp_dilate)
    backbone = IntermediateLayerGetter(backbone, return_layers=return_layers)

    model = DeepLabV3(backbone, classifier)
    return model


# deeplabv3
def deeplabv3_resnet101(num_classes=21, output_stride=8, pretrained_backbone=True):
    """ Wrapper 
    Constructs a DeepLabV3 model with a ResNet-101 backbone.

    Args:
        num_classes (int): number of classes.
        output_stride (int): output stride for deeplab.
        pretrained_backbone (bool): If True, use the pretrained backbone.
    """
    return _segm_resnet('deeplabv3', 'resnet101', num_classes, output_stride=output_stride, pretrained_backbone=pretrained_backbone)
    

# deeplabv3plus
def deeplabv3plus_resnet101(num_classes=21, output_stride=8, pretrained_backbone=True):
    """ Wrapper
    Constructs a DeepLabV3+ model with a ResNet-101 backbone.

    Args:
        num_classes (int): number of classes.
        output_stride (int): output stride for deeplab.
        pretrained_backbone (bool): If True, use the pretrained backbone.
    """
    return _segm_resnet('deeplabv3plus', 'resnet101', num_classes, output_stride=output_stride, pretrained_backbone=pretrained_backbone)


def deeplabv3plus_resnet50(num_classes=21, output_stride=8, pretrained_backbone=True):
    """Constructs a DeepLabV3 model with a ResNet-50 backbone.
    Args:
        num_classes (int): number of classes.
        output_stride (int): output stride for deeplab.
        pretrained_backbone (bool): If True, use the pretrained backbone.
    """
    return _segm_resnet('deeplabv3plus', 'resnet50', num_classes, output_stride=output_stride, pretrained_backbone=pretrained_backbone)


def deeplabv3plus_resnet34(num_classes=21, output_stride=8, pretrained_backbone=True):
    """Constructs a DeepLabV3 model with a ResNet-50 backbone.
    Args:
        num_classes (int): number of classes.
        output_stride (int): output stride for deeplab.
        pretrained_backbone (bool): If True, use the pretrained backbone.
    """
    return _segm_resnet('deeplabv3plus', 'resnet34', num_classes, output_stride=output_stride, pretrained_backbone=pretrained_backbone)


def deeplabv3plus_resnet18(num_classes=21, output_stride=8, pretrained_backbone=True):
    """Constructs a DeepLabV3 model with a ResNet-50 backbone.
    Args:
        num_classes (int): number of classes.
        output_stride (int): output stride for deeplab.
        pretrained_backbone (bool): If True, use the pretrained backbone.
    """
    return _segm_resnet('deeplabv3plus', 'resnet18', num_classes, output_stride=output_stride, pretrained_backbone=pretrained_backbone)


if __name__ == "__main__":
    model = deeplabv3plus_resnet50(1, 8, True)
    x = torch.rand(1, 3, 128, 128)
    y = model(x)
    
