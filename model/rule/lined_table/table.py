# -*- coding: utf-8 -*-
"""
Created on Mon Sep  6 16:37:16 2021

@author: EDZ
"""

import numpy as np
from model.rule.lined_table.utils.general_utils import get_order_of_grid
from model.rule.lined_table.utils.vis_utils import draw_grids
# from utils.cv_utils import analyze_bg_color

class Table():
    """"""
    def __init__(self, img, grids, text, text_bboxes, tolerance=6):
        """
        Args:
            img(np.ndarray): BGR.
            grids(np.ndarray): shape=(#grids, 4), 4 means xyxy.
            text(list): list of str.
            text_bboxes(np.ndarray): shape=(#text, 4), 4 means xyxy, corresponding
                bounding boxes of text.
            tolerance(int): value to settle the floating value of grid coordiates.
        """
        # store
        self.img = img
        self.raw_grids = grids
        self.text = text
        self.text_bboxes = text_bboxes
        
        # get grids and grid_orders
        self.grids, self.grid_orders = get_order_of_grid(grids, tolerance)
        
        # get background color of each grid
        # self.gray_rank, self.unique_gray_values = analyze_bg_color(img, self.grids)
        
        # generate cells
        self.cells = self.generate_cells()
        
    def generate_cells(self):
        # initialize
        cells = []
        
        # generate chars
        chars = []
        for t, b in zip(self.text, self.text_bboxes):
            x0, y0, x1, y1 = b
            chars.append(Char(x0, y0, x1, y1, t))
        chars = np.array(chars)  # (#chars, )
        
        # filter each char to its corresponding grid (get belong matrix)
        center = (self.text_bboxes[:, [0, 1]] + self.text_bboxes[:, [2, 3]]) / 2.0  # (#chars, xy)
        center_ = np.concatenate((center*(-1), center), axis=1)  # (#chars, -x-yxy)
        center_ = np.expand_dims(center_, axis=0)  # (1, #chars, -x-yxy)
        grid_ = self.grids * np.array([[-1, -1, 1, 1]])  # (#grid, -x-yxy)
        grid_ = np.expand_dims(grid_, axis=1)  # (#grid, 1, -x-yxy)
        belong_matrix = (grid_ >= center_).all(axis=2)  #  (#grid, #chars), boolean matrix
        
        # generate cell
        for i, b in enumerate(belong_matrix):
            cell_chars = chars[b]
            cell_grid = self.grids[i]
            cell_grid_order = self.grid_orders[i]
            cells.append(Cell(cell_grid, cell_grid_order, cell_chars))
            
        return cells
    
    def __getitem__(self, index):
        return self.cells[index]
    
    def __len__(self, cells):
        return len(self.cells)
    
    def __repr__(self):
        mes = []
        for cell in self.cells:
            mes.append(repr(cell))
        return '\n'.join(mes)

    def generate_mask(self):
        # visualize grid
        mask = np.zeros_like(self.img, dtype=np.uint8)
        mask_grid = draw_grids(mask, self.grids)                
        return mask_grid
    

class Cell():
    """"""
    def __init__(self, grid, grid_order, chars):
        """
        Args:
        """
        self.coor = np.array(grid)
        self.cell_order = np.array(grid_order)
        self.lines = Lines(chars)
    
    def __repr__(self):
        cell_content = []
        for line in self.lines:
            line_content = line.load_line_content()
            cell_content.append(line_content)
        cell_content = '\\n'.join(cell_content)
        mes = "('%s', (%2.f, %2.f, %2.f, %2.f), (%d, %d, %d, %d))" % (cell_content,
                                                                    *self.coor,
                                                                    *self.cell_order)
        return mes

            
class Lines():
    """Lines, given a list of Char, sort them to form several lines to suit the 
    reading order. It constituents the 'text infomation within' part of Cell."""
    
    def __init__(self, chars):
        self.chars = np.array(chars, dtype=Char)
        self.raw_lines = self.generate_lines(self.chars)
        self.lines = self.get_rid_of_space(self.raw_lines)  # ' hi  ' -> 'hi'
            
    def generate_lines(self, chars):
        # load char coordinates
        char_coors = np.zeros((len(chars), 4))
        char_contents = []
        for i, char in enumerate(chars):
            char_coors[i] = char.coor  # (#char, xyxy)
            char_contents.append(char.content)
        char_contents = np.array(char_contents)
        
        # find start and end of lines(about y axis)
        line_ranges = self.get_projection_ranges(char_coors[:, [1, 3]])  # (#lines, 2), 2 means (start, end)
        
        # filter each char to its corresponding line (get belong matrix)
        char_centers = (char_coors[:, [0, 1]] + char_coors[:, [2, 3]]) / 2.0  # (#char, 2), 2 means (x_center, y_center)
        line_ranges_ = np.expand_dims(line_ranges, axis=1)  # (#lines, 1, 2)
        char_centers_ = np.expand_dims(char_centers, axis=0)  # (1, #content, 2)
        belong_matrix = np.logical_and(line_ranges_[:, :, 0] < char_centers_[:, :, 1],  # (#lines, #content)
                                       line_ranges_[:, :, 1] > char_centers_[:, :, 1])
        
        # sort all chars by x0 and corresponding line index
        lines = []
        for i, b in enumerate(belong_matrix):  # loop by line index
            char_in_line = chars[b]  # (#char in this line,)
            line_coor = char_coors[b]  # (#char in this line, 4)
            # sort by x0
            line_order = line_coor[:, 0].argsort()  # (#char in this line, )
            # (#char in this line)
            line_char_sorted = char_in_line[line_order]  # (#char in this line,)
            # determine char type
            _ = self.determine_char_types(line_char_sorted, line_ranges[i])
            # add to list
            lines.append(Line(line_char_sorted, i))
            
        return lines
    
    def get_rid_of_space(self, lines):
        # initialize
        new_lines = []
        
        # flatten lines 
        content = []
        line_index = []
        chars = []
        for line in lines:
            for char in line:
                content.append(char.content)
                line_index.append(line.line_index)
                chars.append(char)
        content = ''.join(content)  # list to string
        
        # count space both at start and end
        n_total = len(chars)
        n_head_white_space = n_total - len(content.lstrip(' '))  
        n_tail_white_space = len(content.lstrip(' ')) - len(content.strip(' ')) 
        if n_total == n_head_white_space + n_tail_white_space:  # just white space
            return new_lines
        start, end = n_head_white_space, n_total - n_tail_white_space 
        
        # keep the validated part
        line_index_sliced = line_index[start: end]    
        chars_sliced = chars[start: end]
        
        # rearrange lines
        split_index = np.argwhere(np.diff(line_index_sliced) > 0).flatten() + 1  
        split_index = np.insert(split_index, 0, 0)  
        split_index = np.append(split_index, len(chars_sliced))  
        for j in range(len(split_index) - 1):  # loop in pairs
            line_start , line_end = split_index[j], split_index[j+1]  
            new_lines.append(Line(chars_sliced[line_start: line_end], j))  
        
        return new_lines
    
    def __getitem__(self, index):
        return self.lines[index]
    
    def __len__(self):
        return len(self.lines)
    
    def __repr__(self):
        mes = []
        for line in self.lines:
            mes.append(line.__repr__())
        return '\n'.join(mes)
            
    def get_projection_ranges(self, ranges:np.ndarray) -> np.ndarray:
        """given a list of ranges (start, end), and then project them to a axis to 
        get projection ranges, which is also a list of ranges.
        Args:
            ranges(np.ndarray): shape=(#ranges, 2), 2 means (s, e)=(start, end).
        return:
            projection_ranges(np.ndarray): shape=(#ranges, 2), 2 means (s, e)=(start, end).
        """
        assert np.all(ranges[:, 1] >= ranges[:, 0]), "'end' is supposed to be bigger than 'start'"
        projection_ranges = np.empty((0, 2))  # initialize
        for s, e in ranges:
            non_overlap = np.logical_or(projection_ranges[:, 0] > e, projection_ranges[:, 1] < s)  # (#projection_ranges, )
            overlap = np.logical_not(non_overlap)
            if np.any(overlap): # update the overlapped projection_range
                new_s = min(s, projection_ranges[overlap][:, 0].min())
                new_e = max(e, projection_ranges[overlap][:, 1].max())
                projection_ranges = np.vstack((projection_ranges[non_overlap],
                                               [[new_s, new_e]]))
            else:  # add a new projection_range
                projection_ranges = np.vstack((projection_ranges, [[s, e]]))
                
        # sort ranges by y_start
        order = projection_ranges[:, 0].argsort()
        projection_ranges = projection_ranges[order]
                
        return projection_ranges
        
    def determine_char_types(self, chars, line_range, per_thres=0.7):
        """check the type of char(i.e., text). There are sevaral types of char:
            0: normal text, which possess almost the entire space within each line;
            1: upper text, which possess the upper space within each line(like a note symbol);
            2: lower text, which possess the lower space within each line.
        Args:
            chars(np.ndarray): shape=(#chars,), dtype=Char
            line_range(np.ndarray): shape=(2, ), 2 means (y_start, y_end).
            per_thres(float): if (char height) / (line height) above this, it is
                considered as normal text.
        return:
            char_types(np.ndarray): shape=(#chars, ), store the type index of 
                each content.
        """
        # initialize
        char_types = np.zeros(len(chars))  # normal text as default
        
        # load char coordinate
        char_coor = np.zeros((len(chars), 4))
        for i, char in enumerate(chars):
            char_coor[i] = char.coor
        
        # determine
        line_height = line_range[1] - line_range[0]
        line_mid_y = (line_range[1] + line_range[0]) / 2.0
        char_height = char_coor[:, 3] - char_coor[:, 1]
        char_per = char_height / line_height
        args_not_normal = np.nonzero(char_per <= per_thres)[0]
        for arg in args_not_normal:
            not_normal_coor = char_coor[arg]  # (4, ) = xyxy
            y_center = (not_normal_coor[1] + not_normal_coor[3]) / 2.0
            if y_center <= line_mid_y:  # upper text
                char_types[arg] = 1
                chars[arg].change_type(1)
            else:  # lower text
                char_types[arg] = 2
                chars[arg].change_type(2)
        
        return char_types
    
        
class Char():
    """basically contains text and its bounding box"""
    def __init__(self, x0, y0, x1, y1, content):
        self.coor = np.array([x0, y0, x1, y1])
        self.content = content
        self.type = 0  # denote if it is normal text or annotation text
    
    def change_type(self, assign_type):
        self.type = assign_type
        
    def __repr__(self):
        mes = "('%s', (%.2f, %.2f, %.2f, %.2f), %d)" % (self.content, *self.coor, self.type)
        return mes
        

class Line():
    """a line that contains some Char in it."""
    def __init__(self, chars, line_index=0):
       self.chars = np.array(chars)
       self.line_index=line_index
       # load coodinates 
       
    def is_empty(self):
        for char in self.chars:
            if not len(char.content.strip()):
                return False
        return True
    
    def load_line(self):
        char_coors = np.zeros((len(self.chars), 4))
        char_contents = []
        char_types = []
        for i, char in enumerate(self.chars):
            char_coors[i] = char.coor
            char_contents.append(char.content)
            char_types.append(str(char.type))
         
        char_contents = ''.join(char_contents)
        char_types = ''.join(char_types)
        
        return char_coors, char_contents, char_types
    
    def load_line_content(self):
        char_contents = []
        for i, char in enumerate(self.chars):
            char_contents.append(char.content)
        line_content = ''.join(char_contents)
        return line_content
    
    def __repr__(self):
        char_coors, char_contents, char_types = self.load_line()
        min_x0y0 = np.amin(char_coors[:, :2], axis=0)
        max_x1y1 = np.amax(char_coors[:, 2:], axis=0)
        line_coor = np.hstack((min_x0y0, max_x1y1))
        mes = "('%s', (%.2f, %.2f, %.2f, %.2f), %s, %d)" % (char_contents, 
                                                          *line_coor, 
                                                          char_types,
                                                          self.line_index)
        return mes
    
    def __len__(self):
        return len(self.chars)
        
    def __getitem__(self, index):
        return self.chars[index]