# -*- coding: utf-8 -*-
"""
Created on Wed Mar 10 17:57:17 2021

@author: EDZ
"""


import numpy as np
import fitz
from pathlib import Path
from model.rule.lined_table.utils.pdf_utils import extract_text_in_rects
from model.rule.lined_table.utils.pdf_utils import extract_img_in_rects
from model.rule.lined_table.utils.general_utils import form_save_name
from model.rule.lined_table.utils.general_utils import filter_overlapped_grids
from model.rule.lined_table.utils.text_utils import filter_bboxes
from model.rule.lined_table.utils.cv_utils import expand_img
# from utils.cv_utils import guess_if_simple_color
# from utils.cv_utils import filter_white_line
from model.rule.lined_table.utils.cv_utils import convert_to_binary
from model.rule.lined_table.utils.cv_utils import utilize_text_boxes
from model.rule.lined_table.utils.cv_utils import cv2_open 
from model.rule.lined_table.utils.cv_utils import cv2_close
from model.rule.lined_table.utils.cv_utils import add_outer_line
from model.rule.lined_table.utils.cv_utils import close_grids
from model.rule.lined_table.utils.cv_utils import find_grid_coor
from model.rule.lined_table.utils.cv_utils import masks_plus
from model.rule.lined_table.utils.cv_utils import cv_imwrite
from model.rule.lined_table.utils.vis_utils import draw_grids
from model.rule.lined_table.table import Table
from model.rule.lined_table.send_parser import Table_Sender





# logger = logging.getLogger(__name__)
# logging.basicConfig(level=logging.INFO, 
#                     filename='lined_table.log',
#                     filemode='w')



class GridExtractor():
    """
    提取有线表格
    """
    def __init__(self, hyp=None):
        if hyp is None:
            # 参数设置
            self.hyp = {'thresh': 35,   # 二值化阈值（sobel方法）
                        'h kernel size': 41,  # 水平直线提取kernel
                        'v kernel size': 21,  # 竖直直线提取kernel
                        'padding': 40,  # 图片padding像素值
                        'tolerance': 6,  # 属于同一行，列的grid坐标允许变动的范围
                        'close size': (9, 9),  # 闭合短线的参数(h, v)
                        'white line width': 1,  # pdf中出现的白色噪音线条的宽度(像素点)
                        'scale_ratio': 1.33333333, # 图片坐标和pdf坐标之间的比值
                        'shrink': (0.5, 0.5)  # pdf读取的文本框的收缩系数
                        }  
        else:
            assert isinstance(dict, hyp), 'hyp should be a dict.'
            self.hyp = hyp
            
        self.sender = Table_Sender(self.hyp['scale_ratio'])
    
    def extract_grid(self, img, text_boxes=None, save_dir=None):
        """
        opencv通过图像处理得到表格的每个表格格子信息，具体是
        主要通过morph open的方法，除去文字，保留线框，再用寻找contour的方法得到表格
        的每个grid的坐标， (xmin,ymin,xmax,ymax)， 主要步骤有:
            1.二值化
            2.提取表格中的直线(morph open)
            3.补全外框线 
            4.闭合角点处的短线
            5.基于contour的方法获得表格中每个grid的坐标
        Arg:
            img(2d array): 图片 bgr格式
            text_boxes(np array or None): text box坐标,维度(#boxes, 4), 4表示xyxy
            save_dir(str or None): 处理过程的可视化保存路径，如果是None则不生成可
               视化。
        return:
            grid_coor(2d array): 维度(grid个数, 4)，xyxy坐标型式
        """        
        # dir
        if save_dir is not None:
            if not Path(save_dir).is_dir():
                Path(save_dir).mkdir(parents=True, exist_ok=True)
        
        # visualization save config
        save_vis = False
        if save_dir is not None:
            save_vis = True
            save_dir = Path(save_dir)
            step = 0
            
        # 在原图上下左右扩充一定量的背景像素
        img = expand_img(img, self.hyp['padding'])            
        if save_vis:
            save_name = form_save_name(str(step), 'expanded')  # 0_expanded.png
            cv_imwrite(str(save_dir / save_name), img)
            step += 1
        
        # 是否是简单颜色图片(粗略估计),如果不为简单颜色图片，进行白线条噪音修正
        # is_likely_to_be_simple_color = guess_if_simple_color(img)
        # logger.info('is_likely_to_be_simple_color: %r' % is_likely_to_be_simple_color)
        # if not is_likely_to_be_simple_color:
        #     img = filter_white_line(img, self.hyp['white line width'])
        #     if save_vis:
        #         save_name = form_save_name(str(step), 'filter white line')
        #         cv_imwrite(str(save_dir / save_name), img)
        #         step += 1

        # 边缘检测方法得到二值化图片
        masks = convert_to_binary(img, self.hyp['thresh'])  # [h, v, h, v]
        if save_vis:
            for i, name in enumerate(['edge_dark2bright_h', 'edge_bright2dark_v']):
                save_name = form_save_name(str(step), name)
                cv_imwrite(str(save_dir / save_name), masks_plus(masks[i*2], masks[i*2+1]) * 255)
            step += 1
        
        # 利用text box优化v_kernel_size
        v_kernel_size = self.hyp['v kernel size']
        h_kernel_size = self.hyp['h kernel size']
        if text_boxes is not None and len(text_boxes) != 0:
            text_boxes = text_boxes + self.hyp['padding']
            masks, v_kernel_size, message = utilize_text_boxes(masks, text_boxes, self.hyp['shrink'])
            if save_vis:
                save_name = form_save_name(str(step), 'erase text area')
                cv_imwrite(str(save_dir / save_name), masks_plus(*masks) * 255)
                
        
        # 形成线框
        frame_masks = []  # mask1, mask2
        for i in range(2):
            mask = masks_plus(masks[i*2], masks[i*2+1])
            mask_h = cv2_open(mask, kernel=np.ones((1, h_kernel_size)))
            mask_v = cv2_open(mask, kernel=np.ones((v_kernel_size, 1)))
            frame_masks.append(masks_plus(mask_h, mask_v))
        if save_vis:
            for i, name in enumerate(['frame_dark2bright', 'frame_bright2dark']):
                save_name = form_save_name(str(step), name)
                cv_imwrite(str(save_dir / save_name), frame_masks[i] * 255)
            step += 1
        frame_mask = masks_plus(*frame_masks)  # merge 2 sides
            
        # 由于是边缘检测，某些案例下线框不是实线，而是有点空的线，对之后contour的判断
        # 造成干扰，所以需要进行“实化”
        mask = cv2_close(frame_mask, np.ones((3, 3)))            
        if save_vis:
            save_name = form_save_name(str(step), 'closed')
            cv_imwrite(str(save_dir / save_name), mask * 255)
            step += 1
            
        # 补充外框线(某些表格样本是缺少左右边框，甚至缺少上下边框，这里进行补偿)
        mask = add_outer_line(mask, self.hyp['padding'], text_boxes)          
        if save_vis:
            save_name = form_save_name(str(step), 'outer_lined')
            cv_imwrite(str(save_dir / save_name), mask * 255)
            step += 1
            
        # 闭合角点缝隙(某些样本中出现了在表格角点处线框存在缝隙的情况，使得grids没有
        # 完全闭合，对之后的判断造成干扰)
        mask = close_grids(mask, self.hyp['close size'][0], self.hyp['close size'][1])  # 0背景, 1前景
        if save_vis:
            save_name = form_save_name(str(step), 'close_grid')
            cv_imwrite(str(save_dir / save_name), mask * 255)
            step += 1
            
        # 得到坐标(基于open cv的contour方法)
        grid_coor = find_grid_coor(mask)
        
        # 格子筛选(例如有相互包含关系的一对grids A和B，除去被包含的grid A)
        grid_coor_filtered = filter_overlapped_grids(grid_coor)
        if save_vis:
            mask_grid = draw_grids(img, grid_coor_filtered)                
            save_name = form_save_name(str(step), 'final_result')
            cv_imwrite(str(save_dir / save_name), mask_grid)
            step += 1
            
        return grid_coor_filtered - self.hyp['padding']
                 
    
    def __call__(self, pdf_path:str, page_index:int, rects:np.ndarray, save_dir:str=None) -> list:
        """
        Args:
            pdf_path(str): pdf file path.
            page_index(int): start from 0.
            rects(2d np array): shape=(#rects，4), 4 means xyxy, the rect areas
                which lined tables possess.
            save_dir(str or None): visulization save directory.
        return:
            tables(list): a list of Table objects.
        """
        # load pdf page which contains image and text infomation
        doc = fitz.open(pdf_path)
        page = doc[page_index]
        rects = np.array(rects)
        
        # extract image and word info from the given rect areas
        img_list = extract_img_in_rects(page, rects, self.hyp['scale_ratio'])
        bboxes_list, text_list = extract_text_in_rects(page, rects, self.hyp['scale_ratio'])
                
        # process each rect area
        tables = []
        for i, (img, bboxes, text) in enumerate(zip(img_list, bboxes_list, text_list)):
            if save_dir is not None:
                # visualizations saved in save_dir/pdf_name/page/index_of_rect
                sub_save_dir = Path(save_dir) / str(i)
                # extract grids with visualization
                grid = self.extract_grid(img, filter_bboxes(bboxes, text), sub_save_dir)
            
            else:
                # just extract grids
                grid = self.extract_grid(img, filter_bboxes(bboxes, text), None)
            
            # form Table oject
            table = Table(img, grid, text, bboxes, tolerance=self.hyp["tolerance"])
            
            tables.append(table)
        
        return tables
    
    def process_lined_table(self, pdf_path:str, page_index:int, rects:np.ndarray) -> list:
        """ wrapper of __call__, for deployment, do the following 3 things:
            1.convert results from numpy array to list format
            2.convert coordinate from image to pdf(by self.hyp['scale_ratio'])
            3.keep the same format as the previous agreement(yxyx, and the definition
            of the unique_gray_values)
        Args:
            pdf_path(str): pdf文件路径
            page_index(int): 页面index(从0开始)
            rects(2d np array): 页面内的有线表格坐标(#有线表格，xyxy)，是图片坐标，即
                                由pdf坐标放大1.33333333得到的图片坐标
        return:
            results(list): 一个页面内多个有线表格的提取结果，[result,...],一个result=
                [grid, grid_order, grid_regions, grid_info, gray_rank, unique_gray_values]
            其中:
            grid(list): [[yxyx],...],每个格子的坐标。
            grid_order(list):([rcrc],...),每个格子对应的行列信息，rcrc=
                                  (Row_lt, Col_lt,  Row_rb, Col_rb),lt=left top,
                                  rb=right bottom。
            grid_region(list):[[yxyx],...],每个格子内的文本框范围，没有文本
                                    的为[-1,-1,-1,-1]。
            grid_info(list):包含每个格子的文本以及文本坐标信息,[info,...];
                其中info = [[line1,line2,...],[line1_coor, line2_coor,...]];
                    其中line1=str,为格子内一行的文本内容,line1_coor=list,为
                    格子内一行文本的坐标，[[[yxyx],...]]
            gray_rank(list): 表示格子背景灰度排行,最亮的格子为1,次亮的
                                  格子为2，以此类推
            unique_gray_values(list):所有格子特有灰度值，例如格子全是白色背景则=
                                        [255],如果格子有一个是黑色剩下全是白色，那么
                                        =[0,255]        
        """
        rects = np.array(rects) * self.hyp['scale_ratio']
        tables = self.__call__(pdf_path, page_index, rects)
        tables_list = self.sender(tables)
        return tables_list
            
        
if __name__ == "__main__":
    pass
      