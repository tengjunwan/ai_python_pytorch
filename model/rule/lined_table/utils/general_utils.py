# -*- coding: utf-8 -*-
"""
Created on Wed Mar 10 16:51:41 2021

@author: EDZ
"""

import numpy as np
import json


def filter_bboxes_in_rect(bboxes:np.ndarray, rect:np.ndarray) -> np.ndarray:
    """filter bboxes(xyxy) if they are in a given rect area(xyxy)"""
    centers = (bboxes[:, [0, 1]] + bboxes[:, [2, 3]]) / 2.0  #  (#bboxes, xy)
    centers = np.concatenate((centers*(-1), centers), axis=1)  # (#bboxes, -x-yxy)
    rect = rect * np.array([-1, -1, 1, 1])  # (-x-yxy)
    bool_array = (rect >= centers).all(axis=1)  # (#bboxes, )
    bboxes_filtered = bboxes[bool_array]
    return bboxes_filtered, bool_array


def round_up_to_odd(f:float) -> int:
    """得到输入值相对应的奇数，这个奇数是不比该值小的最小奇数,例如3->3, 4->5, 3.5->5
    """
    return int(np.ceil(f) // 2 * 2 + 1)


def get_values(values:np.ndarray, tolerance:float) -> np.ndarray:
    """从一组values（一维向量）中得到该向量中的unique values with tolerance.
    例如[1,1,2,2,9,10,15], tolerance=1
    返回[1,9,15]
    """
    values = np.unique(values)  # [1,2,9,10,15]
    diff = np.diff(values)  # [1,7,1,5]
    arg = diff > tolerance  # [F,T,F,T]
    arg = np.append(True, arg)  # [T,F,T,F,T]
    unique_values = values[arg]  # [1,9,15]
    
    return unique_values


def get_uinque_values_with_tolerace_and_order(values:np.ndarray, tolerance:float, small:bool=True) -> tuple:
    """从一组values（一维向量）中得到该向量中的unique values with tolerance和这组向量
    对应的unique values with tolerance的排序关系
    例如[1,15,10,2,9,1,2], tolerance=1
    返回[1,9,15](unique values with tolerance) + [0,2,1,0,1,0,0](order)
    如果small=False,则返回[2,10,15] + [0,2,1,0,1,0,0]
    """
    # for example, values = [1,15,10,2,9,1,2]
    if not small:
        values = np.array(values) * (-1)
    else:
        values = np.array(values)
        
    args_sort = np.argsort(values)  # [0,5,3,6,4,2,1]
    values[args_sort]
    diff = np.diff(values[args_sort])  # [0,1,0,7,1,5]
    arg = diff > tolerance  # [F,F,F,T,F,T]
    arg = np.append(True, arg)  # [T,F,F,F,T,F,T]
    unique_values = values[args_sort][arg]  # [1,9,15]
    # get order
    arg_acc = np.add.accumulate(arg) - 1  # [0,0,0,0,1,1,2]
    order = np.zeros_like(values, dtype=np.int64)
    order[args_sort] = arg_acc[np.arange(len(values))]  # [0,2,1,0,1,0,0]
    
    if not small:
        unique_values = unique_values[::-1] * -1
        order = np.amax(order) - order
    
    return unique_values, order


def _get_order_of_grid(grid:np.ndarray, tolerance:float) -> tuple:
    """从grid坐标得到grid属于第几行第几列的信息，并对grid坐标进行修正(把相差几个像素
    但是属于一行或者一列的坐标归为一个值)
    即从(ymin,xmin,ymax,xmax)得到(Row_tl,Col_tl,Row_br,Col_br)
    Args:
        grid(2d array): (#grids, yxyx)
        tolerance(float): 认为两个相近的数是属于同一个数的范围
    return:
        grid:
        order:
    """
    # 筛选values，去除相近的值
    y_values = get_values(grid[:, [0, 2]], tolerance)
    x_values = get_values(grid[:, [1, 3]], tolerance)
    order = []
    # 得到(Row_tl,Col_tl,Row_br,Col_br),并对原grid进行修正
    for g in grid:
        x_tl, x_br = g[1], g[3]
        x_order_tl = np.abs(x_values - x_tl).argmin()
        x_order_br = np.abs(x_values - x_br).argmin()
        g[1] = x_values[x_order_tl]  
        g[3] = x_values[x_order_br]
        y_tl, y_br = g[0], g[2]
        y_order_tl = np.abs(y_values - y_tl).argmin()
        y_order_br = np.abs(y_values - y_br).argmin()
        g[0] = y_values[y_order_tl]
        g[2] = y_values[y_order_br]
        order.append([y_order_tl, x_order_tl, y_order_br, x_order_br])
    order = np.asarray(order).astype(np.int32)
    
    return grid, order


def get_order_of_grid(grid:np.ndarray, tolerance:float) -> tuple:
    """从grid坐标得到grid属于第几行第几列的信息，并对grid坐标进行修正(把相差几个像素
    但是属于一行或者一列的坐标归为一个值)
    即从(xmin,ymin,xmax,ymax)得到(Col_tl,Row_tl,Col_br,Row_br)
    Args:
        grid(2d array): (#grids, xyxy)
        tolerance(float): 认为两个相近的数是属于同一个数的范围
    return:
        grid:
        order:
    """
    # flatten to 1d, then get unique values with tolerance and related order
    x_values = np.concatenate((grid[:, 0], grid[:, 2]))
    y_values = np.concatenate((grid[:, 1], grid[:, 3]))
    unique_xs, order_x = get_uinque_values_with_tolerace_and_order(x_values, tolerance)
    unique_ys, order_y = get_uinque_values_with_tolerace_and_order(y_values, tolerance)
    
    # get order
    order_x_reshape = order_x.reshape(2, -1).transpose()
    order_y_reshape = order_y.reshape(2, -1).transpose()
    order = np.concatenate((order_x_reshape[:, [0]], order_y_reshape[:, [0]],
                            order_x_reshape[:, [1]], order_y_reshape[:, [1]]), axis=1)
    
    # amend values to their related unique values
    x_values[np.arange(len(x_values))] = unique_xs[order_x[np.arange(len(x_values))]]
    x_values = x_values.reshape(2, -1).transpose()
    y_values[np.arange(len(y_values))] = unique_ys[order_y[np.arange(len(y_values))]]
    y_values = y_values.reshape(2, -1).transpose()
    
    grid = np.concatenate((x_values[:, [0]], y_values[:, [0]], x_values[:, [1]], y_values[:, [1]]), axis=1)
    
    return grid, order


def filter_overlapped_grids(grid_coor:np.ndarray) -> np.ndarray:
    """过滤掉有包含关系grid（去除被包含的grid）
    Args:
        grid_coor(2d array): 维度(#grid, 4), 4表示xyxy
    return:
        grid_coor_filtered(2d array): 维度(#grid, 4), 4表示xyxy
    """
    n = len(grid_coor)
    if n < 2:
        return grid_coor
    grid_coor_ = grid_coor * np.array([-1, -1, 1, 1])  # -x-yxy
    grid_coor_ += np.array([2, 2, -2, -2])  # 每个grid缩小两个像素，留有误差余地
    grid_coor_A = np.expand_dims(np.copy(grid_coor_), axis=0)  # 维度(1, n, 4)
    grid_coor_B = np.expand_dims(np.copy(grid_coor_), axis=1)  # 维度(n, 1, 4)
    
    coor_diff_matrix = grid_coor_B - grid_coor_A  # 维度(n, n, 4)
    
    # 如果grid i,在grid j内，那么grid_overlap_matrix[i,j]=True, 
    # 下面的式子是为了避免两个grid完全重合的情况，所以写的比较复杂
    grid_overlap_matrix = (coor_diff_matrix >= 0).all(axis=2) * (coor_diff_matrix > 0).any(axis=2)  # (n, n) 

    arg_to_filter = np.argwhere(np.logical_not(grid_overlap_matrix.any(axis=0))).flatten()
    
    return grid_coor[arg_to_filter]  
    

def form_save_name(*name_parts, suffix='.png'):
    # namePart1_namePart2_namePart3.png
    if not suffix.startswith('.'):
        suffix = '.' + suffix
    assert suffix in ['.png', '.PNG', '.jpeg', '.JPEG', '.jpg', '.JPG']
    save_name = '_'.join(name_parts) + suffix
    return save_name


def _form_save_name(suffix, *name_parts):
    # namePart1_namePart2_namePart3.png
    if not suffix.startswith('.'):
        suffix = '.' + suffix
    assert suffix in ['.png', '.PNG', '.jpeg', '.JPEG', '.jpg', '.JPG']
    save_name = '_'.join(name_parts) + suffix
    return save_name


def xyxy2yxyx(bboxes:np.ndarray) -> np.ndarray:
    original_shape = bboxes.shape
    bboxes = bboxes.reshape(-1, 4)
    new_bboxes = np.zeros_like(bboxes)
    new_bboxes[:, 0] = bboxes[:, 1]
    new_bboxes[:, 1] = bboxes[:, 0]
    new_bboxes[:, 2] = bboxes[:, 3]
    new_bboxes[:, 3] = bboxes[:, 2]

    return new_bboxes.reshape(original_shape)

def yxyx2xyxy(bboxes:np.ndarray) -> np.ndarray:
    return xyxy2yxyx(bboxes)
    

def load_json_file_from_yolo(json_path:str, lined_table_idx=2) -> list:
    # yolo result save as json, [page0_detection, page1_detection, ...]
    with open(json_path, encoding='utf-8') as f:
        data = json.load(f)
    data = [np.array(i).reshape(-1, 6) for i in data]
    # filter lined table, detection=(#bboxes, xyxy+conf+cls)
    data = [i[i[:, 5]==lined_table_idx][:,:4] for i in data]  # just xyxy of lined table
    return data    
        

if __name__ == "__main__":
    pass
    
    