# -*- coding: utf-8 -*-
"""
Created on Tue Mar  9 16:56:26 2021

@author: EDZ
"""

import cv2
import numpy as np
import fitz
from PIL import ImageFont, ImageDraw, Image
from model.rule.lined_table.utils.pdf_utils import page2image
from model.rule.lined_table.utils.pdf_utils import get_word_bboxes



def draw_bboxes(img:np.ndarray, draw_bboxes:np.ndarray, color:tuple=(0,0,255)) -> np.ndarray:
    for x0, y0, x1, y1 in draw_bboxes:
        start_point = (int(round(x0)), int(round(y0)))
        end_point = (int(round(x1)), int(round(y1)))
        cv2.rectangle(img, start_point, end_point, color, thickness=1)  # red color
    return img


def draw_text_bbox(page:fitz.fitz.Page, scale_ratio:float=1.33333333) -> np.ndarray:
    img = page2image(page, scale_ratio)
    bboxes = get_word_bboxes(page, scale_ratio)
    img = draw_bboxes(img, bboxes)
    return img


def draw_text(img:np.ndarray, text:list, coor:list, fontScale:int=1, 
              color:tuple=(0,0,255), thickness:int=1) -> np.ndarray:
    for t, (x0, y0, x1, y1) in zip(text, coor):
        mid_point = (int((x0+x1)/2.0), int((y0+y1)/2.0))
        img = cv2.putText(img, t, mid_point, cv2.FONT_HERSHEY_SIMPLEX, fontScale, 
                          color, thickness, cv2.LINE_AA)
                        
    return img


def draw_lines(img:np.ndarray, lines:np.ndarray, color:tuple=(0,0,255),
              thickness:int=1, style:str='dotted', gap:int=5) -> np.ndarray:
    for x0, y0, x1, y1 in lines:
        dist = ((x1 - x0)**2 + (y1 - y0)**2)**0.5  # distance of lines
        pts = []
        
        for i in np.arange(0, dist, gap):  # get points on the line 
            ratio = i / dist
            x = int(x0 * (1 - ratio) + x1 * ratio + 0.5)
            y = int(y0 * (1 - ratio) + y1 * ratio + 0.5)
            pts.append((x, y))
        
        if style == "dotted":
            for p in pts:
                print(color)
                img = cv2.circle(img, p, thickness, color, -1)
        else:  # dashed
            s = pts[0]
            e = pts[0]
            i = 0
            for p in pts:
                s = e
                e = p
                if i % 2 == 1:  # only draw line with odd index
                    img = cv2.line(img, s, e, color, thickness)
                i += 1
    return img
        

def draw_grids(img, grids):
    """格子可视化"""
    mask_grid = np.zeros_like(img).astype(np.uint8)
    for x0, y0, x1, y1 in grids:  # xyxy
        start_point = (int(round(x0)), int(round(y0)))
        end_point = (int(round(x1)), int(round(y1)))
        mid_point = (int(round((x0+x1)/2.0)), int(round((y0+y1)/2.0)))
        mask_grid = cv2.rectangle(mask_grid, start_point, end_point, (255,255,255), thickness=2)
        mask_grid = cv2.circle(mask_grid, mid_point, 7, (255,255,255), -1)
    return mask_grid


def draw_table(img_size:tuple, grids:np.ndarray,
               grid_regions:np.ndarray, grid_info:list,
               gray_rank:np.ndarray,
               unique_gray_values:np.ndarray) -> np.ndarray:
    table_img = np.ones(img_size, dtype=np.uint8) * 255
    for i, (grid_x0, grid_y0, grid_x1, grid_y1) in enumerate(grids):
        # draw grid background 
        gray = unique_gray_values[len(unique_gray_values) - gray_rank[i]]
        table_img[int(round(grid_y0)): int(round(grid_y1))+1,
                  int(round(grid_x0)): int(round(grid_x1))+1] = gray
        
        # draw region
        r_x0, r_y0, r_x1, r_y1 = grid_regions[i]
        table_img[int(round(r_y0)): int(round(r_y1))+1,
                  int(round(r_x0)): int(round(r_x1))+1] = gray * 0.95  # darker        
        
        # draw grid
        table_img = cv2.rectangle(table_img, 
                                  (int(round(grid_x0)), int(round(grid_y0))), 
                                  (int(round(grid_x1)), int(round(grid_y1))),
                                  (0,0,0),
                                  thickness=1)
            
    # draw text, cause open-cv doesn't support chinese writing, use PIL here
    table_img_pil = Image.fromarray(table_img)
    font = ImageFont.truetype('font/simsun.ttc', 10)
    draw = ImageDraw.Draw(table_img_pil)
    for g_info in grid_info:
        lines = g_info[0]
        line_coors = g_info[1]
        for line, line_coor in zip(lines, line_coors):
            l_x, l_y, _, _ = line_coor[0]  # first character's top-left coordinate
            draw.text(xy=(int(round(l_x)), int(round(l_y))),
                      text=line,
                      font=font,
                      fill=(0,0,0))
        
    table_img = np.array(table_img_pil)  # back to cv2
    return table_img