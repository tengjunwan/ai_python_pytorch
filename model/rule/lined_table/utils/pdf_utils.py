# -*- coding: utf-8 -*-
"""
Created on Tue Mar  9 14:25:24 2021

@author: EDZ
"""

import fitz
import cv2
import numpy as np
from PIL import Image
import io
from model.rule.lined_table.utils.general_utils import filter_bboxes_in_rect


def read_image_in_bytes(bytes_image:str) -> np.ndarray:
    image = Image.open(io.BytesIO(bytes_image))
    return cv2.cvtColor(np.array(image, dtype=np.uint8), cv2.COLOR_RGB2BGR)


def page2image(page:fitz.fitz.Page, scale_ratio:float=1.33333333, 
               rotate:'int'=0, alpha=False) -> np.ndarray:
    trans = fitz.Matrix(scale_ratio, scale_ratio).preRotate(rotate)  # rotate means clockwise 
    pm = page.getPixmap(matrix=trans, alpha=alpha)
    data = pm.getImageData()  # bytes
    # img = read_image_in_bytes(data)
    img_np = np.frombuffer(data, dtype=np.uint8)
    img = cv2.imdecode(img_np, flags=-1)
    return img


def bgra2bgr(img):
    alpha = img[:, :, 3]
    if True:
        _, alpha = cv2.threshold(alpha, 50, 255, cv2.THRESH_BINARY)
    alpha = alpha / 255.0
    alpha = np.expand_dims(alpha, axis=2)
    source = img[:, :, :3] / 255.0
    bg = np.array([255, 255, 255]) / 255.0
    target = (1 - alpha) * bg + alpha * source
    ret = (target * 255).astype(np.uint8)
    return ret


def page2image_linetable(page:fitz.fitz.Page, scale_ratio:float=1.33333333) -> np.ndarray:
    img = page2image(page, scale_ratio=1.33333333, rotate=0, alpha=True) 
    if img.shape[2] == 4:
        img = bgra2bgr(img)
        return img
    else:
        return img

    

def pdf2img(pdf_path, page): 
    """pdf转图片,图片格式BGR"""
    doc = fitz.open(pdf_path)
    rotate = int(0)
    zoom_x = 1.33333333
    zoom_y = 1.33333333
    trans = fitz.Matrix(zoom_x, zoom_y).preRotate(rotate)
    pm = doc[page].getPixmap(matrix=trans, alpha=True)
    img = pm.getPNGData()
    img_np = np.frombuffer(img, dtype=np.uint8)
    img = cv2.imdecode(img_np, flags=1)
    return img


# def form_text_binary_image(page:fitz.fitz.Page, scale_ratio:float=1.33333333) -> np.ndarray:
#     img = page2image(page, scale_ratio)
#     binary_image = np.zeros(img.shape[:2], dtype=np.uint8)  # background as zero
#     text_bboxes, _ = get_word_bboxes(page, scale_ratio).round().astype(np.int32)
#     for x0, y0, x1, y1 in text_bboxes:
#         binary_image[y0: y1+1, x0: x1+1] = 1  # text area as one
#     return binary_image


def get_word_bboxes(page:fitz.fitz.Page, scale_ratio:float=1.33333333) -> tuple:
    # use Page.getText('words')
    words = page.getText('words')  # (x0, y0, x1, y1, "string", blocknumber, linenumber, wordnumber)
    bboxes = []
    text = []
    for word in words:
        bboxes.append([word[0], word[1], word[2], word[3]])
        text.append(word[4])
    bboxes = np.array(bboxes).reshape(-1, 4) *  scale_ratio # in case bboxes is empty
    
    # adjust coordinate according to Page.rotation
    bboxes = change_bbox_coordinate_by_rotation(bboxes, page)
    
    return bboxes, text


def get_block_bboxes(page:fitz.fitz.Page, scale_ratio:float=1.33333333) -> tuple:
    # use Page.getText('blocks')
    words = page.getText('blocks')  # (x0, y0, x1, y1, "lines in block", block_type, block_no)
    bboxes = []
    text = []
    for word in words:
        bboxes.append([word[0], word[1], word[2], word[3]])
        text.append(word[4])
    bboxes = np.array(bboxes).reshape(-1, 4) *  scale_ratio # in case bboxes is empty
    
    # adjust coordinate according to Page.rotation
    bboxes = change_bbox_coordinate_by_rotation(bboxes, page)
    return bboxes, text


def change_bbox_coordinate_by_rotation(bboxes:np.ndarray, page:fitz.fitz.Page, 
                                       scale_ratio:float=1.33333333) -> np.ndarray:
    # cause getText return the coordinate before rotation, we need to ajust to
    # the coordinate after rotation
    if page.rotation == 0:
        return bboxes
    else:
        img = page2image(page, scale_ratio)
        h, w = img.shape[: 2]
        if page.rotation == 90:
            # y = x', x = w - y'
            x0 = w - bboxes[:, 3]
            y0 = bboxes[:, 0]
            x1 = w - bboxes[:, 1]
            y1 = bboxes[:, 2]
            bboxes = np.stack((x0, y0, x1, y1), axis=1)
            return bboxes
        elif page.rotation == 180:
            # y = h - y', x = w - x'
            x0 = w - bboxes[:, 3]
            y0 = h - bboxes[:, 2]
            x1 = w - bboxes[:, 1]
            y1 = h - bboxes[:, 0]
            bboxes = np.stack((x0, y0, x1, y1), axis=1)
            return bboxes
        elif page.rotation == 270:
            # y = h - x', x = y'
            x0 = bboxes[:, 1]
            y0 = h - bboxes[:, 2]
            x1 = bboxes[:, 3]
            y1 = h - bboxes[:, 0]
            bboxes = np.stack((x0, y0, x1, y1), axis=1)
            return bboxes
        else:
            raise ValueError('page.rotation should be 0, 90, 180, 270, not %g'
                             % page.rotation)
    

def get_char_bboxes(page:fitz.fitz.Page, scale_ratio:float=1.33333333, 
                    shrink:bool=True) -> tuple:
    # use Page.getText('rawdict') to get chars bboxes
    text = []
    bboxes = []
    content = page.getText('rawdict')  # content(dict): 'width', 'height', 'blocks'
    for block in content['blocks']:  # block(dict): 'number', 'type', 'bbox', 'lines'
        if block['type'] != 0:  # not a text block
            continue
        for line in block['lines']:  # line(dict): 'spans', 'wmode', 'dir', 'bbox'
            for span in line['spans']:  # span(dict): 'size', 'flags', 'font', 'color', 
                                        # 'ascender', 'descender', 'chars', 'origin', 'bbox'
                ascender = span['ascender']
                descender = span['descender']
                size = span['size']
                for char in span['chars']:  # char(dict): 'origin', 'bbox', 'c'
                    text.append(char['c'])
                    if not shrink:
                        bboxes.append(char['bbox'])        
                    else:
                        x0, y0, x1, y1 = char['bbox']
                        y_origin = char['origin'][1]
                        y0, y1 = shink_bbox(ascender, descender, size, y0, y1, y_origin)
                        bboxes.append((x0, y0, x1, y1))
                    
    bboxes = np.array(bboxes).reshape(-1, 4) *  scale_ratio # in case bboxes is empty
    
    # adjust coordinate according to Page.rotation
    bboxes = change_bbox_coordinate_by_rotation(bboxes, page)
    return bboxes, text


def get_word_bboxes_by_dict(page:fitz.fitz.Page, scale_ratio:float=1.33333333, 
                            shrink:bool=True) -> tuple:
    # use Page.getText('dict') to get word bboxes
    text = []
    bboxes = []
    content = page.getText('dict')  # content(dict): 'width', 'height', 'blocks'
    for block in content['blocks']:  # block(dict): 'number', 'type', 'bbox', 'lines'
        if block['type'] != 0:  # not a text block
            continue
        for line in block['lines']:  # line(dict): 'spans', 'wmode', 'dir', 'bbox'
            for span in line['spans']:  # span(dict): 'size', 'flags', 'font', 'color', 
                                        # 'ascender', 'descender', 'chars', 'origin', 'bbox'
                ascender = span['ascender']
                descender = span['descender']
                size = span['size']
                y_origin = span['origin'][1]
                text.append(span['text'])
                if not shrink:
                    bboxes.append(span['bbox'])        
                else:
                    x0, y0, x1, y1 = span['bbox']
                    y0, y1 = shink_bbox(ascender, descender, size, y0, y1, y_origin)
                    bboxes.append((x0, y0, x1, y1))
                    
    bboxes = np.array(bboxes).reshape(-1, 4) *  scale_ratio # in case bboxes is empty
    
    # adjust coordinate according to Page.rotation
    bboxes = change_bbox_coordinate_by_rotation(bboxes, page)
    return bboxes, text


def shink_bbox(ascender:float, descender:float, size:float, y0:float, y1:float,
               y_origin:float) -> tuple:
    # shink bbox to the reduced glyph heights
    # details on https://pymupdf.readthedocs.io/en/latest/textpage.html#dictionary-structure-of-extractdict-and-extractrawdict
    if size >= y1 - y0:  # don't need to shrink
        return y0, y1
    else:
        new_y1 = y_origin - size * descender / (ascender - descender)
        new_y0 = new_y1 - size
        return new_y0, new_y1
    
    
def extract_img_and_text_in_rect(page:fitz.fitz.Page, rect:np.ndarray, scale_ratio:float=1.33333333) -> tuple:
    # given a rect area, extract img and all text within it, then ajust the 
    # coordinate to the left top of the rect area
    img = page2image(page, scale_ratio)
    bboxes, text = get_char_bboxes(page, scale_ratio)
    img = img[rect[1]: rect[3]+1, rect[0]: rect[2]+1, :]  # slice
    bboxes, bool_array = filter_bboxes_in_rect(bboxes, rect)  # filter bboxes
    text = np.array(text)[bool_array].tolist()  # filter text in bboxes
    
    # ajust coordinate to the left top of the rect area
    bboxes = bboxes - rect[[0, 1, 0, 1]]
    return img, bboxes, text


def extract_img_and_text_in_rects(page:fitz.fitz.Page, rects:np.ndarray, scale_ratio:float=1.33333333) -> tuple:
    # given rect areas, extract img and all text within'em, then ajust the 
    # coordinate to the left top of the rect area; (i don't want to call "extract
    # _img_and_text_in_rect" multiple times, cause it's not effective)
    img_list = []
    bboxes_list = []
    text_list = []
    img = page2image(page, scale_ratio)
    bboxes, text = get_char_bboxes(page, scale_ratio)
    rects = rects.reshape(-1, 4).round().astype(np.int64)
    for rect in rects:
        img_list.append(img[rect[1]: rect[3]+1, rect[0]: rect[2]+1, :])  # slice
        bboxes_rect, bool_array = filter_bboxes_in_rect(bboxes, rect)  # filter bboxes
        bboxes_list.append(bboxes_rect - rect[[0, 1, 0, 1]])  # ajust coordinate to the left top of the rect area
        text_list.append(np.array(text)[bool_array].tolist())  # filter text in bboxes
    return img_list, bboxes_list, text_list
    

def extract_text_in_rects(page:fitz.fitz.Page, rects:np.ndarray, scale_ratio:float=1.33333333) -> tuple:
    bboxes_list = []
    text_list = []
    bboxes, text = get_char_bboxes(page, scale_ratio)
    rects = rects.reshape(-1, 4).round().astype(np.int64)
    for rect in rects:
        bboxes_rect, bool_array = filter_bboxes_in_rect(bboxes, rect)  # filter bboxes
        bboxes_list.append(bboxes_rect - rect[[0, 1, 0, 1]])  # ajust coordinate to the left top of the rect area
        text_list.append(np.array(text)[bool_array].tolist())  # filter text in bboxes
    return bboxes_list, text_list


def extract_img_in_rects(page:fitz.fitz.Page, rects:np.ndarray, scale_ratio:float=1.33333333) -> list:
    img_list = []
    img = page2image_linetable(page, scale_ratio)
    rects = rects.reshape(-1, 4).round().astype(np.int64)
    for rect in rects:
        img_list.append(img[rect[1]: rect[3]+1, rect[0]: rect[2]+1, :])  # slice
    return img_list


if __name__ == "__main__":
    doc = fitz.open('../sample/宁波银行：2017年年度报告.PDF')
    page_index = 1
    page = doc[page_index]
    content = page.getText('dict')
    # bboxes, _ = get_char_bboxes(page)
    