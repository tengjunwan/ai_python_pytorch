# -*- coding: utf-8 -*-
"""
Created on Wed Mar 10 18:30:59 2021

@author: EDZ
"""

import numpy as np
import json
from model.rule.lined_table.utils.general_utils import get_uinque_values_with_tolerace_and_order

               
    
def filter_bboxes(bboxes:np.ndarray, text:list) -> np.ndarray:
    """根据文本内容过滤bboxes
    Arg:
        bboxes(np.ndarray): (#bboxes, xyxy)
        text(list of str): content in bboxes
    return:
        bboxes(np.ndarray): (#bboxes, xyxy)
    """
    punc = "。，《》<>()（）,.、\~：:;；%“”‘’''+-_．【】"  # 经验测试(后期可能需要补充)
    bboxes_filtered = []
    for i, t in enumerate(text):
        if t in punc or t == ' ':
            pass
        else:
            bboxes_filtered.append(bboxes[i])
    
    bboxes_filtered = np.array(bboxes_filtered).reshape(-1, 4)
    
    return bboxes_filtered


def get_grid_text(grid, content_coor, content):
    """通过文字的坐标和表格grid的坐标，返回各个grid对应的文字string,以及各个grid中
    文字的区域
    Arg:
        grid(np array):维度(#grid, 4)，坐标型式xyxy
        content_coor(np array): 维度(#content, 4)，坐标型式xyxy
        content(list of string): 长度= #content
    return:
        grid_region(np array):维度(#grid, 4),坐标形式xyxy，没有内容的grid的region
            为-1,-1,-1,-1
        grid_info(list): 每一个元素是表示一个grid的信息，该grid又包含两个信息，一个是
            按照行分割的字符串，一个是字符串对应的字符坐标xyxy
            
    """
    # 初始化
    grid_info_list = [
                        {
                     "lines": [],
                     "region": [-1, -1, -1, -1],  # default value, means no content in the grid
                     "line_coordinates":[],
                     "line_type":[],
                         } 
                    for _ in range(len(grid))]
    # grid_region_list = np.ones((len(grid), 4)) * (-1)  # 每个grid的内容总区域坐标(初始化标记为-1)
    # grid_info_list = [[[], []] for _ in range(len(grid))]  # 每个grid的内容行string以及对应的坐标
    

    assert len(content_coor) == len(content), "the number of coordinates doesn't equals to the number of content "
    if len(content_coor) == 0:  # do nothing
        return grid_info_list
    
    content =np.array(content)  # list of str转化为array型式
    
    # 用矩阵运算求每个格子(grid)和每个文字(content)的包含关系
    center = (content_coor[:, [0, 1]] + content_coor[:, [2, 3]]) / 2.0  # (#content, xy)
    center = np.concatenate((center*(-1), center), axis=1)  # (#content, -x-yxy)
    center = np.expand_dims(center, axis=0)  # (1, #grid, -x-yxy)
    grid_ = grid * np.array([[-1, -1, 1, 1]])  # (#grid, -x-yxy)
    grid_ = np.expand_dims(grid_, axis=1)  # (#grid, 1, -x-yxy)
    belong_matrix = (grid_ >= center).all(axis=2)  #  (#grid, #content), boolean matrix
    
    # 遍历每一个grid
    for i, g in enumerate(belong_matrix):
        g_content_coor = content_coor[g]  # (#content in grid, xyxy)
        g_content = content[g]  # (#content in grid, xyxy)
        
        lines, line_coors, region = arrange_text_old(g_content_coor, g_content)
        grid_info_list[i]["region"] = region
        grid_info_list[i]["lines"] = lines
        grid_info_list[i]["line_coordinates"] = line_coors
        
    return grid_info_list


def arrange_text(content_coor:np.ndarray, content:np.ndarray) -> tuple:
    """given a list of text and its coordinates, arrange the text for a reasonable
    reading order.
    Args:
        content_coor(np.ndarray): shape=(#len(content), 4)，4 means xyxy.
        content(np.ndarray): dtype='<U1', shape=(#len(content)).                                     
    return:
    """
    assert len(content_coor) == len(content), "the number of coordinates doesn't equals to the number of content"
    
    # initialize
    lines = []  # 长度为格子内的行数, list of string, ['line1', 'line2', ...]
    line_coors = []  # 长度为格子内的行数, list of array, [(#words in line1, 4), (#words in line2, 4), ...]
    line_types = []  # 长度为格子内的行数, list of string, ['', 'line2', ...]
    region = [-1, -1, -1, -1]  # default
    
    # early return
    if len(content_coor) == 0:
        return lines, line_coors, region
    
    # find start and end of lines
    line_ranges = get_projection_ranges(content_coor[:, [1, 3]])  # (#lines, 2), 2 meanes (start, end)
    
    # check content belong to lines 
    content_center = (content_coor[:, [0, 1]] + content_coor[:, [2, 3]]) / 2.0  # (x_center, y_center)
    line_ranges_ = np.expand_dims(line_ranges, axis=1)  # (#lines, 1, 2)
    content_center_ = np.expand_dims(content_center, axis=0)  # (1, #content, 2)
    belong_matrix = np.logical_and(line_ranges_[:, :, 0] < content_center_[:, :, 1],  # (#lines, #content)
                                   line_ranges_[:, :, 1] > content_center_[:, :, 1])
    
    content_coor_sorted = []  
    content_sorted = []  
    line_index = []  
    content_type = []
    for i, b in enumerate(belong_matrix):
        line_content_coor = content_coor[b]
        line_content = content[b]
        # sort by x0
        line_order = line_content_coor[:, 0].argsort()  # (#content in this line, )
        line_content_coor_sorted = line_content_coor[line_order]  # (#content in this line, 4)
        line_content_sorted = line_content[line_order]  # (#content in this line, )
        # content type
        line_content_type = get_content_type(line_content_coor_sorted, line_ranges[i])
        # add to list
        content_coor_sorted.append(line_content_coor_sorted)
        content_sorted.append(line_content_sorted)
        line_index.extend([i] * len(line_content))
        content_type.append(line_content_type)
    content_coor_sorted = np.vstack((content_coor_sorted))  # (#content, 4)
    content_sorted = np.hstack((content_sorted))  # (#content, )
    content_sorted = ''.join(content_sorted)  # str with #content char
    content_type = np.hstack((content_type))  # (#content, )
    
    # get rid of space()
    n_total = len(content_sorted)
    n_head_white_space = n_total - len(content_sorted.lstrip(' '))  
    n_tail_white_space = len(content_sorted.lstrip(' ')) - len(content_sorted.strip(' ')) 
    if n_total == n_head_white_space + n_tail_white_space:  # just white space in the grid, ignore them
        return lines, line_coors
    start, end = n_head_white_space, n_total - n_tail_white_space 
    content_sliced = content_sorted[start: end]  
    content_coor_sliced = content_coor_sorted[start: end]
    line_index_sliced = line_index[start: end]    
    content_type_sliced = content_type[start: end]
        
    # 求得文字区域
    if len(content_coor_sliced) != 0:
        ymax = np.amax(content_coor_sliced[:, 3])
        ymin = np.amin(content_coor_sliced[:, 1])
        xmax = np.amax(content_coor_sliced[:, 2])
        xmin = np.amin(content_coor_sliced[:, 0])
        region = [xmin, ymin, xmax, ymax]  # the bounding rectangular of the text area
        
    # 切分content为一行一行的lines
    split_index = np.argwhere(np.diff(line_index_sliced) > 0).flatten() + 1  
    split_index = np.insert(split_index, 0, 0)  
    split_index = np.append(split_index, len(content_sliced))  
    for j in range(len(split_index) - 1):  # loop in pairs
        start , end = split_index[j], split_index[j+1]  
        lines.append(content_sliced[start: end])  
        line_coors.append(content_coor_sliced[start: end])  
        line_types.append(content_type_sliced[start: end])
    
    return lines, line_coors, region, line_types

    
def arrange_text_old(content_coor:np.ndarray, content:np.ndarray) -> tuple:
    """given a list of text and its coordinates, arrange the text for a reasonable
    reading order.
    Args:
        content_coor(np.ndarray): shape=(#len(content), 4)，4 means xyxy.
        content(np.ndarray): dtype='<U1', shape=(#len(content)).                                     
    return:
    """
    assert len(content_coor) == len(content), "the number of coordinates doesn't equals to the number of content"
    # config
    tolerance = 4
    
    # 初始化
    lines = []  # 长度为格子内的行数, list of string, ['line1', 'line2', ...]
    line_coors = []  # 长度为格子内的行数, list of array, [(#words in line1, 4), (#words in line2, 4), ...]
    region = [-1, -1, -1, -1]  # default
    
    if len(content_coor) == 0:
        return lines, line_coors, region
    
    # 取得y上的unique values(容许浮动)，并获得相关的排序(left top y)
    y_values, y_orders = get_uinque_values_with_tolerace_and_order(content_coor[:, 1], tolerance=tolerance)  
    
    # 排序
    index = y_orders * 10000 + content_coor[:, 0]  # 优先考虑y再考虑x
    order = index.argsort()  
    content_sorted = ''.join(content[order].tolist())  # str
    content_coor_sorted = content_coor[order]
    y_orders_of_content =y_orders[order]  
    
    # 去掉首尾的空格(首行的开始+尾行的结束)
    n_total = len(content_sorted)
    n_head_white_space = n_total - len(content_sorted.lstrip(' '))  
    n_tail_white_space = len(content_sorted.lstrip(' ')) - len(content_sorted.strip(' ')) 
    if n_total == n_head_white_space + n_tail_white_space:  # just white space in the grid, ignore them
        return lines, line_coors, region
    start, end = n_head_white_space, n_total - n_tail_white_space 
    content_sliced = content_sorted[start: end]  
    content_coor_sliced = content_coor_sorted[start: end]
    y_orders_of_content_sliced = y_orders_of_content[start: end]  
    
    # 求得文字区域
    if len(content_coor_sliced) != 0:
        ymax = np.amax(content_coor_sliced[:, 3])
        ymin = np.amin(content_coor_sliced[:, 1])
        xmax = np.amax(content_coor_sliced[:, 2])
        xmin = np.amin(content_coor_sliced[:, 0])
        region = [xmin, ymin, xmax, ymax]  # the bounding rectangular of the text area
    
    # 切分content为一行一行的lines
    split_index = np.argwhere(np.diff(y_orders_of_content_sliced) > 0).flatten() + 1  
    split_index = np.insert(split_index, 0, 0)  
    split_index = np.append(split_index, len(content_sliced))  
    for j in range(len(split_index) - 1):  # loop in pairs
        start , end = split_index[j], split_index[j+1]  
        lines.append(content_sliced[start: end])  
        line_coors.append(content_coor_sliced[start: end])  
    
    return lines, line_coors, region


def get_projection_ranges(ranges:np.ndarray) -> np.ndarray:
    """given a list of ranges (start, end), and then project them to a axis to 
    get projection ranges, which is also a list of ranges.
    Args:
        ranges(np.ndarray): shape=(#ranges, 2), 2 means (s, e)=(start, end).
    return:
        projection_ranges(np.ndarray): shape=(#ranges, 2), 2 means (s, e)=(start, end).
    """
    assert np.all(ranges[:, 1] >= ranges[:, 0]), "'end' is supposed to be bigger than 'start'"
    projection_ranges = np.empty((0, 2))  # initialize
    for s, e in ranges:
        non_overlap = np.logical_or(projection_ranges[:, 0] > e, projection_ranges[:, 1] < s)  # (#projection_ranges, )
        overlap = np.logical_not(non_overlap)
        if np.any(overlap): # update the overlapped projection_range
            assert np.sum(overlap) == 1, "something wrong here."
            new_s = min(s, projection_ranges[overlap][0][0])
            new_e = max(e, projection_ranges[overlap][0][1])
            projection_ranges[overlap][0] = [new_s, new_e]
        else:  # add a new projection_range
            projection_ranges = np.vstack((projection_ranges, [[s, e]]))
            
    return projection_ranges


def get_content_type(content_coor, line_range, per_thres=0.7):
    """check the type of content(i.e., text). There are sevaral types of content:
        0: normal text, which possess almost the entire space within each line;
        1: upper text, which possess the upper space within each line(like a note symbol);
        2: lower text, which possess the lower space within each line.
    Args:
        content_coor(np.ndarray): shape=(#content, 4), 4 means xyxy.
        line_range(np.ndarray): shape=(2, ), 2 means (y_start, y_end).
        per_thres(float): if (content height) / (line height) above this, it is
            considered as normal text.
    return:
        content_type(np.ndarray): shape=(#content, ), store the type index of 
            each content.
    """
    # initialize
    content_type = np.zeros(len(content_coor))  # normal text as default
    
    # check
    line_height = line_range[1] - line_range[0]
    line_mid_y = (line_range[1] + line_range[0]) / 2.0
    content_height = content_coor[:, 3] - content_coor[:, 1]
    content_per = content_height / line_height
    args_not_normal = np.nonzero(content_per <= per_thres)[0]
    for arg in args_not_normal:
        not_normal_coor = content_coor[arg]  # (4, ) = xyxy
        y_center = (not_normal_coor[1] + not_normal_coor[3]) / 2.0
        if y_center <= line_mid_y:  # upper text
            content_type[arg] = 1
        else:  # lower text
            content_type[arg] = 2
    
    return content_type


def save_text_info_as_json(save_path:str, grid_order:np.ndarray, grid_info:list) -> None:
    # save grid text content as json file
    save_content = []
    for i, (col0, row0, col1, row1) in enumerate(grid_order):
        grid_dict = {"ltCol": int(col0),
                     "ltRow": int(row0),
                     "rbCol": int(col1),
                     "rbRow": int(row1),
                     "text": grid_info[i][0]}
        save_content.append(grid_dict)
        
    with open(save_path, 'w', encoding='utf-8') as f:
        json.dump(save_content, f, ensure_ascii=False)
        
        
if __name__ == "__main__":
    import fitz
    import cv2
    from utils.pdf_utils import page2image
    from utils.pdf_utils import extract_img_and_text_in_rect
    
    # config
    # pdf_path = '../sample/高澜股份：2021年半年度报告-1629715726468.PDF'
    # page_index = 51
    # rect = np.array([593, 748, 660, 767])
    
    # pdf_path = '../sample/华夏幸福：华夏幸福2021年半年度报告.PDF'
    # page_index = 47
    # rect = np.array([271, 506, 304, 521])
    
    # pdf_path = '../sample/高澜股份：2021年半年度报告-1629715726468.PDF'
    # page_index = 51
    # rect = np.array([76, 479, 216, 501])
    
    pdf_path = '../sample/华夏幸福：华夏幸福2021年半年度报告.PDF'
    page_index = 229
    rect = np.array([96, 232, 186, 338])
    
    
    doc = fitz.open(pdf_path)
    page = doc[page_index]
    img = page2image(page)
    # cv2.imshow('page', img)
    # cv2.waitKey()
    
    # img_save_path = save_dir / (str(page_index) + '.png')
    # cv_imwrite(str(img_save_path), img)

    
    img, bboxes, text = extract_img_and_text_in_rect(page, rect)
    text = np.array(text)
    cv2.imshow('grid', img)
    cv2.waitKey()
    
    list_of_chars = []
    for b, t in zip(bboxes, text):
        x0, y0, x1, y1 = b
        list_of_chars.append(Char(x0, y0, x1, y1, t))
    lines = Lines(list_of_chars)
    print(lines)
   
 
    
    
