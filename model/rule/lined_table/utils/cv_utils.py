# -*- coding: utf-8 -*-
"""
Created on Wed Mar 10 17:58:01 2021

@author: EDZ
"""

from functools import reduce
import cv2
import numpy as np
from scipy.ndimage import label
from model.rule.lined_table.utils.general_utils import round_up_to_odd
from model.rule.lined_table.utils.general_utils import get_uinque_values_with_tolerace_and_order



def cv_imwrite(path:str, img:np.ndarray) -> bool:
    # support chinese path
    return cv2.imencode('.png', img)[1].tofile(path)


def cv_imread(path:str, channel_fixed_3=True) -> np.ndarray:
    # support chinese path
    img = cv2.imdecode(np.fromfile(path, dtype=np.uint8), -1)
    if not channel_fixed_3:
        return img
    else:
        if img.shape[2] == 3:
            return img
        elif img.shape[2] == 4:  # bgra
            img = cv2.cvtColor(img, cv2.COLOR_BGRA2BGR)
            return img
        elif img.shape[2] == 1:  # gray
            img = img = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
            return img
        else:
            raise ValueError("image has weird channel number: %d" % img.shape[2])
    

def fix_img_to_bgr(img:np.ndarray, img_form:str='rgb') -> np.ndarray:
    """固定图片格式为bgr三个channel的格式
    Args:
        img(2d array): 维度(H*M),或者(H*M*3),或者(H*M*4)
        input_form(str): 基于opencv读取的图片是'bgr'格式为主,基于PIL是'rgb'格式为主
    return:
        img(2d array):(H*M*3)
    """
    assert img_form in ['rgb', 'bgr'], "input image form should be 'rgb' or 'bgr'"
    
    if img_form == 'rgb':
        if img.shape[2] == 1:  # gray to bgr
            img = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
            return img
        if img.shape[2] == 4:  # rgba to bgr
            img = cv2.cvtColor(img, cv2.COLOR_RGBA2BGR)
            return img
        if img.shape[2] == 3:  # rgb to bgr
            img = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)
            return img
    else:
        if img.shape[2] == 1:  # gray to bgr
            img = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
            return img
        if img.shape[2] == 4:  # bgra to bgr
            img = cv2.cvtColor(img, cv2.COLOR_BGRA2BGR)
            return img
        if img.shape[2] == 3:  # do nothihg
            return img
        
        
def expand_img(img:np.ndarray, pad:int=40) -> np.ndarray:
    """在原图基础上扩张:上下左右 x个像素，防止表格占满图片对边缘检测算法造成干扰"""
    h, w = img.shape[:2]
    # 默认是最亮色为背景
    padding_value = (int(img[:,:,0].max()), int(img[:,:,1].max()), int(img[:,:,2].max())) 
    img_expanded = cv2.copyMakeBorder(img, pad, pad, pad, pad, cv2.BORDER_CONSTANT, value=padding_value)
    
    return img_expanded


def filter_white_line(img:np.ndarray, white_line_width:int):
    """pdf中有白色线条会干扰判断，规则处理掉,注意这个方法非常原始暴力，如果有更好的
    方法，建议改进。
    """
    if white_line_width < 1:
        return img
    else:
        white_value = (int(img[:,:,0].max()), int(img[:,:,1].max()), int(img[:,:,2].max()))  # 默认为白色(最亮色)为杂音颜色
        regions = (img == white_value)[:, :, 0].astype(np.uint8)  # 0,1矩阵
        # 过滤非直线
        h_mask = cv2_open(regions, kernel=np.ones((1, 41)))
        v_mask = cv2_open(regions, kernel=np.ones((21, 1)))
        regions = np.logical_or(h_mask, v_mask).astype(np.uint8)
        # 用scipy.ndimage import label函数找到矩阵中等于1的联通区域，如果连通区域
        # 足够小，则认为是干扰的白色线条，并将此区域的颜色用右下角的区域颜色替代。
        labels, numL = label(regions)
        label_indices = [(labels == i).nonzero() for i in range(1, numL+1)]
        mask = np.zeros((img.shape[0], img.shape[1]))  # 创建一个mask
        for i in range(numL):
            ys = label_indices[i][0]
            xs = label_indices[i][1]
            ymax, ymin = np.amax(ys), np.amin(ys)
            xmax, xmin = np.amax(xs), np.amin(xs)
            area_thresh = white_line_width * (ymax - ymin + 1) + white_line_width * (xmax - xmin + 1) - white_line_width**2
            if len(ys) <= area_thresh:
                mask[ys, xs] = 1
                img[ys, xs] = img[ys + white_line_width, xs + white_line_width]

    return img


def cv2_open(img:np.ndarray, kernel:np.ndarray) -> np.ndarray:
    """open = erode + dilate 
    Args:
        img(2d array): 二值化图片,0表示背景,1表示前景
        kernel(2d array): 卷积核
    return:
        img_open(2d array): open处理后的二值化图片,0表示背景,1表示前景
    """
    img = cv2.erode(img, kernel, iterations = 1)
    img_open = cv2.dilate(img, kernel, iterations = 1)
    
    return img_open


def cv2_close(img:np.ndarray, kernel:np.ndarray) -> np.ndarray:
    """close = dilate + erode
     Args:
        img(2d array): 二值化图片,0表示背景,1表示前景
        kernel(2d array): 卷积核
    return:
        img_close(2d array): open处理后的二值化图片,0表示背景,1表示前景
    """
    img = cv2.dilate(img, kernel, iterations = 1)
    img_close = cv2.erode(img, kernel, iterations = 1)
    
    return img_close


def convert_to_binary(img:np.ndarray, thresh:int) -> np.ndarray:
    """
    基于sobel边缘检测来获得二值化图片：
    1.如果是简单颜色图片，那么边缘检测只检测由“由暗到亮”的一边，为的是获得更加清楚的
    二值化图片，能够更好的清除文字；
    2.如果是复杂颜色图片，那么边缘检测检查“由暗到亮”和“由亮到暗”双边，可以检测多种
    表格的线框，但是缺点是文字会“膨胀”，对清除文字带来困难。
    Args:
        img(2d array):3通道的bgr图像
        thresh(int): 判断边缘的阈值
        text_mask(2d array or None): 和img同尺寸二值化图片,1表示text区域,0表示非text区域
    return:
        binary_masks(2d array):二值化图像,0为背景，1为前景
    """
    
    def get_edge_mask(img, kernel, thres):
        """sobel方法提取线条mask"""
        img = img.astype(np.float32)
        img_filtered = cv2.filter2D(img, -1, kernel)
        # below thres force to 0, above thres to 1, operate at each channel
        img_filtered[img_filtered < thres] = 0  
        mask = img_filtered.astype(np.bool)  # (h, w, 3)
        mask = (np.sum(mask, axis=2) >= 1).astype(np.uint8)
        
        return mask
    
    h, w, _ = img.shape
    config = {'dark2bright': {}, 'bright2dark': {}}
    
    # dark2bright setting
    config['dark2bright']['v_filter'] = np.array([[-1, 1],
                                                  [-2,  2],
                                                  [-1,  1],], dtype=np.float32)
    config['dark2bright']['h_filter'] = np.array([[-1, -2, -1],
                                                  [1, 2, 1]], dtype=np.float32)
    config['dark2bright']['x_shift'] = -1
    config['dark2bright']['y_shift'] = -1
    # bright2dark setting    
    config['bright2dark']['v_filter'] = np.array([[1, -1],
                                                  [2, -2],
                                                  [1, -1],], dtype=np.float32)
    config['bright2dark']['h_filter'] = np.array([[1, 2, 1],
                                                  [-1, -2, -1]], dtype=np.float32)
    config['bright2dark']['x_shift'] = 0
    config['bright2dark']['y_shift'] = 0
    
    # filter
    img = img.astype(np.float32)
    masks = []
    for mode in ['dark2bright', 'bright2dark']:
        v_mask = get_edge_mask(img, config[mode]['v_filter'], thresh)
        v_mask = translate_img(v_mask, config[mode]['x_shift'], 0)
        h_mask = get_edge_mask(img, config[mode]['h_filter'], thresh)
        h_mask = translate_img(h_mask, 0, config[mode]['y_shift'])
        masks.append(h_mask)
        masks.append(v_mask)
    
    return masks  # [h_mask, v_mask, h_mask, v_mask]


def mask_a_minus_mask_b(mask_a:np.ndarray, mask_b:np.ndarray) -> np.ndarray:
    mask = np.logical_and(mask_a, np.logical_not(mask_b)).astype(np.uint8)
    return mask


def mask_a_plus_mask_b(mask_a:np.ndarray, mask_b:np.ndarray) -> np.ndarray:
    mask = np.logical_or(mask_a, mask_b).astype(np.uint8)
    return mask


def masks_plus(*masks) -> np.ndarray:
    mask = reduce(mask_a_plus_mask_b, masks)
    return mask


def guess_if_simple_color(img:np.ndarray, sub:int=3) -> bool:
    """
    将图片分为3*3的九宫格，判断每个区域的背景颜色，如果都相等，并且都是“白色”或者接近
    “白色”， 则认为是“简单颜色"
    Args:
        img(2d array): h,w,3
        sub(int): 切割块数，默认为3*3切割
    return:
        if_simple_color(bool):是否是简单颜色
    """
    # 对图片进行3*3的切割，得到9个子图片的坐标
    h, w, _ = img.shape
    h_sub = h//sub
    w_sub = w//sub
    ys = np.linspace(0, h, sub + 1)[:-1]
    xs = np.linspace(0, w, sub + 1)[:-1]
    xv, yv = np.meshgrid(xs, ys)
    boxes = np.stack((xv, yv, xv+w_sub, yv+h_sub), axis=2).reshape(-1, 4).astype(np.int32)  # 维度(9, 4), yxyx的坐标型式
    
    _, gray_values = get_color_info(img, boxes, offset=0, k=2, sample=True, sn=200)

    if len(gray_values) == 1 and gray_values[0] <= 10:  # gray_value越小表示越亮
        is_simple_color = True
    else:
        is_simple_color = False
        
    return is_simple_color


def generate_text_mask(img_shape:tuple, text_boxes:np.ndarray, shrink:tuple=(1, 1)) -> np.ndarray:
    """根据text的bboxes形成0-1mask，0表示背景，1表格text bbox
    Args:
        img_shape(tuple): 图片的尺寸
        text_boxes(2d array): pdf中读取的文本bbox，(#bboxes, xyxy)
        shrink(tuple): (水平方向的缩减， 竖直方向的所见)，用来衰减bbox,防止bbox压倒
            表格的线框
    return:
    """
    h_s, v_s = shrink
    text_mask = np.zeros(img_shape, dtype=np.uint8)  # 初始化text mask
    if text_boxes is None:
        return text_mask
    
    for text_box in text_boxes:  # xyxy
        text_mask[int(round(text_box[1]+v_s)): int(round(text_box[3]-v_s+1)),
                  int(round(text_box[0]+h_s)): int(round(text_box[2]-h_s+1))] = 1 
    return text_mask
    

def check_is_text_dense(text_mask:np.ndarray) -> bool:
    """判断表格是否为文本密集型(例如某些表格的文字列方向距离非常近，几乎挨在一起),
    判断标准为close结果比原图的像素点的增加比例"""
    ratio = 0.001
    v_kernel = np.ones((3, 1))   # 由于之前以及进行了1个像素的缩减，所以选择3*1
    text_mask_closed = cv2_close(text_mask, v_kernel)
    mask_add = text_mask_closed - text_mask
    if np.sum(mask_add) > text_mask.shape[0] * text_mask.shape[1] * ratio:
        return True
    else:
        return False
    
    
def utilize_text_boxes(masks:list, text_boxes:np.ndarray, shrink:tuple=(1, 1)) -> tuple:
    """利用pdf中的文本信息，用来：
        1.获得更清晰的二值化图片
        2.获得更准确的v_kernel_size
    Args:
        masks(list): list of numpy array, mask是0-1二值化图片
        text_boxes(numpy array): (#boxes, xyxy)
        shrink(tuple): (水平方向的缩减， 竖直方向的所见)，用来衰减bbox,防止bbox压倒
        表格的线框
    return:
        mask_filtered(list): 除去text bboxes区域的masks
        v_kernel_size(int): 优化的竖直方向卷积核大小
        message(str): 给logger记录的处理结果
    """
    # 生成text mask
    img_shape = masks[0].shape[: 2]
    text_mask = generate_text_mask(img_shape, text_boxes, shrink)
    message = []  # return for log
    
    # 判断表格是否为文本密集型
    is_text_dense = check_is_text_dense(text_mask)
    if is_text_dense:  # 重新生成text mask（防止擦线,采取更加严格的shrink策略）
        shrink = (shrink[0] + 1, shrink[1])
        text_mask = generate_text_mask(img_shape, text_boxes, shrink)

    message.append('is text dense: %r' % (is_text_dense))
        
    # 高度上限
    max_text_hight = np.amax(text_boxes[:, 3] - text_boxes[:, 1])  # 文字最大高度
    message.append('max text hight: %g' % max_text_hight)
    
    # 提取大直线
    large_masks = []  # h, v, h, v
    large_h = max(round_up_to_odd(img_shape[1]*0.333), 71)  # 图片三分之一宽
    large_v = max(round_up_to_odd(img_shape[0]*0.333), 41) if is_text_dense else 41
    large_kernels = [np.ones((1, large_h)),
                     np.ones((large_v, 1)), 
                     np.ones((1, large_h)),
                     np.ones((large_v, 1))]
    
    text_mask_eroded = cv2.erode(text_mask, np.ones((3,3)), iterations=1)
    for mask, large_kerenl in zip(masks, large_kernels):
        large_masks.append(mask_a_minus_mask_b(cv2_open(mask, large_kerenl), text_mask_eroded))
        
    # 清除text mask（清除完之后在补齐“大直线”，防止bbox擦到线框）
    masks_filtered = []
    for mask, large_mask in zip(masks, large_masks):
        mask = mask_a_minus_mask_b(mask, text_mask)
        mask = np.logical_or(mask, large_mask).astype(np.uint8)
        masks_filtered.append(mask)
    
    # 估算行间距最大 
    h_large_mask = np.logical_or(large_masks[0], large_masks[2]).astype(np.uint8)
    try:
        # 横向线过滤，只保留长度过3分之一图片宽的横线
        rows = np.any(h_large_mask==1, axis=1).astype(np.int32)  # 001100110010
        rows_se = np.diff(rows)  # 010-1010-101-1
        rows_start_idx = np.argwhere(rows_se==1).flatten()  # 1, 5, 10
        rows_dis = np.diff(rows_start_idx)  # 4, 5
        max_row_height = np.amax(rows_dis)  # 5
    except:
        max_row_height = img_shape[0]
        
    # 1.如果是密集text + 最大行距也很小，往“特大”取值
    # 2.如果是最大行距很小，往“稍大”取值
    # 3.如果上述都不满足，往“比字体高”取值
    if max_row_height < max_text_hight + 5 and is_text_dense:
        v_kernel_size = round_up_to_odd(img_shape[0]*0.3333)
        message.append('state: dense table')
    elif max_row_height < max_text_hight + 5:
        v_kernel_size = round_up_to_odd(max_row_height)
        message.append('state: just narrow rows')
    else:
        v_kernel_size = round_up_to_odd(max_text_hight + 5)
        message.append('state: normal table')
    message.append('v_kernel_size: %g' % v_kernel_size)
    message = '\n'.join(message)
    
    return masks_filtered, v_kernel_size, message


def add_outer_line(mask:np.ndarray, pad:int, text_boxes:np.ndarray) -> np.ndarray:
    """
    基于contour的方法，找到最外层(面积最大)的contour boxes
    Arg:
        mask(array):二值化图片,,0表示背景,1表示前景
        pad(int): 处理图片时的在图片周围padding像素个数
        text_boxes(2d array or None): text box坐标,维度(#boxes, 4), 4表示xyxy       
    return:
        mask(array):二值化图片,补充表格缺失的最外边线,其中0表示背景,1表示前景
    """
    # 在原图的copy上画出辅助区域，是原图中心3/4处的一个矩形区域，帮助寻找外边框
    
    mask_copy = np.copy(mask)
    h, w = mask.shape
    h_original = h - pad * 2
    w_original = w - pad * 2
    y_start = int(h_original * 1/8) + pad
    y_end = int(h_original * 7/8 ) + pad
    x_start = int(w_original * 1/8) + pad
    x_end = int(w_original * 7/8 ) + pad
    
    # 如果有text boxes可供参考，则根据ocr box调整辅助区域大小，使之包含ocr box
    if text_boxes is not None:  # xyxy
        try:
            y_start = int(min(np.amin(text_boxes[:, 1])+1, y_start))  # 给予适当的缩减 ‘1’
            y_end = int(max(np.amax(text_boxes[:, 3])-1, y_end))
            x_start = int(min(np.amin(text_boxes[:, 0])+1, x_start))
            x_end = int(max(np.amax(text_boxes[:, 2])-1, x_end))
        except:
            pass
        
    
    mask_copy[y_start: y_end + 1, x_start: x_end + 1] = 1
    contours, hierarchy = cv2.findContours(mask_copy,
                                           cv2.RETR_TREE,
                                           cv2.CHAIN_APPROX_SIMPLE)
    
    # 找到面积最大的contour rect，作为外边框，并补充到原图中
    area = np.array([], dtype=np.float32)
    for cnt in contours:
        _, _, w, h = cv2.boundingRect(cnt)
        bounding_box_area = w * h
        area = np.append(area, bounding_box_area)
    idx = np.argmax(area)
    x, y, w, h = cv2.boundingRect(contours[idx])
    mask[y: y+3, x: x+w] = 1  # 2 pixels的宽度
    mask[y+h-3: y+h, x: x+w] = 1
    mask[y: y+h, x: x+3] = 1
    mask[y: y+h, x+w-3: x+w] = 1
    
    return mask


def close_grids(mask:np.ndarray, h_size:int, v_size:int) -> np.ndarray:
    """补齐表格中线框在角点出现的缝隙，实现思路看日志《ts7 短线表格》
    Arg:
        mask(array):二值化图片,只保留有线表格中的线框部分,其中0表示背景,1表示
            线框
        h_size(int): 水平方向修复的缝隙大小(像素)
        v_size(int): 竖直方向修复的缝隙大小(像素)
    return:
        mask(array):二值化图片,补充表格缺失的最外边线,其中0表示背景,1表示前景"""
    # 补全缝隙点
    
    if h_size == 0 and v_size == 0:  # do nothing
        return mask
    else:
        # 过滤mask线条，只保留长边进行操作，减少噪音的干扰，这里选取41是因为够长，
        # 也可以选择其他合理值，主要是尽量避免遗留下“文字”的线条
        h_mask = cv2_open(mask, kernel=np.ones((1, 41)))
        v_mask = cv2_open(mask, kernel=np.ones((41, 1)))
        # 补齐横向缝隙
        if h_size > 0:  
            h_kernel = np.ones((1, h_size))
            v_dilated = cv2.dilate(v_mask, h_kernel, iterations = 1)
            mask_1_1 = np.logical_or(v_dilated, h_mask).astype(np.uint8)
            mask_1_2 = cv2.erode(mask_1_1, h_kernel, iterations = 1)
            mask_1_3 = cv2.erode(v_dilated, h_kernel, iterations = 1)
            mask = np.logical_or(mask_1_2 - mask_1_3, mask).astype(np.uint8)
            mask = np.logical_or(mask, h_mask).astype(np.uint8)
        # 补齐竖向缝隙
        if v_size > 0:  
            v_kernel = np.ones((v_size, 1))
            h_dilated = cv2.dilate(v_mask, h_kernel, iterations = 1)
            mask_2_1 = np.logical_or(h_dilated, v_mask).astype(np.uint8)
            mask_2_2 = cv2.erode(mask_2_1, v_kernel, iterations = 1)
            mask_2_3 = cv2.erode(h_dilated, v_kernel, iterations = 1)
            mask = np.logical_or(mask_2_2 - mask_2_3, mask).astype(np.uint8)

        return mask


def find_grid_coor(mask:np.ndarray) -> np.ndarray:
    """
    用opencv的findContours()来找到表格grid的中心坐标，思路是：
    1.找到图片中最大面积的contour(表格)
    2.所有是其子contour的就是我们要找的grid
    Arg:
        mask(array):二值化图片,只保留线框部分,其中0表示背景,1表示前景
        
    return:
        grid(array): 表格grid的坐标(xmin,ymin,xmax,ymax), 维度(#grid, 4)
    """
    contours, hierarchy = cv2.findContours(mask,
                                           cv2.RETR_TREE,
                                           cv2.CHAIN_APPROX_SIMPLE)

    # 找到面积最大的contour的编号
    area = np.array([], dtype=np.float32)
    for cnt in contours:
        area = np.append(area, cv2.contourArea(cnt))
    idx = np.argmax(area)
        
    grid = []
    # 基于contour的方法
    for cnt, hie in zip(contours, hierarchy[0]):
        # 判断是最外部contour的child contour并需要满足最小面积(因为还有可能有文字
        # 残留形成非常小的grid造成干扰)
        if hie[3] == idx:
            x, y, w, h = cv2.boundingRect(cnt)
            if w <= 10 or h <=8 :  # 过滤掉过小的grid
                continue
            grid.append([x, y, x + w, y + h])
        
    return np.asarray(grid)


def get_color_info(img, boxes, k=2, offset=2, sample=True, sn=60):
    """根据得到的grid坐标信息，返回每个grid的背景颜色信息(灰度图)
    Args:
        img(2d array): 图片地址，或者图片
        boxes(2d array): 格子坐标xyxy
        k(int): k means中的k
        offset(int): boxes的向内衰减值（防止取到线框的像素）
        sample(bool): 是否进行像素点抽样进行k means
        sn(int): 抽样值
    returns:
        ranking(1d array): 对应格子的颜色深浅排序，1为最亮(开始从1)
        gray_values(1d array): 所有格子的灰度值(255最亮，0最暗)
    """
    def get_bg_color(img, boxes, offset=2):
        # offset 是避免提取到线框
        bg_colors = np.zeros(len(boxes))
        img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)  # 用灰度图处理
        for i, box in enumerate(boxes):
            sub_img = img_gray[box[1]+offset: box[3]-offset+1, box[0]+offset: box[2]-offset+1]
            ret, label, center = k_means(sub_img, k, sample, sn)
            # 检查背景颜色是否占多数
            if np.sum(label==1) > 0.5 * label.shape[0]:
                bg_color = center[1].astype(np.int32)
            else:
                bg_color = center[0].astype(np.int32)
                
            bg_colors[i] = bg_color
        return bg_colors.astype(np.int32)
    
    def gray_sorting(bg_colors, tolerance=8):
        gray_values, gray_rank = get_uinque_values_with_tolerace_and_order(bg_colors, tolerance)
        gray_rank = len(gray_values) - gray_rank
    
        return gray_rank, gray_values
    
    bg_colors = get_bg_color(img, boxes, offset)
    gray_rank, gray_values = gray_sorting(bg_colors)
    
    return gray_rank, gray_values


def k_means(img, k=2, sample=True, sn=60):
    # https://docs.opencv.org/3.0-beta/doc/py_tutorials/py_ml/py_kmeans/py_kmeans_opencv/py_kmeans_opencv.html
    last_channel = 1 if len(img.shape)!=3 else img.shape[2]
    pixels = img.reshape((-1, last_channel))
    # convert to np.float32
    pixels = np.float32(pixels)
    
    # subsample
    if sample:
        sample_num = min(int(len(pixels) * 0.1) + 2, sn)
        sample_idx = np.random.choice(np.arange(len(pixels)), sample_num, replace=False)
    
    # define criteria, number of clusters(K) and apply kmeans()
    criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 10, 1)
    ret,label,center=cv2.kmeans(pixels[sample_idx], k, None, criteria, 10, 
                                cv2.KMEANS_RANDOM_CENTERS)
    
    return ret, label, center


def translate_img(img, x_shift, y_shift):
    """平移操作"""
    if x_shift==0 and y_shift==0:
        return img
    
    if len(img.shape) == 3:  # 3 channel
        h, w, _ = img.shape
    elif len(img.shape) == 2:  # 1 channel
        h, w = img.shape
    else:
        raise ValueError('image ndimension should be 2 or 3')
        
    img = cv2.warpAffine(img, np.float32([[1,0,x_shift],[0,1,y_shift]]), (w, h))
    return img


def analyze_bg_color(img, grids):
    """获得每个格子内的背景颜色
    Args:
        img(2d array): BGR 
        grids(array): 
    """
    try:
        gray_rank, unique_gray_values = get_color_info(img, grids)
    except:
        gray_rank = np.ones(len(grids)).astype(np.int32)  # 默认最亮
        unique_gray_values = np.array([255])
        
    return gray_rank, unique_gray_values