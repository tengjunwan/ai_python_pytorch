# -*- coding: utf-8 -*-
"""
Created on Mon Sep  6 20:38:43 2021

@author: EDZ
"""

from abc import ABC, abstractmethod
from model.rule.lined_table.utils.general_utils import xyxy2yxyx
import numpy as np


class Sender(ABC):
    """parser that converts the results to a given format."""
    def __init__(self):
        pass
    
    @abstractmethod
    def __call__(self, ):
        pass
    
    
class Former_Sender(Sender):
    """the sender i used before"""
    def __init__(self, scale_ratio=1.33333333):
        self.scale_ratio = scale_ratio
    
    def __call__(self, results):
        """
        Args:
            results(list): a list of result. result is a tuple containing the 
                following parts:
            
                1.grid(np array): (#grid, xyxy).
                2.grid_order(np array):(#grid, crcr),每个格子对应的行列信息，crcr=
                                  (Col_lt, Row_lt, Col_rb, Row_rb),lt=left top,
                                  rb=right bottom。
                3.grid_regions(np array):(#grid, xyxy),每个格子内的文本框范围，没有文本
                                    的为[-1,-1,-1,-1]。
                4.grid_info(list):包含每个格子的文本以及文本坐标信息,[info,...];
                其中info = [[line1,line2,...],[line1_coor, line2_coor,...]];
                    其中line1=str,为格子内一行的文本内容,line1_coor=np.ndarray,为
                    格子内一行文本的坐标，(#chars, xyxy)
                5.gray_rank(np array): (#grid,)表示格子背景灰度排行,最亮的格子为1,次亮的
                                  格子为2，以此类推
                6.unique_gray_values(np array):所有格子特有灰度值，例如格子全是白色背景则=
                                        [255],如果格子有一个是黑色剩下全是白色，那么
                                        =[0,255]
        return:
            results(list): 一个页面内多个有线表格的提取结果，[result,...],一个result=
                [grid, grid_order, grid_regions, grid_info, gray_rank, unique_gray_values]
            其中:
            grid(list): [[yxyx],...],每个格子的坐标。
            grid_order(list):([rcrc],...),每个格子对应的行列信息，rcrc=
                                  (Row_lt, Col_lt,  Row_rb, Col_rb),lt=left top,
                                  rb=right bottom。
            grid_region(list):[[yxyx],...],每个格子内的文本框范围，没有文本
                                    的为[-1,-1,-1,-1]。
            grid_info(list):包含每个格子的文本以及文本坐标信息,[info,...];
                其中info = [[line1,line2,...],[line1_coor, line2_coor,...]];
                    其中line1=str,为格子内一行的文本内容,line1_coor=list,为
                    格子内一行文本的坐标，[[[yxyx],...]]
            gray_rank(list): 表示格子背景灰度排行,最亮的格子为1,次亮的
                                  格子为2，以此类推
            unique_gray_values(list):所有格子特有灰度值，例如格子全是白色背景则=
                                        [255],如果格子有一个是黑色剩下全是白色，那么
                                        =[0,255]        
        """
        sending_results = []
        for result in results:
            # grid: xyxy to yxyx, and change coodinate
            grid = xyxy2yxyx(result[0] / self.scale_ratio).tolist() 
            
            # grid order
            grid_order = xyxy2yxyx(result[1]).tolist()
            
            # grid regions
            grid_region = xyxy2yxyx(result[2])
            grid_region[grid_region != [-1,-1,-1,-1]] /= self.scale_ratio
            grid_region = grid_region.tolist()
            
            # grid info
            grid_info = result[3]
            for i in range(len(grid_info)):
                grid_info[i][1] = [(xyxy2yxyx(coor) / self.scale_ratio).tolist() for coor in grid_info[i][1]]
                
            # gray rank
            gray_rank = result[4].tolist()
            
            # unique_gray_values
            unique_gray_values = result[5].tolist()
            
            sending_results.append([grid, grid_order, grid_region, grid_info, gray_rank, unique_gray_values])
            
        return sending_results
    
    
class Table_Sender(Sender):
    """to extract information from Table object to json format"""
    def __init__(self, scale_ratio=1.33333333):
        self.scale_ratio = scale_ratio
    
    def __call__(self, tables):
        """
        Args:
            tables(list): a list of Table instances.
        return:
            results(list): 一个页面内多个有线表格的提取结果，[result,...],一个result=
                [grid, grid_order, grid_regions, grid_info, gray_rank, unique_gray_values]
            其中:
            grid(list): [[yxyx],...],每个格子的坐标。
            grid_order(list):([rcrc],...),每个格子对应的行列信息，rcrc=
                                  (Row_lt, Col_lt,  Row_rb, Col_rb),lt=left top,
                                  rb=right bottom。
            grid_region(list):[[yxyx],...],每个格子内的文本框范围，没有文本
                                    的为[-1,-1,-1,-1]。
            grid_info(list):包含每个格子的文本以及文本坐标信息,[info,...];
                其中info = [[line1,line2,...],[line1_coor, line2_coor,...]];
                    其中line1=str,为格子内一行的文本内容,line1_coor=list,为
                    格子内一行文本的坐标，[[[yxyx],...]]
            gray_rank(list): 表示格子背景灰度排行,最亮的格子为1,次亮的
                                  格子为2，以此类推
            unique_gray_values(list):所有格子特有灰度值，例如格子全是白色背景则=
                                        [255],如果格子有一个是黑色剩下全是白色，那么
                                        =[0,255]        
            
        """
        tables_list = []
        for table in tables:
            table_list = self.process_one_table(table)
            tables_list.append(table_list)
        return tables_list
        
    
    def process_one_table(self, table):
        # initialize
        table_list = []
        
        for cell in table:
            cell_dict = {}
            # cell_dict = {
            #              "coor": [],  
            #              "order": [],
            #              "lines:"[{
            #                       "text": '',
            #                       "coor": [],
            #                       "type": '',
            #                       }, ...],
            #              "region": []
            #              }
            
            # coor
            coor = (cell.coor / self.scale_ratio).tolist()  # xyxy, scale to original pdf size
            cell_dict["coor"] = coor
            # order
            order = cell.cell_order.tolist()  
            cell_dict["order"] = order
            # lines
            lines = []
            line_coors = []  # for region calculation
            for line in cell.lines:
                line_dict = {}
                # coor, text, type
                line_coor, line_text, line_type = line.load_line()
                line_coors.append(line_coor)
                line_coor = (line_coor / self.scale_ratio).tolist()
                line_dict = {"text": line_text, "coor": line_coor, "type": line_type}
                lines.append(line_dict)
            cell_dict["lines"] = lines
            # region
            if len(line_coors):
                line_coors = np.vstack(line_coors)
                min_x0y0 = np.amin(line_coors[:, :2], axis=0)
                max_x1y1 = np.amax(line_coors[:, 2:], axis=0)
                region = np.hstack((min_x0y0, max_x1y1))
                region = (region / self.scale_ratio).tolist()
            else:
                region = np.array([-1, -1, -1, -1], dtype=np.float32).tolist()
            cell_dict["region"] = region
            
            table_list.append(cell_dict)
        
        return table_list
                

if __name__ == "__main__":
    ts = Table_Sender(scale_ratio=1.33333333)

            
            