# -*- coding: utf-8 -*-
"""
Created on Wed Mar 24 11:06:42 2021

@author: EDZ
"""

import numpy as np
from model.rule.xy_cut_preprocess.utils.general_utils import point2range
from model.rule.xy_cut_preprocess.utils.pdf_utils import page2image
import fitz
from model.rule.xy_cut_preprocess.utils.pdf_utils import form_text_binary_image
from model.rule.xy_cut_preprocess.xy_cut import segment_binary_image
import cv2
from model.rule.xy_cut_preprocess.utils.general_utils import parse_json_file
from model.rule.xy_cut_preprocess.utils.general_utils import parse_predict


def generate_regions_by_projection(binary_img:np.ndarray):
    """参考叶龙的方法写的规则得到pdf页面region坐标，
    step1: 进行y轴投影，得到连续的区域的起始和中止坐标[[y_min, y_max],...]
    step2: 根据y轴投影对原图进行裁剪(宽度保持原图宽)
    step3: 对裁剪区域进行x轴投影，找到有text的区域的坐标[x_min, x_max]
    Args:
        img(array): 维度H*W*3 (bgr模式)
    return:
        regions(2d array): 维度(#regions, xyxy)
    """
    # config
    y_region_thres = 0  #  忽略调y方向投影连续区域过小的阈值
    x_region_thres = 0  #  忽略调x方向投影连续区域过小的阈值
    delta = 0.0001  # 拓宽y方向投影连续区域的值，为了ymin=ymax的“线”拥有一点面积
    
    if binary_img.size ==0:
        return np.zeros((0, 4))
        
    # y轴投影
    projection_in_y = np.any(binary_img==1, axis=1).astype(np.uint8)  
    y_cuts = point2range(projection_in_y)  # 投影中连续区域的起始和中止坐标[[y_s1, y_e1], [y_s2, y_e2], ...]
    y_cuts = y_cuts[(y_cuts[:, 1] - y_cuts[:, 0]) >= y_region_thres]  # 过滤
    
    # y轴裁剪，对每个y轴裁剪的结果进行x轴投影，投影区域只考虑最小起始点和最大中止点
    x_cuts = np.zeros(y_cuts.shape)  # [[x_s1, x_e1], [x_s2, x_e2], ...]
    for i, y_cut in enumerate(y_cuts):
        projection_in_x = np.any(binary_img[y_cut[0]: y_cut[1]+1, :]==1, axis=0)  # 维度(W,)
        x_max = np.max(projection_in_x.nonzero())
        x_min = np.min(projection_in_x.nonzero())
        if x_max - x_min >= x_region_thres:
            x_cuts[i] = [x_min, x_max]
        
    regions = np.concatenate((x_cuts[:, [0]], y_cuts[:, [0]], x_cuts[:, [1]], y_cuts[:, [1]]), axis=1)  #xyxy
    regions[:, 3] += delta  # 使得直线也可以成为region,拥有非0的面积  
    
    return regions.reshape(-1, 4)  # ensure its shape even if there is no region


def preprocess_page(page:fitz.fitz.Page, header:np.ndarray=None, footer:np.ndarray=None,
                    extra_regions:np.ndarray=None) -> np.ndarray:
    """perform xy-cut on a page object
    Args:
        page(Page): a page object from PyMupdf module
        header(numpy array or None): 1d array, (xyxy, ), defines header region,
            None means no footer region was given.
        footer(numpy array or None): 1d array, (xyxy, ), defines footer region,
            None means no footer region was given.
        extra_regions(numpy array or None): 2d array, (#regions, xyxy), these regions have
            already been provided by deeplearning model, e.g. protects unlined 
            table region from being cutted into different parts due to its sparsity.
            None means no extra regions were given.
    return:
        text_regions(numpy array): (#text_regions, xyxy), the bboxes of each line
            means reading order.
        cuts(numpy array): (#cuts, xyxy), the egdes which seperate different regions.
    """
    # convert page object to binary image, 1 means foreground, 0 means background 
    b_img, main_region= form_text_binary_image(page, header, footer, extra_regions)
    
    # layout analysis
    regions, cuts = segment_binary_image(b_img, main_region)
    
    # apply projection method of YeLong to each region 
    text_regions = []
    for x0, y0, x1, y1 in regions.round().astype(np.int64):
        b_img_part = b_img[y0: y1+1, x0: x1+1]
        text_regions_part = generate_regions_by_projection(b_img_part)
        text_regions_part += np.array([x0, y0, x0, y0])  # ajust to orignal coordinate
        text_regions.append(text_regions_part)
    
    # apply projection to none main region
    if header is not None:
        x0, y0, x1, y1 = header.round().astype(np.int64)
        b_img_part = b_img[y0: y1+1, x0: x1+1]
        text_regions_part = generate_regions_by_projection(b_img_part)
        text_regions_part += np.array([x0, y0, x0, y0])  # ajust to orignal coordinate
        text_regions.append(text_regions_part)
        
    if footer is not None:
        x0, y0, x1, y1 = footer.round().astype(np.int64)
        b_img_part = b_img[y0: y1+1, x0: x1+1]
        text_regions_part = generate_regions_by_projection(b_img_part)
        text_regions_part += np.array([x0, y0, x0, y0])  # ajust to orignal coordinate
        text_regions.append(text_regions_part)
    
    text_regions = np.concatenate(text_regions, axis=0).reshape(-1, 4)
        
    return text_regions


def _preprocess_pdf(pdf_path:str, page_index:list=None, headers:list=None, footers:list=None, 
                   extra_regions:list=None) -> list:
    # load pdf file
    doc = fitz.open(pdf_path)
    
    # default value when it's None
    if page_index is None:  
        page_index = list(range(len(doc)))
    if headers is None:  
        headers = [None for _ in range(len(doc))]
    if footers is None:
        footers = [None for _ in range(len(doc))]
    if extra_regions is None:  
        extra_regions = [None for _ in range(len(doc))]
        
    # loop through pages 
    text_regions_list = []
    for j, i in enumerate(page_index):
        text_regions = preprocess_page(doc[i], headers[j], footers[j], extra_regions[j])
        text_regions_list.append(text_regions)
        
    return text_regions_list


def preprocess_pdf_by_json(pdf_path:str, json_path:str, page_index=None) -> list:
    # parse json file
    header_list, footer_list, extra_region_list = parse_json_file(json_path)
    
    # preprocess
    text_regions_list = _preprocess_pdf(pdf_path, page_index, headers=header_list, 
                                       footers=footer_list, extra_regions=extra_region_list)
    
    # to list
    text_regions_list = [i.tolist() for i in text_regions_list]
    return text_regions_list


def preprocess_pdf(pdf_path:str, predict:list) -> list:
    # parse predict
    header_list, footer_list, extra_region_list = parse_predict(predict)
    
    # preprocess
    text_regions_list = _preprocess_pdf(pdf_path, page_index=None, headers=header_list, 
                                       footers=footer_list, extra_regions=extra_region_list)
    
    # to list
    text_regions_list = [i.tolist() for i in text_regions_list]
    return text_regions_list

def preprocess_pdf_single_page(pdf_path:str, pageIndex:int, predict:list) -> list:
    # parse predict
    header_list, footer_list, extra_region_list = parse_predict([predict])
    
    # prprocess
    text_regions_list = _preprocess_pdf(pdf_path, page_index=[pageIndex], headers=header_list, 
                                       footers=footer_list, extra_regions=extra_region_list)
    
    # to list
    text_regions_list = [i.tolist() for i in text_regions_list]  # length=1
    
    return text_regions_list[0]

if __name__ == "__main__":
    from utils.vis_utils import draw_bboxes
    pdf_path = 'sample/宁波银行：2017年年度报告.PDF'
    doc = fitz.open(pdf_path)
    page_index = [120]
    text_regions = _preprocess_pdf(pdf_path, page_index)
    page_image = page2image(doc[120])
    page_image = draw_bboxes(page_image, text_regions[0])
    cv2.imshow('', page_image)
    cv2.waitKey()
    
    
