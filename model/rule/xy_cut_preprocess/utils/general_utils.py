# -*- coding: utf-8 -*-
"""
Created on Mon Mar 22 10:37:35 2021

@author: EDZ
"""

import numpy as np  
import json


def cut_profile(profile:np.ndarray, width_thres:int, min_thres:int,
                prominence_thres:int, prominence_search_range:int) -> np.ndarray:    
    """obtain cuts in between the gap of profile
    Args:
        profile(numpy array): 1d-array, projection profile of a binary image.
        width_thres(int): minimal validate gap width.
        min_thres(int): value of area of profile below it is considered as a gap
            candiate.
        prominence_thres(int): the marginal change at the ends of a gap should
            be more than it.
        prominence_search_range(int): change is caculated within it
    return:
        cuts(numpy array):1d-array, (#cuts, ), the coordinate of the middle of 
            gap.
    """
    # select candidates
    gaps = point2range(profile <= min_thres)  # [[gap_start, gap_end],...]
    
    # get rid of gaps not in between
    try:  # the start region of the profile
        if gaps[0, 0] == 0:
            gaps = np.delete(gaps, 0, axis=0)
    except: 
        pass
    try:  # the end region of the profile
        if gaps[-1, 1]==len(profile)-1:
            gaps = np.delete(gaps, -1, axis=0) 
    except:
        pass
    
    # filter gaps satisfying width
    gaps = gaps[(gaps[:, 1] - gaps[:, 0] + 1 >= width_thres)]   
    
    # filter gaps satisfying prominence
    gaps_filtered = []
    for s, e in gaps:
        # start of gap
        gap_s_region = profile[max(0, int(s) + 1 - prominence_search_range): int(s)+1]
        change_s = np.amax(gap_s_region) - np.amin(gap_s_region)
        # end of gap
        gap_e_region = profile[int(e) : min(int(e) + prominence_search_range, len(profile)-1)]
        change_e = np.amax(gap_e_region) - np.amin(gap_e_region)
        if change_s >= prominence_thres and change_e >= prominence_thres:
            gaps_filtered.append(s)
            gaps_filtered.append(e)
    gaps = np.array(gaps_filtered).reshape(-1, 2)
    
    # cuts which cut in the middle of gaps
    cuts = (gaps[:, 0] + gaps[:, 1]) * 0.5
    return cuts


def point2range(vector:np.ndarray) -> np.ndarray:
    """从连续标记的1的list找连续1区域的开始和结束坐标
    Arg:
        vector(1d array):例如[1, 1, 0, 1, 1, 0]
    return:
        ran(2d array): 例如[[0,1], [3,4]], 
    """
    vector = np.insert(vector, 0, 0)
    vector = np.append(vector, 0)
    diff = np.diff(vector)
    start = np.argwhere(diff==1).flatten()           
    end = np.argwhere(diff==-1).flatten() - 1  
    ran = np.stack((start, end), axis=1)
         
    return ran


def filter_bboxes_in_rect(bboxes:np.ndarray, rect:np.ndarray) -> np.ndarray:
    # filter bboxes(#bboxes, xyxy) in a given rect(xyxy)
    rect = rect.reshape(-1, 4) * np.array([-1, -1, 1, 1])  # (1, 4)
    bboxes_ = bboxes.reshape(-1, 4) * np.array([-1, -1, 1, 1])  # (#bboxes, 4)
    belong_arg = (bboxes_ <= rect).all(axis=1)  # (#bboxes, )
    return bboxes[belong_arg]


def parse_json_file(json_path:str) -> tuple:
    """parse json file made by yolov5, to extract:
        1. main region, whichs means the region that excludes footer and header
        2. extra regions, which mean the regions like unlined table; it's likely
        to be xy-cutted into different parts due to its sparsity; If we kown it
        is a unlined table region before hand, we can prevent its happenning.
    cls_index and its corresponding cls name:
    ['lined table', 'unlined table', 'header', 'pic', 'chart', 'formular', 'catalog']
    [0, 1, 2, 3, 4, 5, 6]
    json file format:
        [page_0_result, page_1_result, ...]
        page_0_result(list): [[xyxy+conf+cls],...]
    Args:
        json_path(str): path of json file
    return:
        header_list(list): e.g., [[xyxy], None, [xyxy],...].
        footer_list(list): e.g., [[xyxy], None, [xyxy],...].
        extra_region_list(list): e.g., [[[xyxy],[xyxy]], None, ...]
    """
    # yolo result save as json, [page0_detection, page1_detection, ...]
    with open(json_path, encoding='utf-8') as f:
        data = json.load(f)
    
    return parse_predict(data)


def parse_predict(predict:list) -> tuple:
    predict = [np.array(i).reshape(-1, 6) for i in predict]
    header_list = []
    footer_list = []
    extra_region_list = []
    for d in predict:
        # header
        header = d[d[:, 5]==2]
        header = header.reshape(-1, 6)
        if header.size == 0:
            header = None
        else:
            header = header[0][:4]  # in case we have many headers, choose the 1st one
        header_list.append(header)
    
        # footer
        footer = None  # todo
        footer_list.append(footer)
        
        # extra regions : lined table, unlined table, chart, catalog
        args = (d[:, 5]==0) | (d[:, 5]==1) | (d[:, 5]==4) | (d[:, 5]==6) 
        extra_regions = d[args][:, :4]
        if extra_regions.size == 0:
            extra_regions = None
        extra_region_list.append(extra_regions)
        
    return header_list, footer_list, extra_region_list
    
    
    