# -*- coding: utf-8 -*-
"""
Created on Mon Mar  8 09:56:25 2021

@author: EDZ
"""

import cv2
import numpy as np
import fitz
from model.rule.xy_cut_preprocess.utils.pdf_utils import page2image
from model.rule.xy_cut_preprocess.utils.pdf_utils import get_text_bboxes
import matplotlib.pyplot as plt


def draw_bboxes(img:np.ndarray, draw_bboxes:np.ndarray) -> np.ndarray:
    color = (0, 0, 255)  # red
    for x0, y0, x1, y1 in draw_bboxes:
        start_point = (int(round(x0)), int(round(y0)))
        end_point = (int(round(x1)), int(round(y1)))
        cv2.rectangle(img, start_point, end_point, color, thickness=1)  # red color
    return img


def draw_text_bboxes(page:fitz.fitz.Page, scale_ratio:float=1.33333333) -> np.ndarray:
    img = page2image(page, scale_ratio)
    bboxes = get_text_bboxes(page, scale_ratio)
    img = draw_bboxes(img, bboxes)
    return img


def draw_text(img:np.ndarray, text:list, coor:list, fontScale:int=1, 
              color:tuple=(0,0,255), thickness:int=1) -> np.ndarray:
    for t, (x0, y0, x1, y1) in zip(text, coor):
        mid_point = (int((x0+x1)/2.0), int((y0+y1)/2.0))
        img = cv2.putText(img, t, mid_point, cv2.FONT_HERSHEY_SIMPLEX, fontScale, 
                          color, thickness, cv2.LINE_AA)
                        
    return img


def draw_lines(img:np.ndarray, lines:np.ndarray, color:tuple=(0,0,255),
              thickness:int=1, style:str='dotted', gap:int=5) -> np.ndarray:
    for x0, y0, x1, y1 in lines:
        
        dist = ((x1 - x0)**2 + (y1 - y0)**2)**0.5  # distance of lines
        pts = []
        
        for i in np.arange(0, dist, gap):  # get points on the line 
            ratio = i / dist
            x = int(x0 * (1 - ratio) + x1 * ratio + 0.5)
            y = int(y0 * (1 - ratio) + y1 * ratio + 0.5)
            pts.append((x, y))
        
        if style == "dotted":
            for p in pts:
                print(color)
                img = cv2.circle(img, p, thickness, color, -1)
                
        else:  # dashed
            s = pts[0]
            e = pts[0]
            i = 0
            for p in pts:
                s = e
                e = p
                if i % 2 == 1:  # only draw line with odd index
                    img = cv2.line(img, s, e, color, thickness)
                i += 1
    return img
        

def draw_regions_and_cuts(img:np.ndarray, regions:np.ndarray, cuts:np.ndarray, thickness:int=1) -> np.ndarray:
    # generate region indices
    region_indices = [str(i) for i in range(len(regions))]
    
    # draw region indices at the centers of regions
    img = draw_text(img, region_indices, regions, thickness=thickness)
    
    # draw cuts
    img = draw_lines(img, cuts, style='dashed', thickness=thickness)
    return img


def profile_visualize(profile:np.ndarray) -> None:
    plt.bar(np.arange(len(profile)), profile)
    plt.show()
    
    
def cv_imwrite(path:str, img:np.ndarray) -> bool:
    # support chinese path
    return cv2.imencode('.png', img)[1].tofile(path)


def cv_imread(path:str) -> bool:
    # support chinese path
    return cv2.imdecode(np.fromfile(path, dtype=np.uint8), -1)
    

