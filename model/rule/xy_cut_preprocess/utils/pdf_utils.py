# -*- coding: utf-8 -*-
"""
Created on Thu Mar  4 16:02:47 2021

@author: EDZ
"""

import fitz
import cv2
import numpy as np
from model.rule.xy_cut_preprocess.utils.general_utils import filter_bboxes_in_rect


def page2image(page:fitz.fitz.Page, scale_ratio:float=1.33333333, 
               rotate:'int'=0) -> np.ndarray:
    trans = fitz.Matrix(scale_ratio, scale_ratio).preRotate(rotate)  # clockwise 
    pm = page.getPixmap(matrix=trans, alpha=False)
    data = pm.getImageData()  # bytes
    img_np = np.frombuffer(data, dtype=np.uint8)
    img = cv2.imdecode(img_np, flags=1)
    return img
    

def get_text_bboxes(page:fitz.fitz.Page, scale_ratio:float=1.33333333,
                    shrink:bool=True) -> np.ndarray:
    if shrink:
        fitz.TOOLS.set_small_glyph_heights(True)  # a tighter bboxes
    words = page.getText('words')  # (x0, y0, x1, y1, "string", blocknumber, linenumber, wordnumber)
    bboxes = np.array([[i[0], i[1], i[2], i[3]] for i in words]) * scale_ratio 
    
    # adjust coordinate according to Page.rotation
    bboxes = change_bbox_coordinate_by_rotation(bboxes, page)
    
    return bboxes.reshape(-1, 4)  # ensure its shape if it's empty


def change_bbox_coordinate_by_rotation(bboxes:np.ndarray, page:fitz.fitz.Page, 
                                       scale_ratio:float=1.33333333) -> np.ndarray:
    # cause getText return the coordinate before rotation, we need to ajust to
    # the coordinate after rotation
    if page.rotation == 0:
        return bboxes
    else:
        img = page2image(page, scale_ratio)
        h, w = img.shape[: 2]
        if page.rotation == 90:
            # y = x', x = w - y'
            x0 = w - bboxes[:, 3]
            y0 = bboxes[:, 0]
            x1 = w - bboxes[:, 1]
            y1 = bboxes[:, 2]
            bboxes = np.stack((x0, y0, x1, y1), axis=1)
            return bboxes
        elif page.rotation == 180:
            # y = h - y', x = w - x'
            x0 = w - bboxes[:, 3]
            y0 = h - bboxes[:, 2]
            x1 = w - bboxes[:, 1]
            y1 = h - bboxes[:, 0]
            bboxes = np.stack((x0, y0, x1, y1), axis=1)
            return bboxes
        elif page.rotation == 270:
            # y = h - x', x = y'
            x0 = bboxes[:, 1]
            y0 = h - bboxes[:, 2]
            x1 = bboxes[:, 3]
            y1 = h - bboxes[:, 0]
            bboxes = np.stack((x0, y0, x1, y1), axis=1)
            return bboxes
        else:
            raise ValueError('page.rotation should be 0, 90, 180, 270, not %g'
                             % page.rotation)
            
            
def form_text_binary_image(page:fitz.fitz.Page, header:np.ndarray=None,
                           footer:np.ndarray=None, extra_regions:np.ndarray=None, 
                           scale_ratio:float=1.33333333) -> np.ndarray:
    img = page2image(page, scale_ratio)
    binary_image = np.zeros(img.shape[:2], dtype=np.uint8)  # background as 0
    text_bboxes = get_text_bboxes(page, scale_ratio)
    
    # get binary image(text bboxes on the whole page)
    for x0, y0, x1, y1 in text_bboxes.round().astype(np.int64):
        # print(x0, y0, x1, y1)
        binary_image[y0: y1+1, x0: x1+1] = 1  
    
    # exclude header and footer area
    ymin = header[3] if header is not None else 0
    ymax = footer[1] if footer is not None else img.shape[0] - 1
    rect = np.array([0, ymin, img.shape[1]-1, ymax])
    
    # filter text bboxes 
    if header is not None or footer is not None:
        text_bboxes = filter_bboxes_in_rect(text_bboxes, rect)
    
    # filter extra regions
    if extra_regions is not None:
        # get binary image(detected regions on the whole page)
        for x0, y0, x1, y1 in extra_regions.round().astype(np.int64):
            binary_image[y0: y1+1, x0: x1+1] = 1  
        extra_regions = filter_bboxes_in_rect(extra_regions, rect)
        text_bboxes = np.concatenate((text_bboxes, extra_regions), axis=0).reshape(-1, 4)
        
    # get main region
    if text_bboxes.size != 0:  # the whole page if there is no text box 
        main_region = np.array([text_bboxes[:,0].min(),
                                text_bboxes[:,1].min(),
                                text_bboxes[:,2].max(),
                                text_bboxes[:,3].max()])
    else:
        main_region = np.array([0, 0, img.shape[1]-1, img.shape[0]-1])
        
    return binary_image, main_region


if __name__ == "__main__":
    from utils.vis_utils import draw_bboxes
    doc = fitz.open('../sample/宁波银行：2017年年度报告.PDF')
    page = doc[120]
    bboxes = get_text_bboxes(page)

    image = page2image(page)
    image = draw_bboxes(image, bboxes)
    cv2.imshow('', image)
    cv2.waitKey()
    
    binary_image, main_region = form_text_binary_image(page)
    cv2.imshow('', binary_image * 255)
    cv2.waitKey()
    
    
    