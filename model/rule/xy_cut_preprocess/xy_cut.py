# -*- coding: utf-8 -*-
"""
Created on Wed Mar 31 19:23:01 2021

@author: EDZ
"""


import fitz
import numpy as np
from model.rule.xy_cut_preprocess.utils.pdf_utils import form_text_binary_image
from model.rule.xy_cut_preprocess.utils.general_utils import cut_profile


class Node():
    """ node (and also a tree)"""
    
    # config
    first_cut_axis = 1  # cut horizontally as first choice
    h_gap_width = 12  # horizontal gap width threshold
    v_gap_widht = 15  # vertical gap width threshold
    min_thres = 1   # area value below this is a gap
    h_prominence = 10  # change of gap margin should be bigger than the value 
    v_prominence = 10
    prominence_search_range = 25  # range where change happens 
    
    def __init__(self, region, binary_image, cut_axis):
        self.region = np.array(region)
        self.binary_image = binary_image
        self.cut_axis = cut_axis
        self.cut_count = 0
        self.parent = None  # parent node
        self.children = []  # children nodes
        self.cuts = self._cut_node()  # form a node tree taking the node as a root node
        
    def _add_child(self, node):
        self.children.append(node)  # assign the node a child
        node.parent = self  # assign a child the node as a parent node
            
    def is_leaf(self):
        return len(self.children) == 0
    
    def __repr__(self):
        return 'region: x0: %g, y0: %g, x1: %g, y1: %g' % self.region
        
    def _get_profile(self):
        x0, y0, x1, y1 = self.region
        binary_image = self.binary_image[int(y0): int(y1+1), int(x0): int(x1+1)]
        return np.sum(binary_image.astype(np.int32), self.cut_axis)
    
    def _cut_node_once(self):
        # get projection profile
        profile = self._get_profile()
        
        # get cuts
        min_thres = Node.min_thres
        width_thres = Node.v_gap_widht if self.cut_axis == 1 else Node.h_gap_width
        prominence_thres = Node.h_prominence if self.cut_axis == 1 else Node.v_prominence
        cuts = cut_profile(profile, width_thres, min_thres,
                           prominence_thres, Node.prominence_search_range)
        
        # adjust to the left top origin of the whole page
        if self.cut_axis == 0:
            cuts += self.region[0]
        else:
            cuts += self.region[1]
        
        return cuts
    
    def _cut_node(self):
        x0, y0, x1, y1 = self.region
        cutted = False
        # if a node can't cut at horizontal and vertical direction, then stop
        while self.cut_count < 2 and not cutted:
            cuts = self._cut_node_once()
            self.cut_count += 1
            if len(cuts) == 0:  # no cut has been made, change direction
                if self.cut_axis == 1:
                    self.cut_axis = 0
                else:
                    self.cut_axis = 1
            else:  # generate child nodes by cuts
                cutted = True
                if self.cut_axis == 1:
                    ys = [y0] + cuts.tolist() + [y1]
                    for i in range(len(cuts) + 1):
                        child_region = (x0, ys[i], x1, ys[i+1])
                        child_cut_axis = 0  # child node cut at a different direction
                        self._add_child(Node(child_region, self.binary_image, child_cut_axis))
                else:  # cut_axis == 0
                    xs = [x0] + cuts.tolist() + [x1]
                    for i in range(len(cuts) + 1):
                        child_region = (xs[i], y0, xs[i+1], y1)
                        child_cut_axis = 1  # child node cut at a different direction
                        self._add_child(Node(child_region, self.binary_image, child_cut_axis))
        
        # adjust cuts to xyxy form,  
        if self.cut_axis == 0:  # a vertical cut: [cut_x,...] -> [[cut_x, y0, cuts_x, y1],...]
            y0s = np.repeat(y0, len(cuts))
            y1s = np.repeat(y1, len(cuts))
            cuts = np.stack((cuts, y0s, cuts, y1s), axis=1)
        else:  # a horizontal cut: [cut_y,...] -> [[x0, cut_x, ,x1, cuts_x],...]
            x0s = np.repeat(x0, len(cuts))
            x1s = np.repeat(x1, len(cuts))
            cuts = np.stack((x0s, cuts, x1s, cuts), axis=1)

        return cuts
    
    def merge_horizontal_leaf_nodes(self):
        # cause we don't need leaf nodes which all belong to the same node to be 
        # divided as paralled sections if the cuts are made horizontally
        all_children_is_leaf = True  # default
        for child in self.children:
            if not child.is_leaf():
                child.merge_horizontal_leaf_nodes()
                all_children_is_leaf = False
           
        if all_children_is_leaf and self.cut_axis == 1:  
            self.children = []  # eliminate all child nodes
            
    def get_segmentations(self):
        # recursively traverse the whole tree and extract the cuts of none leaf 
        # nodes, and extract the regions of leaf nodes
        regions = []
        cuts = []
        def _exhibit_cuts(node):
            if not node.is_leaf():
                cuts.append(node.cuts)
                for child in node.children:
                    _exhibit_cuts(child)
            else:
                regions.append(node.region)
                
        _exhibit_cuts(self)
        
        if len(cuts) != 0:
            cuts = np.concatenate(cuts, axis=0)  
        else:
            cuts = np.empty((0, 4))  # (#cuts, xyxy)
            
        regions = np.stack(regions, axis=0)  # (#regions, xyxy)
        return regions, cuts


def segment_binary_image(binary_image:np.ndarray, main_region:np.ndarray=None) -> tuple:
    # just focus on the main part, for example, ignore header and footer area
    if main_region is not None:
        x0, y0, x1, y1 = main_region.round().astype(np.int64)
    else:
        x0, y0, x1, y1 = 0, 0, binary_image.shape[1] - 1, binary_image.shape[0] - 1
        
    # x-y cut
    root_node = Node((x0, y0, x1, y1), binary_image, cut_axis=1)
    
    # merge horizontal leaf nodes which all belong to the same parent node
    root_node.merge_horizontal_leaf_nodes()
    regions, cuts = root_node.get_segmentations()
    return regions, cuts
    

def segment_page(page:fitz.fitz.Page, header:np.ndarray=None, footer:np.ndarray=None,
                 extra_regions:np.ndarray=None) -> tuple:
    """perform xy-cut on a page object
    Args:
        page(Page): a page object from PyMupdf module
        header(numpy array or None): 1d array, (xyxy, ), defines header region,
            None means no footer region was given.
        footer(numpy array or None): 1d array, (xyxy, ), defines footer region,
            None means no footer region was given.
        extra_regions(numpy array or None): 2d array, (#regions, xyxy), these regions have
            already been provided by deeplearning model, e.g. protects unlined 
            table region from being cutted into different parts due to its sparsity.
            None means no extra regions were given.
    return:
        regions(numpy array): (#regions, xyxy), the result of xy-cut,its order
            means reading order.
        cuts(numpy array): (#cuts, xyxy), the egdes which seperate different regions.
    """
    # convert page object to binary image, 1 means foreground, 0 means background 
    b_img, main_region= form_text_binary_image(page, header, footer, extra_regions)
    
    # layout analysis
    regions, cuts = segment_binary_image(b_img, main_region)
    return regions, cuts


def segment_pdf(pdf_path:str, page_index:list=None, headers:list=None, footers:list=None, 
                extra_regions:list=None) -> list:
    """perform xy-cut on pdf level
    Args:
        pdf_path(str): path of pdf file.
        page_index(list or None): list of int or None, None means all pages
        header(list or None): list of 1d array, (x0, y0, x1,y1) means the 
            region for layout analysis, e.g. we don't want footer and header 
            areas invovled. None means the whole page area is taken as main region.
        extra_regions(list or None): list of 2d array, (#regions, xyxy), these regions
            have already been provided by deeplearning model, e.g. protects unlined 
            table region from being cutted into different parts due to its sparsity.
            None means no extra regions were given.
    return:
        seg_result(list): [[regions, cuts], ...]. "regions" is numpy array, (#regions, xyxy),
            the result of xy-cut,its order means reading order. "cuts" is numpy
            array, (#cuts, xyxy), the egdes which seperate different regions.
    """
    # load pdf file
    doc = fitz.open(pdf_path)
    
    # default value when it's None
    if page_index is None:  
        page_index = list(range(len(doc)))
    if headers is None:  
        headers = [None for _ in range(len(doc))]
    if footers is None:
        footers = [None for _ in range(len(doc))]
    if extra_regions is None:  
        extra_regions = [None for _ in range(len(doc))]
        
    # loop through pages 
    seg_result = []
    for i in page_index:
        regions, cuts = segment_page(doc[i], headers[i], footers[i], extra_regions[i])
        seg_result.append([regions, cuts])
        
    return seg_result
        
    
            

if __name__ == "__main__":
    from utils.pdf_utils import page2image
    import cv2
    from utils.vis_utils import draw_text_bboxes, draw_bboxes
    doc = fitz.open('sample/'+'宁波银行：2017年年度报告.PDF')
    page_index = 103
    page = doc[page_index]
    img = page2image(page)
    
    cv2.imshow('original image', img)
    cv2.waitKey()
    img = draw_text_bboxes(page)
    cv2.imshow('text bounding box', img)
    cv2.waitKey()
    b_img, main_region = form_text_binary_image(page)
    cv2.imshow('binary image', b_img*255)
    cv2.waitKey()
    img = draw_bboxes(img, main_region.reshape(-1, 4))
    cv2.imshow('main region', img)
    cv2.waitKey()
    
    regions, cuts = segment_page(page)
    
    # visualize result
    img = page2image(page)
    from utils.vis_utils import draw_regions_and_cuts
    img = draw_regions_and_cuts(img, regions, cuts, thickness=2)
    
    cv2.imwrite('%d_result.png' % page_index, img)
    cv2.imshow('result', img)
    cv2.waitKey()
    cv2.destroyAllWindows()
   
    
    
    
    
    
    
    
    
    
    
    
    
    


